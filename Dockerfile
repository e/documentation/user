ARG IMAGE_TAG=master
FROM registry.gitlab.e.foundation/e/documentation/user/jekyll:$IMAGE_TAG AS jekyll

COPY . /tmp
RUN date > /tmp/build_date
RUN jekyll build -d /tmp/e_docs_website && rm /tmp/Gemfile*

# Fixme: Minify is generating unexpected output, and not even working in prod currently
#FROM ubuntu:22.04 AS ubuntu
#COPY --from=jekyll /tmp/e_docs_website/ /tmp/e_docs_website/
#RUN apt-get update && apt-get install minify -y
#RUN minify -r -o /tmp/e_docs_website/ /tmp/e_docs_website/

FROM registry.gitlab.e.foundation/e/documentation/user/nginx:$IMAGE_TAG
COPY --chown=www-data:www-data --from=jekyll /tmp/e_docs_website/ /tmp/e_docs_website/
