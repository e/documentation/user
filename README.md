# General information
The current documentation uses [Jekyll](https://jekyllrb.com/docs) as the static site generator. [Docker](https://www.docker.com/) is required to run the project.

# How to run

## Jekyll container only

For faster development process, the following command is recommended:

```bash
docker run \
--rm \
-v="$PWD/htdocs:/srv/jekyll" \
-v="$PWD/jekyll_cache:/usr/local/bundle" \
-v="$PWD/gems_cache:/usr/gem" \
-p 4000:4000 \
-p 35729:35729 \
-it \
jekyll/jekyll:4.2.0 \
jekyll serve --profile --livereload
```

Note: the port 35729 is used for live reload

Or you could use:

```bash
docker run --rm --volume="$PWD/htdocs:/srv/jekyll" -p 4000:4000 -it jekyll/jekyll:4.2.0 jekyll serve
```

## Full compose stack (search)

Add `edocs.local` to your `/etc/hosts` or customize `NGINX_HOST` in .env as desired.

```bash
cp .env.dev .env
docker-compose up -d
docker-compose exec -T bridge_server python main.py /public_html
```

# Deployment

Since this project depends on [ElasticSearch](https://www.elastic.co/), the deployment should follow the recommended approach and configuration as detailed in [here](https://www.elastic.co/guide/en/elasticsearch/reference/current/docker.html)
