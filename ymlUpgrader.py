import os
import re

def update_fields(file_path, build_version_value, upgrade_available_value):
    if not os.path.exists(file_path):
        return False

    with open(file_path, 'r') as yaml_file:
        content = yaml_file.read()

    content = re.sub(r'(?<=build_version_dev:\s)[A-Z]', build_version_value, content)
    content = re.sub(r'(?<=upgrade_available:\s)[A-Z]', upgrade_available_value, content)

    with open(file_path, 'w') as yaml_file:
        yaml_file.write(content)

    return True

def process_devices(device_names, values):
    updated_files = []
    not_found_files = []

    for name in device_names:
        yaml_file_path = f"htdocs/_data/devices/{name}.yml"
        if update_fields(yaml_file_path, values, values):
            updated_files.append(yaml_file_path)
        else:
            not_found_files.append(yaml_file_path)

    with open("updated_files.txt", "w") as updated_file:
        for updated in updated_files:
            updated_file.write(updated + "\n")

    with open("not_found_files.txt", "w") as not_found_file:
        for not_found in not_found_files:
            not_found_file.write(not_found + "\n")

    print("Updated files saved to updated_files.txt")
    print("Not found files saved to not_found_files.txt")

if __name__ == "__main__":
    devices_input = input("Enter devices separated by spaces: ")
    devices = devices_input.strip().split()
    value = input("Enter the new value for build_version_dev and upgrade_available (e.g., 'T'): ")
    process_devices(devices, value)

