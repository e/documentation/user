### PreDeployment and Deployment steps for Elastic Jekyll Integration Deployment
### Pre Deployment procedure

- [ ] SSH into the `web01.ecloud.global` and `web01.ecloud.global`

  ```bash
  ssh web01.ecloud.global
  ```

  ```bash
  ssh web02.ecloud.global
  ```

- [ ] Go to the directory

  ```bash
  # on web01.ecloud.global and web02.ecloud.global
  # tmux: set-window-option synchronize-panes on
  cd /mnt/repo-base/user-documentation/
  ```

- [ ] Change user to `ansible`

  ```bash
  # on web01.ecloud.global and web02.ecloud.global
  # on /mnt/repo-base/user-documentation/
  sudo su ansible
  ```

- [ ] Ensure that file ownership is correct

  ```bash
  # on web01.ecloud.global and web02.ecloud.global
  # on /mnt/repo-base/user-documentation/
  ls -la
  # should show ansible:ansible
  ```

- [ ] Ensure that the local changes are taken care of

  ```bash
  # on web01.ecloud.global and web02.ecloud.global
  # on /mnt/repo-base/user-documentation/
  git status
  # git stash
  # git restore
  ```

- [ ] Checkout current deployed state as `revert` branch with:

  ```bash
  # on web01.ecloud.global and web02.ecloud.global
  # on /mnt/repo-base/user-documentation/
  git branch -D revert
  git branch revert
  ```

### Deployment procedure

- [ ] Update git repo

  ```bash
  # on web01.ecloud.global and web02.ecloud.global
  # on /mnt/repo-base/user-documentation/
  git pull
  ```

- [ ] Check `SERVER_IMAGE_TAG` tag is set to `master`

  ```bash
  # on web01.ecloud.global and web02.ecloud.global
  # on /mnt/repo-base/user-documentation/
  grep SERVER_IMAGE_TAG .env
  ```

- [ ] Pull `bridge_server` services new image

  ```bash
  # on web01.ecloud.global and web02.ecloud.global
  # on /mnt/repo-base/user-documentation/
  docker compose pull bridge_server
  ```

- [ ] Restart `bridge_server` services with new version:

  ```bash
  # on web01.ecloud.global and web02.ecloud.global
  # on /mnt/repo-base/user-documentation/
  docker compose up -d bridge_server
  ```

### Post deployment

- [ ] Check `bridge_server` services log:

  ```bash
  # on web01.ecloud.global and web02.ecloud.global
  # on /mnt/repo-base/user-documentation/
  docker compose logs -f --tail 100 bridge_server
  ```

### Steps to validate

- [ ] Confirm behaviour/version in production
- [ ] Close related issues

## Steps to revert:

- [ ] Checkout `revert` branch with:

  ```bash
  # on web01.ecloud.global and web02.ecloud.global
  # on /mnt/repo-base/user-documentation/
  git checkout revert
  ```

- [ ] Set `SERVER_IMAGE_TAG` tag is set to `before-mr-16`

  ```bash
  # on web01.ecloud.global and web02.ecloud.global
  # on /mnt/repo-base/user-documentation/
  vim .env
  # grep SERVER_IMAGE_TAG .env
  # should show before-mr-16
  ```

- [ ] Restart `bridge_server` services with old version:

  ```bash
  # on web01.ecloud.global and web02.ecloud.global
  # on /mnt/repo-base/user-documentation/
  docker compose up -d bridge_server
  ```
