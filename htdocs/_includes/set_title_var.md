{% assign title = page.title %}

{% if device == nil %}
  {% assign device = page.device %}
{% endif %}

{%- capture title_template_str -%}
  {% translate title %}
{%- endcapture -%}
{% comment %}
<!-- Backwards compatibility: When a page does not define it's title on the lang files we do not use the translated (null) version -->
{% endcomment %}
{% if title_template_str != "" %}
  {% assign title = title_template_str | replace: '$vendor$', device.vendor | replace: '$name$', device.name | replace: '$codename$', device.codename %}
{% endif %}