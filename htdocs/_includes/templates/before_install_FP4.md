## Before install

### How to Boot into bootloader mode
This section will be relevant later throughout the guide, when you need to reboot into bootloader mode.

1. Remove any USB-C cable and turn off your Fairphone 4
   {% include alerts/tip.html content="If you cannot turn your device off, remove the battery for about 5 seconds, then put it back in." %}
1. Press and hold the Volume Down button.
1. Insert a USB-C cable connected to the power (can either be a power outlet or a computer).
1. Release the Volume Down button as soon you boot into bootloader mode

For more details on how to erase the data on your {{ device.codename }} refer [this guide](https://support.fairphone.com/hc/en-us/articles/4405445579537-FP4-Factory-reset-erase-all-data-)
