## Install /e/OS

1. Download and extract the /e/OS ROM
     > You can unzip the files into the folder where you copied the fastboot files.
   
     > Alternately you can create a folder and unzip the ROM there. 
     
     >  In the second case the below commands will need the path to the folder where you unzipped the ROM files. 
1. Reboot in bootloader with `adb reboot bootloader`

1. Run the following commands to install /e/OS

    > these img files are available in the /e/OS zip you downloaded

    ```  
    fastboot flash system system.img
    fastboot flash vendor vendor.img
    fastboot flash boot boot.img
    fastboot flash dtbo dtbo.img
    fastboot flash recovery recovery.img
    fastboot flash logo logo.img
    fastboot flash md1dsp md1dsp.img
    fastboot flash md1img md1img.img
    fastboot flash spmfw spmfw.img
    fastboot flash lk lk.img
    fastboot flash lk2 lk.img
    fastboot flash sspm_1 sspm.img
    fastboot flash sspm_2 sspm.img
    fastboot flash tee1 trustzone1.bin
    fastboot flash tee2 trustzone2.bin
    fastboot flash preloader preloader.img

    fastboot -w
    fastboot reboot

    ```

