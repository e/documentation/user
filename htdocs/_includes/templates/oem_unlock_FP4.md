## Unlocking the bootloader

{% include templates/unlock_bootloader.md %}

1. Boot the device
1. Enable and connect Wifi
1. Enable Developer options
1. From developer options, enable OEM unlock
    - Get the unlock code from [this site](https://www.fairphone.com/en/bootloader-unlocking-code-for-fairphone) and enter it
1. Reboot on fastboot by running the command in the PC console `adb reboot bootloader`

   {% include alerts/tip.html content=" All the console commands are run on an adb enabled PC and in the `adb` console." %}

1. Ask for unlock with `fastboot flashing unlock`
1. Approve with <kbd>volume +</kbd> then <kbd>power</kbd>
    - The device proceed with a factory reset, and automatically reboots
1. Reboot again into bootloader
1. Unlock critical with `fastboot flashing unlock_critical`
1. Approve with <kbd>volume + </kbd> then <kbd>power</kbd>
    - The device proceeds with a factory reset, and automatically reboots
