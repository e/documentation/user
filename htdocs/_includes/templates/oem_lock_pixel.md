## Locking the bootloader

In /e/OS recovery main screen:

1. Select `Advanced`
1. Select `Reboot to bootloader`

Once the device is in fastboot mode:
1. Verify your PC finds it by typing:
    ```
    fastboot devices
    ```
    {% include alerts/tip.html content="If you see `no permissions fastboot` while on Linux or macOS, try running `fastboot` as root." %}

1. Download the [avb custom key](https://images.ecloud.global/stable/pkmd_pixel.bin)
1. Erase the previous key `fastboot erase avb_custom_key`
1. Flash the new key previously downloaded `fastboot flash avb_custom_key pkmd_pixel.bin`
1. Lock the device `fastboot flashing lock`
1. Use the volume key to select `Lock the bootloader`

## Reboot the device

1. Use power button to `Start` the device
    > The reboot process may take 5 - 10 minutes

{% include templates/post_oem_lock.md %}
