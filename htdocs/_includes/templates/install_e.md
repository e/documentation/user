{% assign device = page.device %}

## Installing /e/OS from recovery

1. Before proceeding ensure you have downloaded the /e/OS for your device, from the link given in the **Downloads** section above

2. If you aren’t already in recovery mode, reboot into recovery mode 

    {% if device.requires_specific_reboot_command %}

    - {% t devices.with_the_device_powered_off %}, {% t device.recovery_boot %}

    {% else %}

    - by typing the below command in the console of an adb enabled PC
    
    
    ```shell
    adb reboot recovery
    ```
   

    {% endif %}

3. In TWRP return to main menu, then tap `Wipe`

4. Now tap `Format Data` and continue with the formatting process.

    Here a new screen may show where you will have to type `Yes` and confirm the action by clicking a button.

    {% include alerts/danger.html content="Format Data will remove encryption as well as **delete all files** stored on the internal storage. Remember to take a backup first. Check **Requirements** section above." %}

5. Return to the previous menu and tap `Advanced Wipe`.

6. Select the `Cache` and `System` partitions to be wiped and then `Swipe to Wipe`

{% if device.install_without_sideload %}
7. Push and install the /e/OS .zip package:
    ```shell
    adb push </e/OS zip package> /sdcard/
    adb shell twrp install /sdcard/</e/OS zip package>
    adb shell rm /sdcard/</e/OS zip package>
    ```
{% else %}
7. Sideload the /e/OS .zip package.

{% include templates/sideload.md -%}

{% endif %}
8. Once installation has finished, return to the main menu, tap Reboot, and then System

{% include alerts/warning.html content="Avoid installing any additional apps or services if suggested by the recovery. They may cause your device to bootloop, as well as attempt to access and or corrupt your data." %}

