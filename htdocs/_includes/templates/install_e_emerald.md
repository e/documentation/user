## Installing /e/OS from the bootloader

1. Download the build package linked in the `Downloads` section above and extract it to a folder of your choice
2. Connect the device to your PC if not already done
> Ensure you have a cable which allowed file transfer. Some cables can only be used for charging.

3. Move to the directory where you have extracted the files in the first step. To do that type the below command in a console
```bash
cd path-of-the-extracted-files
```
4. Install /e/OS by typing the following commands in the console

```
fastboot erase userdata
fastboot format md_udc

fastboot flash boot_a boot.img
fastboot flash dtbo_a dtbo.img
fastboot flash vbmeta_a vbmeta.img
fastboot flash vbmeta_system_a vbmeta_system.img
fastboot flash vbmeta_vendor_a vbmeta_vendor.img
fastboot flash super super.img
fastboot flash lk_a lk.img
fastboot flash logo logo.bin
fastboot flash preloader_a preloader_emerald.bin
fastboot flash tee_a tee.img
fastboot flash gz_a gz.img
fastboot flash sspm_a sspm.img
fastboot flash scp_a scp.img
fastboot flash spmfw_a spmfw.img
fastboot flash md1img_a md1img.img

fastboot --set-active=a
fastboot reboot
```

{% include alerts/tip.html content="If your device is not detect by fastboot, try to use `sudo fastboot` instead"%}

## Locking the Bootloader
 Once you have completed the above steps  and before rebooting you can lock your Bootloader. 

1. Boot your device into bootloader if not already there, and plug it to your computer
1. Lock the device with the following command
   - `fastboot flashing lock`
   - Approve with  `Volume up`
1. Reboot with `fastboot reboot`

