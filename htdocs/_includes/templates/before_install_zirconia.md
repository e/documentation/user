## Unlock the bootloader

{% include alerts/danger.html content="Please take a backup of your data as unlocking the bootloader will reset your device to factory defaults. "%}

1. Enable `USB debugging` and `Allow OEM Unlock` following the guide given [here](/pages/enable-usb-debugging)
1. Reboot in fastboot.

1. You can follow either of two methods (Automatic or Manual) to continue. We give details of both the methods below. Only one needs to be selected

    - Automatic
      - From your computer, run

        `adb reboot bootloader`

    - Manual
      - Turn off your phone and start it with `power` + `volume up` keys.

1. The menu will present three options, press `volume up` until `Bootloader` is selected. Press `volume down` to accept this choice.

## OEM unlock the device

1. Once your phone is in fastboot mode, on your PC type in a console the below command

    `fastboot flashing unlock`

1. Press `volume up` on your phone to accept.
1. The device will reboot automatically after an unlock.
