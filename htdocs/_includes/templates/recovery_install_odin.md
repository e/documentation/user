{%- assign device = page.device -%}
{% if device.custom_recovery_codename %}
{% assign custom_recovery_codename = device.custom_recovery_codename %}
{% else %}
{% assign custom_recovery_codename = device.codename %}
{% endif %}


{% if device.required_bootloader %}
## Special Requirements
{% capture bootloader_versions -%}
    `{% for bootloader in device.required_bootloader -%}
        {{ bootloader }}{% unless forloop.last %} / {% endunless %}
    {%- endfor %}`
{%- endcapture %}

{% capture content %}
    Your device must be on bootloader version: {{ bootloader_versions }} otherwise the instructions found in this page will not work. The current bootloader version can be checked by running the command `getprop ro.bootloader` in a terminal app or an adb shell from a command prompt (on Windows) or terminal (on Linux or macOS) window.
{% endcapture %}

{% include alerts/danger.html content=content %}
{% endif%}


## Important Information

{% include alerts/danger.html content="The following instructions **require** a machine running Windows 10 build 17063 or newer.." %}

Samsung devices come with a unique boot mode called "Download mode", which is very similar to "Fastboot mode" on some devices with unlocked bootloaders.
Odin is a Samsung-made tool for interfacing with Download mode on Samsung devices.
The preferred method of installing a custom recovery is through Download Mode{% unless custom_root_instructions %} -- rooting the stock firmware is neither necessary nor required{% endunless %}.

{% if custom_downgrade_instructions %}
{{ custom_downgrade_instructions }}
{% endif %}

{% if custom_root_instructions %}
{{ custom_root_instructions }}
{% endif %}

{% include alerts/tip.html content="If this device's install instructions already had you download Odin/Enable OEM Unlocking earlier in the installation process, you can skip steps 1, 3, 5, 6, and 7 below." %}
## Installing a custom recovery using `Odin`
{% unless device.no_oem_unlock_switch %}
1. Enable Developer Options by pressing the "Build Number" option 10 times, in the "Settings" app within the "About" menu
 * From within the Developer options menu, enable OEM unlock.
{% endunless %}
{% if device.custom_recovery_link %}
1. Download a custom recovery - you can download one [here]({{ device.custom_recovery_link }}).
{% else %}
1. Download a custom recovery - you can download 
. 
{%-if device.uses_twrp  %} 
 [TWRP](https://dl.twrp.me/{{ custom_recovery_codename }}). Simply download the latest recovery file, named something like `twrp-x.x.x-x-{{ custom_recovery_codename }}.tar`.
 {% include alerts/tip.html content="Ensure you download the `.tar` or the `.tar.md5` file and not the `.img` version." %}
{%- else %}
Download the latest recovery file, mentioned in the `Downloads for {{ device.codename }}`  section above

1. Rename the downloaded image to “recovery.img”, Open a Command Prompt in the folder where the file was downloaded, and TAR the file by running 

    `tar --format=ustar -cvf recovery.tar recovery.img` 

    {% include alerts/tip.html content="The path may vary depending on where you download the recovery image." %}
    {%-endif%}

{% endif %}

1. Power off the device, and boot it into download mode:
    * {% t device.download_boot %}
    * Now, click the button that the onscreen instructions coorelate to "Continue", and insert the USB cable into the device.

1. Download and install the necessary drivers.
    * Download the newest Samsung drivers from [here](https://developer.samsung.com/mobile/android-usb-driver.html). You will need to create a Samsung account and login to download them.
    * Install `SAMSUNG_USB_Driver_for_Mobile_Phones.exe`.

1. Download [this](https://androidfilehost.com/?fid=4349826312261712202) version of Odin.

1. Extract "Odin_3.13.1.zip".


1. Run `Odin3 v3.13.1` found in the newly  extracted "Odin_3.13.1" folder.

1. Check in the top left of the Odin window that you see a valid device, it will show up as something like `COM0`.

    {% include alerts/tip.html content="The `COM` port, or the number succeeding `COM`, may be any valid number." %}

1. In the left side of the Odin window, you will see an "Options" tab, click it, and then un-check the "Auto Reboot" option.

1. Check the box labeled next to the button labeled "AP", and then click the "AP" button.
    * In the menu that pops up, select the newly downloaded custom recovery `.tar` or `.tar.md5`.

    {% include alerts/tip.html content="The filename may very depending on your device, and the version of your custom recovery." %}

1. A blue transfer bar will appear on the device showing the recovery image being flashed.

    {% include alerts/tip.html content="The device will continue to display `Downloading... Do not turn off target!!` even after the process is complete. When the status message in the top left of the devices's display reports that the process is complete, you may proceed." %}

1. Unplug the USB cable from your device.

1. Manually reboot into recovery, this may require pulling the device's battery out and putting it back in, or if you have a non-removable battery, press the Volume Down + Power buttons for 8~10 seconds until the screen turns black & release the buttons *immediately* when it does, then boot to recovery:
    * {% t devices.with_the_device_powered_off %}, {% t device.recovery_boot %}

{% include alerts/danger.html content="Be sure to reboot into recovery immediately after installing the custom recovery. If you don't the custom recovery will be overwritten on boot." %}
