{% include templates/requirements.md %}

{% include alerts/danger.html content="The SHIFTphone 8 comes with an anti-rollback feature. Google Android anti-roll back feature is supposedly a way to ensure you are running the latest software version, including the latest security patches.

<br><br>If you try installing a version of /e/OS based on a security patch that is older than the one on your device, you will brick your device. Click on `Details` below for detailed information<br>
<details>
<br>
To check the security patch level on your phone with a locked bootloader, prior to installing /e/OS, open your phone `Settings` >> `About Phone` >> `Android Version` >> `Android Security Patch Level`.Then compare it against the level of the security patch on the /e/OS build as visible in the Downloads for SHIFTPhone 8 section below.

<br><br><strong>The following values control whether anti-rollback features are triggered on SHIFTphone 8:</strong>
<br>
<br> * Rollback protection errors trigger if you install an update whose version number is `LESS` than the rollback index's value stored on device.
<br> * The value of rollback index is `UPDATED` to match `ro.build.version.security_patch`'s value of the currently installed version, but only if the bootloader is `LOCKED`.
<br> * The value of rollback index is `NOT` dependent on the currently installed `ANDROID VERSION`.
<br> * The value of rollback index can `NEVER` be `DOWNGRADED`.
<br> * Rollback protection errors are `FATAL` when the bootloader is `LOCKED`.
<br> * Rollback protection errors are `IGNORED` when the bootloader is `UNLOCKED`.
<br>
<br>
Here are some examples to help you understand how anti-rollback features work:
<br><br>
<strong>Example 1</strong>

<br> * Your SHIFTphone 8 with Google Android has a Security Patch Level saying June 5, 2022
<br> * The /e/OS build available says: /e/OS build : R official (Security patch: 2022-05-05)
<br> * In this example, the /e/OS build has an older Security Patch level than the origin, so the anti-roll back protection will trigger, and you will brick your phone
<br>
<strong>Example 2</strong>

<br> * Your SHIFTphone 8 with Google Android has a Security Patch Level saying June 5, 2022.
<br> * The /e/OS build available says: /e/OS build : R official (Security patch: 2022-06-05)
<br> * In this example, the /e/OS build has the same Security Patch level than the origin, so the anti-roll back protection will pass, and you will be able to install /e/OS with no issues.
<br>
<br>
<strong>Example 3</strong>

<br> * Your SHIFTphone 8 runs Google Android -R while /e/OS is now available based on AOSP -S.
<br> * Your SHIFTphone 8 with Google Android has a Security Patch Level saying 2022-10-03 or October 3rd, 2022.
<br> * The /e/OS build available says: /e/OS build : S official (Security patch: 2022-06-05)
<br> * In this example, the /e/OS build has an older Security Patch level than the origin, so the anti-roll back protection will trigger, even if the /e/OS version runs on a more recent version of AOSP. In this example, you will brick your phone.
</details>



" %}

