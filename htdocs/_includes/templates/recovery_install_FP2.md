## Installing a custom recovery

1. Download a custom recovery as linked in the **Downloads** section above.

1. Connect your device to your PC via USB.

1. On the computer, open a command prompt (on Windows) or terminal (on Linux or macOS) window, and type

    `adb reboot bootloader`

1. Once the device is in fastboot mode, verify your PC finds it by typing


    `fastboot devices`

1. Flash recovery onto your device

    `fastboot flash recovery recoveryfilename.img`

    > {% translate content.recovery_file_name_text %} 


1. Now reboot into recovery to verify the installation
