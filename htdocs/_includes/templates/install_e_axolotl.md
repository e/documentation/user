## Installing /e/OS
{% include alerts/tip.html content="For Windows users, we advise to use Git Bash. For more info about Downloads and Installation see [Official Documentation](https://git-scm.com/about)"%}

{% include alerts/danger.html content="axolotl comes with an anti-rollback feature. Please read the paragraph marked `Caution` in Requirements section of this guide, before you proceed to install /e/OS on axolotl." %}

1. Unzip the archive

    ```
    unzip <fileyoudownloaded>
    ```

   > In the command above, replace `<fileyoudownloaded>` with path of the downloaded archive.

   > Alternatively, you can unzip the archive using a File Manager/File Explorer.


1. Confirm that extracted directory contains following content:

    - `bin-linux-x86` directory - which contains linux tools including fastboot and adb
    - `bin-msys` directory - which contains Windows tools including fastboot and adb
    - `.img` files - The `.img` files that are to be flashed onto the device.
    - `flash_axolotl_factory.sh` - The installer script.

1. Boot into bootloader/fastboot mode.

    - Power OFF the device.
    - Hold `Power` + `Volume Down` button combo simultaneously.

1. Run the installer script on Terminal(Linux) or on Git Bash UI(Windows).
    - Make sure you are in the current directory which contains the `flash_axolotl_factory.sh` file.
    - **Linux** 
        - Right click -> Open terminal here
    - **Windows**
        - Right click -> Git Bash here
    - Execute the following command:
        ```
        chmod +x flash_axolotl_factory.sh && ./flash_axolotl_factory.sh
        ```

    The script will flash all required files and will wait for input at the last step. Proceed to locking the bootloader.


## Locking the Bootloader

{% include alerts/danger.html content="axolotl comes with an anti-rollback feature. Please read the paragraph marked `Caution` in Requirements section of this guide, before you proceed to install /e/OS on axolotl." %}

Once you have completed the above steps and before rebooting you can lock your Bootloader.

1. Download the [avb custom key](https://images.ecloud.global/devices/keys/avb_murena_rsa4096.bin)
1. Erase the previous key `fastboot erase avb_custom_key`
1. Flash the new key previously downloaded `fastboot flash avb_custom_key avb_murena_rsa4096.bin`
1. Lock critical partition with the following command
   - `fastboot flashing lock_critical`
   -  Approve with  <kbd>Volume +</kbd> then <kbd>power</kbd>
1. Reboot your device into bootloader, and plug it to your computer
    - Power OFF the device.
    - Hold `Power` + `Volume Down` button combo simultaneously.
1. Lock the device with the following command
   - `fastboot flashing lock`
   - Approve with  <kbd>Volume +</kbd> then <kbd>power</kbd>

