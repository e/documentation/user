{% if device.required_bootloader %}
## Special requirements
{% capture bootloader_versions -%}
    `{% for bootloader in device.required_bootloader -%}
        {{ bootloader }}{% unless forloop.last %} / {% endunless %}
    {%- endfor %}`
{%- endcapture %}

{% capture content %}
    Your device must be on bootloader version: {{ bootloader_versions }} otherwise the instructions found in this page will not work. The current bootloader version can be checked by running the command `getprop ro.bootloader` in a terminal app or an adb shell from a command prompt (on Windows) or terminal (on Linux or macOS) window.
{% endcapture %}

{% include alerts/danger.html content=content %}

{% endif %}

## Unlocking the bootloader

{% include templates/unlock_bootloader.md %}

{% include alerts/tip.html content="It is highly recommended to have the latest official MIUI dev package installed on the device, before proceeding with unlock." %}

1. Create a Mi account on [Xiaomi’s website](https://global.account.xiaomi.com/pass/register).
1. Add a phone number to your Mi account.
1. Insert a SIM into your phone.
1. Enable developer options in `Settings` > `About Phone` by repeatedly tapping `MIUI Version`.
1. Link the device to your Mi account in `Settings` > `Additional settings` > `Developer options` > `Mi Unlock status`.
1. Download the [Mi Unlock app](https://en.miui.com/unlock/download_en.html) (Windows is required to run the app).
1. Run the Mi Unlock app and follow the instructions provided by the app. **It may tell you that you have to wait up to 30 days.** If it does so, please wait the quoted amount of time before continuing to the next step!
1. After device and Mi account are successfully verified, the bootloader should be unlocked.
1. Since the device resets completely, you will need to re-enable USB debugging to continue.

> A Mi account is required to apply for permissions. You don’t need to re-apply for permissions to unlock new devices, but beware that one account is only allowed to unlock one unique device every 30 days.
