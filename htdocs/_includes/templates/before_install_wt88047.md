## Updating firmware

- If you are installing from LP (5.x) stock MIUI, nothing needs to be done.
- If you are installing from KK (4.4) stock MIUI, you will have to update the device's **firmware**.

1. To find your model number, "Open "Settings", navigate to "Phone Info", and take note of the model number below the "model" header:
      - Devices with model numbers `2014811, 2014812, 2014817 2014818, 2014819, and 2014821`
       should download the firmware labeled `wt88047`.
      - Devices with model numbers `2014813, and 2014112` should download the firmware labeled
       `wt86047`.
2. Sideload the firmware file downloaded from the link in the **Downloads** section

{% include templates/sideload.md %}
