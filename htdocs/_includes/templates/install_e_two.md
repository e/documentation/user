## Installing /e/OS 

### Installing /e/OS using IMG or image file

1. Start your Murena Two in bootloader mode and connect it to your computer.
2. Download the zip file from the provided link on your computer. Extract the folder and navigate to its contents.
3. Use the included script in the zip file, named `flash_two_factory.sh`, to install /e/OS. Execute the script by running `bash flash_two_factory.sh`.
