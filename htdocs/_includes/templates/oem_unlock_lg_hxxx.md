{% if device.vendor == "LG" %}
   {% include alerts/danger.html content="Please note LG has stopped officially supporting the unlocking of its bootloader. For more details please refer this [document](https://www.xda-developers.com/lg-bootloader-unlocking-shut-down-december-31/)" %}
{% endif %}
## Unlocking the bootloader

{% include templates/unlock_bootloader.md %}

1. Enable OEM unlock in the Developer options under device Settings
1. Connect the device to your PC via USB.
1. On the computer, open a command prompt (on Windows) or terminal (on Linux or macOS) window, and type:

    `adb reboot bootloader`

    You can also boot into fastboot mode via a key combination

    {% t device.download_boot %}

1. Once the device is in fastboot mode, verify your PC finds it by typing:

    `fastboot devices`

    If you do not get any output or an error:

   - on Windows: make sure the device appears in the device manager without a triangle. Try other drivers until the command above works!
   - on Linux or macOS: If you see no permissions fastboot try running fastboot as root. When the output is empty, check your USB cable and port!

1. Now type the following command to unlock the bootloader

    `fastboot oem unlock`

    {% include alerts/tip.html content="At this point the device may display on-screen prompts which will require interaction to continue the process of unlocking the bootloader. Please take whatever actions the device asks you to to proceed."%}

1. If the device doesn’t automatically reboot, reboot it. It should now be unlocked.

1. Since the device resets completely, you will need to re-enable USB debugging to continue
