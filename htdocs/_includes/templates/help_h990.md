{% if device.ignore_troubleshooting_tips != true %}
{% include details.html title="To find some troubleshooting tips… click here" details=site.data.troubleshooting_h990.tips %}
{% endif %}

{% if device.additional_install_guides %}

## Still need help to complete the installation ?

Check out this document: An [install Guide]({{ device.additional_install_guides }}) for the  {{ device.codename}}

{% endif %}

{% include templates/suggestions.md %}

{% if device.is_lineageos_official %}
{% include templates/licence_device.md %}
{% endif %}
