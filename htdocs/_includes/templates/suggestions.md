Please share your experience , suggest tips or changes to this install guide documentation by visiting the {{ device.name }} specific [topic on our community forum]({{device.community_doc_url}}). 

To report issues in /e/OS please refer [this guide]({% tl support-topics/report-an-issue %})
