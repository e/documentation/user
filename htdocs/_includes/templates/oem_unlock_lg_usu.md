{% if device.vendor == "LG" %}
   {% include alerts/danger.html content="Please note LG has stopped officially supporting the unlocking of its bootloader. For more details please refer this [document](https://www.xda-developers.com/lg-bootloader-unlocking-shut-down-december-31/)" %}
{% endif %}
## Unlocking the bootloader

{% include templates/unlock_bootloader.md %}

1. Download and Boot [FWUL/mAid](https://leech.binbash.rocks:8008/FWUL/stable/)
> Always download the latest iso file 

1. Start SALT (folder `LG`)
1. Select backup and create a full backup of your device
1. Check ARB value (must be 0, 1 or 2 but not higher then 2)
1. Open the `Advanced` menu
1. Select `Unlock G4 (UsU)`
1. Follow the instructions
{% include alerts/tip.html content="It is highly recommended to check and follow the [detailed guide here](https://forum.xda-developers.com/t/unlock-unofficial-g4-unlock-any-lg-g4-device-with-usu.3760451/)"%}
