## Preparing for installation using Heimdall

Samsung devices come with a unique boot mode called "Download mode", which is very similar to "Fastboot mode" on some devices with unlocked bootloaders.
Heimdall is a cross-platform, open-source tool for interfacing with Download mode on Samsung devices.
The preferred method of installing a custom recovery is through Download Mode{% unless custom_root_instructions %} -- rooting the stock firmware is neither necessary nor required{% endunless %}.

{% unless device.no_oem_unlock_switch %}
1. Enable Developer Options by pressing the `Build Number` option in the `Settings` app within the `About` menu
 * From within the Developer options menu, enable OEM unlock.
{% endunless %}
2. Download and install the appropriate version of the [Heimdall suite](https://www.androidfilehost.com/?w=files&flid=304516) for your machine's OS
    * **Windows**: Extract the Heimdall suite zip and take note of the new directory containing `heimdall.exe`. You can verify Heimdall is functioning by opening a Command Prompt or PowerShell in that directory and running `heimdall version`.
      * If you receive an error, install the [Microsoft Visual C++ 2015-2019 Redistributable Package (x86)](https://support.microsoft.com/en-us/help/2977003/the-latest-supported-visual-c-downloads) on your computer.
    * **Linux**: Extract the Heimdall suite zip and take note of the new directory containing `heimdall`. Now copy `heimdall` into a directory in $PATH, a common one on most distros will be /usr/local/bin. For example `cp heimdall /usr/local/bin`. You can verify Heimdall is functioning by opening a Terminal and running `heimdall version`.
    * **macOS**: Mount the Heimdall suite DMG. Now drag `heimdall` down into the `/usr/local/bin` symlink provided in the DMG. You can verify Heimdall is functioning by opening a Terminal and running `heimdall version`.
    {% include alerts/tip.html content="These Heimdall suite distributions were built by LineageOS Developer Nolen Johnson (npjohnson) and Jan Altensen (Stricted)" %}
4. Power off the device, and boot it into download mode:
    * {% t devices.with_the_device_powered_off %}
    * {% t device.download_boot %}
    * Now, click the button that the on screen instructions correlate to `Continue`, and insert the USB cable into the device.
5. **For Windows users only**: install the necessary drivers. A more complete set of instructions can be found in the [Zadig user guide](https://github.com/pbatard/libwdi/wiki/Zadig).
    1. If nothing relevant appears, try uninstalling any Samsung related Windows software, like Samsung Windows drivers and/or Samsung Kies.
    2. Run `zadig.exe` found in your extracted Heimdall directory.
        {% include alerts/tip.html content="For the interested, source and documentation for zadig.exe can be found [here](https://github.com/pbatard/libwdi/releases)." %}
    3. Choose **Options** &raquo; **List all devices** from the menu.
    4. Select **Samsung USB Composite Device** or **MSM8x60** or **Gadget Serial** or **Device Name** from the drop down menu.
    5. Click **Replace Driver**, then selecting **Install Driver** from the drop down list built into the button.
    6. If you are prompted with a warning that the installer is unable to verify the publisher of the driver, select **Install this driver anyway**. You may receive two more prompts about security. Select the options that accept the warnings and allow you to carry on.
6. On your machine, open a Command Prompt or PowerShell (Windows) window, or Terminal (Linux or macOS) window, and type:
```
heimdall print-pit
```
7. If the device reboots that indicates that Heimdall is installed and working properly. If it does not, please refollow these instructions to verify steps weren't missed, try a different USB cable, and a different USB port.

{% if device.required_bootloader %}

{% capture bootloader_versions -%}
    `{% for bootloader in device.required_bootloader -%}
        {{ bootloader }}{% unless forloop.last %} / {% endunless %}
    {%- endfor %}`  
{%- endcapture %}

{% capture content %}
    Your device must be on bootloader version: {{ bootloader_versions }} otherwise the instructions found in this page will not work. The current bootloader version can be checked by running the command `getprop ro.bootloader` in a terminal app or an adb shell from a command prompt (on Windows) or terminal (on Linux or macOS) window.
{% endcapture %}

{% include alerts/danger.html content=content %}

{% endif %}


## Installing a custom recovery using `Heimdall`

1. Download the custom recovery linked in the Download section above
  
1. Power off the device and connect the USB adapter to the computer (but not to the device, yet).
1. Boot into download mode:
* {% t devices.with_the_device_powered_off %}
* {% t device.download_boot %}

   Accept the disclaimer, then insert the USB cable into the device.

1. On the computer, open a command prompt (on Windows) or terminal (on Linux or macOS) window in the directory the recovery image is located, and type:
```shell
heimdall flash --RECOVERY recoveryfilename.img --no-reboot
```
> Replace the recoveryfilename with the name of the recovery image you downloaded

1. A blue transfer bar will appear on the device showing the recovery being transferred.
1. Unplug the USB cable from your device.
1. Manually reboot into recovery:
* {% t devices.with_the_device_powered_off %}, {% t device.recovery_boot %}

    {% if device.update_vendors %}
    > On some devices, installation can fail if vendors are not up-to-date. In this case, please:
    > * Download [those vendors](https://androidfilehost.com/?fid=11410932744536982158)
    > * Install them
    >    * On the device, go into `Advanced` > `ADB Sideload`, then swipe to begin sideload
    >    * From the computer, please run `adb sideload <vendors file>`

    {% endif %}

    > Note: Be sure to reboot into recovery immediately after having installed the custom recovery. Otherwise the custom recovery will be overwritten and the device will reboot (appearing as though your custom recovery failed to install).

{% if device.codename == "starlte" or device.codename == "star2lte" %}
## Resize userdata
1. Boot into TWRP
1. `Wipe` > `Format data`
1. `Wipe` > `Advanced Wipe` > Select `Data` and tap `Repair or Change File System` > `Resize File system`
{% endif %}
{% if device.recovery_install_need_patch %}
## Patch the device

1. Download the patch from [here](https://androidfilehost.com/?fid=962187416754474353)
1. Install the patch
1. On the device, in TWRP go into `Advanced` > `ADB Sideload`, then swipe to begin sideload
1. From the computer, please run `adb sideload <patch file>`
{% endif %}
