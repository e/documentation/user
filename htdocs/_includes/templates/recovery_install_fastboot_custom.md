## Unlocking the bootloader
{% if device.custom_warning %}
   {% t custom_bootloader_warning.lg %}
{% else %}
{% include templates/unlock_bootloader.md %}

{% if device.unlock_bootloader_lg %}

{% include templates/unlock_bootloader_lg.md %}

{% else %}

1. Unlock your bootloader by following [this]({{ device.unlock_bootloader_guide }}) guide.

{% endif %}
{% endif %}
{% include templates/recovery_install_fastboot_generic.md %}
