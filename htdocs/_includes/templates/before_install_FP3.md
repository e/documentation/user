## Unlock the bootloader

-  Allow the bootloader unlocking by following the [official FP3 unlock documentation](https://www.fairphone.com/en/bootloader-unlocking-code-for-fairphone) 
- Reboot the device on fastboot mode by running `adb reboot bootloader` (or press `Power` + `Volume -` if your device is off)
- Once on fastboot mode, unlock the device by running `fastboot flashing unlock`
    > On previous versions of `fastboot`, the command is `fastboot oem unlock`

- The device will ask you to confirm the unlocking. Confirm by selecting **UNLOCK THE BOOTLOADER** and confirm with `Power`
- The device reboot automatically. Press and hold Volume Down + Power buttons, to boot to Fastboot Mode.
