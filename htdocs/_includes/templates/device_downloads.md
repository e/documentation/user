## Downloads for {{ device.codename }}

[//]: # ( Stock download related )

{% if device.before_install_device_variants and device.before_install_device_variants.size > 0 %}
- Before following these instructions please ensure that the device is currently using the firmware version mentioned below. If your current installation is newer or older than the required version, please upgrade or downgrade to the required version before proceeding by following [this guide.](/{{ device.firmware_update }})
{%- if device.before_install_device_variants and device.before_install_device_variants.size > 0 %}


Download the required firmware for your model.
{%- for el in device.before_install_device_variants %}
   - {{ el.device }} - [{{ el.firmware }}]({{ el.download_link }})
{%- endfor %}
{%- else %}
1. Download the latest firmware for your device from official [website](https://new.c.mi.com/global/miuidownload/index).
{%- endif %}
{% endif %}



{%- if device.before_install_args %}

{% capture upgrade_os_version %}
Before installing /e/OS on the  {{device.vendor}} {{device.name }} {{ device.codename }} for the first time, install the latest stock OS version {{ device.before_install_args.version }} build on the device
{% endcapture%}

{% include alerts/tip.html content = upgrade_os_version  %}

{%- endif %}

{%- if device.revert_to_stock %}

- If you need to revert to stock ROM (vendor provided ROM), Please follow the instructions given in the OS specific guide linked here


{%- for guide in device.revert_to_stock %}
     - {{ guide.os | capitalize}} OS  [Guide]({{guide.url | relative_url}})
{%- endfor %}
{%- endif  %}

{%- if device.name == "FP2" %}
{% include alerts/tip.html content = "Please note that the bootloader of the FP2 is unlocked by default."%}
{%-endif %}
{% assign backup_and_restore_with_twrp_link = "/pages/backup-and-restore-with-twrp" | relative_url%}

[//]: # ( Custom recovery install related )

{% if device.uses_twrp %}
    {% capture content %}
        Installing a custom recovery or unlocking the bootloader will erase all data on your device! [Take a backup]({{ backup_and_restore_with_twrp_link }}) of your data before proceeding!!
    {% endcapture %}
{% include alerts/tip.html content=content %}
{% endif %}



[//]: # ( Recovery related )

{% if device.custom_recovery %}

- [Custom recovery]({{ device.custom_recovery }}) (not required if you already have it running on your phone)
{% else %}
{% if device.display_erecovery != false %}

Try the /e/OS Recovery (**limited functionality**)

{% if device.build_version_stable %}
- [/e/OS Recovery for official build](https://images.ecloud.global/official/{{ device.codename }})
{% endif %}
{% if device.build_version_dev %}
-  [/e/OS Recovery for community build](https://images.ecloud.global/community/{{ device.codename }})
{% endif %}

{% endif %}
{% endif %}

[//]: # ( Patches related )


{% if device.additional_downloads.additional_patch %}
Additional Patches :
Flash this [patch]({{ device.additional_downloads.additional_patch }}) on your {{ device.codename }} before flashing /e/OS
{% endif %}

{% if device.additional_downloads.latest_firmware %}
- Latest [firmware zip file]({{device.additional_downloads.latest_firmware}})
{% endif %}

[//]: # ( Build type related )

{% assign security_patch = '' %}
{% assign security_patch_dev = '' %}

{% if device.security_patch_date %}
    {% assign security_patch = '(Security patch: ' | append: device.security_patch_date | append: ')' %}
{%- endif %}

{% if device.security_patch_date_dev %}
    {% assign security_patch_dev = '(Security patch: ' | append: device.security_patch_date_dev | append: ')' %}
{%- else %}
    {% assign security_patch_dev = security_patch %}
{%- endif %}

{% if device.build_version_dev %}
- /e/OS build : [{{ device.build_version_dev }} community](https://images.ecloud.global/community/{{ device.codename }}) {{ security_patch_dev }}
{%- endif %}


{% if device.build_version_stable %}
- /e/OS build : [{{ device.build_version_stable }} official](https://images.ecloud.global/official/{{ device.codename }}) {{ security_patch }}
{%- endif %}

> To understand the difference between /e/OS builds check [this guide](/build-status)

[//]: # ( Before Install related )


{% if device.before_e_install %}
- Check the **Pre-Install Instructions** section below for some additional downloads for your device.
{% endif  %}

{% if device.additional_downloads.file %}
{% capture additional_downloads %}templates/additional_downloads_{{ device.additional_downloads.file }}.md{% endcapture %}
{% include {{ additional_downloads }} %}
{% endif %}

{% if device.additional_downloads.stockupgrade %}
- Stock ROM : [here]({{ device.additional_downloads.stockupgrade }})
{% if device.additional_downloads.stockupgrade_tool %}
- Stock flashing tool : [here]({{ device.additional_downloads.stockupgrade_tool }})
{% if device.additional_downloads.stockupgrade_toolver %}
- Tool version tested {{ device.additional_downloads.stockupgrade_toolver }}
{% endif %}
{% endif %}

> - Follow the steps to install the stock ROM as given on the vendor site
> - Once the Stock ROM is flashed check if it is working for e.g receiving and making calls, audio, FMRadio ..
> - Next enable developer options on the phone
> - Ensure the device bootloader is unlocked. Flashing stock ROM can lock the bootloader on some devices.
> - Follow the device unlock guidelines as suggested by your vendor. For e.g. on Xiaomi devices it would require running the Mi Unlock tool. Since the device was already unlocked the process should take a couple of minutes only.

{% endif %}

{% include alerts/warning.html content="Please note some of the above links can lead to external sites" %}
