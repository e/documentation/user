## Unlocking the bootloader

{% include templates/unlock_bootloader.md %}

1. Enable OEM unlock in the Developer options under device Settings, if present.

1. Connect your device to your PC via USB.

1. On the computer, open a command prompt (on Windows) or terminal (on Linux or macOS) window, and type:
    ```
    adb reboot bootloader
    ```
    You can also boot into fastboot mode via a key combination:
    - {% t devices.with_the_device_powered_off %}
    - {% t device.download_boot %}

1. Once the device is in fastboot mode, verify your PC finds it by typing:
    ```
    fastboot devices
    ```
    {% include alerts/tip.html content="If you see `no permissions fastboot` while on Linux or macOS, try running `fastboot` as root." %}

1. Now type the following command to unlock the bootloader:

    ```shell 
    {% if device.custom_unlock_cmd -%}
        {{ device.custom_unlock_cmd }}
    {%- else -%}
        fastboot oem unlock
    {%- endif %}
    ```

1. If the device doesn’t automatically reboot, reboot it. It should now be unlocked.

1. Since the device resets completely, you will need to **re-enable USB debugging** to continue.
