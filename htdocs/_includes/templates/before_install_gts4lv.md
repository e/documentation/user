## Pre-Install Instructions

{% include alerts/warning.html content="The following instructions will unlock the bootloader and wipe all userdata on the device." %}

{% unless device.no_oem_unlock_switch %}
1. Connect the device to a Wi-Fi network.

1. Enable Developer Options by pressing the "Build Number" option in the "Settings" app within the "About" menu

    * From within the Developer options menu, enable OEM unlock.
{% endunless %}

1. Download the /e/OS build zip file from the section marked 'Downloads for {{ device.codename }}'. Extract the contents of the /e/OS build zip file. You will find a `vbmeta.img` in the extracted files. 

1. Open a Command Prompt in the folder where the file was downloaded and TAR the file by running `tar -cvf vbmeta.tar vbmeta.img`

1. Power off the device, and boot it into download mode:

    * {% t device.download_boot %}
    * Now, click the button that the onscren instructions coorelate to "Continue" and/or "Unlock Bootloader", and insert the USB cable into the device.

1. Download and install the necessary drivers.

    * Download the newest Samsung drivers from [here](https://developer.samsung.com/mobile/android-usb-driver.html). You will need to create a Samsung account and login to download them.
    * Install `SAMSUNG_USB_Driver_for_Mobile_Phones.exe`.

1. Download [this](https://androidfilehost.com/?fid=4349826312261712202) version of Odin.

1. Extract "Odin_3.13.1.zip".

1. Run `Odin3 v3.13.1` found in the newly  extracted "Odin_3.13.1" folder.

1. Check the box labeled next to the button labeled "AP", and then click the "AP" button.

    * In the menu that pops up, select the newly downloaded custom VBMeta `.tar` file.
    {% include alerts/tip.html content="The filename may very depending on your device, and version of TWRP." %}

1. Check in the top left of thne Odin window that you see a valid device, it will show up as something like `COM0`.

    {% include alerts/tip.html content="The `COM` port, or the number succeeding `COM`, may be any valid number." %}

1. A blue transfer bar will appear on the device showing the recovery image being flashed.

1. Your device will reboot, you may now unplug the USB cable from your device.

1. The device will demand you format userdata, please follow the onscreen instructions to do so.

{% unless device.no_oem_unlock_switch %}

1. Run through Android Setup skipping everything you can, then connect the device to a Wi-Fi network.

1. Re-enable Development settings by clicking the "Build Number" option 10 times, in the "Settings" app within the "About" menu, and verify that "OEM Unlock" is still enabled in the "Developer options" menu.

{% endunless %}
