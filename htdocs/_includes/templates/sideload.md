    {% include alerts/tip.html content="Sideload  requires actions in two places to be done. One on your phone in the TWRP UI and secondly on the connected PC which should have adb enabled. Also note the sideload process while running may stop abruptly around 50%. The console would show a message `Total xfer: 1.00x`. This would mean that the transfer has successfully completed."%}

    - On the device in TWRP,
    > select `Advanced`, `ADB Sideload`, then swipe to begin sideload.

{% if device.codename == "herolte" or device.codename == "hero2lte" %}

    {% include alerts/tip.html content="If your device doesn't appear during adb sideload or shows that it is waiting for the device, try running ```adb usb``` to restart ADB in USB mode, if it isn't already in that mode."%}

{% endif %}

    - On the host machine, in the console , start the sideload of the package by typing

    ```
    adb sideload filename.zip
    ```
    {% include alerts/tip.html content="Change the filename to the name of the file you downloaded"%}
