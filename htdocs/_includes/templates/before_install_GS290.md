## Before install

   
1. Check fastboot version

   > You do this by typing the below command in a console app on an adb enabled PC

    
    `fastboot --version`    
    

   > This would show a response like `fastboot version 30.0.5-6877874`

   
    {% include alerts/danger.html content="Versions provided by Ubuntu or some operating system are not up to date, do not try to flash your Gigaset with it. Instead, follow the instructions given [here](/pages/install-adb)" %}

1. Enable developer mode and following the instructions given [here](/pages/enable-usb-debugging)


1. Unlock your device


            {% include templates/unlock_bootloader.md %}


- Run these commands in an adb enabled computer console 

        
   `adb reboot bootloader`

    
   `fastboot flashing unlock`



  
{% include alerts/tip.html content="In case of issues with the latest fastboot version, try using version 30.0.5"%}



- On your phone screen confirm by pressing volume up
