## Updating firmware

1. Ensure you have downloaded the latest Stock ROM for your {{ device.codename }} which is compatible with the /e/OS {{ device.build_version_dev }}  build you are about to install. 
  - If the vendor provided multiple updates for that version, e.g. security updates, make sure you are on the latest!
  - If your current installation is newer or older than {{ device.build_version_dev }} , please downgrade or upgrade to the required version before proceeding.

1. Now we will sideload the downloaded firmware file

{% include templates/sideload.md %}
