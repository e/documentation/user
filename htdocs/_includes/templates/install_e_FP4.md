## Installing /e/OS 
{% include alerts/tip.html content="For Windows users, we advise to use Git Bash. For more info about Downloads and Installation see [Official Documentation](https://git-scm.com/about)"%}

{% include alerts/danger.html content="The FP4 comes with an anti-rollback feature. Read the paragraph marked `Caution` in Requirements section of this guide, before proceeding." %}

1. Unzip the archive

    ```
    unzip <fileyoudownloaded>
    ```

   > Replace `<fileyoudownloaded>` with the name of the archive file you downloaded.

   > Alternately you can create a folder and unzip the archive there.


1. Confirm that extracted directory contains following content:

    - `bin-linux-x86 directory` - which contains linux tools including fastboot and adb
    - `bin-msys directory` - which contains Windows tools including fastboot and adb
    - `img files` - The `img` files that are to be flashed onto the device.
    - `flash_FP4_factory.sh` - The installer script.

1. Boot into bootloader/fastboot mode.

    - Power OFF the device.
    - While holding `Volume Down` button, plug in a USB Cable that's connected to a PC on the other end.

1. Run the installer script on Terminal(Linux) or on Git Bash UI(Windows).
    - Make sure you are in the current directory which contains the `flash_FP4_factory.sh` file.

    **Linux**
    - Right click -> Open terminal here
    - Execute the following command:

    ```
    chmod +x flash_FP4_factory.sh && ./flash_FP4_factory.sh
    ``` 

    **Windows**
    - Right click -> Git Bash here
    - Execute the following command:

    ```
    chmod +x flash_FP4_factory.sh && ./flash_FP4_factory.sh
    ```

    The script will flash all required files and will wait for input at the last step. Proceed to locking the bootloader.


## Locking the Bootloader

{% include alerts/danger.html content="The FP4 comes with an anti-rollback feature. Read the paragraph marked `Caution` in Requirements section of this guide, before proceeding." %}

Once you have completed the above steps  and before rebooting you can lock your Bootloader. 

1. Lock critical partition with the following command
   - `fastboot flashing lock_critical`
   -  Approve with  <kbd>Volume +</kbd> then <kbd>power</kbd>
1. Reboot your device into bootloader, and plug it to your computer
1. Lock the device with the following command
   - `fastboot flashing lock`
   - Approve with  <kbd>Volume +</kbd> then <kbd>power</kbd>




