{% assign device = page.device -%}
{% if device.custom_recovery_codename %}
{% assign custom_recovery_codename = device.custom_recovery_codename %}
{% else %}
{% assign custom_recovery_codename = device.codename %}
{% endif %}

{% if device.required_bootloader %}
## Special Requirements
{% capture bootloader_versions -%}
    `{% for bootloader in device.required_bootloader -%}
        {{ bootloader }}{% unless forloop.last %} / {% endunless %}
    {%- endfor %}`
{%- endcapture %}

{% capture content %}
    Your device must be on bootloader version: {{ bootloader_versions }} otherwise the instructions found in this page will not work. The current bootloader version can be checked by running the command `getprop ro.bootloader` in a terminal app or an adb shell from a command prompt (on Windows) or terminal (on Linux or macOS) window.
{% endcapture %}

{% include alerts/danger.html content=content %}
{% endif%}

## Installing a custom recovery using `fastboot`
1. Before proceeding ensure you have downloaded the custom recovery from the link given in the Download section above

1. Connect your device to your PC via USB.

1. On the computer, open a command prompt (on Windows) or terminal (on Linux or macOS) window, and type:
    ```
    adb reboot bootloader
    ```
{% if device.download_boot %}
    You can also boot into fastboot mode via a key combination:
    * {% t devices.with_the_device_powered_off %}
    * {% t device.download_boot %}
{% endif %}

1. Once the device is in fastboot mode, verify your PC finds it by typing:
```
fastboot devices
```
    {% include alerts/tip.html content="If you see `no permissions fastboot` while on Linux or macOS, try running fastboot as root"%}
    {% include alerts/tip.html content="Some devices have buggy USB support while in bootloader mode. If fastboot hangs with no output when using commands such as `fastboot getvar` .. , `fastboot boot` ..., `fastboot flash` ... try a different USB port (preferably a USB Type-A 2.0 one) or a USB hub"%}


{%- if device.is_retrofit_dynamic_partitions and device.is_ab_device != true %}
1. since your device uses retrofitted dynamic partitions extract the below file from the recovery zip linked in the section 'Downloads for {{ device.codename }}'
    - `super_empty.img`
1.  In a adb enabled console on your PC run the below command

    `fastboot wipe-super super_empty.img`
{%- endif %}


1. Flash a recovery image onto your device by typing
{% if device.recovery_install_command %}
    ```
    {{ device.recovery_install_command }} recoveryfilename.img
    ```
    > {% translate content.recovery_file_name_text %} 

{% else %} 
    ```
    fastboot flash boot recoveryfilename.img
    ```

on some devices the below command may be required

    ```
    fastboot boot recoveryfilename.img
    ```
or

    ```
    fastboot flash recovery recoveryfilename.img
    ```
    > {% translate content.recovery_file_name_text  %}

{% endif %}

1. Now reboot into recovery to verify the installation:
     * {% t devices.with_the_device_powered_off %}
     * {% t device.recovery_boot %}

