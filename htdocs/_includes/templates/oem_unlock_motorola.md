{% if device.custom_warning %}
    {% t custom_bootloader_warning.motorola %}
{% endif %}

## Unlocking the bootloader

{% include templates/unlock_bootloader.md %}

1. Connect the device to your PC via USB.
1. On the computer, open a command prompt (on Windows) or terminal (on Linux or macOS) window, and type:

    ```shell
    adb reboot bootloader
    ```
1. Once the device is in fastboot mode, verify your PC finds it by typing:

    ```shell
    fastboot devices
    ```
1. Now type the following command to get the bootloader status:

    ```shell
    fastboot oem get_unlock_data
    ```
1. Follow the instructions at [Motorola Support](http://motorola-global-portal.custhelp.com/app/standalone/bootloader/unlock-your-device-a) to unlock your bootloader.
1. Since the device resets completely, you will need to re-enable USB debugging to continue.

{% include alerts/tip.html content="If your device is not supported by the Motorola Bootloader Unlock website, you may be able to use an alternative bootloader unlock method like [SunShine](https://web.archive.org/web/20250108150254/http://theroot.ninja/), though they only support some devices/firmwares." %}