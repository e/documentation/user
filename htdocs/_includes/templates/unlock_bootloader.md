
   {% include alerts/tip.html content="The steps given below only need to be run once per device." %}
{% if device.vendor != "Sony" %}
   {% include alerts/warning.html content="Unlocking the bootloader will erase all data on your device! Before proceeding, ensure the data you would like to retain is backed up to your PC and/or an online drive" %}
{% endif %}
{% if device.custom_warning  %}
   {% t device.custom_warning  %}
{% else %}
   {% include alerts/warning.html content="Some vendors/manufacturers prevent the bootloader from being unlocked. Depending on where your device was acquired you may or may not be able to unlock the bootloader. To verify if your device is compatible please check the [devices list](/devices/)." %}
{% endif %}


