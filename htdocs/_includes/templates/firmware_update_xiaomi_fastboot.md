{% include alerts/warning.html content="Make sure to download exact firmware for your model. You can discern your model from Settings > About phone > MIUI version (Eg: In MIUI version V13.0.6.0 SKHMIXM 'MI' represents your phone region, which is Global). You can also verify your Xiaomi phone at [official site](https://www.mi.com/global/verify)." %}
{% include alerts/tip.html content="Some devices might have buggy USB support while in bootloader mode, if you see `fastboot` hanging with no output when using commands such as `fastboot getvar ...`, `fastboot flash ...` you may have to try a different USB port (preferably a USB Type-A 2.0 one) or use a USB hub." %}


1. Extract firmware files from zip
   ```
   unzip -qq miui_*.zip firmware-update/*
   cd firmware-update
   ```
1. Power off the device, and boot it into bootloader mode
   
1. Run the following commands to flash firmware:
   ```
   {{- include.content -}}
   ```
1. Reboot:
   ```
   fastboot reboot
   ```
