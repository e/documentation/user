## Installing /e/OS from the bootloader
{% include alerts/tip.html content="For Windows users, we advise to use Git Bash. For more info about Downloads and Installation see [Official Documentation](https://git-scm.com/about)"%}

1. Unzip the archive

    ```
    unzip fileyoudownloaded
    ```

   > Replace `fileyoudownloaded` with the name of the archive file you downloaded.

   > Alternately you can create a folder and unzip the archive there.


1. Confirm that extracted directory contains following content:

    - `bin-linux-x86 directory` - which contains linux tools including fastboot and adb
    - `bin-msys directory` - which contains Windows tools including fastboot and adb
    - `img files` - The `img` files that are to be flashed onto the device.
    - `flash_FP3_factory.sh` - The installer script.

1. Boot into bootloader/fastboot mode.

    - Power OFF the device and press `Volume Down + Power` buttons simultaneously.


1. Run the installer script on Terminal(Linux) or on Git Bash UI(Windows).

1. Make sure you are in the current directory which contains the `flash_FP3_factory.sh` file.

    Linux
    - Right click -> Open terminal here
    - Execute the following command:

    ```
    chmod +x flash_FP3_factory.sh && ./flash_FP3_factory.sh
    ``` 

    Windows
    - Right click -> Git Bash here

    - Execute the following command:

    ```
    chmod +x flash_FP3_factory.sh && ./flash_FP3_factory.sh
    ```

    - The script will flash all required files and will wait for input at the last step.

    - The last command will ask you to confirm on the device to lock the bootloader.


    - Select **LOCK THE BOOTLOADER** with `Volume +` and validate with `Power` button.


1. Enjoy! The device will now reboot automatically on /e/OS.

{% include alerts/tip.html content= "If you find an issue with this installation document or if you want to suggest improvements, Please share your suggestion in [this document](https://community.e.foundation/t/fairphone-fp3-fp3-documentation-suggestions/20955) "%}

