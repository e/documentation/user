{%- assign device = page.device -%}


## Rooting your device

> Important: The device must be rooted before proceeding any further.

{% case device.root_method[0] %}
{% when 'custom' %}
1. Root your device by following [this]({{ device.root_method[1] }}) guide.
{% when 'kingroot' %}
1. Download KingRoot from [here](https://kingroot.net/).
   1. Install and run the apk to achieve root. Ensure you have a working Internet connection.
{% when 'towelroot' %}
1. Download TowelRoot from [here](https://towelroot.com/).
   1. Click the large lambda symbol to download the apk.
   1. Install and run the apk to achieve root.
{% endcase %}



## Flashing additional partitions

> Warning: This platform requires additional partitions to be flashed for recovery to work properly, the process to do so is described below.

1. First unzip the /e/OS download file linked in the section marked `Downloads for {{ device.codename }}` . 


1. The following files would show up in the location where you unzipped the contents of the  zip file

    
    {%- for partition in device.before_recovery_install.partitions %}
      * {{ partition }}.img
    {%- endfor %}
    

1. Place the files on the root of `/sdcard`:

    - `Using adb`:
  
    ```
        adb -d push dtbo.img /sdcard/filename.img
    ```

1. You can use any method you are comfortable with. `adb` is universal across all devices, and works both in Android and recovery mode, provided USB debugging is enabled.

1. Now, open an `adb -d shell` from a command prompt (on Windows) or terminal (on Linux or macOS) window. In that shell, type the following commands:

    ```
    su
    dd if=/sdcard/dtbo.img of=dtbo_a
    dd if=/sdcard/dtbo.img of=dtbo_b

    ```


## Installing a custom recovery using `dd`

> Warning: Installing a custom recovery will erase all data on your device! Before proceeding, ensure the data you would like to retain is backed up to your PC and/or a online drive.

1. Place the recovery image file on the root of /sdcard (USB debugging have to be enabled in developer options) :

    ```shell
    adb push recoveryfilename.img /sdcard/recoveryfilename.img
    ```
2. Now, open an `adb shell` from a command prompt (on Windows) or terminal (on Linux or macOS) window. In that shell, type the following commands:

    {%- if device.is_ab_device %}
    ```
    su
    dd if=/sdcard/<recovery_filename>.img of={{ device.recovery_partition }}_a
    dd if=/sdcard/<recovery_filename>.img of={{ device.recovery_partition }}_b
    ```
    {%- else %}
    ```
    su
    dd if=/sdcard/<recovery_filename>.img of={{ device.recovery_partition }}
    ```
    {%- endif %}
    

    {% translate content.device_install %}
3. Manually reboot into recovery
   - From the same shell, type the below command:
    
    ```
    reboot recovery
    ```
