## Preparing for installation

Tegra devices come with a unique boot mode called `Tegra ReCovery Mode` or RCM, also known as `APX``. Tegraflash is the official Nvidia tool for interfacing with RCM mode on recent Tegra devices. Tegraflash is only available for Linux. It will not run properly in a VM


## Installing a custom recovery using Tegraflash

1. Download a [flash package](https://www.androidfilehost.com/?w=files&flid=328892).
    -It will be named p3450_flash_package.tar.xz
    {% include alerts/tip.html content="These flash packages were built by LineageOS Developer Aaron Kling (webgeek1234) from officially supported LineageOS repositories, using Tegraflash and device configurations from Nvidia Linux4Tegra releases and a few custom scripts." %}
1. Install xz and python3. To do so, run sudo apt -y install xz-utils python3 (Debian based hosts) or dnf -y install xz-utils python3 (Redhat based hosts).

1. Run the following command in the folder which you downloaded the flash package to:
    `tar -xvf p3450_flash_package.tar.xz`    
1. Power off the device, and boot it into download mode:
    * With the device unplugged, jump the FRC REC pin, then plug in. Remove the jumper once apx enumerates.
1. Now, from the same terminal as before, run:

    `sudo ./flash.sh`

    {% include alerts/warning.html content="Please note this will require your user’s sudo password, as the script requires direct hardware access, which requires root privledges." %}    
    
1. The script will flash the boot stack and boot directly to recovery.
