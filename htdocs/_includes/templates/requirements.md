# Install {%- if device.minimal_os %} /e/OS minimal {%- else %} /e/OS {%- endif %}  on a {{ device.vendor }} {{ device.name }} - "{{ device.codename }}" {%- if device.is_alpha %} <span class="badge badge-warning badge-pill">alpha</span> {%- elsif device.is_beta %} <span class="badge badge-warning badge-pill ">beta</span> {%- endif %}

The instructions in this guide will assist you to install /e/OS on your {{ device.codename }}. Please read through the instructions at least once before actually following them, so as to avoid any problems later.
{% if device.can_rollback != true %}
It is important that you know that downgrading Smartphones on OS versions greater than the /e/OS OS version you plan to install, can cause instability or at worst brick some devices. We recommend that you check your current OS version, before attempting the installation.
{% endif %}

It is advisable to flash your device only if you **know** what you are doing and are ok taking the **associated risk**. 
All /e/OS builds are provided as best effort, **without any guarantee**. The /e/OS project and its project members deny any and all responsibility about the consequences of using /e/OS software and or /e/OS services.


{% assign install_adb_link = "/pages/install-adb" | relative_url%}
{% assign enable_usb_debug_link = "/pages/enable-usb-debugging" | relative_url %}
# Requirements
- If required take a backup of all important data from your phone on an external storage device before proceeding.
- Do not take a backup on the same device as some of these actions will format the device and delete the backup.
{%- if device.uses_twrp %}
- A guide on backing up with TWRP is available [here]({{ backup_and_restore_with_twrp_link }})
{%- endif %}
- Ensure your phone is charged more than 50%
{%- if device.vendor == "Samsung" %}
- Check that `adb` is enabled on your PC. If not you can find the setup instructions [here]({{ install_adb_link }})
{%- else %}
- Check that `adb` and `fastboot` are enabled on your PC. If not you can find the setup instructions [here]({{ install_adb_link }})
{%- endif %}
- Download all the files and images mentioned in the download section below before starting the installation
- Make sure you have a working data cable to connect your device to the PC. There are cables which are only for charging and do not transfer data.
{% if device.oem_unlock_code_required %}
- Your device requires a code to unlock the bootloader. Get the code [here]({{ device.oem_unlock_code_required }}) before proceeding with the next steps
{% endif %}
- Enable USB debugging on your device. You can find the instructions [here]({{ enable_usb_debug_link }})
- Make sure that your model is listed in the [Smartphone Selector](https://doc.e.foundation/devices). Check the model supported. Where available, information on the model supported should show as a pop-up when you hover the cursor on the Device name. The model number supported should be the exact same.
- Boot your device with the stock OS at least once and check every functionality. 

{% include alerts/warning.html content="Make sure that you can send and receive SMS and place and receive calls (also via WiFi and LTE, if available), otherwise it will not work on /e/OS as well. Additionally, some devices require that VoLTE/VoWiFi be utilized once on stock to provision IMS."%}






{% include templates/suggestions.md %}

