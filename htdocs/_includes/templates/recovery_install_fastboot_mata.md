{% assign device = page.device %}

## Unlocking the bootloader

{% include templates/unlock_bootloader.md %}

1. Connect your device to your PC via USB.


1. Enable OEM unlock in the Developer options under device Settings, if present.

1. On the computer, open a command prompt (on Windows) or terminal (on Linux or macOS) window, and type:
    ```
    adb reboot bootloader
    ```
1. Once the device is in fastboot mode, verify your PC finds it by typing:
    ```
    fastboot devices
    ```

1. Unlock bootloader

    ```shell
    fastboot flashing unlock
    ```

    {% include alerts/tip.html content="At this point, the device may display on-screen prompts which will require interaction to continue the process of unlocking the bootloader. Please take whatever actions the device asks you to in order to proceed." %}


1.  If the device doesn’t automatically reboot, reboot it. It should now be unlocked.

1. Since the device resets completely, you will need to re-enable USB debugging to continue.

{% include templates/recovery_install_fastboot_ab.md %}
