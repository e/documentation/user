## Unlocking the bootloader

{% include templates/unlock_bootloader.md %}

1. Boot the device
1. Enable Developer options
1. From developer options, enable OEM unlock
1. Reboot on fastboot by running the command in the PC console `adb reboot bootloader`

   {% include alerts/tip.html content=" All the console commands are run on an adb enabled PC and in the `adb` console." %}

1. Ask for unlock with `fastboot flashing unlock`
1. Approve with <kbd>volume +</kbd>
1. Unlock critical with `fastboot flashing unlock_critical`
1. Approve with <kbd>volume + </kbd>
1. Reboot the device `fastboot reboot`
    - The device proceeds with a factory reset
