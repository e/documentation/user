## OEM Unlock your device

{% include templates/unlock_bootloader.md %}

*To flash a custom ROM like LineageOS or /e/OS you need to unlock your device Bootloader. To unlock your Bootloader you as the owner of the device need to approve the unlocking. By enabling the OEM Unlock you are giving your permission for the Bootloader to be unlocked.*

1. Enable developer options and usb debugging following the instructions given [here](/pages/enable-usb-debugging)

1. Allow OEM unlock from developer options

    **Settings** > **System** > **Developer option** > **OEM Unlock**

1. Reboot into bootloader (*fastboot mode*) with

    ```
    adb reboot bootloader
    ```

1. Unlock your device bootloader

    {% include alerts/danger.html content="Unlocking the bootloader will wipe out all your data. Ensure you have taken a backup as mentioned in the Requirements section above." %}

    ```
    fastboot flashing unlock
    ```
1. On your phone screen confirm by pressing volume up
