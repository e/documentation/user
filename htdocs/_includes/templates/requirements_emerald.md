{% include templates/requirements.md %}

{% include alerts/danger.html content="Before moving forward, ensure your device is a Teracube 2e (2021). Please look at the serial number under the battery or dial \*#06#. If the serial number starts with 2021 (ex: **2021**11T2Exxxxxx), you are at the right place! If it starts with 2020, please move to [Teracube 2e (2020) documentation](../zirconia/install)"%}
