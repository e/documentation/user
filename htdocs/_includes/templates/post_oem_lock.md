## Disabling OEM Unlocking.

Once you boot your device after locking bootloader:

1. Finish `SetupWizard`
1. Go to Settings > About phone > Build number.
1. Tap the Build Number option seven times until you see the message You are now a developer! This enables developer options on your device.
1. Return to the previous screen and go to `System` to find Developer options at the bottom.
1. In `Developer Options`, turn `OEM Unlocking` off.
