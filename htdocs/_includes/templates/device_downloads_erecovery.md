## Downloads for {{ device.codename }}

{% if device.before_install_device_variants and device.before_install_device_variants.size > 0 %}
- Before following these instructions please ensure that the device is currently using the firmware version mentioned below. If your current installation is newer or older than the required version, please upgrade or downgrade to the required version before proceeding by following [this guide.](/{{ device.firmware_update }})
{%- if device.before_install_device_variants and device.before_install_device_variants.size > 0 %}


Download the required firmware for your model.
{%- for el in device.before_install_device_variants %}
   - {{ el.device }} - [{{ el.firmware }}]({{ el.download_link }})
{%- endfor %}
{%- else %}
1. Download the latest firmware for your device from official [website](https://new.c.mi.com/global/miuidownload/index).
{%- endif %}
{% endif %}
- [/e/OS Recovery for community build](https://images.ecloud.global/community/{{ device.codename }})
{% if device.build_version_dev %}
- /e/OS build : [{{ device.build_version_dev }} community](https://images.ecloud.global/community/{{ device.codename }})
{%- endif %}

{% if device.build_version_stable %}
- /e/OS build : [{{ device.build_version_stable }} official](https://images.ecloud.global/official/{{ device.codename }})
{%- endif %}

> To understand the difference between /e/OS builds check [this guide](/build-status)
