## Flashing additional partitions

{% include alerts/warning.html content="This platform requires additional partitions to be flashed for recovery to work properly.The steps to flash the partitions are given below" %}

{% include alerts/tip.html content="In case the partition files are not available in the /e/OS recovery zip, users can try and download them from the LineageOS wiki install page specific to this device and OS build. " %}

1. First unzip the /e/OS Recovery file linked in the section marked `Downloads for {{ device.codename }}` . The following files would show up in the location where you unzipped the contents of the eRecovery zip file

  

    {%- for partition in device.before_recovery_install.partitions %}
      * {{ partition }}.img
    {%- endfor %}

1. Next we manually reboot into bootloader or download mode
  * {% t devices.with_the_device_powered_off %}
  * {% t device.download_boot %}

1. Flash the downloaded image files to your device by typing (replace `<...>` with the actual filenames):
  ```
  {%- for partition in device.before_recovery_install.partitions %}
fastboot flash {{ partition }} <{{ partition }}>.img
  {%- endfor %}
  ```
