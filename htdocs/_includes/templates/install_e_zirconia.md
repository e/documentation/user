## Installing /e/OS from the bootloader

1. Download the build package linked in the `Downloads` section above and extract it to a folder of your choice
2. Connect the device to your PC if not already done
> Ensure you have a cable which allowed file transfer. Some cables can only be used for charging.

3. Move to the directory where you have extracted the files in the first step. To do that type the below command in a console
```bash
cd path-of-the-extracted-files
```
4. Install /e/OS by typing the following commands in the console
```bash
fastboot -w
fastboot flash preloader preloader_zirconia.bin
fastboot flash lk lk.img
fastboot flash md1img md1img.img
fastboot flash logo logo.bin
fastboot flash scp1 scp.img
fastboot flash scp2 scp.img
fastboot flash sspm_1 sspm.img
fastboot flash sspm_2 sspm.img
fastboot flash spmfw spmfw.img
fastboot flash tee1 tee.img
fastboot flash tee2 tee.img
fastboot flash boot boot.img
fastboot flash dtbo dtbo.img
fastboot flash recovery recovery.img
fastboot flash vbmeta vbmeta.img
fastboot flash vbmeta_system vbmeta_system.img
fastboot flash vbmeta_vendor vbmeta_vendor.img
fastboot flash super super.img
fastboot reboot
```

{% include alerts/tip.html content="If your device is not detect by fastboot, try to use `sudo fastboot` instead"%}
