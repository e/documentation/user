## Installing /e/OS 

### Installing /e/OS using IMG or image file



1. Boot your Murena One in bootloader mode, and plug it to your computer
1. On your computer, download the zip file from the link provided above. Unzip the folder and browse into it 
1. Flash /e/OS with the following commands:

>>

    fastboot erase userdata
    fastboot format md_udc

    fastboot flash boot boot.img
    fastboot flash cache cache.img
    fastboot flash cam_vpu1 cam_vpu1.img
    fastboot flash cam_vpu2 cam_vpu2.img
    fastboot flash cam_vpu3 cam_vpu3.img
    fastboot flash dtbo dtbo.img
    fastboot flash gz1 gz.img
    fastboot flash gz2 gz.img
    fastboot flash lk2 lk.img
    fastboot flash lk lk.img
    fastboot flash logo logo.bin
    fastboot flash md1img md1img.img
    fastboot flash preloader preloader_g1970upt_v2_gk_p60c_oj_r.bin
    fastboot flash recovery recovery.img
    fastboot flash scp2 scp.img
    fastboot flash scp1 scp.img
    fastboot flash spmfw spmfw.img
    fastboot flash sspm_2 sspm.img
    fastboot flash sspm_1 sspm.img
    fastboot flash super super.img
    fastboot flash tee2 tee.img
    fastboot flash tee1 tee.img
    fastboot flash teksunhw teksunhw.bin
    fastboot flash userdata userdata.img
    fastboot flash vbmeta_vendor vbmeta_vendor.img
    fastboot flash vbmeta_system vbmeta_system.img
    fastboot flash vbmeta vbmeta.img

    fastboot reboot

