## Unlocking the bootloader

{% include templates/unlock_bootloader.md %}

{% include alerts/warning.html content="Unlocking the bootloader will erase all data on your device! This also includes your DRM keys, which are stored in the Trim Area partition (also called TA). Before proceeding, ensure the data you would like to retain is backed up to your PC and/or your Google account, or equivalent. Please note that OEM backup solutions like Samsung and Motorola backup may not be accessible from /e/OS once installed. If you wish to backup the TA partition first, you can find tutorials related to your device on the internet." %}

1. On the device, dial `*#*#7378423#*#*` (`*#*#SERVICE#*#*`) to launch the service menu.
1. Go to `service info` > `configuration` and check `rooting status` - you can only continue if it says `Bootloader unlock allowed: Yes`.
1. Connect the device to your PC via USB.
1. On the computer, open a command prompt (on Windows) or terminal (on Linux or macOS) window, and type:

    ```shell
    adb reboot bootloader
    ```

1. Follow the instructions on [Sony’s official unlocking website](https://developer.sony.com/open-source/aosp-on-xperia-open-devices/get-started/unlock-bootloader/) to unlock your bootloader.
1. Use your code to unlock the bootloader of your device:

    `fastboot oem unlock <your_unlock_code>`
1. Since the device resets completely, you will need to re-enable USB debugging to continue (Settings->About Phone->Build version, and then you can go to Settings->Developer opptions to enable USB debugging).

{% if device.is_ab_device %}
{% include templates/recovery_install_fastboot_ab.md %}
{% else %}
{% include templates/recovery_install_fastboot_generic.md %}
{% endif %}
