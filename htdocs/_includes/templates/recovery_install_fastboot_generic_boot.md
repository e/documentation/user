
{% assign device = page.device -%}
{% if device.custom_recovery_codename %}
{% assign custom_recovery_codename = device.custom_recovery_codename %}
{% else %}
{% assign custom_recovery_codename = device.codename %}
{% endif %}
## Temporarily Booting a custom recovery using `fastboot`

1. Before proceeding ensure you have downloaded the custom recovery from the link given in the Download section above

1. Connect your device to your PC via USB.


1. On the computer, open a command prompt (on Windows) or terminal (on Linux or macOS) window, and type:
    ```
    adb reboot bootloader
    ```
{% if device.download_boot %}
    You can also boot into fastboot mode via a key combination:
    * {% t devices.with_the_device_powered_off %}
    * {% t device.download_boot %}
{% endif %}

1. Once the device is in fastboot mode, verify your PC finds it by typing:
```
fastboot devices
```
 - If you see `no permissions fastboot` while on Linux or macOS, try running fastboot as root
 - Some devices have buggy USB support while in bootloader mode. If fastboot hangs with no output when using commands such as `fastboot getvar` .. , `fastboot boot` ..., `fastboot flash` ... try a different USB port (preferably a USB Type-A 2.0 one) or a USB hub


{% if device.is_retrofit_dynamic_partitions %}

1.  Flash empty super image: 
    ````
    fastboot wipe-super super_empty.img
    ````
 - If you get the following error: fastboot: usage: unknown command wipe-super, make sure ADB and fastboot are updated to the latest version. You need fastboot version 28.0.2 or greater.

{% endif %}

1.  Flash a recovery image onto your device by typing
    {% if device.recovery_install_command %}
    ```
    {{ device.recovery_install_command }} recoveryfilename.img
    ```
    > {% translate content.recovery_file_name_text %} 

    {% else %} 
    ```
    fastboot flash boot recoveryfilename.img
    ```

    on some devices the below command may be required

    ```
    fastboot boot recoveryfilename.img
    ```
    or

    ```
    fastboot flash recovery recoveryfilename.img
    ```
    > {% translate content.recovery_file_name_text  %}

    {% endif %}



1.  Now reboot into recovery to verify the installation:
  * {% t devices.with_the_device_powered_off %}
  * {% t device.recovery_boot %}