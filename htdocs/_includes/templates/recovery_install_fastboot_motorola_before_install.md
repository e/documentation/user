{% if device.flash_compatible_firmware_file %}
{% include templates/flash_compatible_firmware_motorola.md %}
{% endif%}

{% if device.is_ab_device %}
{% include templates/recovery_install_fastboot_ab.md %}
{% else %}
{% include templates/recovery_install_fastboot_generic.md %}
{% endif %}
