## Unlocking the bootloader

{% include templates/unlock_bootloader.md %}

1. Download the bootloader unlock app for your device:

    - Navigate to [ASUS' support site](https://www.asus.com/support) and go to your device's page by entering its model number.
    - Click **Driver & Tools**
    - Select **Android** from the drop-down menu
    - Open the **Utilities** section
    - Download the **Unlock Device App**
    - If the downloaded file is packaged in a `.zip` archive, extract it.

1. Connect the device to your PC via USB.

1. On the computer, open a command prompt (on Windows) or terminal (on Linux or macOS) window and change to the location where the Unlock Device App `.apk` resides.

1. Install the `.apk` by typing:
```
adb install Name_of_unlock_app.apk
```

1. Run the application **Unlock Device Tool** from the device and agree to the terms.
    
    > Follow the steps as given on [the ASUS support site](https://www.asus.com/support) to complete the unlock process

1. Press the button to unlock your device.

1. The device should automatically reboot into bootloader mode and the message `unlock successfully...reboot after 5 seconds` should be displayed. The device will then reboot and load Android.

{% if device.flash_compatible_firmware_file %}
{% include templates/flash_compatible_firmware_asus.md %}
{% endif%}
{% if device.is_ab_device %}
{% include templates/recovery_install_fastboot_ab.md %}
{% else %}
{% include templates/recovery_install_fastboot_generic.md %}
{% endif %}
