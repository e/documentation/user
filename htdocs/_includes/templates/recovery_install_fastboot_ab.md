{%- assign device = page.device -%}
{% if device.custom_recovery_codename %}
{% assign custom_recovery_codename = device.custom_recovery_codename %}
{% else %}
{% assign custom_recovery_codename = device.codename %}
{% endif %}

## Temporarily Booting a custom recovery using `fastboot`

1. Connect your device to your PC via USB.

2. On the computer, open a command prompt (on Windows) or terminal (on Linux or macOS) window, and type:
    ```  
    adb reboot bootloader
    ```  

1. Once the device is in fastboot mode, verify your PC finds it by typing:
    ```
    fastboot devices
    ```   

{% if device.recovery_install_command %}
1. Flash a recovery image onto your device
    ```   
    {{ device.recovery_install_command }} recoveryfilename.img
    ```  
    > {% translate content.recovery_file_name_text %} 

1. Manually reboot into recovery mode
   
  * {% t devices.with_the_device_powered_off %}
  * {% t device.recovery_boot %}
      
{% else %}
1. Temporarily flash a recovery on your device by typing:
    ```    
    fastboot flash recovery recoveryfilename.img
    ```
    on some devices the below command may be required
    ```
    fastboot boot recoveryfilename.img
    ```
    or
    ```  
    fastboot flash boot recoveryfilename.img
    ```

> {% translate content.recovery_file_name_text %}    

1. Manually reboot into recovery mode
  * {% t devices.with_the_device_powered_off %}
  * {% t device.recovery_boot %}

{% endif %}

{% include alerts/tip.html content="Outdated fastboot releases dropped legacy A/B support, so it might attempt to flash to boot__a / boot__b rather than boot_a / boot_b if you try to flash boot. In this case, you must update fastboot to a release newer than or equal to 31.0.2. Alternatively, you can manually specify which slot to flash to based on what slot fastboot failed to flash to. For example, if fastboot fails to flash to boot__a, you must flash to boot_a."%}