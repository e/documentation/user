
{% if device.before_install_args.version %}
- Before following these instructions please ensure that the device is on the latest **Android {{ device.before_install_args.version }}** firmware.
{% endif %}
{% if device.before_install_stable_args.version %}
- If installing the official build, please ensure that the device is on the latest **Android {{ device.before_install_stable_args.version }}** firmware.
{% endif%}

