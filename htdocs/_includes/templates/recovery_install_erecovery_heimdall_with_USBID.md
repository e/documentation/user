{% if device.required_bootloader %}
## Special requirements
{% capture bootloader_versions -%}
    `{% for bootloader in device.required_bootloader -%}
        {{ bootloader }}{% unless forloop.last %} / {% endunless %}
    {%- endfor %}`
{%- endcapture %}

{% capture content %}
    Your device must be on bootloader version: {{ bootloader_versions }} otherwise the instructions found in this page will not work. The current bootloader version can be checked by running the command `getprop ro.bootloader` in a terminal app or an adb shell from a command prompt (on Windows) or terminal (on Linux or macOS) window.
{% endcapture %}

{% include alerts/danger.html content=content %}

{% endif %}


## Preparing for installation using Heimdall

Samsung devices come with a unique boot mode called “Download mode”, which is very similar to “Fastboot mode” on some devices with unlocked bootloaders. Heimdall is a cross-platform, open-source tool for interfacing with Download mode on Samsung devices. The preferred method of installing a custom recovery is through this boot mode – rooting the stock firmware is neither necessary nor required.

Please note: On windows PC's to run the installation using ODIN check this [HOWTO](https://community.e.foundation/t/howto-install-e-on-a-samsung-smartphone-with-windows-easily/3186). The steps given below explain how to install Heimdall and flash /e/OS

1. Enable OEM unlock in the Developer options under device Settings.
> It appears sometime that the OEM unlock option is missing from Development options, especially on new devices. Please connect your device to the wifi and check of system updates. After a reboot, the option should appear.

1. [Install Heimdall](../../support-topics/install-heimdall)  or if you prefer you can [Build latest Heimdall suite](../../support-topics/build-heimdall)
1. Power off the device and connect the USB adapter to the computer (but not to the device, yet).
1. Boot into download mode:
    * {% t device.download_boot %}

    You must accept/continue with the warning in "Download mode" to unlock the bootloader (see image below). You can accept/continue by short-pressing the `Volume UP` button. Then, connect your device with your PC if you have already not done it.
    ![](/images/download-mode.png)

1. Windows only: install the drivers. A more complete set of instructions can be found in the ZAdiag user guide.
   1. Run `zadiag.exe` from the Drivers folder of the Heimdall suite.
   1. Choose `Options » List all` devices from the menu.
   1. Select device with USB ID `04E8 685D` from the drop down menu.
   1. Click `Replace Driver` (having to select `Install Driver `from the drop down list built into the
   1. If you are prompted with a warning that the installer is unable to verify the publisher of the driver, select `Install this driver anyway`. You may receive two more prompts about security. Select the options that allow you to carry on.

1. On the computer, open a command prompt (on Windows) or terminal (on Linux or macOS) window, and type:

    ```shell
    heimdall print-pit
    ```
1. If the device reboots, Heimdall is installed and working properly.


## Installing the /e/OS Recovery on your {{device.codename }}

1. Download the /e/OS recovery (linked in the Download section above)

1. Power off the device and connect the USB adapter to the computer (but not to the device, yet).
1. Boot into download mode:
* {% t devices.with_the_device_powered_off %}
* {% t device.download_boot %}

   Accept the disclaimer, then insert the USB cable into the device.
1. On the computer, open a command prompt (on Windows) or terminal (on Linux or macOS) window in the directory the recovery image is located, and type:
```shell
heimdall flash --RECOVERY recoveryfilename.img --no-reboot
```
> Replace `recoveryfilename` with the name of the recovery you downloaded

1. A blue transfer bar will appear on the device showing the recovery being transferred.
    {% include alerts/tip.html content="The device will continue to display `Downloading... Do not turn off target!!` even after the process is complete. When the status message in the top left of the device's display reports that the process is complete, you may proceed." %}
1. Manually reboot into recovery:
* {% t devices.with_the_device_powered_off %}, {% t device.recovery_boot %}

    {% if device.update_vendors %}
    > On some devices, installation can fail if vendors are not up-to-date. In this case, please:
    > * Download [those vendors](https://androidfilehost.com/?fid=11410932744536982158)
    > * Install them
    >    * On the device, go into `Advanced` > `ADB Sideload`, then swipe to begin sideload
    >    * From the computer, please run `adb sideload <vendors file>`

    {% endif %}
  > Note: Be sure to reboot into recovery immediately after having installed the custom recovery. Otherwise the custom recovery will be overwritten and the device will reboot (appearing as though your custom recovery failed to install).

{% if device.recovery_install_need_patch %}
## Patch the device

1. Download the patch from [here](https://androidfilehost.com/?fid=962187416754474353)
1. Install the patch
1. On the device, in TWRP go into `Advanced` > `ADB Sideload`, then swipe to begin sideload
1. From the computer, please run `adb sideload <patch file>`
{% endif %}

> Note: Be sure to reboot into recovery immediately after having installed the custom recovery. Otherwise the custom recovery will be overwritten and the device will reboot (appearing as though your custom recovery failed to install).
