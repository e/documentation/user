module Jekyll
  module HideParentheses
    def hide_parentheses(input)
      input.to_s.gsub(/\(.*?\)/, "") 
    end
  end
end

Liquid::Template.register_filter(Jekyll::HideParentheses)