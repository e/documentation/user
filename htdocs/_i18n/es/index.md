## Acerca de /e/OS

>Más información sobre el proyecto /e/OS

- ¿Qué es /e/OS? Lee la descripción del producto]({% translate_link what-s-e %})
- Instalar /e/OS en un smartphone compatible]({% translate_link devices %})
- Crear una cuenta gratuita en Murena Workspace]({% translate_link create-a-murena-cloud-account %})
- CÓMO]({% translate_link support-topics %})
- Preguntas frecuentes]({% translate_link support-topics %})
- ¡Comparte tu /e/experiencia!]({% translate_link testimonios %})


## Contribuye a /e/OS

> ¿Quieres ayudar al proyecto?

- Proyectos buscando colaboradores]({% translate_link projects-looking-for-contributors %}) 
- [Puestos vacantes en Murena](https://murena.com/jobs/) 
- [Mantenedor de ROM]({% translate_link rom-maintainer %}) 
- [Traductores]({% translate_link translators %}) 
- [Probadores]({% translate_link testers %}) 
- [Voluntarios]({% translate_link volunteers %}) 

## Más información

> Información sobre el proyecto /e/OS o sus aplicaciones

- [/e/OS easy-installer]({% translate_link easy-installer %})
- Aplicación de mapas]({% translate_link maps %})
- Los diferentes tipos de compilaciónes]({% translate_link build-status %})
- App Lounge]({% translate_link app-lounge-main %})
- Privacidad avanzada]({% translate_link support-topics/advanced_privacy_main %})
- e.foundation Aviso legal y privacidad](https://e.foundation/legal-notice-privacy/)

## Atajos

> Enlaces rápidos a sitios web, foros y canales de Telegram de /e/OS

- [/e/OS sitio web](https://e.foundation/)
- Código fuente de /e/OS](https://gitlab.e.foundation/e)
- Ayuda o soporte](https://e.foundation/contact/)
- Informar de un error en /e/OS o aplicaciones](https://gitlab.e.foundation/e/backlog/-/issues/new)
- Agrega tu smartphone a la lista de compatibles](https://community.e.foundation/c/e-devices/request-a-device)

