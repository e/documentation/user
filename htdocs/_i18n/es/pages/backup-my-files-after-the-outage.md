## El proceso de recuperación
Una vez que comiences el proceso de recuperación de Murena Workspace, tus entradas y datos en línea faltantes reaparecerán gradualmente. Esto es lo que puedes esperar:

1. **Entrada de archivos**
   Esta será la primera entrada en regresar, inicialmente sin datos.

2. **Otras entradas**
   Las entradas como Notas y Galería reaparecerán progresivamente.

3. **Recuperación de datos**
   Tus datos recuperados se colocarán en una carpeta llamada `MurenaRecovery` en la entrada de Archivos. Algunos archivos pueden faltar o estar vacíos, así que revisa cuidadosamente. Además, como tus archivos recuperados están en una nueva carpeta dedicada, los compartimientos de archivos que hayas configurado antes de la interrupción no serán efectivos: deberás configurarlos nuevamente si deseas que funcionen.

Murena Workspace bloqueará las solicitudes provenientes de los siguientes clientes de Nextcloud:
- Aplicación Nextcloud (Windows, macOS, Linux, Android, iOS)
- Carnet
- EasySync
- eDrive (solo para /e/OS antes de 1.13)
- Notas /e/OS
- Notas Nextcloud

La razón detrás de esto es que estas aplicaciones podrían eliminar algunos archivos cuando reabramos, ya que el cliente detectará una diferencia entre lo que está presente localmente y lo que está presente en Murena Workspace. Al bloquear estas solicitudes, damos tiempo a cada usuario para respaldar sus archivos locales. La siguiente documentación explica, cliente por cliente, cómo puedes respaldar tus archivos como usuario.

## Respalda tus datos locales

### eDrive
Primero, debes verificar si eDrive se está ejecutando en tu dispositivo. Ve a `Configuración` > `Cuentas` > tu dirección de correo electrónico de Murena Workspace > `Sincronización de cuenta`. Si `Fotos y videos` está deshabilitado, significa que eDrive no se está ejecutando y puedes ignorar esta parte.

Si está ejecutando **/e/OS inferior a 1.13** ([¿cómo verifico mi versión de /e/OS?](https://community.e.foundation/t/howto-give-complete-e-os-version-info-easily-for-support-answers-comparison-etc/45030)):
- Actualiza a la última versión de /e/OS (`Configuración` > `Actualizaciones`)
- Si no puede actualizar, copia los datos del dispositivo /e/OS de las siguientes carpetas a otro(s) dispositivo(s):
    - DCIM
    - Documents
    - Movies
    - Music
    - Pictures
    - Podcasts
    - Recordings  
    - Ringtones

Si estás usando **/e/OS 1.13 y versiones posteriores**, ¡no tienes que hacer nada! eDrive nunca elimina archivos, así que no te preocupes por la pérdida de datos. La sincronización de archivos estará habilitada desde el principio con esta versión.

> **Nota:** De manera predeterminada, eDrive no volverá a enviar todos los archivos locales que tienes en tu teléfono inteligente. Una próxima actualización de eDrive corregirá este comportamiento y forzará el envío de los archivos que tienes en tu dispositivo a Murena Workspace.

### Aplicación Nextcloud (Android, iOS, Linux, macOS, Windows)
Nextcloud eliminará archivos que estaban presentes localmente pero no en el servidor. De manera predeterminada, las aplicaciones Nextcloud no se sincronizarán cuando reabramos la aplicación Archivos de Murena Workspace. Copia tus archivos a otra ubicación.

#### Windows, macOS y Linux
Para hacer una copia de seguridad:
1. **Abre la aplicación Nextcloud**: inicia la aplicación Nextcloud en cada dispositivo donde esté instalada.
2. **Accede a la carpeta local**: haz clic en "Abrir carpeta local" para ver tus archivos sincronizados.
3. **Selecciona archivos**
   - **macOS** Usa `Cmd (⌘) + A` para seleccionar todos los archivos, luego cópialos usando `Cmd (⌘) + C`
   - **Windows/Linux** Usa `Ctrl + A` para seleccionar todos los archivos, luego cópialos usando `Ctrl + C`
4. **Crea una carpeta de respaldo**: crea una nueva carpeta (por ejemplo, `Nextcloud-Backup`) en una ubicación segura.
5. **Pega tus archivos**
   - **macOS** Pega los archivos copiados en la carpeta de respaldo usando `Cmd (⌘) + V`
   - **Windows/Linux** Pega los archivos copiados en la carpeta de respaldo usando `Ctrl + V`

{% include alerts/warning.html content="Debes aplicar este proceso en cada dispositivo donde hayas instalado un cliente Nextcloud." %}

#### Android
1. Abre la aplicación `Archivos` y haz clic en el *botón Nextcloud* en el panel lateral (si no vez este ícono, ve al paso 9).

    ![image](/images/murena-workspace-recovery/nextcloud-1.png){: height="400" }
2. Ahora mantén presionado cualquier elemento, lo seleccionará, haz clic en el *menú de tres puntos (⋮)* en la esquina superior derecha y presiona *seleccionar todo*.

    ![image](/images/murena-workspace-recovery/nextcloud-2.png){: height="400" }
3. Esto seleccionará todos los archivos restantes.

    ![image](/images/murena-workspace-recovery/nextcloud-3.png){: height="400" }
4. Ahora haz clic en el *menú de tres puntos (⋮)* en la esquina superior derecha y haz clic en *copiar a...*.

    ![image](/images/murena-workspace-recovery/nextcloud-4.png){: height="400" }
5. Esto te llevará a una página en la carpeta raíz del dispositivo. Haz clic en el *botón agregar carpeta* para crear una nueva carpeta.

    ![image](/images/murena-workspace-recovery/nextcloud-5.png){: height="400" }
6. Te pedirá el nombre de la carpeta, en nuestro caso la llamamos `Nextcloud.bak`.

    ![image](/images/murena-workspace-recovery/nextcloud-6.png){: height="400" }
7. Ahora abre la carpeta recién creada y presiona el *botón copiar* en la parte inferior de la pantalla. Esto iniciará el proceso de copia.

    ![image](/images/murena-workspace-recovery/nextcloud-7.png){: height="400" }
8. Es posible que veas una notificación si el tamaño de la copia es grande.

    ![image](/images/murena-workspace-recovery/nextcloud-8.png){: height="400" }

    {% include alerts/success.html content="Tu proceso de respaldo está completo. En cualquier caso, si falló en el paso 1, haz lo siguiente:" %}
9. Abre la aplicación `Nextcloud`, selecciona `Configuración` desde el panel izquierdo.

    ![image](/images/murena-workspace-recovery/nextcloud-9.png){: height="400" }
10. Haz clic en la opción `Carpeta de almacenamiento de datos`, esto abrirá un popup. Toma nota del elemento seleccionado en el popup.

    ![image](/images/murena-workspace-recovery/nextcloud-10.png){: height="400" }
11. Abre la aplicación `Archivos` y navega hasta la ruta anotada en precedencia (por defecto debería ser: `Android/media/com.nextcloud.client`). Aquí encontrarás una carpeta llamada `nextcloud`. Manténla presionada para seleccionarla.

    ![image](/images/murena-workspace-recovery/nextcloud-11.png){: height="400" }
12. Ahora sigue los pasos 4 al 8 para completar el proceso de respaldo.

#### iOS

1. **Abre la aplicación Nextcloud** en tu iPhone o iPad.
2. **Navegua a los archivos/carpetas** que deseas respaldar.
3. **Toca el menú de tres puntos (⋮)** junto al nombre del archivo o carpeta.

    ![image](/images/murena-workspace-recovery/Nextcloud-Backup-iOS1.jpeg){: height="400"}
4. **Selecciona "Establecer como disponible sin conexión"** para descargar los archivos para acceso sin conexión.

    ![image](/images/murena-workspace-recovery/Nextcloud-Backup-iOS2.jpeg){: height="400"}
5. **Abre la aplicación Archivos** en tu iPhone.

    ![image](/images/murena-workspace-recovery/Nextcloud-Backup-iOS3.jpeg){: height="400"}
6. **Ve a "En mi iPhone"** o **"iCloud Drive"** bajo **Ubicaciones**.

    ![image](/images/murena-workspace-recovery/Nextcloud-Backup-iOS4.jpeg){: height="400"}
7. **Toca el menú de tres puntos (⋮)** en la esquina superior derecha y selecciona **"Nueva carpeta"**.
8. **Nombra la carpeta** (por ejemplo, `Nextcloud-Backup`).

    ![image](/images/murena-workspace-recovery/Nextcloud-Backup-iOS5.jpeg){: height="400"}
9. **Abre la aplicación Archivos** en tu iPhone.
10. **Ve a "Nextcloud"** bajo **Ubicaciones**.
11. **Toca el menú de tres puntos (⋮)** en la esquina superior derecha y selecciona **"Seleccionar"**.
12. **Selecciona todos los archivos** que deseas respaldar.

    ![image](/images/murena-workspace-recovery/Nextcloud-Backup-iOS6.jpeg){: height="400"}
13. **Toca el menú de tres puntos (⋮)** en la parte inferior y elije **"Copiar"**.

    ![image](/images/murena-workspace-recovery/Nextcloud-Backup-iOS7.jpeg){: height="400"}
14. **Navega a la carpeta "Nextcloud-Backup"**.
15. **Manten presionado dentro de la carpeta**, luego toca **"Pegar"** para copiar los archivos.

    ![image](/images/murena-workspace-recovery/Nextcloud-Backup-iOS8.jpeg){: height="400"}
### EasySync
EasySync elimina archivos que están presentes localmente pero no en el servidor. De manera predeterminada, la aplicación EasySync no se sincronizará cuando reactivemos la entrada de Archivos de Murena Workspace. Copia tus archivos a otra ubicación.

1. Abre la aplicación `Archivos` y manten presionados los elementos que desea respaldar.

   ![image](/images/murena-workspace-recovery/easysync-1.png){: height="400" }
2. Ahora haz clic en el *botón de tres puntos (⋮)* en la esquina superior derecha y haz clic en *copiar a...*.

   ![image](/images/murena-workspace-recovery/easysync-2.png){: height="400" }
3. Esto te llevará a una página en la carpeta raíz del dispositivo. Haz clic en el *botón agregar carpeta* para crear una nueva carpeta. Te pedirá el nombre de la carpeta, en nuestro caso la llamamos `easySync.bak`.

   ![image](/images/murena-workspace-recovery/easysync-3.png){: height="400" }
4. Ahora abre la carpeta recién creada y presiona el *botón copiar* en la parte inferior de la pantalla. Esto iniciará el proceso de copia.

   ![image](/images/murena-workspace-recovery/easysync-4.png){: height="400" }

### Notas /e/OS
La aplicación Notas /e/OS eliminará las notas que estaban presentes en el dispositivo pero no en el servidor. La entrada de Notas de Murena Workspace está actualmente deshabilitada para evitar este comportamiento. Para respaldar tus notas:

1. **A corto plazo: usa el botón Compartir**
   - Abre las notas una por una
   - Usa el botón compartir para exportar cada nota

2. **Próximamente: usa la última aplicación de Notas /e/OS**
   Proporcionaremos un APK para ayudarte a extraer todas tus notas. Manténte atento a las actualizaciones que se lanzarán a través de App Lounge en particular. Para poder recibir la última actualización de /e/OS Notes cuando se publique:
   - Actualiza a la última versión de /e/OS
   - O, al menos, instala la última versión de App Lounge siguiendo [esta documentación](https://doc.e.foundation/support-topics/app_lounge.html#download-app-lounge-apks)

### Nextcloud Notes
- Abre las notas una por una
- Usa el botón compartir para exportar cada nota

### Carnet
1. Crea el respaldo
   - Usa el menú de tres líneas (☰) en la parte superior izquierda
   - Selecciona el botón *Configuración*

       ![image](/images/murena-workspace-recovery/carnet-Backup1.jpeg){: height="400" }
   - Usa el botón *Exportar*

       ![image](/images/murena-workspace-recovery/carnet-Backup2.jpeg){: height="400" }
   - Ve a la carpeta que deseas usar para el respaldo y presiona el botón *Guardar* en la esquina inferior derecha

       ![image](/images/murena-workspace-recovery/carnet-Backup3.png){: height="400" } 

2. Verifica el respaldo
   - Abre la aplicación Archivos
   - Navega a la carpeta seleccionada previamente
   - Abre `carnet_archive.zip`
   - Deberías ver archivos `.sqd` correspondientes a tus notas, así como carpetas si tenías alguna

## Sección de preguntas frecuentes
- **P: ¿Por qué está deshabilitada la sincronización?**
  - R: La sincronización está deshabilitada para evitar la pérdida de datos durante el proceso de recuperación.

- **P: ¿Cuándo puedo volver a habilitar la sincronización?**
  - R: Le notificaremos cuando sea seguro volver a habilitar la sincronización.
