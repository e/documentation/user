¿Qué hacer cuando tengas un problema en /e/OS?

## Conéctate a nuestro servidor GitLab

   * Si tienes una cuenta, [iniciar sesión](https://gitlab.e.foundation/users/sign_in)
 * Si no, [crear una](https://gitlab.e.foundation/e)

## Ir a nuestro panel de problemas

   - Problemas con /e/OS se listan y crean [aquí](https://gitlab.e.foundation/e/backlog/-/issues)

   - Por favor, antes de reportar un problema, comprueba que no ha sido reportado anteriormente. Para comprobarlo

   - Escribe el error en la barra de búsqueda en la parte superior de la pantalla como se muestra a continuación

   ![Nueva búsqueda](/images/new_issue_search.png)

   - La pantalla también mostrará otros temas con términos similares

   - Si tu búsqueda no devuelve ningún resultado, crea un nuevo reporte

## Crear un nuevo reporte

   - Selecciona una plantilla para el reporte en el menú desplegable "Elegir una plantilla" 
- "Aplicar plantilla" 
- Agrega un título 
- Rellena todos los datos solicitados en la plantilla 
- Siempre que sea posible, [agregar registros](/support-topics/create-a-log). Esto ayuda a los desarrolladores a depurar mejor el problema. 
- Cuando hayas terminado, haz clic en "Enviar reporte".



## ¿Recibes un mensaje de error durante la creación de un nuevo ID en /e/OS Gitlab?

   - Algunos usuarios puede que reciban un mensaje de error al intentar crear un nuevo ID en /e/OS Gitlab
   - Este mensaje quizás te pedirá ponerte en contacto con Murena en <helpdesk@murena.com>. 
   - En caso de que encuentres un error de este tipo, envía un correo electrónico con los siguientes datos
      - Nombre completo:
      - Nombre de usuario deseado:
      - Email:
   - <a href="mailto:helpdesk@murena.com?subject=Open%20GitLab%20account&body=Hi%2CI%20want%20to%20create%20a%20Gitlab%20acccount%20to%20report%20issue.%0A%0AFull%20Name%3A%20%0ADesired%20username%3A%20%0AEmail%3A%20">Crear un ticket de Helpdesk para la creación de una cuenta GitLab</a>



   - Vamos a explicar la razón de este error. Hace algún tiempo tuvimos un problema con un gran número de ID falsos que inundaban nuestros servidores. Como medida preventiva, ahora permitimos registros desde un número limitado de dominios de correo electrónico. Los usuarios que deseen registrarse con su dirección de correo electrónico privada o, por ejemplo, su dirección de Gmail, pueden enviar una solicitud a <helpdesk@murena.com> y el equipo creará una cuenta para ellos. 



## Sintaxis y fuente

   * [Fuente](https://docs.gitlab.com/ee/user/project/description_templates.html#setting-a-default-template-for-issues-and-merge-requests)
   * [Markdown (GitLab)](https://docs.gitlab.com/ee/user/markdown.html)

