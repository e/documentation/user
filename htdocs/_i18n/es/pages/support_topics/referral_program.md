# Programa de recomendación de Murena Workspace

¡Consigue hasta 40€ de créditos para tu almacenamiento en la nube invitando a tus amigos a Murena Workspace!

Por cada amigo que se una y cree una cuenta en Murena Workspace, ganarás 2€ que podrás utilizar para
almacenamiento en la nube Murena.

Murena Workspace es tu cuenta personal de correo electrónico, tu agenda y contactos, tu disco en la nube y
su suite ofimática en línea, todo combinado en un único servicio, fácil de usar. Murena Workspace funciona con
software de código abierto probado como NextCloud y OnlyOffice.

Tu cuenta Murena Workspace es gratuita hasta 1GB de almacenamiento. Nuestros planes de pago empiezan en 1,99€ al mes para
20GB.

## ¿Cómo funciona?

Una vez tienes una cuenta Murena Workspace (e.email/murena.io), es muy fácil.

Todo lo que tienes que hacer es identificarte en [https://murena.com/es/my-account/](https://murena.com/es/my-account/) – nuestra plataforma de comercio electrónico – con tu Murena ID.

En el espacio de tu cuenta murena.com, localiza la sección 'Recomendaciones' y copia tu enlace único y anónimo.

Puede copiar su enlace único fácilmente y utilizarlo en cualquier otro lugar. Por ejemplo, enviándolo por correo electrónico a sus
amigos o incluso compartirlo en un tweet.

Por cada amigo que utilice este enlace para crear una cuenta gratuita, recibirás un crédito de 2€
en tu cuenta de murena.com.

## ¿Cómo invito a mis amigos?

Es muy sencillo, solo tienes que enviar el enlace de registro único que se encuentra en tu cuenta de murena.com
cuenta. El enlace es totalmente anónimo, no recoge información identificativa como
nombre o dirección de correo electrónico.

## ¿Cómo puedo usar el crédito?

Sus crédito puede utilizarse una vez que se suscriba a un plan de almacenamiento ampliado. Se aplicarán como descuento en el pago de suscripción.

## ¿Por cuánto tiempo será válido mi crédito?

Su crédito será válido durante 24 meses.

## ¿Cuánto durará este programa de recomendación?

El plan de recomendaciones de Murena Workspace es un plan permanente.

## ¿Puedo canjear mis créditos por la compra de un teléfono u otro artículo en murena.com?

Sus créditos sólo son válidos para la suscripción de un plan de pago de Murena Workspace.

## ¿Cómo funciona para mis amigos?

Al hacer clic en el enlace de recomendación, su amigo accederá a la página de invitación a la cuenta donde
podrá crear un nombre de usuario y una contraseña.

Una vez completado el proceso de creación de la cuenta, tu amigo dispondrá de un enlace a una cuenta Murena en la nube desde la que podrá acceder a la cuenta de correo electrónico recién creada, al almacenamiento en la nube o empezar a utilizar la única suite ofimática.

## ¿Cómo puedo crear una cuenta Murena Workspace?

Es muy sencillo. Visite [https://murena.io/signup](https://murena.io/signup/es/e-email-invite) para solicitar su invitación. Sólo se le incluirá en el programa de recomendación cuando también se identifique con su Murena ID en [https://murena.com/es/my-account/](https://murena.com/es/my-account/) y utilice sus enlaces de recomendación.

## ¿Qué es Murena Workspace?

Murena Workspace es un ecosistema en línea completo y totalmente libre de Google.

Murena Workspace es tu cuenta personal de correo electrónico con el dominio murena.io/e.email, tu agenda y contactos, tu disco en la nube y tu suite ofimática online, todo combinado en un único servicio, sencillo de usar. Murena Workspace funciona con software de código abierto de eficacia probada como NextCloud y OnlyOffice.

## ¿Es anónimo el enlace de recomendación?

El enlace es completamente anónimo, no recoge información identificativa como nombre o
dirección de correo electrónico. La persona que envía el enlace no tiene visibilidad de qué personas lo han utilizado.

## No veo mis créditos en mi espacio murena.com, ¿qué debo hacer?

Contáctenos a [helpdesk@murena.com](mailto:helpdesk@murena.com) para que podamos investigar y resolver el problema.
