### Mi dispositivo ya no es compatible con LineageOS. ¿Dejará /e/OS también de dar soporte??

/e/ no depende de LineageOS a la hora de decidir qué dispositivos soporta o deja de soportar. Ya tenemos varios dispositivos que no están en la lista de soporte de LineageOS. Planeamos portar más dispositivos para que funcionen con el código de /e/OS.

### Si mi dispositivo no es compatible con LineageOS, ¿seguiré recibiendo los parches de seguridad a tiempo??

Sí. Descargamos y aplicamos los parches de seguridad para los dispositivos si están disponibles para su descarga desde la fuente.

### ¿Será el ROM /e/OS compatible con smartphones con teclado físico? ?

La portabilidad en cualquier dispositivo depende de:
- ¿Es posible desbloquear el cargador de arranque?
- ¿Tenemos acceso a algún árbol de código fuente de referencia para construir Android en él?
- Ancho de banda del desarrollador (portar un nuevo dispositivo con todas las características funcionando, toma entre 2 y 3 meses para un desarrollador experto)

### Mi dispositivo tiene casi diez años. ¿Continuará el soporte técnico?

Tendremos que dejar de dar soporte a versiones antiguas de Android como Nougat. La razón detrás de esto es
- Es muy difícil dar soporte a varias versiones del sistema operativo al mismo tiempo
- Las actualizaciones de seguridad no pueden retroceder eternamente. 

Ten en cuenta que la experiencia con /e/OS es similar sea cual sea la versión de Android que se esté ejecutando en segundo plano. Puedes consultar la lista de dispositivos compatibles [aquí]({% tl devices %})
### ¿Me recomendarían un teléfono?

La decisión de elegir un teléfono debe basarse en 

- Estar en la lista de [/e/OS soportados](dispositivos/) (si planeas instalar /e/OS )
- Disponibilidad en su ubicación
- Fácil acceso a los centros de servicio de hardware
- Por último, pero no menos importante, si se ajusta en tu presupuesto 

### Permite /e/OS bloquear el cargador de arranque en los teléfonos que soportan el arranque verificado

Por el momento, sólo bloqueamos el cargador de arranque en los Fairphone 3/3+ preinstalados. Está en desarrollo implementar esta característica en todos los dispositivos compatibles con /e/OS.

### Siendo una bifurcación de LineageOS, usan el último código fuente de LineageOS al construir su ROM

Sí, antes de cada desarrollo mayor, primero sincronizamos las últimas derivaciones de LOS. A continuación, fusionamos con nuestras derivaciones. 

### ¿Permite el sistema operativo /e/OS root o se pueden utilizar aplicaciones que requieran root?

Se puede hacer root en /e/OS. Dicho esto, no está realmente en el ámbito de nuestros proyectos. Como se ha mencionado antes, estamos construyendo /e/OS para el usuario medio y no para geeks que ya tienen un montón de opciones o saben cómo modificar técnicamente sus dispositivos. 

Para los usuarios que quieran rootear el dispositivo, tengan en cuenta que /e/OS no requiere root.  Además, /e/OS no permite el rooteo de aplicaciones, excepto adb en las opciones de desarrollador. Los usuarios pueden utilizar herramientas de terceros (como Magisk) para acceder a estas características, pero ese tema está fuera del alcance de esta guía.
