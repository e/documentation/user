### ¿Por qué no convertir todas tus aplicaciones preinstaladas a código abierto?

Con la excepción de la aplicación de Mapas (Magic Earth) todas nuestras aplicaciones de sistema estándar son de código abierto. Usted puede encontrar la explicación detrás de la elección de Magic Earth [aquí](/maps)

### ¿Dónde podemos ver el código de las aplicaciones de código abierto que están bifurcando? 


Puede navegar por los proyectos en nuestro [gitlab](https://gitlab.e.foundation) y consultar el código fuente.

Por ejemplo, para encontrar el código de la aplicación Mail puedes [buscar aquí](https://gitlab.e.foundation/e/apps/mail)

Para obtener enlaces a la ubicación del código fuente de cada una de nuestras aplicaciones del sistema estándar, consulte el [Documento de descripción del producto aquí]({% tl what-s-e %}#e-default-applications)

### ¿Por qué no contribuyes a la fase inicial?

Contribuimos en la fase inicial 
 - cuando es posible
 - cuando el proyecto ascendente lo acepta
 - cuando una contribución ascendente tiene sentido. 

Sin embargo, hemos observado que, a menos que las contribuciones sean para corregir errores triviales, en el 99% de los casos las fusiones son rechazadas. La razón detrás del rechazo es que los mantenedores de los proyectos ascendentes tienen sus propias ideas sobre su proyecto, sus características, la interfaz de usuario, los colores, etc. y no están dispuestos a aceptar nuestras sugerencias o cambios de código. 

Esta es la razón principal por la que la bifurcación es común en los proyectos de código abierto.


### ¿Siguen los esfuerzos por conseguir que Magic Earth abra el código de su aplicación? 

Nuestros planes para discutir con Magic Earth se pospusieron debido a la pandemia. Esperamos discutir con ellos y hacer algún progreso en este tema este año. Sin embargo, mantenemos nuestras opciones abiertas. 

Estamos buscando otras aplicaciones de Mapas disponibles que no requieran que el usuario descargue constantemente nuevas versiones de los mapas. Esto haría que la experiencia del usuario fuera más fluida y ocupara menos ancho de banda. Lo más importante es que la aplicación sea de código abierto, en línea con nuestros ideales.
