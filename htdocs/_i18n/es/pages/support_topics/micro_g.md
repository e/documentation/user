## Estado del desarrollo de microG

El desarrollo de microG es un proyecto en curso. Estamos en contacto regular con su mantenedor y apoyamos financieramente el proyecto.

Se están planificando e implementando mejoras y actualizaciones. Hace algún tiempo añadimos compatibilidad con FCM y optimizamos el código.

También se ha añadido la API de rastreo de contactos (aunque no está activada por defecto en /e/OS, véase más abajo).

Hay más actualizaciones en preparación. 
El proyecto es complejo y sería estupendo si más usuarios contribuyeran.

## microG y microG EN

- microG es una reimplementación de código abierto de las aplicaciones y bibliotecas de usuario de Android propiedad de Google.

- microG EN es una compilación de microG y sólo es necesaria para los usuarios que necesiten utilizar las aplicaciones de seguimiento de contactos COVID-19.

##Todo sobre la búsqueda de contactos

### Una nota del equipo de Desarrollo de /e/OS 
- La aplicación de rastreo de contactos no es requerida por /e/OS para funcionar y puede ser fácilmente eliminada por los usuarios a su discreción 
- /e/OS no almacena ningún dato generado por la aplicación o la API de Notificaciones de Exposición 
- Esta función se proporciona a los usuarios de /e/OS que están obligados a utilizar una aplicación de rastreo Covid-19 específica del gobierno.

### Más información sobre la API microG EN 
La API de notificaciones de exposición es necesaria para que el smartphone interactúe con las aplicaciones de seguimiento covid-19 utilizadas en determinados países. 
- La API está desactivada por defecto 
- La implementación requiere la intervención directa del usuario para ser activada. 
Esto incluye 
- Instalar la API 
- Instalar aplicaciones específicas para su país 
- Activar la API cuando se le solicite

Esta guía comparte los pasos necesarios para activar la estructura de /e/OS

{% include alerts/tip.html content="Not all covid tracing apps require the Exposure Notifications API to function. For example, the TousAntiCovi app of France does not require the Exposure Notifications API. Check the app specific to your country works on the /e/OS, before enabling the API"  %}

### ¿Cómo funciona el rastreo de contactos de Covid-19?

El rastreo de contactos COVID-19 se realiza a través de la API de notificaciones de exposición. La API tiene las siguientes cuatro características

- Publicidad via Bluetooth : Tu teléfono emite un Identificador de Proximidad Móvil (RPI) a través de Bluetooth. Cada diez minutos se publica un nuevo RPI, lo que hace imposible rastrearte. Los RPI se derivan de una clave privada que tu dispositivo genera aleatoriamente una vez al día. Esta derivación garantiza criptográficamente que los RPI no puedan vincularse entre sí, salvo en el caso de que compartas la clave privada.
- Escaneo via Bluetooth : Cada 5 minutos aproximadamente, el teléfono busca dispositivos Bluetooth cercanos que emitan RPI y almacena el RPI junto con una marca de tiempo en una base de datos local.
- Intercambio de claves : Cuando hayas dado positivo en la prueba de COVID-19, puedes pedir a tu teléfono que comparta tus claves privadas de los últimos 14 días con la aplicación de rastreo de contactos de COVID-19. Sólo con esta clave privada es posible que otras personas vean si han estado en contacto contigo durante más de 10 minutos. La implementación de microG se asegurará de que este intercambio de claves no se produzca sin el consentimiento explícito del usuario.
- Cálculo del riesgo : A partir de la lista de claves privadas compartidas por la aplicación complementaria y la lista de RPI recopilados en los últimos 14 días, el teléfono comprueba si haz estado en contacto estrecho con una persona que, según los informes, ha dado positivo. El teléfono calcula una puntuación de riesgo total y proporciona información sobre la fecha y la hora, la duración del contacto y la intensidad de la señal de la exposición a la aplicación complementaria. La aplicación complementaria no sabrá qué cláves o personas estuvieron en contacto estrecho, ni obtendrá información sobre dónde se produjo el contacto.

{% include alerts/tip.html content="microG ofrece la única implementación de código abierto de esta API."%}

### Steps to install Exposure Notifications API on /e/OS

1. Download the latest [microG EN](https://gitlab.e.foundation/e/apps/GmsCore/-/releases)

     - Las descargas para ambas versiones (`/dev` y `/stable`) están disponibles en esta ubicación. Comprueba tu versión y descarga el apk correspondiente

1. Install it manually
1. Check regularly for any updates and if available apply them manually


### Configuring an application to work with microG EN

Here we will use the example of the Covid Radar app (from Spain)
 - After the installation of the application, tap to open it on the phone

     ![](/images/Covid_3.png)
 - This will display a pop up asking permissions to
   - Enable Bluetooth
   - Enable activation of the Exposure Notifications API
 - Choose `ok` for both these pop ups
 - Now your app which in our example was Covid Radar will start working.
 - The Exposure Notifications API will also be enabled at this stage as you can see in this screenshot

     ![](/images/Covid_7.png)


### How to update the microG Exposure Notification Framework

The new **microG EN version** has some improvements in the Exposure Notification Framework API.

The Exposure Notification Framework has been developed by Apple and Google to enable contact tracing on iOS and Android. This Exposure Notification Framework API is necessary to use most COVID-19 contact tracing apps.

{% include alerts/tip.html content="Not all covid tracing apps require the Exposure Notifications API to function. For example, the TousAntiCovi app of France does not require the Exposure Notifications API. Check the app specific to your country works on the /e/OS, before enabling the API"  %}

Unfortunately, the microG EN update we share on Apps does not install or update automatically and needs to be applied manually.

To apply it manually, you have to:

- Download the latest microG EN

The downloads for both (`/dev` and `/stable`) builds are available at [this location](https://gitlab.e.foundation/e/apps/GmsCore/-/releases). Check your version and download the corresponding apk.

- Install the apk file manually
- Check regularly for any updates, and if available, apply each update manually.

If microG EN becomes unavailable on your device after an /e/OS update, do not worry, you can simply reinstall it on your device following the steps given in this guide.


## How to disable the app or the framework?

You can easily disable the COVID-19 contact tracing from your /e/OS. All that you need to do is..
- Uninstall the contact tracing application you downloaded
- Disable Exposure Notifications API from `Settings` > `System` > `microG` > `Exposure Notifications`

In addition, it's also possible to uninstall the microG Exposure Notifications version. To achieve that, kindly go into `Settings` > `Apps & notifications` > `microG Services Core`, and then tap on the `3 dots button` at top right and choose `Uninstall updates`.

## Additional References

- [microG project](https://microg.org/)
- [microG on Github](https://github.com/microg)
- [microG projects on /e/OS Gitlab](https://gitlab.e.foundation/e?filter=microg)
