## Objetivo del documento

Este documento explica el uso del alias de correo electrónico de murena.io para tu cuenta de correo electrónico.

## Direcciones de correo murena.io

Los usuarios de Murena Workspace tienen ahora la opción de usar el dominio murena.io para sus comunicaciones de correo electrónico. Por ejemplo, una usuaria llamada "María" puede ahora usar tanto `maria@e.email` como `maria@murena.io` para escribir y recibir correos. Puedes mandar e-mails con este alias, de forma que tus contactos verán los correos escritos desde murena.io. Puedes incluso establecer esta dirección como predeterminada en distintas aplicaciones, pero no olvides que tu identificador de usuario sigue siendo el correo acabado en `@e.email`.

## Cómo configurar el alias en Murena Workspace

Abre la aplicación de correo en Murena Workspace y haz click en el botón de ajustes abajo a la izquierda.

![ajustes de correo](/images/howtos/ecloud/email_settings.png)

Haz clic en "Cuentas". En la sección de identidades verás sus identidades e.email y murena.io. Puedes mover tu identidad murena.io por encima de la identidad e.email utilizando el icono con 6 puntos. La de la parte superior será utilizada como estándar cuando escribas un correo electrónico.

![identidades](/images/howtos/ecloud/rainloop_identities.png)

Ahora, clic 'Regresar' y luego en Nuevo mensaje. Verás tu nueva identidad en la sección "De". Si haces clic en ella, podrás cambiarla por la antigua. Para el siguiente correo electrónico, volverás a ver la identidad estándar.

## Cómo configurarlo en el teléfono

Abre la aplicación "Correo". Haz clic en el icono de 3 líneas arriba a la izquierda y luego en "Configuración". En Cuentas, haz clic en tu dirección de correo electrónico.

![ajustes de correo](/images/howtos/ecloud/phone_email_settings.png)

Haz clic en "Enviar correo", luego en "Gestionar identidades". Haz clic en el icono con 3 puntos arriba a la derecha y despues en "Nueva identidad".
Introduce tu nombre y tu nueva dirección de correo electrónico. Ahora haz clic en "Guardar".

![editar identidad](/images/howtos/ecloud/phone_email_edit_identity.png)

Ahora en la pantalla de "Gestionar identidades", mantén pulsada la nueva identidad que has creado hasta que aparezca un menú contextual donde elegirás "Mover arriba / hacer predeterminada". Esta dirección se usará para cada nuevo correo que escribas.

![hacer identidad predeterminada](/images/howtos/ecloud/phone_make_default_identity.png)

Clic "Atrás" varias veces hasta que llegues a tu bandeja de entrada. Ahora cuando redactes un correo (icono del lápiz), verás tu nueva identidad murena.io en el campo "De". Si haces clic en ella, puedes elegir la anterior sólo para ese correo.

## Cómo configurar en un cliente de correo de PC

Ten en cuenta que puedes utilizar murena.io como dirección de correo electrónico **pero no como identificación de usuario**.
En tu cliente de correo electrónico, aún debes identificarse con tu usuario `@e.email` o `@murena.io`. 
Normalmente, tu configuración debería detectarse automáticamente. Introduce tu contraseña y guárdala si no quieres que se te pregunte cada vez. Los ajustes para la configuración manual están disponibles [aquí](https://e.foundation/email-configuration/).

![email_client_1](/images/howtos/ecloud/email_client_1.png)

Una vez que tu correo electrónico esté configurado en la aplicación como de costumbre, abre 'propiedades'.

![Propiedades de correo electrónico](/images/howtos/ecloud/evolution_email_properties.png)

En "alias" añade tu alias de murena.io. Luego haz clic en OK.

![alias](/images/howtos/ecloud/evolution_aliases.png)

Ahora deberías poder elegir qué dirección usar cada vez que escribes un correo. También hay una opción para elegir cuál es la identidad por defecto. 

## Consejos

También puedes utilizar un [filtro de correo electrónico](/support-topics/email-filters) para clasificar los correos electrónicos enviados a ambas direcciones en carpetas diferentes.
