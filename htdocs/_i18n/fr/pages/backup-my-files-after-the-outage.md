## Le processus de récupération
Après le début du processus de récupération de Murena Workspace, vos applications et données en ligne manquantes réapparaîtront progressivement. Voici à quoi vous attendre :

1. **Application Fichiers**
   Ce sera la première application à revenir, dans un premier temps sans aucune donnée.

2. **Autres applications**
   Les applications comme Notes et Galerie réapparaîtront progressivement.

3. **Récupération des données**
   Vos données récupérées seront placées dans un dossier nommé `MurenaRecovery` dans l'application Fichiers. Certains fichiers peuvent être manquants ou vides, veuillez donc vérifier cette récupération soigneusement. De plus, comme vos fichiers récupérés sont dans un nouveau dossier dédié, les partages de fichiers que vous avez pu configurer avant la panne ne seront pas effectifs : vous devez les configurer à nouveau si vous souhaitez qu'ils fonctionnent.

Murena Workspace bloquera les requêtes provenant des clients Nextcloud suivants :
- Application Nextcloud (Windows, macOS, Linux, Android, iOS)
- Carnet
- EasySync
- eDrive (uniquement pour /e/OS avant 1.13)
- Notes /e/OS
- Notes Nextcloud

La raison derrière ce choix est que ces applications pourraient supprimer certains fichiers à la réouverture du service, car le client détecterait une différence entre ce qui est présent localement et ce qui est présent sur Murena Workspace. En bloquant les requêtes, nous laissons le temps à chaque utilisateur de sauvegarder ses fichiers locaux. La documentation ci-dessous explique, client par client, comment en tant qu'utilisateur vous pouvez sauvegarder vos fichiers.

## Sauvegardez vos données locales

### eDrive
Tout d'abord, vous devez vérifier si eDrive fonctionne sur votre appareil. Allez dans `Paramètres` > `Comptes` > votre adresse e-mail Murena Workspace > `Synchronisation du compte`. Si `Photos et vidéos` est désactivé, cela signifie qu'eDrive ne fonctionne pas et que vous pouvez ignorer cette partie.

Si vous utilisez **/e/OS en version inférieure à 1.13** ([comment vérifier ma version /e/OS ?](https://community.e.foundation/t/howto-give-complete-e-os-version-info-easily-for-support-answers-comparison-etc/45030)):
- Veuillez mettre à jour vers la dernière version de /e/OS (`Paramètres` > `Mises à jour`)
- OU si vous ne pouvez pas mettre à jour, copiez les données des dossiers suivants de l'appareil /e/OS vers un autre appareil :
    - DCIM
    - Documents
    - Movies
    - Music
    - Pictures
    - Podcasts
    - Recordings
    - Ringtones

Si vous utilisez **/e/OS 1.13 et versions ultérieures**, vous n'avez rien à faire ! eDrive ne supprime jamais de fichiers, donc pas besoin de s'inquiéter de la perte de données. La synchronisation des fichiers sera activée dès la réouverture avec cette version.

> **Note :** Par défaut, eDrive ne repoussera pas tous les fichiers locaux que vous avez sur votre smartphone. Une mise à jour à venir d'eDrive corrigera ce comportement et forcera la resynchronization sur Murena Workspace des fichiers que vous avez sur votre appareil.

### Application Nextcloud (Android, iOS, Linux, macOS, Windows)
Nextcloud supprimera les fichiers qui sont présents localement mais absents sur le serveur. Par défaut, les applications Nextcloud ne lanceront pas de synchronisation lorsque nous rouvrirons l'application Fichiers de Murena Workspace. Pour sauvegarder vos fichiers locaux, veuillez copier vos fichiers vers un autre emplacement en suivant l'une des procédures ci-dessous.

#### Windows, macOS et Linux
Pour sauvegarder :
1. **Ouvrez l'application Nextcloud** : Lancez l'application Nextcloud _sur chaque appareil où elle est installée_.
2. **Accédez au dossier local** : Cliquez sur "Ouvrir le dossier local" pour voir vos fichiers synchronisés.
3. **Sélectionnez les fichiers**
   - **macOS** Utilisez `Cmd (⌘) + A` pour sélectionner tous les fichiers, puis copiez-les en utilisant `Cmd (⌘) + C`
   - **Windows/Linux** Utilisez `Ctrl + A` pour sélectionner tous les fichiers, puis copiez-les en utilisant `Ctrl + C`
4. **Créez un dossier de sauvegarde** : Créez un nouveau dossier (par exemple, `Nextcloud-Backup`) dans un emplacement sûr.
5. **Collez les fichiers**
   - **macOS** Collez les fichiers copiés dans le dossier de sauvegarde en utilisant `Cmd (⌘) + V`
   - **Windows/Linux** Collez les fichiers copiés dans le dossier de sauvegarde en utilisant `Ctrl + V`

{% include alerts/warning.html content="Vous devez appliquer ce processus sur chaque appareil où vous avez installé un client Nextcloud." %}

#### Android
1. Ouvrez l'application `Fichiers` et cliquez sur le *bouton Nextcloud* dans le panneau latéral (si vous ne voyez pas cette icône, passez à l'étape 9).

   ![image](/images/murena-workspace-recovery/nextcloud-1.png){: height="400" }
2. Maintenez une pression longue sur un élément, cela le sélectionnera, cliquez sur le *menu à 3 points (⋮)* en haut à droite, puis appuyez sur *sélectionner tout*.

   ![image](/images/murena-workspace-recovery/nextcloud-2.png){: height="400" }
3. Cela sélectionnera tous les fichiers restants.

   ![image](/images/murena-workspace-recovery/nextcloud-3.png){: height="400" }
4. Cliquez à nouveau sur le *menu à 3 points (⋮)* en haut à droite et cliquez sur *copier vers...*.

   ![image](/images/murena-workspace-recovery/nextcloud-4.png){: height="400" }
5. Cela ouvrira une page pointant vers le dossier racine de l'appareil. Cliquez sur le *bouton ajouter un dossier* pour créer un nouveau dossier.

    ![image](/images/murena-workspace-recovery/nextcloud-5.png){: height="400" }
6. Cela ouvrira une invite de texte, demandant le nom du dossier, dans notre cas nous l'avons nommé `Nextcloud.bak`.

    ![image](/images/murena-workspace-recovery/nextcloud-6.png){: height="400" }
7. Ouvrez maintenant le dossier nouvellement créé et appuyez sur le *bouton copier* en bas de l'écran. Cela démarrera le processus de copie.

    ![image](/images/murena-workspace-recovery/nextcloud-7.png){: height="400" }
8. Vous verrez peut-être une notification si la taille de la copie est importante.

    ![image](/images/murena-workspace-recovery/nextcloud-8.png){: height="400" }

    {% include alerts/success.html content="Votre processus de sauvegarde est terminé. Dans tous les cas, si vous avez échoué à l'étape 1, veuillez suivre les instructions ci-dessous :" %}
9. Ouvrez l'application `Nextcloud`, sélectionnez `Paramètres` dans le panneau de gauche.

    ![image](/images/murena-workspace-recovery/nextcloud-9.png){: height="400" }
10. Cliquez sur l'option `Dossier de stockage des données`, cela ouvrira une fenêtre contextuelle. Notez l'élément sélectionné dans la fenêtre contextuelle.

    ![image](/images/murena-workspace-recovery/nextcloud-10.png){: height="400" }
11. Ouvrez l'application `Fichiers` et accédez au chemin précédemment noté (par défaut, ce devrait être : `Android/media/com.nextcloud.client`). Vous y trouverez un dossier nommé `nextcloud`. Maintenez une pression longue pour le sélectionner.

    ![image](/images/murena-workspace-recovery/nextcloud-11.png){: height="400" }
12. Suivez maintenant les étapes 4 à 8 pour terminer le processus de sauvegarde.

#### iOS

1. **Ouvrez l'application Nextcloud** sur votre iPhone ou iPad.
2. **Naviguez vers les fichiers/dossiers** que vous souhaitez sauvegarder.
3. **Appuyez sur le menu à trois points (⋮)** à côté du nom du fichier ou du dossier.

    ![image](/images/murena-workspace-recovery/Nextcloud-Backup-iOS1.jpeg){: height="400"}
4. **Sélectionnez "Définir comme disponible hors ligne"** pour télécharger les fichiers pour un accès hors ligne.

    ![image](/images/murena-workspace-recovery/Nextcloud-Backup-iOS2.jpeg){: height="400"}
5. **Ouvrez l'application Fichiers** sur votre iPhone.

    ![image](/images/murena-workspace-recovery/Nextcloud-Backup-iOS3.jpeg){: height="400"}
6. **Allez dans "Sur mon iPhone"** ou **iCloud Drive** sous **Emplacements**.

    ![image](/images/murena-workspace-recovery/Nextcloud-Backup-iOS4.jpeg){: height="400"}
7. **Appuyez sur le menu à trois points (⋮)** en haut à droite et sélectionnez **"Nouveau dossier"**.
8. **Nommez le dossier** (par exemple, `Nextcloud-Backup`).

    ![image](/images/murena-workspace-recovery/Nextcloud-Backup-iOS5.jpeg){: height="400"}
9. **Ouvrez l'application Fichiers** sur votre iPhone.
10. **Allez dans "Nextcloud"** sous **Emplacements**.
11. **Appuyez sur le menu à trois points (⋮)** en haut à droite et sélectionnez **"Sélectionner"**.
12. **Sélectionnez tous les fichiers** que vous souhaitez sauvegarder.

    ![image](/images/murena-workspace-recovery/Nextcloud-Backup-iOS6.jpeg){: height="400"}
13. **Appuyez sur le menu à trois points (⋮)** en bas et choisissez **"Copier"**.

    ![image](/images/murena-workspace-recovery/Nextcloud-Backup-iOS7.jpeg){: height="400"}
14. **Naviguez vers le dossier "Nextcloud-Backup"**.
15. **Maintenez une pression longue à l'intérieur du dossier**, puis appuyez sur **"Coller"** pour copier les fichiers.

    ![image](/images/murena-workspace-recovery/Nextcloud-Backup-iOS8.jpeg){: height="400"}

### EasySync
EasySync supprime les fichiers qui sont présents localement mais pas sur le serveur. Par défaut, l'application EasySync ne synchronisera pas lorsque nous réactiverons l'application Fichiers Murena Workspace. Veuillez copier vos fichiers vers un autre emplacement.

1. Ouvrez l'application `Fichiers` et maintenez une pression longue sur les éléments que vous souhaitez sauvegarder.

   ![image](/images/murena-workspace-recovery/easysync-1.png){: height="400" }
2. Cliquez maintenant sur le *menu à 3 points (⋮)* en haut à droite et cliquez sur *copier vers...*.

   ![image](/images/murena-workspace-recovery/easysync-2.png){: height="400" }
3. Cela ouvrira une page pointant vers le dossier racine de l'appareil. Cliquez sur le *bouton ajouter un dossier* pour créer un nouveau dossier. Cela ouvrira une invite de texte, demandant le nom du dossier, dans notre cas nous l'avons nommé `easySync.bak`.

   ![image](/images/murena-workspace-recovery/easysync-3.png){: height="400" }
4. Ouvrez maintenant le dossier nouvellement créé et appuyez sur le *bouton copier* en bas de l'écran. Cela démarrera le processus de copie.

   ![image](/images/murena-workspace-recovery/easysync-4.png){: height="400" }

### /e/OS Notes
L'application Notes /e/OS supprimera les notes qui existaient sur l'appareil mais pas sur le serveur. L'application Notes Murena Workspace est actuellement désactivée pour éviter ce comportement. Pour sauvegarder vos notes :

1. **À court terme : Utilisez le bouton Partager**
   - Ouvrez les notes une par une
   - Utilisez le bouton de partage pour exporter chaque note

2. **À venir : Utilisez la dernière application Notes /e/OS**
   Nous fournirons un APK pour vous aider à extraire toutes vos notes. Restez à l'écoute pour connaître la publication des mises à jour qui seront notamment diffusées via App Lounge. Afin de pouvoir recevoir la dernière version de /e/OS Notes quand elle sera disponible :
   - Mettez votre appareil à jour vers la dernière version de /e/OS
   - OU, a minima, installez la dernière version d'App Lounge en suivant [cette documentation](https://doc.e.foundation/support-topics/app_lounge.html#download-app-lounge-apks)

### Nextcloud Notes
- Ouvrez les notes une par une
- Utilisez le bouton de partage pour exporter chaque note

### Carnet
1. Créez la sauvegarde
   - Utilisez le menu à trois lignes (☰) en haut à gauche
   - Sélectionnez le bouton *Paramètres*
       
       ![image](/images/murena-workspace-recovery/carnet-Backup1.jpeg){: height="400" }
   - Utilisez le bouton *Exporter*
       
       ![image](/images/murena-workspace-recovery/carnet-Backup2.jpeg){: height="400" }
   - Allez dans le dossier que vous souhaitez utiliser pour la sauvegarde, et appuyez sur le bouton *Enregistrer* en bas à droite
       
       ![image](/images/murena-workspace-recovery/carnet-Backup3.png){: height="400" }

2. Vérifiez la sauvegarde
   - Ouvrez l'application Fichiers
   - Accédez au dossier précédemment sélectionné
   - Ouvrez `carnet_archive.zip`
   - Vous devriez pouvoir voir des fichiers `.sqd` correspondant à vos notes ainsi que des dossiers si vous en aviez

## Section FAQ
- **Q : Pourquoi la synchronisation est-elle désactivée ?**
  - R : La synchronisation est désactivée pour éviter la perte de données pendant le processus de récupération.

- **Q : Quand pourrai-je réactiver la synchronisation ?**
  - R : Nous vous informerons lorsqu'il sera sûr de réactiver la synchronisation.
