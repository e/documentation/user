Que faire lorsque vous rencontrez un problème dans /e/OS ?

## Connectez-vous à notre serveur GitLab

   * Si vous avez un compte, [connectez-vous](https://gitlab.e.foundation/users/sign_in)
   * Sinon, [créez-en un](https://gitlab.e.foundation/e)

## Allez sur notre tableau de bord des tickets

   - Les tickets concernant /e/OS sont listés et créés [ici](https://gitlab.e.foundation/e/backlog/-/issues)

   -Avant de créer un nouveau ticket, veuillez vérifier s'il n'y en a pas déjà un similaire. Pour cela

   - Tapez des mots clefs dans la zone de recherche en haut de l'écran comme indiqué ci-dessous

   ![Nouvelle recherche](/images/new_issue_search.png)

   - L'écran affichera également d'autres tickets similaires

   - Si cette recherche ne renvoie aucun résultat, créez un nouveau ticket

## Créer un nouveau ticket

   - Sélectionnez un modèle pour le ticket dans le menu déroulant indiquant ‘Choisissez un modèle’
   - 'Appliquer le modèle'
   - Ajoutez un titre
   - Remplissez tous les détails demandés dans le modèle
   - [Ajoutez toujours des journaux/logs](/support-topics/create-a-log)  si possible. Cela aide les développeurs à mieux déboguer le problème. 
   - Une fois terminé, cliquez sur `Créer ticket`.



## Vous rencontrez un message d'erreur lors de la création d'un nouvel ID sur le Gitlab /e/OS ?

   - Certains utilisateurs peuvent rencontrer un message d'erreur en tentant de créer un nouvel ID sur le Gitlab de /e/OS
   - Généralement le message d'erreur vous demande de contacter Murena sur  <helpdesk@murena.com>. 
   - Si c'est bien ce message qui s'affiche, veuillez envoyer un e-mail avec les détails suivants
      - Nom complet :
      - Nom d'utilisateur souhaité :
      - Adresse e-mail :
   - <a href="mailto:helpdesk@murena.com?subject=Open%20GitLab%20account&body=Hi%2CI%20want%20to%20create%20a%20Gitlab%20acccount%20to%20report%20issue.%0A%0AFull%20Name%3A%20%0ADesired%20username%3A%20%0AEmail%3A%20">Créer un ticket Helpdesk au sujet de la création de compte GitLab </a>



   - Laissez-nous vous expliquer la raison de ce message d'erreur. Il y a quelque temps, nous avons fait face à un problème de création massive d'ID fictifs qui a surchargé nos serveurs. À titre de mesure préventive, nous n'autorisons désormais les inscriptions qu'à partir d'un nombre limité de domaines de messagerie. Les utilisateurs souhaitant s'inscrire avec leur domaine e-mail privé ou, par exemple, leur adresse Gmail peuvent envoyer une demande à <helpdesk@murena.com> et l'équipe leur créera un compte. 



## Syntaxe et Source

   * [Source](https://docs.gitlab.com/ee/user/project/description_templates.html#setting-a-default-template-for-issues-and-merge-requests)
   * [Markdown (GitLab)](https://docs.gitlab.com/ee/user/markdown.html)

