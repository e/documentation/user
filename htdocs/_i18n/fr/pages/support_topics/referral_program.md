# Programme de parrainage Murena Workspace

Obtenez jusqu'à 40€ de crédits pour votre stockage cloud en invitant vos amis sur Murena Workspace !

Pour chaque ami·e qui rejoint la plateforme et crée un compte Murena Workspace, vous gagnerez 2€ que vous pourrez utiliser pour
acheter du stockage Murena Workspace.

Murena Workspace c'est votre compte de messagerie personnel, votre agenda et vos contacts, votre disque sur le cloud et
votre suite bureautique en ligne, le tout réuni en un seul service, simple d'utilisation. Murena Workspace est basé sur
des logiciels Open Source éprouvés, comme NextCloud et OnlyOffice.

Votre compte Murena Workspace est gratuit jusqu'à 1 Go de stockage. Nos forfaits payants commencent à 1,99€ par mois pour
20 Go.

## Comment ça marche ?

Une fois que vous avez un compte murena.io/e.email valide, c'est très facile.

Il vous suffit de vous inscrire sur [https://murena.com/my-account/](https://murena.com/my-account/), notre plateforme de e-commerce, afin que nous puissions attribuer vos bons d'achat cloud.

Dans votre espace client murena.com, repérez la rubrique « Parrainage » et copiez votre
lien de parrainage unique et anonyme.

Vous pouvez facilement copier votre lien unique et l'utiliser ailleurs, par exemple en l'envoyant par e-mail à vos
ami·e·s ou en le partageant dans un tweet.

Pour chaque ami qui utilise ce lien pour créer un compte gratuit, vous recevrez 2€ de bon d'achat cloud
dans votre compte murena.com.

## Comment inviter mes ami·e·s ?

C'est très simple, il vous suffit d'envoyer le lien d'inscription unique présent dans votre compte murena.com.
Ce lien est totalement anonyme, il ne collecte aucune information d'identification telles que
le nom ou l'adresse e-mail.

## Comment utiliser les bons d'achat ?

Vos bons d'achat peuvent être utilisés lorsque vous souscrivez à un plan de stockage étendu. Ils seront
convertis en remise sur votre abonnement payant.

## Combien de temps mes bons d'achat sont-ils valables ?

Vos bons d'achat sont valables 24 mois.

## Combien de temps va durer ce programme de parrainage ?

Le programme de parrainage Murena Workspace est un programme permanent.

## Puis-je faire valoir mes bons d'achat pour l'achat d'un téléphone ou d'un autre équipement sur murena.com ?

Vos bons d'achat ne sont valables que pour la souscription d'un forfait Murena Workspace payant.

## Comment ça marche pour mes ami·e·s ?

En cliquant sur le lien de parrainage, votre ami·e sera redirigé·e vers la page de création de compte où
elle/il pourra créer un identifiant et un mot de passe.

Une fois le processus de création de compte terminé, votre ami·e disposera d'un lien vers un compte Murena Workspace où elle/il pourra accéder au compte murena.io nouvellement créé, au stockage cloud ou utiliser la suite OnlyOffice.

## Comment créer un compte Murena Workspace ?

C'est très simple. Rendez-vous sur [https://murena.io/signup](https://murena.io/signup) pour demander votre invitation. Vous ne serez inclus·e dans le programme de parrainage qu'une fois que vous vous serez également inscrit·e sur [https://murena.com/my-account/](https://murena.com/my-account/) et que vous utiliserez vos liens de parrainage.

## C'est quoi Murena Workspace ?

Murena Workspace est un écosystème en ligne complet et entièrement « déGooglisé ».

Murena Workspace c'est votre compte mail personnel avec les domaines murena.io/e.email, votre agenda et vos contacts, votre disque sur le cloud et votre suite bureautique en ligne, le tout réuni en un seul service, facile à utiliser. Murena Workspace est basé sur des logiciels Open Source éprouvés comme NextCloud et OnlyOffice.

## Le lien de parrainage est-il anonyme ?

Le lien est totalement anonyme, il ne collecte aucune information d'identification telles que le nom ou
l'adresse e-mail. La personne qui envoie le lien n'a aucune visibilité sur les personnes qui ont utilisé le 
lien de parrainage.

## Je ne vois pas mes bons d'achat dans mon espace murena.com, que dois-je faire ?

Contactez-nous sur [helpdesk@murena.com](mailto:helpdesk@murena.com) pour que nous puissions investiguer et voir comment résoudre ce souci
pour vous.
