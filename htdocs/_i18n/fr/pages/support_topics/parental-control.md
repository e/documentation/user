Contrôle parental sur /e/OS permet aux parents de protéger les enfants contre les contenus inappropriés. Cela se fait en restreignant l'accès à certaines applications mobiles et contenus web, en fonction de l'âge de l'enfant.

## Importance du contrôle parental

* Internet offre un accès à l'information, à la culture et aux relations sociales mais le prix à payer est énorme quand les enfants sont exposés à des activités et contenus qui affectent leur santé et leur développement.

* Temps d'écran prolongé : L'exposition précoce des enfants à des écrans peut impacter leur développement. La surexposition ou une gestion inappropriée peuvent entraîner certains risques et des effets sur la santé de l'enfant, notamment des problèmes de sommeil, de vue et de poids. Il est important que les parents contrôlent et limitent le temps d'écran des enfants.

* Contenu inapproprié: Les enfants risquent d'être exposés à du contenu inapproprié en ligne. Et cela particulièrement via des sites et des applications non adaptés aux familles qui affichent du contenu pornographique, des applications de réseaux sociaux et des plateformes de partage qui facilitent la propagation de contenu explicite. Les parents devraient bloquer l'accès aux applications et sites web inappropriés aux enfants pour limiter leur exposition à ces contenus.

* Harcèlement en ligne: Le harcèlement en ligne représente de sérieux risques pour les enfants, y compris de détresse émotionnelle, d'isolation sociale et de baisse des résultats scolaires. Pour amoindrir ces risques, les parents devraient éduquer leurs enfants sur la sécurité en ligne, surveiller leurs activités, encourager le signalement des incidents et favoriser une communication ouverte.

* Les parents peuvent jouer un rôle crucial en configurant un contrôle parental sur le téléphone de leur enfant pour les guider dans une expérience numérique sûre et responsable. Il est également important que les parents communiquent ouvertement avec leurs enfants sur la manière de naviguer en toute sécurité dans l'espace numérique.

## Activation du Contrôle Parental

* Le Contrôle Parental peut être activé avec l'`Assistant de configuration initiale du téléphone` ou depuis `Paramètres > Contrôle parental` plus tard.  
![alt text](/images/parental-control/fr/parental-control-activate-1.png)   

* Appuyez sur le bouton `Activer le contrôle parental`   
![alt text](/images/parental-control/fr/parental-control-activate-2.png)   

* Sélectionnez la `Tranche d'âge de l'enfant` et appuyez sur `Suivant`.   
![alt text](/images/parental-control/fr/parental-control-activate-3.png)   

* Configurez un `code PIN` ou un `Mot de passe` pour gérer les paramètres du Contrôle Parental. Ce PIN/Mot de passe sera nécessaire pour modifier ou désactiver les paramètres du Contrôle Parental. Votre enfant ne doit pas connaître ce PIN/Mot de passe.  
![alt text](/images/parental-control/fr/parental-control-activate-4.png)   

## Conditions pour activer le Contrôle Parental

* Les restrictions parentales, comme le *DNS familial sécurisé*, peuvent être contournées si le Contrôle Parental est configuré dans un environnement multi-utilisateur. Nous recommandons de supprimer les utilisateurs supplémentaires lorsque cela est demandé.   
![alt text](/images/parental-control/fr/multiple-users-warning.png)

* Le Contrôle Parental ne peut pas être configuré si une autre application administrateur (comme Shelter) est utilisée. Veuillez désactiver/désinstaller l'application pour continuer.   
![alt text](/images/parental-control/fr/other-admin-app-disable.png)

## Restrictions appliquées lorsque le Contrôle Parental est activé

* Les requêtes DNS sont interceptées sur l'appareil pour refuser l'accès aux sites web pour adultes et bloquer les pisteurs.   

* Le DNS privé est réglé sur le DNS familial de Cloudflare et ne peut pas être modifié. 
{% include alerts/tip.html content="Cloudflare Family DNS aide à protéger les enfants sur Internet en :<br>
    - Bloquant les contenus pour adulte<br>
    - Filtrant les sites malveillants et de phishing<br>
    - Empêchant l'accès au contenu violent ou nuisible<br>
    - Limitant l'accès au contenu inapproprié pour l'âge"%}   
![alt text](/images/parental-control/fr/private-dns-cannot-be-changed1.png) ![alt text](/images/parental-control/fr/private-dns-cannot-be-changed2.png)

* Les applications ne peuvent être installées que via [App Lounge](/support-topics/app_lounge.html), qui donne accès aux applications de Google Play et F-Droid.    
![alt text](/images/parental-control/fr/app-lounge-ss2.png) ![alt text](/images/parental-control/fr/app-lounge-ss1.png)   


* Les applications ne peuvent être installées que si la classification de contenu de l'application correspond à la tranche d'âge de l'enfant définie lors de la configuration du Contrôle Parental. Les classifications d'âge des applications sont publiées par Google Play ou F-Droid selon l'application.  
![alt text](/images/parental-control/fr/age-restricted-apps-cant-install-1.png) ![alt text](/images/parental-control/fr/age-restricted-apps-cant-install-2.png)

* Les applications nécessitant une supervision parentale ne peuvent pas être installées pour la tranche d'âge 0-3 ans. Nous prévoyons de rendre cela personnalisable dans les prochaines mises à jour. 
![alt text](/images/parental-control/fr/parental-consent-required1.png) ![alt text](/images/parental-control/fr/parental-consent-required2.png)   

* Les applications déjà installées dont la classification de contenu est supérieure à la tranche d'âge définie par les parents sont grisées et ne peuvent pas être lancées.  
![alt text](/images/parental-control/fr/greyed-out.png) ![alt text](/images/parental-control/fr/cant-open-app.png)   

* Après avoir configuré le Contrôle Parental avec un seul utilisateur, votre enfant ne pourra pas créer de nouveaux utilisateurs.   
![alt text](/images/parental-control/fr/multiple-user-disabled.png)   

* `Effacer les données`, `Effacer le cache` ou `Forcer l'arrêt` ne fonctionneront pour aucune application.    
![alt text](/images/parental-control/fr/force-stop-clear-cache-greyed-out.png)   

* L'appareil ne peut pas être réinitialisé aux paramètres d'usine.    
![alt text](/images/parental-control/fr/factory-reset-disabled1.png) ![alt text](/images/parental-control/fr/factory-reset-disabled2.png)   

## Désactivation ou modification du Contrôle Parental

* Pour désactiver le Contrôle Parental, modifier la tranche d'âge ou changer le code PIN/mot de passe, le parent doit saisir le code PIN/mot de passe défini lors de la configuration.   