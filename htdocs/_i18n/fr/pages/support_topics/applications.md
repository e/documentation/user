### Why not Open source all your pre-installed apps?

With the exception of Maps app (Magic Earth) all our default system Applications are open source. You can find the explanation behind choosing Magic Earth [here](/maps)

### Where can we see the code of the open source apps that you're forking? 


You can browse through the projects on our [gitlab](https://gitlab.e.foundation) and check out the source code.

For example to find the code for the Mail app you can [look here](https://gitlab.e.foundation/e/apps/mail)

For links to the source code location of each of our default system applications check the [Product Description Document here]({% tl what-s-e %}#e-default-applications)

### Why do you not contribute upstream?

We contribute upstream 
 - when it's possible
 - when the upstream project accepts it
 - when a contribution upstream makes sense. 

However we have observed that unless the contributions are for trivial bugfixes, in 99% of the cases the merges are rejected. The reason behind the rejection is that upstream project maintainers have their own ideas about their project, its features ,UI, colors etc and are not ready to accept our suggestions or code changes. 

This is the primary reason why forking is common in open source projects.


### Are there still any efforts ongoing to get Magic Earth to open source their application? 

Our plans to discuss with Magic Earth got postponed due to the pandemic. We hope to discuss with them and make some progress on this topic this year. However we are keeping our options open. 

We are looking out for other available Maps applications which do not require the user to constantly download newer versions of the maps. This would make the user experience fluid and take up less bandwidth. Most important of all the application should be Open Source in line with our ideals.
