## Configuration de l'e-mail

 - Vous pouvez utiliser votre compte e-mail @e.email ou @murena.io pour vous loguer ici : [https://murena.io/](https://murena.io/)  

De nombreux clients mails sont capables de configurer automatiquement votre compte e-mail – il vous suffit de fournir l'adresse e-mail et le mot de passe de connexion.

- For example:
Murena Mail, Mozilla Thunderbird 3.1 or newer, Evolution, KMail, Kontact and Outlook 2007 or newer.

- If you are using a current Apple Mail version (both on macOS or iOS) you can visit [https://autoconfig.murena.io/](https://autoconfig.murena.io/) and fill in your details to download a configuration package that automatically configures your client.


   - nom d'utilisateur SMTP/IMAP : votre adresse Murena en entier (exemple : martin.dupont@e.email or martin.dupont@murena.io)

   - mot de passe SMTP/IMAP : votre mot de passe Murena


   - port IMAP : 993, SSL/TLS, authentification requise

   - SMTP/IMAP username: your Murena address in full (example: john.doe@e.email or john.doe@murena.io)

   - SMTP/IMAP password: your Murena password

   - port SMTP : 587, mot de passe en clair, STARTTLS, authentification requise

   - IMAP port: 993, SSL/TLS, requires authentication
