### Advantages of an Murena Workspace account

* having a new brand email address @murena.io
* having a single identity to access all Murena services :

   - Murena Email : username@murena.io
   - Murena Drive : photos, videos, documents...
   - Murena Calendar
   - Murena Contacts
   - Murena Notes
   - Murena Tasks

* in your Murena smartphone and online at : [https://murena.io](https://murena.io)

### How to get your free Murena Workspace account ?

* Go to [https://e.foundation/e-mail-invite/](https://e.foundation/e-mail-invite/) you should see a screen like this

![](images/e-email.png)

* And follow the below steps :
	 - enter your current email address
	 - wait for an invitation in your mailbox ! Usually it takes < 2 minutes. Check your SPAM box !

### Get Started

* After you clicked the link in the invitation email you can proceed to the actual account creation :

![](images/create-e-account.png)

### Creating your own Murena Workspace account @murena.io

* Fill the fields correctly! This example will create an Murena identity as John Doe <john.doe@murena.io>
* Your username may contain Latin letters \(a-z\), digits \(0-9\) and special characters (\- . \_)
* Your username must be at least 3 characters long and at most 30 characters long
* Your username must not contain special characters in succession
* Your username must not start or end with special characters
* Please note that your username will be automatically converted to lowercase during account creation \(i.e. "John.Doe@e.email" becomes "john.doe@murena.io"\)

![](images/your-e-id.png)

### Using an Murena Workspace account @murena.io

* Now that your Murena Workspace account is created, you can use it in your Murena smartphone 
  - In the first time usage wizard just after Murena OS installation
  - You can add account by going to..
     ````
     Settings >> Accounts >> Add account >> /e/OS
     ````

* On the web at [https://murena.io](https://murena.io) to retrieve all the pictures, videos you took with your Murena smartphone
* To access on the [Murena](https://murena.io) all you emails and check your calendar
* 

### Having trouble creating an Murena Workspace account @murena.io?

* If you never received the invitation on your current email, check your email client SPAM folders
* If you cannot login in your Murena smartphone or at [https://murena.io](https://murena.io) try to reset your password with the reset link available at [https://murena.io](https://murena.io)
* Make sure that you use your username@murena.io as email for the reset process, NOT your recovery email.
* If you are still facing issues you can {% translate content.contact_helpdesk %}

### How can I delete my Murena Workspace account @murena.io?

* Log in to your [Murena Workspace account](https://murena.io/) using the ID you want to delete
* Take a backup of your data and emails (if required) before proceeding
* Click on your profile image
* This will open up a menu
* Click on Settings
* This will display the Setting options as shown in the screenshot below

![](images/Account_delete.png)

* Click on "Delete Account"
* This will open up a screen with the text "Check this to confirm the deletion request"
* Select the check box
* Click "Delete my Account" button
* The operation could take up to a minute. Please wait
  >  This action will remove your Murena ID and Murena Workspace account, data stored on the Murena if any and all emails.

* You will be logged out of [murena.io](https://murena.io/)
* The Murena Workspace account deletion is complete
> If you are still facing issues you can {% translate content.contact_helpdesk %}

### Some more tips on your Murena Workspace account @e.email or @murena.io

* Your Murena identity at murena.io is not (yet) compatible with our various websites and forums.

* This means that you will have to create specific login/password at [community.e.foundation](https://community.e.foundation/), [gitlab.e.foundation](https://gitlab.e.foundation/e), and [e.foundation (shop)](https://e.foundation/e-pre-installed-refurbished-smartphones/)
* Do not try to login in your Murena smartphone with your community or gitlab identity. It will not work.
* Your Murena identity is under this form :
  ***username@e.email or username@murena.io***

* This is your email address too. Yes. « .email » is a valid extension on internet, like .com, .org, .xyz and thousands others.

* And forget username@e.email.org we don't own the email.org domain.

### Want to manually configure your mail client for /e/OS?

Most mail clients should be able to configure your Murena ID automatically, once you input your Murena username and password. In case that is not possible, please use these settings to manually configure:

SMTP/IMAP server: mail.ecloud.global

SMTP/IMAP username: your Murena email address in full (example: john.doe@e.email or john.doe@murena.io)

SMTP/IMAP password: your Murena email password

SMTP port: 587, plain password, STARTTLS, requires authentication

IMAP port: 993, SSL/TLS, requires authentication


### Storage Options
* Your free account will be **limited to 1GB** online storage. 

* We now have some premium storage plans available. You can also **upgrade it to 20GB** while supporting the Murena project by becoming an [Murena Early Adopter](https://e.foundation/donate/).

**PLEASE KEEP IN MIND** that Murena Free drive and mail accounts are supported by donations! 

Please [support us now and receive a gift](https://e.foundation/support-us/).

### Is an Murena Workspace account mandatory to use /e/OS ?
* An Murena Workspace account at murena.io is not mandatory to use /e/OS, but is highly recommended if you want to benefit from the full Murena experience as described in this guide. 



