## Objectif du document

Expliquer ce qu'est Masquer mon adresse e-mail, et comment l'utiliser.

## Masquer mon adresse e-mail

Masquer mon adresse e-mail est une nouvelle adresse e-mail aléatoire (alias), qui ne peut être utilisée que pour recevoir des e-mails. Vous pouvez l'utiliser sur des sites Web pour éviter de divulguer votre véritable adresse électronique.

## Où là trouver

Cliquez sur l'icône de votre compte principal en haut à droite, puis sur paramètres.

![account settings](/images/howtos/ecloud/account_settings.png)

Cliquez sur "Masquer mon adresse e-mail" dans le menu latéral gauche. Vous verrez votre adresse. 
Vous pouvez également cliquer sur le bouton "Copier" pour copier cette adresse électronique, afin de la coller directement sur le site Web où vous souhaitez l'utiliser.

![copy hide my email](/images/howtos/ecloud/copy_hide_my_email.png)

## Comment l'utiliser de manière optimale sans affecter votre boîte de réception

Consultez [ce document](/support-topics/email-filters#how-to-use-with-hide-my-email) qui montre comment définir un filtre qui collectera les e-mails envoyés à votre adresse Masquer mon adresse e-mail dans un autre dossier.


