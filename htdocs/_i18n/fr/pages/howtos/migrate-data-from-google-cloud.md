### Connectez votre compte Google
1. Connectez-vous à votre compte [Murena](https://murena.io) à l'aide de vos identifiants
1. Accédez à "Paramètres", puis à l'onglet "Migration de Données"
![Écran montrant comment accéder aux Paramètres](/images/howtos/ecloud/navigate-to-settings.jpg)
![Écran montrant comment accéder aux paramètres de Migration de Données](/images/howtos/ecloud/navigate-to-data-migration-settings.jpg)
1. Utilisez le bouton "Connexion avec Google" pour authentifier votre compte Google sur ecloud
![Écran montrant comment se connecter avec Google](/images/howtos/ecloud/click-on-sign-in-with-google.jpg)
1. Vous serez ensuite invité·e par Google à accorder des autorisations d'accès à des services tels que Calendrier, Drive, Photos et Contacts. Vous pouvez choisir de ne donner que des autorisations spécifiques pour déverrouiller des parties spécifiques de l'application
![Écran montrant comment donner son consentement à Murena Workspace](/images/howtos/ecloud/give-consent-google.jpg)
1. Après vous être connecté·e et avoir donné les autorisations, vous pourrez importer vos données


### Importer les Contacts
1. Cliquez sur le bouton "Importer les contacts Google" pour importer vos contacts dans Murena Workspace
![Écran montrant le bouton "Importer les contacts Google"](/images/howtos/ecloud/import-contacts.jpg)
1. Vous recevrez une notification dès que vos contacts sont importés
![Notification après l'importation des contacts](/images/howtos/ecloud/contacts-import-successful.jpg)
1. Vous pouvez maintenant accéder à vos contacts importés via l'application de contacts Murena Workspace, et synchroniser avec votre appareil /e/OS

### Importer des Calendriers
1. Veuillez choisir le calendrier que vous souhaitez importer parmi les calendriers répertoriés de votre compte Google (ici, nous avons choisi "my-tasks-calendar")
1. Cliquez sur le bouton "Importer le calendrier" pour importer le calendrier choisi
![Écran affichant le bouton Importer le calendrier](/images/howtos/ecloud/import-calendar.jpg)
1. Une fois votre calendrier importé, vous verrez une notification
![Notification après l'importation du calendrier](/images/howtos/ecloud/calendar-import-successful.jpg)
1. Vous pouvez maintenant accéder à votre calendrier importé via l'application de calendrier Murena Workspace ou synchroniser avec votre appareil /e/OS

### Importer des Photos
1. Pour importer vos photos Google dans Murena Workspace, ajoutez votre répertoire d'importation préféré
1. Une fois votre répertoire d'importation défini, cliquez sur le bouton "Importer les photos Google"
![Écran affichant le bouton "Importer les photos Google"](/images/howtos/ecloud/import-photos.jpg)
1. Comme vos photos sont importées en arrière-plan, vous pouvez maintenant quitter cette page et revenir de temps en temps pour vérifier la progression de l'import
1. Une fois l'importation réussie, vous recevrez une notification
![Notification après importation réussie des photos](/images/howtos/ecloud/photos-import-success-notification.jpg)
1. Vous pouvez désormais accéder à vos photos à l'aide de l'application Photos de Murena Workspace

### Importer des fichiers du Drive
1. Choisissez votre format préférentiel pour l'importation des documents 
1. Définissez votre répertoire d'importation préféré
1. Cliquez sur le bouton "Importer les fichiers de Google Drive"
![Écran affichant le bouton "Importer les fichiers de Google Drive"](/images/howtos/ecloud/import-drive.jpg)
1. Comme les fichiers de votre Drive sont importés en arrière-plan, vous pouvez maintenant quitter cette page et revenir de temps en temps pour vérifier la progression de l'importation.
1. Une fois l'importation réussie, vous recevrez une notification
![Notification après une importation de fichiers Drive réussie](/images/howtos/ecloud/drive-import-success-notification.jpg)
1. Vous pouvez maintenant accéder à vos fichiers depuis l'application Files de Murena Workspace