
### Manually upgrading your {{ device.codename }} to {{ device.upgrade_available }}


1. Download the /e/OS installation package as linked in the `Downloads for {{ device.codename }} ` section in this guide.

1. Make sure your computer has working `adb`. Setup instructions are linked in the `Basic Requirements` section above

1. Open a console window on your computer. The next set of commands will be run on the console.
{{ adbRoot }}
{%- if device.format_on_upgrade %}
1. Reboot into recovery by running `adb reboot recovery`, or by performing the following:
    * {{ device.recovery_boot }}
1. Wipe your data partition (this is usually named `Wipe`, or `Format`)
    {% include alerts/warning.html content="Without this step your device will not boot on the new version!" %}
{%- if device.uses_twrp != true %}
1. Click `Advanced`, then `Enable ADB`.
{%- endif %}
{%- endif %}
1. Run `adb reboot sideload`.
    {% include alerts/warning.html content="The device may reboot to a blank black screen, this is a known bug on some recoveries, proceed with the instructions." %}
1. Run `adb sideload /path/to/zip` (inserting the path to your /e/OS package).
  

{% if device.uses_twrp and device.is_ab_device != true %}
1. Once you have installed everything successfully, run `adb reboot`.
{% else %}
1. Once you have installed everything successfully, click the back arrow in the top left of the screen, then `Reboot system now`.
{% endif %}
