## À propos du Projet /e/OS

> Découvrez le projet /e/OS

- [C'est quoi /e/OS ? Lisez la description du produit !]({% translate_link what-s-e %})
- [Installez l'OS mobile /e/OS sur un smartphone pris en charge !]({% translate_link devices %})
- [Créez votre compte gratuit sur l'espace de travail en ligne Murena.io ]({% translate_link create-an-murena-account %})
- [HOWTOs]({% translate_link support-topics %})
- [FAQ (Questions Fréquement posées) à propos de /e/OS]({% translate_link support-topics %})
- [Partagez votre expérience /e/OS !]({% translate_link testimonies %})

## En savoir plus

> Information about the /e/OS project or its applications

- [/e/OS easy-installer]({% translate_link easy-installer %})
- [Maps application]({% translate_link maps %})
- [The different build types]({% translate_link build-status %})
- [App Lounge ]({% translate_link app-lounge-main %})
- [Advanced Privacy]({% translate_link support-topics/advanced_privacy_main %})
- [e.foundation Legal Notice & Privacy](https://e.foundation/legal-notice-privacy/)

## Contribuez à /e/OS !

> Voulez-vous aider le projet ?

- [Projects looking for contributors]({% translate_link projects-looking-for-contributors %})
- [Postes à pourvoir à Murena](https://murena.com/jobs/)
- [ROM Maintainer]({% translate_link rom-maintainer %})
- [Translators]({% translate_link translators %})
- [Testers]({% translate_link testers %})
- [Volunteers]({% translate_link volunteers %})

## Raccourcis

> Quelques liens utiles vers les sites web /e/OS, les forums et les canaux de discussion Telegram

- [/e/OS web site](https://e.foundation/)
- [/e/OS Source Code](https://gitlab.e.foundation/e)
- [Need Help? Support?](https://e.foundation/contact/)
- [Report a bug in /e/OS or apps](https://gitlab.e.foundation/e/backlog/-/issues/new)
- [Add your Smartphone to the supported list](https://community.e.foundation/c/e-devices/request-a-device)
