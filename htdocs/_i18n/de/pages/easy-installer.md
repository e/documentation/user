## What is the /e/OS easy-installer?

{% include alerts/warning.html content=" Ein neues Installationsprogramm, [der WebInstaller](https://e.foundation/installer/) ist nun als Alpha-Version verfügbar. Dieser Webinstaller funktioniert in Browsern, die auf Chrome basieren, weil er WebUSB benötigt.
Der EasyInstaller wird bald nicht mehr unterstützt. " %}






- The Easy Installer is a desktop application which **helps users install /e/OS** on supported devices.

- The latest version of the tool works on **Linux** , **Windows** and **macOS**

![](/images/easy-installer-mainscreen.png)

## How can I install the /e/OS easy-installer?

The easy-installer beta is available for Linux, Windows and macOS.

[Installation guide for Linux](easy-installer-linux)

[Installation guide for Windows](easy-installer-windows)

[Installation guide for macOS](easy-installer-macos)

## List of devices supported by the easy-installer

- The easy-installer beta version supports **21 devices**
- Note that you can install /e/OS on 200+ devices using the command-line interface (CLI) install method. For details on the devices and the install guides check out [this table](https://doc.e.foundation/devices)
- You can check which version of the /e/OS build the EasyInstaller flashes on your device from the details in the `Additional Info` column

{% include alerts/tip.html content="To get the Easy Installer working on your PC you would need to install the OS specific fastboot drivers first. Follow the instructions in the Install Guides gives against each device." %}


| **Vendor** | **Device Name**                     | **CodeName** | **Additional Info**   |**Driver Installation**                                          | **Model (s)**         |
| ---------- | ----------------------------------- | ------------ | --------------------- | --------------------------------------------------------------- | --------------------- |
| Gigaset    | [GS290](devices/GS290)              | gs290        |  Official build       |[Install Guide](pages/install_easy_installer/win_gs290)          | gs290                 |
| Fairphone  | [Fairphone 3](devices/FP3)          | FP3          |  Official build       |[Install Guide](pages/install_easy_installer/win_fp3)            | FP3                   |
| Fairphone  | [Fairphone 4](devices/FP4)          | FP4          |  Official build       |[Install Guide](pages/install_easy_installer/win_fp4)            | FP4                   |
| Samsung    | [Galaxy S9](devices/starlte)        | starlte      | Exynos only, Official build     |[Install Guide](pages/install_easy_installer/win_samsung_phones) | SM-G960F, SM-G960F/DS |
| Samsung    | [Galaxy S9 Plus](devices/star2lte)  | star2lte     | Exynos only, Official build    |[Install Guide](pages/install_easy_installer/win_samsung_phones) | SM-G965F, SM-G965F/DS |
| Samsung    | [Galaxy S8](devices/dreamlte)       | dreamlte     | Exynos only, Official build    |[Install Guide](pages/install_easy_installer/win_samsung_phones) | SM-G950F              |
| Samsung    | [Galaxy S8 Plus](devices/dream2lte) | dream2lte    | Exynos only, Official build           |[Install Guide](pages/install_easy_installer/win_samsung_phones) | SM-G955F              |
| Samsung    | [Galaxy S7](devices/herolte)        | herolte      | Exynos only, Official build    |[Install Guide](pages/install_easy_installer/win_samsung_phones) | SM-G930F              |
| Samsung    | [Galaxy S7 Edge](devices/hero2lte)  | hero2lte     | Exynos only, Official build    |[Install Guide](pages/install_easy_installer/win_samsung_phones) | SM-G935F              |
| Google    | [Pixel 4](devices/flame)             | flame        | Community build  | | flame              |
| Google    | [Pixel 4XL](devices/coral)           | coral        | Community build           | | coral              |
| Google    | [Pixel 4a](devices/sunfish)          | sunfish      | Community build           | | sunfish              |
| Google    | [Pixel 4a 5G](devices/bramble)       | bramble      | Community build           | | bramble              |
| Google    | [Pixel 5](devices/redfin)            | redfin       | Official build           | | redfin              |
| OnePlus    | [Nord](devices/avicii)              | avicii       | Official build           | | avicii              |
| OnePlus    | [7T](devices/hotdogb)               | hotdogb      | Community build           | | hotdogb              |
| OnePlus    | [7 pro](devices/guacamole)          | guacamole    | Community build           | | guacamole              |
| OnePlus    | [8](devices/instantnoodle)          | instantnoodle| Community build           | | instantnoodle              |
| OnePlus    | [8 pro](devices/instantnoodlep)     | instantnoodlep| Community build            | | instantnoodlep              |
| Teracube   | [2e 2020](devices/2e)               | 2e zirconia           | Official build           | | 2e zirconia              |
| Teracube   | [2e 2021](devices/emerald)          | emerald     | Official build           | | emerald              |

>For more details you can refer a [user updated post](https://community.e.foundation/t/list-devices-working-with-the-easy-installer/28396) on the [forum](https://community.e.foundation) where the Easy Installer was tested by /e/OS users against these models



## Why are more devices and OS not supported on the easy-installer?

- Support new devices
   - with its architecture based configuration files, it is easy to add new devices to the Easy Installer

- Support new devices
   - with its architecture based configuration files, it is easy to add new devices to the Easy Installer
  
- Add support for more Operating systems

## I want to develop the source further. How can I help?


## I want to develop the source further. How can I help?

**Thank you in advance!**


**Thank you in advance!**

## Still have some queries on the easy-installer ?
Check out the [easy-installer FAQ](easy-installer-faq)

**Thank you in advance!**
