{% include alerts/warning.html content=" Ein neues Installationsprogramm, [der WebInstaller](https://e.foundation/installer/) ist nun als Alpha-Version verfügbar. Dieser Webinstaller funktioniert in Browsern, die auf Chrome basieren, weil er WebUSB benötigt.
Der EasyInstaller wird bald nicht mehr unterstützt. " %}


## /e/OS Easy-Installer-Build-Dokumentation


### Tools required to build
> The team member who developed the easy-installer used

- Netbeans 11.3 (/!\ netbeans 11.0 doesn't work properly with javaFX)
- Gradle 4.10
- OpenJFX11+: [doc](https://openjfx.io/openjfx-docs/#IDE-NetBeans)
- Java JDK 11+


### Das Projekt klonen

Führen Sie diesen Befehl im Terminal aus

```shell
git clone https://gitlab.e.foundation/e/tools/easy-installer.git
```

### Wie man Builds erstellt

Öffnen Sie ein Terminal an der Wurzel des Projektes.
Prompt: `./gradlew dist`

Hinweis: „dist“ steht für „Distribution“

#### Ergebnis
Es wird 3 gezippte Artefakte erstellen, die in ein gemeinsames Archiv gepackt sind:
- linux-x64
- windows-x64
- mac

Ort der Artefakte :

Archiviertes Build : "/build/distributions/"

Entzippter Build : "/build/image/"

#### Was macht dieser Befehl?

Die kompilierte Java-Version wird im Ordner „buildSrc“ bereitgestellt. Es gibt eine Version für jede Plattform (Windows, OSX, Linux). Dies ermöglicht eine installationsfreie Anwendung. Dank Java 9+ enthält sie nur die erforderlichen Module.

99% erledigt "badass jlink plugin" (siehe obere Sektion).
Die verbleibenden 1% werden erledigt von:
- ADB kopieren
- heimdall kopieren
- fastboot kopieren
- Flash-Skripte

von den "buildSrc"- und "flash-scripts"-Ordnern in das Build.


#### Was wird benutzt, um das Build zu erstellen ?
Es benutzt das ["badass jlink plugin"](https://badass-jlink-plugin.beryx.org/releases/latest/#_properties)

{% include alerts/warning.html content=" Ein neues Installationsprogramm, [der WebInstaller](https://e.foundation/installer/) ist nun als Alpha-Version verfügbar. Dieser Webinstaller funktioniert in Browsern, die auf Chrome basieren, weil er WebUSB benötigt.
Der EasyInstaller wird bald nicht mehr unterstützt. " %}
## Feature idea
- **Add support for a new device**
    - You need to own the device
    - First step is to create configuration files for the Easy Installer
    - If necessary, create the missing scripts to be able to achieve the installation

1. Ich möchte helfen **/e/ auf Windows zu portieren**
    - Sie können uns helfen, Skripte für Windows zu übersetzen.
    - wenn Sie etwas Wissen mitbringen, um Windows-Anwendungen zu paketieren (wie wir mit Snap auf Linux gemacht haben)
    - Sobald die 2 vorherigen Punkte erledigt sind, können Sie testen, ob alles unter Windows genauso läuft wie unter Linux

## How to support or add a new device ?

The easy-installer uses two configuration files for each device.

_note: We are using herolte (Samsung galaxy S7) as an example here_

The config files are located in "src/main/resources/yaml/" folder.

```
src
--main
----java
----resources
------css
------fonts
------fxml
------images
------instructions
------lang
------yaml
--------**herolte.yml**
--------**herolte_fs.yml**
```

#### 1. First file: <device_code_name>.yml

The first file "herolte.yml" defines the flashing process.
The file is a [YAML](https://yaml.org/) resource.

#### Structures and nodes of the file

``` yaml
name: herolte
flash:
    f1:
        script: wait-download
        parameters:
            heimdall_folder_path: ${HEIMDALL_FOLDER_PATH}
        codes:
            ok:
                0: ~
            ko:
                1: script_error_waitDownload_1
        output: ~
        succeed: f2
        failed: ~  
    f2:
        script: oem-unlock
        parameters:
            heimdall_folder_path: ${HEIMDALL_FOLDER_PATH}
        codes:
            ok:
                0: ~
            ko:
                10: script_error_oemUnlock_10
        output: ~
        succeed: f3
        failed: ~
...
```

- `Name` defines the codename of the device. This value is a simple String.
_example: `herolte` for S7, `hero2lte` for S7 edge, `dreamlte` for S8, ..._

- `f1` is the step key (it identifies the step). The value is the step defined by following elements:

- `script` contains the name of the flash-script to call. There is no extension because it is defined at runtime (this allows to use either ".sh" for linux and mac or ".bat" for windows)
- `parameters` contains a map of parameters to use when the script is called. The order should be the one expected by the script. There are two types of values for a parameter:

- `Name` defines the codename of the device. This value is a simple String.
_example: `herolte` for S7, `hero2lte` for S7 edge, `dreamlte` for S8, ..._

- `codes` contains two sub lists: `ok` corresponds to script's exit when the step succeeds, and `ko` corresponds to script's exit when there is an error.
The codes returned by the script are always numeric value.
	The `ko` codes can be defined with an error message which will be displayed to the user if it happened.

	_example: `10: script_error_oemUnlock_10`_.

	The error message is a key corresponding to a value in the translations file (`src/main/resources/lang/traduction.properties`)

- `output` can be used to define a variable than must be read from the last line of the running script.
example: `output: ${MY_KEY}` corresponds to a step where the installer should read the last line provided by the script and store it in a java variable "MY_KEY" which would be used in a later step. The value of output is always structured like a changing parameter.

- `succeed` specifies the next step's code after a success.

- `failed:` defines the next step's code to call after a failure.

#### Special step:
``` yaml
    f7:
        script: askAccount
        parameters: ~
        codes: ~
        output: ~
        succeed: f8
        failed: ~
```

This is the only step which is empty. There is no script "askAccount". It is used by easy-intaller to know when to show the UI which lets the user request an invitation email to create an account. Please always add this step before the last step.


#### 2. Second files: <device_code_name>_fs.yml

This second file specifies extra data (which aren't optional)

### Structures and nodes of the file

``` yaml
sources:
   rom:
        url: https://images.ecloud.global/stable/herolte/e-latest-herolte.zip
        filePath: e-latest-herolte.zip
   twrp:         
        url: https://images.ecloud.global/stable/twrp/herolte/twrp-3.2.3-0-herolte.img
        filePath: twrp-3.2.3-0-herolte.img
flash:
    f1:
        ui:
            type: action
            title: stepTitle1On7
            instruction:
            - install_instr_turnOff
            - install_instr_startDownload
            - install_instr_acceptWarning
            stepNumber: 1/7
            titleIcon: Download.png
            instructionImg: group2.png
    f2:
        ui:
            type: load
            title: stepTitle2On7
            instruction:
            - install_instr_oemUnlock
            stepNumber: 2/7
            averageTime: 8
...
```

It contains two parts: `sources` which specifies files that need to be downloaded and `flash` which defines UI for the different steps.

### `sources`
Each different files that needs to be downloaded should be specified here.
Use a key to identify it :
_example: `rom`, `twrp`, `vendor`, `patch`, ..._

``` yaml
sources:
   rom:
        url: https://images.ecloud.global/stable/herolte/e-latest-herolte.zip
        filePath: e-latest-herolte.zip
   twrp:         
        url: https://images.ecloud.global/stable/twrp/herolte/twrp-3.2.3-0-herolte.img
        filePath: twrp-3.2.3-0-herolte.img
flash:
    f1:
        ui:
            type: action
            title: stepTitle1On7
            instruction:
            - install_instr_turnOff
            - install_instr_startDownload
            - install_instr_acceptWarning
            stepNumber: 1/7
            titleIcon: Download.png
            instructionImg: group2.png
    f2:
        ui:
            type: load
            title: stepTitle2On7
            instruction:
            - install_instr_oemUnlock
            stepNumber: 2/7
            averageTime: 8
...
```

It contains two parts: `sources` which specifies files that need to be downloaded and `flash` which defines UI for the different steps.


__/!\ files must have a sha256sum to check integrity. The sha256 should be the exact filename(with extension) +".sha256sum". The sha256sum should be in the same folder as the target file. It will be automaticaly downloaded.__


#### `flash`

- `url` specifies the url that points to the resource.
	_example: `url: https://path/to/the/file.extension`_

-`filePath` specifies the file's name on the user's computer once it has been downloaded

There are three types of UI (user interface): `action`, `load` and `askAccount`

#### `action` type
It corresponds to the step where the user has to perform an action (like pressing button).

It has the following structure:
```yaml
            type: action
            title: stepTitle1On7
            instruction:
            - install_instr_turnOff
            - install_instr_startDownload
            - install_instr_acceptWarning
            stepNumber: 1/7
            titleIcon: Download.png
            instructionImg: group2.png
```

- `title` contains the translations' key for the step's title. The value is defined in `src/main/resources/lang/traduction.properties`

- `instruction` are translations' keys for the instruction at this step. Their values are defined in `src/main/resources/lang/traduction.properties`
It is also used to identify pictures to display along with the instruction. Pictures are defined like instructions' text but are locatd in `src/main/resources/instructions/imageName.properties`

- `stepNumber` defines the step place in the process

- `titleIcon` is displayed left to the title value

- `instructionImg` is ignored and will be removed soon.

#### `load` type
``` yaml
            type: load
            title: stepTitle2On7
            instruction:
            - install_instr_oemUnlock
            stepNumber: 2/7
            averageTime: 8
```
- `title` is the same as for `action type`

- `instruction` is the same as for `action` type except that it must have only one instruction.

- `stepNumber` is the same as for `action` type.

- `averageTime` defines the averageTime the step is required to do its job. it is in second.



- `instruction` is the same as for `action` type except that it must have only one instruction.

#### Good practice:
- '~': is used for empty or null value.
- defines "normal" step code with 'f' and a incremental number. It helps to understand the whole process.
- defines "failure" step code with 'e' and a incremental number. It helps to understand the whole process.

## Lets make the translation file


### Was ist die Translation-Datei?

Eine Translation-Datei hat die Erweiterung „.properties“.

It also contains country code & local lang code before the extension and is separated by `"_"`.

Base name is: translation.

The complete name is:
- `translation.properties` for the default translation's file. This file will be used when no translations are found in a specific language.
- `translation_xx_XX.properties` It is a language specific translation's file.
__i.e :__ `translation_fr_FR.properties`.


It is stored in the project and build with the App.
`To update translation, we need to rebuild the application.`


### Contents of the translation file

The content of a translation file is not limited in size.

_one row = one translation

a Row is structured with the following pattern: `reference=value`
- `reference` is the code used to load the value.
- `value` is the raw text that will be display to the user


_example:_ trad_1_title=first title of the translation

_one row = one translation

a Row is structured with the following pattern: `reference=value`
- `reference` is the code used to load the value.
- `value` is the raw text that will be display to the user

_example_: trad-example=firstline\nsecondline

Result:
```
firstline
secondline
```

_example_: trad-example=firstline\nsecondline

Each part of the pattern will be separated by `"_"`

1. First part is an identifier for the UI which use the button.

Result:
```
firstline
secondline
```

3. third part is a unique identifier


_Theorical examples:_
```
UI-1_label_uid1

UI-2_button_uid1

UI-2_button_uid2

UI-2_title_uid1
```


#### a. UI identifier
The different interface are listed below (inspired by the tab's name in [this document](https://office.ecloud.global/products/files/doceditor.aspx?fileid=2121#) :
- welcome
- before
- connect
- devMode
- ADBdebug
- detect
- download
- install
- eAccount
- congrats
- feedback


_note:_  we don't use `UI` with a number corresponding at the order of the interface in the whole process.
Because if we want to change the process order, we'll have to change all translation reference...


#### b. Element type

possible element's type with corresponding code to use, are:

- Main title : `mTitle`
- title: `title`
- subtitle : `sbTitle`
- Label : `lbl`
- Button : `btn`
- instruction: `instr`

#### c. Unique identifier

use 'uid' with a simple incremental value.

The incremental value restart from 0 for each UI's identifier and each element type.

or use a text identifier.

- Main title : `mTitle`
- title: `title`
- subtitle : `sbTitle`
- Label : `lbl`
- Button : `btn`
- instruction: `instr`

#### d. Real example
_note_: in the following example, the value describe the position or the content, but it can be anything else.

```
interface_mTitle_uid1=This is a main title
interface_sbTitle_uid1=this is the first subtitle
interface_sbTitle_uid2=this is the second subtitle
interface_sbTitle_uid3=this is the third subtitle
interface_lbl_version=this is the version Number
interface_lbl_uid1=this is the label below the first subtitle
interface_lbl_uid2=this is the label below the second subtitle
interface_lbl_uid3=this is the label below the third subtitle
interface_btn_uid1=this is the button to change interface
```
