Murena Workspace unterstützt die Benutzung von E-Mail-Filtern, die automatisch Ihre eingehenden E-Mails organisieren.

{% include alerts/tip.html content="Bevor Sie den untenstehenden Schritten folgen, entscheiden Sie sich, welche Filter Sie einstellen wollen, und erstellen Sie die entsprechenden Ordner dafür."%}

![Einstellungen und Erstellen neuer Ordner](/images/setup-and-create-new-folders.png)

## Einstellen eines E-Mail-Filters im Murena Workspace
{:.mt-5}

- Visit [this link to the Filters section](https://murena.io/apps/snappymail/#/settings/filters) or navigate to `Murena dashboard` -> `Email` -> `Settings` (gear icon at bottom) -> `filters`.

- If asked, login with your Murena Workspace account e-mail and password.

- Klicken Sie auf `rainloop.user` wie unten gezeigt.

![Klicken Sie auf rainloop.user](/images/click-on-rainloop-user.png)

- Klicken Sie auf `+ Eine Filter hinzufügen`

![Klicken Sie auf einen Filter hinzufügen](/images/click-on-add-a-filter.png)

- Next, setup the filter as per your requirements. For example, the configuration of filter in the screenshot below will move all incoming emails from `mybankemail@example.com` to the folder named `Banking`.

![Setup the filter](/images/setup-the-filter.png)

- Don't forget to click on `Done` and then `Save`, otherwise your filter will not be saved.

![Click done and save](/images/click-done-and-save.png)

- Finally, activate your filter by selecting the `Dot` beside `rainloop.user` option.

![Activate your filter](/images/activate-your-filter.png)

## Benutzung von E-Mail-Filter mit Meine E-Mail verbergen
{:.mt-5}

- Besuchen Sie [diesen Link, um Meine E-Mail verbergen](https://murena.io/apps/snappymail/#/settings/hide-my-email) Alias-E-Mail-Adresse anzuschauen.

- If asked, login with your Murena Workspace account username and password.

- Klicken Sie auf `KOPIEREN`, um Ihre Meine E-Mail verbergen-Adresse zu kopieren.

![Mein E-Mail verbergen-Alias kopieren](/images/copy-hide-my-email.png)

- Use the copied email in the `Conditions` section when setting up an filter.

So wird z.B. die folgende Filter-Einstellung im untenstehenden Bildschirmfoto alle von `r4ndom.alia5.m4il@murena.io` eingehenden E-Mails in den Ordner `Alias emails` verschieben.

![Den Filter mit Meine E-Mail-verbergen einstellen](/images/setup-the-filter-with-hide-my-email.png)

- Vergessen Sie nicht, zuerst auf `Fertig` zu klicken, dann auf `Speichern`, und danach den `Punkt` neben dem `rainloop.user` auszuwählen. Sonst wird der Filter nicht gespeichert.