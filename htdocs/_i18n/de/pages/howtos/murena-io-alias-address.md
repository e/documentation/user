## Ziel des Dokuments

Dieses Dokument erklärt, wie Sie den murena.io E-Mail-Alias für Ihr E-Mail-Konto verwenden können.

## murena.io E-Mail-Adressen

Murena Workspace users now have the choice to use murena.io domain for emails. For example, a user called "jane.doe" is now able to use both `jane.doe@e.email` and `jane.doe@murena.io` email adresses. You can send emails with this alias, so others will see received emails coming from your murena.io alias. You can use it as your default email address for sending emails, but don't forget that your account name is still the one with e.email

## How to configure it on Murena Workspace

Öffnen Sie die E-Mail-App in der Murena Workspace und klicken Sie unten auf „Einstellungen“.

![email settings](/images/howtos/ecloud/email_settings.png)

Klicken Sie auf „Konten“. Unter „Identitäten“ sehen Sie sowohl Ihre e.email- als auch Ihre murena.io-Identität. Sie können Ihre murena.io-Identität über die e.email-Identität verschieben, indem Sie das Symbol mit den 6 Punkten verwenden. Die oberste Identität wird standardmäßig verwendet, wenn Sie eine neue E-Mail schreiben.

![identities](/images/howtos/ecloud/rainloop_identities.png)

Klicken Sie nun auf „Zurück“ und dann auf „Neue Nachricht“. Sie sehen Ihre neue Identität im Abschnitt „von“. Wenn Sie darauf klicken, können Sie in die alte Identität wechseln. Bei der nächsten E-Mail sehen Sie wieder die Standardidentität.

## Konfiguration am Telefon

Öffnen Sie die E-Mail-App. Tippen Sie auf das Symbol mit den drei Linien oben links und dann auf „Einstellungen“. Tippen Sie unter „Konten“ auf Ihre E-Mail-Adresse.

![email settings](/images/howtos/ecloud/phone_email_settings.png)

Tippen Sie nun auf „E-Mail senden“, dann auf „Identitäten verwalten“. Anschließend tippen Sie auf das Symbol mit den 3 Punkten oben rechts, dann auf „Neue Identität“.
Geben Sie Ihren Namen und Ihre neue E-Mail-Adresse ein. Dann tippen Sie auf „Speichern“.

![edit identity](/images/howtos/ecloud/phone_email_edit_identity.png)

Halten Sie nun auf dem Fenster "Identitäten verwalten" den Finger über die neu erstellte Identität und wählen Sie "Nach oben verschieben / als Standard". Die Identität, die oben steht, wird jedes Mal verwendet, wenn Sie eine neue E-Mail schreiben.

![make it default identity](/images/howtos/ecloud/phone_make_default_identity.png)

Tippen Sie mehrmals auf "Zurück", bis Sie sich wieder in Ihrem Posteingang befinden. Jedes Mal, wenn Sie jetzt eine E-Mail schreiben (Sie können auf das Stiftsymbol tippen), sehen Sie Ihre neue Identität im Abschnitt "von". Wenn Sie darauf tippen, können Sie vorübergehend in die alte Identität wechseln.

## Konfiguration in einer Desktop-E-Mail-Anwendung

Bitte beachten Sie, dass Sie murena.io als E-Mail-Adresse, **aber nicht als Benutzer-ID** verwenden können.
In Ihrem E-Mail-Programm müssen Sie sich immer noch als `@e.email`oder `murena.io`-Benutzer identifizieren. 
Normalerweise sollten Ihre Einstellungen automatisch erkannt werden. Geben Sie Ihr Passwort ein und speichern Sie es, wenn Sie nicht jedes Mal gefragt werden wollen. Einstellungen für die manuelle Konfiguration finden Sie [hier] (https://e.foundation/email-configuration/).

![email_client_1](/images/howtos/ecloud/email_client_1.png)

Sobald Ihre E-Mail in der App wie gewohnt konfiguriert ist, öffnen Sie die E-Mail-Eigenschaften.

![email properties](/images/howtos/ecloud/evolution_email_properties.png)

Fügen Sie unter "Aliase" Ihren murena.io-Alias hinzu. Klicken Sie dann auf OK.

![aliases](/images/howtos/ecloud/evolution_aliases.png)

Jetzt sollten Sie in der Lage sein, jedes Mal manuell auszuwählen, welche E-Mail-Adresse im Feld „von“ verwendet werden soll. In Ihrer E-Mail-Anwendung sollten Sie die standardmäßig zu verwendende „Absender“-Adresse festlegen können, indem Sie eine neue Identität hinzufügen und auswählen, dass diese standardmäßig verwendet werden soll. 

## Tipps

Sie können auch einen [E-Mail-Filter](/support-topics/email-filters) verwenden, um Ihre an beide Adressen gesendeten E-Mails in verschiedene Ordner zu sortieren.
