## FP3/FP3+
### Solution 1: Using Windows update


1. Gehe zu „Windows-Update“
  - Klicke auf das Startmenü in der unteren linken Ecke.
  - Klicke auf `Einstellungen`.
  - Gehe zu `Update und Sicherheit`

   <img src="../../images/windows-update-menu.png" width="400" alt="screenshot of windows update menu">

1. Installiere die nötigen Treiber
  - Klicke auf `Suche nach Updates`

   <img src="../../images/windows-searchForUpdate.png" width="400" alt="screenshot of windows looking for update">

1. A new section named `Show optional updates` should appear. Click on it.

   <img src="../../images/windows-showOptionnalUpdate.png" width="400" alt="Scrrenshot of window optionnal update menu">

1. Klicke auf `Treiberaktualisierungen`, um die Liste anzuzeigen.

   <img src="../../images/windows-clickOnDriverUpdates.png" width="400" alt="screenshot of windows drivers update menu">

1. Wähle den Treiber namens `Google, Inc. - Other hardware - Android Bootloader Interface` aus, klicke auf `Herunterladen` und installiere ihn. 
  {% include alerts/tip.html content="Falls der Treiber nicht funktioniert, sind andere Treiber aufgelistet. Probiere sie einfach aus." %}
  

   <img src="../../images/windows-selectDriverToUpdate.png" width="400" alt="Screenshot of driver selection of 'Google, Inc. - Other hardware - Android Bootloader Interface'">

  [source from community forum](https://community.e.foundation/t/howto-fairphone-3-3-stuck-in-fastboot-bootloader-mode-with-the-easy-installer-on-windows/24914)


Falls die vorherige Lösung nicht funktioniert, kannst du diese ausprobieren:
### Lösung 2: Manuelle Installation des Fastboot-Treibers für FP3/FP3+

1. Download the driver. Unfortunately, Fairphone does not provide an official "fastboot interface driver".
You can use [the Android one](https://developer.android.com/tools/releases/platform-tools) but there can be cases where it may not work.
1. Start your phone in Fastboot mode (turn it off then keep pressing Power + Volume down until it start in fastboot mode)
1. Plug your phone to your windows computer
1. Open device manager from windows settings
1. Select the android device with the warning icon
1. Right click to open details of the device
1. Move to Driver section, and select install/update driver
1. A message prompt will open. Choose to use local file
1. Select the folder where you downloaded the driver
1. Wait for the driver to install

{% include alerts/tip.html content="You can follow the[official android documentation about driver installation](https://developer.android.com/studio/run/oem-usb)" %}
  
{% include alerts/tip.html content="You may have to reboot your computer after driver installation, to make them available." %}

