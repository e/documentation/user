Was soll man tun, wenn man einen Fehler in /e/OS entdeckt?

## Logge dich in unseren GitLab Server ein

   * Falls Du ein Konto besitzt, [Anmeldung](https://gitlab.e.foundation/users/sign_in)
   * Falls nicht, [erstelle eines](https://gitlab.e.foundation/e)

## Unsere Fehlerberichts-Übersicht

   - /e/OS Fehlermeldungen werden [hier](https://gitlab.e.foundation/e/backlog/-/issues) aufgelistet und erstellt

   - Bevor Du einen neuen Fehlerbericht erstellt, überprüfe bitte, ob er nicht schon gemeldet wurde. Dafür bitte:

   - Gebe den Fehler in dem Suchtextfeld oben an, wie es hier gezeigt wird

   ![Neue Suche](/images/new_issue_search.png)

   - Es werden auch Ergebnisse angezeigt, die ähnliche Wörter enthalten

   - Falls die Suche kein zufriedenstellendes Ergebnis anzeigt, kann eine neue Fehlermeldung erstellt werden

## Eine neue Fehlermeldung erstellen

   - Wähle eine Vorlage für die Fehlermeldung aus dem Menü 'Eine Vorlage auswählen' (Choose a template)
   - 'Vorlage anwenden' (Apply template)
   - Füge einen Titel hinzu
   - Fülle alle erforderlichen Details aus, die von der Vorlage vorgegeben sind
   - Füge immer [Fehlerberichte/Logs](/support-topics/create-a-log) hinzu, wenn es möglich ist. Dies hilft den Entwicklern, den Fehler besser zu verstehen.
   - Wenn du fertig bist, klicke auf `Fehlermeldung erstellen` (Submit issue).



## Erhältst du eine Fehlermeldung, wenn du eine neue ID erstellen willst auf /e/OS Gitlab?

   - Einige Benutzer erhalten eine Fehlermeldung, wenn sie versuchen, eine neue ID auf /e/OS Gitlab zu erstellen.
   - Lassen Sie uns den Grund für diese Fehlermeldung erklären. Vor einiger Zeit hatten wir ein Problem mit einer großen Anzahl von Dummy-IDs, die unsere Server überschwemmten. Als Präventivmaßnahme lassen wir nun nur Registrierungen von einer begrenzten Anzahl von E-Mail-Domänen zu. 
   - Die Fehlermeldung beim Versuch, eine /e/OS Gitlab ID zu erstellen, kann die Aufforderung enthalten, den Murena Support zu kontaktieren.
   - Um den Support zu kontaktieren:
     - Zahlende Kunden können sich [hier](https://murena.com/contact-us/) mit den Details ihres Problems melden.
     - Benutzer, die /e/OS oder die Murena-Cloud kostenlos nutzen, können [im Forum](https://community.e.foundation/) schreiben, um Hilfe zu erhalten.





## Syntax und Quelle

   * [Quelle](https://docs.gitlab.com/ee/user/project/description_templates.html#setting-a-default-template-for-issues-and-merge-requests)
   * [Markdown (GitLab)](https://docs.gitlab.com/ee/user/markdown.html)

