{% include alerts/warning.html content=" Ein neues Installationsprogramm, [der WebInstaller](https://e.foundation/installer/) ist nun als Alpha-Version verfügbar. Dieser Webinstaller funktioniert in Browsern, die auf Chrome basieren, weil er WebUSB benötigt.
Der EasyInstaller wird bald nicht mehr unterstützt. " %}

## Einrichtungsschritte für Linux

### Über die Kommandozeile

> Eventuell musst du ein `sudo` vor die Befehle stellen. Das hängt von deiner Konfiguration ab.

- Öffne eine Konsole und tippe den unten stehenden Befehl, um das Easy-Installer App über den Snap Store zu installieren.

    ```shell
 snap install easy-installer --channel=latest/beta
 ```

### Über die Benutzung des App Store (der einfache Weg)

> Hier benutzen wir den Ubuntu Software Center

  - Öffne die Ubuntu Software Applikation. Es sollte ein Fenster wie dieses erscheinen:
![](/images/ubuntu-software.png)

  - Gehe zum 'Erkunden' Tab oder klicke auf das Suche-Symbol
  - Gib 'easy-installer' ein. Das sollte den /e/OS Easy Installer anzeigen, wie unten gezeigt

![](/images/easy-installer_search.png)

 - Click on the 'install' button to start the installation process
 ![](images/easy-installer_2.png)
 -
 - Depending on your configuration you may be asked to provide the admin user password to complete the installation
 - Once the installation is complete you will see this screen  

 ![](images/easy-installer_4.png)

- After the installation is complete a new icon should appear in the apps section as shown in the screenshot below. Click on it to open and try out our Easy Installer !!

![](/images/easyinstaller-in-apps.png)

{% include snippets/easy_installer.html
   title="Which permissions are required for Easy Installer to work? "
   details=site.data.easy-installer.list
 %}

### How to install EasyInstaller using snap? (Works for most Distros/OS)
Don't use Ubuntu? You can still install the EasyInstaller on other operating systems by following [our EasyInstaller Guide](https://snapcraft.io/easy-installer).
