{% include alerts/warning.html content=" Ein neues Installationsprogramm, [der WebInstaller](https://e.foundation/installer/) ist nun als Alpha-Version verfügbar. Dieser Webinstaller funktioniert in Browsern, die auf Chrome basieren, weil er WebUSB benötigt.
Der EasyInstaller wird bald nicht mehr unterstützt. " %}

## What does the Easy Installer do?

The Easy Installer is a software that will guide you as you flash /e/OS on your phone all by yourself. Unfortunately, you can't just plug in your phone and wait until the process is done. There is always some manual action required. The Easy Installer will tell you what and when you have to do something. For example: pressing one or several physical buttons, taping on some buttons, enabling options, rebooting your phone, etc.

It will also perform commands in the background that you would otherwise have to run yourself. 

Within a shell...(you know, this scary black screen window where geeks write strange things :computer:)

<img src="../images/Shell2.png" width="400" alt="Linux shell prompt example">

In addition, it will download source files and check their integrity. 

Please do not rush, and read all the instructions carefully. They aren't perfect yet, and are still being improved.

## My phone isn't detected

###  **Windows users**:  YOU NEED drivers for your phone.
This is a complicated subject, but you have to know that there isn't one single driver that works for everything.

A driver is a piece of software that allows your Windows computer to communicate with a plugged phone. There are drivers for your screen, mouse, keyboard, printer, gamepad, smartphone, etc. Unfortunately, some drivers are hard to find and don't work with all smartphone modes. 

{% tf pages/easy-installer-install-drivers.md %}


### The phone asks me to allow USB debugging. What should I do?
The Easy Installer communicates with your phone thanks to a tool called ADB (Android Debug Bridge). When you use it for the first time, on a specific computer, a prompt message is displayed on your phone. 

<img src="../images/Easy-installer-rsaFingerprint.png" width="300" alt="Example of RSA fingerprint message">

If you're on a trusted computer, you should also tick the checkbox to always allow connections between the computer and the device. If you do not tick the checkbox, you will have to accept it again next time.
You must accept this message by clicking on the "Allow" button. 

####  I cancelled the message on my phone 
If you clicked on the cancel button, then your phone can't communicate with your PC.
Just unplug then replug your phone, the message will show up again.


### Try another cable
You should try to use a different USB cable. Only use a cable that allows data transfer.
Some cables are only meant for battery charging.
USB cables are the source of many issues, because they're often not made to last.

### Try another computer
If any of the above solutions worked, you should try with another computer


## Downloading of sources failed immediatly after the beginning
[Check that "images" server is available](https://status.e.foundation).
If server is unavailable, please try again later.

## Downloading of sources failed after a long period
Click on the restart button displayed, then the downloading will restart from where it stopped.
The sources server has a timeout set to 1 hours. Consequently, if your connection is slow, it might stop before the end. 

## Instructions and illustrations don't correspond to my phone
Pictures are here to help but there aren't the main content. If you must choose, please follow the text intructions.

You'll find  help from [the community forum](https://community.e.foundation/c/community/easy-installer/136)

Help to improve instruction by making suggestion, translating or providing screenshots of your phone would be appreciate a lot!

## I want to select community or official builds
This is possible for some devices. Check the `Additional Info` column in [this table](https://doc.e.foundation/easy-installer#list-of-devices-supported-by-the-easy-installer) 


## Ich verstehe kein Englisch, ich möchte den Easy-Installer in meiner Sprache benutzen
Der Easy-Installer benutzt die Sprache deines Computers. Falls die Unterstützung für deine Sprache fehlt, 
wird Englisch als Standard benutzt.

Easy-installer supports (partially sometimes) the following languages:
- English
- French
- German
- Dutch
- Russian
- Basque

## GS290 flashing does not finalize
We don't have all the card in hand yet, but we're investigating into this issue to bring a solution as soon as possible.
Please share logs with us, and join [this discussion](https://community.e.foundation/t/gs290-easy-installer-stops/27917)  on our community forum.

## "OEM unlock" option is missing in settings of my Samsung phone
The Samsung process to enable to activate "oem unlock" option in developer settings is quite complicated
because they want to block the bootloader. Consequently, you won't find official documentation about it.

However, you can find many links on internet on how to enable
the "OEM unlock" option.

Community members also suggested:
- https://www.xda-developers.com/fix-missing-oem-unlock-samsung-galaxy-s9-samsung-galaxy-s8-samsung-galaxy-note-8/
- https://androidmore.com/fix-missing-oem-unlock-toggle-samsung/


There are already several discussions on our forum about this:
- https://community.e.foundation/t/i-do-not-have-the-option-for-unlock-oem-in-developer-options/20222/2
- https://community.e.foundation/t/galaxy-s8-oem-unlock/28305
- https://community.e.foundation/t/unclear-about-instructions-unlock-oem/20875/9


You might also want to do the following:
- Connect your phone to Wi-fi
- Update your device to the latest version of the original OS

## The installation of "TWRP" (the recovery) fails
There is a "special" step during the process for Samsung phones. After the recovery is installed,
you'll have to leave the download mode (by pressing continuously on "Power"+"volume down"). As soon as the screen become black (like an off device) you must immediatly start into recovery (by pressing continuously on "Power"+"volume up"+"bixby"/"home"). Our tip is to only move the finger from the volume button.
The manipulation is a little tricky but not impossible.

**Note:** We know that there is no warning about it yet, but we'll add it in future release. 

You can find more information on the community forum :

- https://community.e.foundation/t/e-on-samsung-galaxy-s8-cant-reboot-into-twrp/21696
- https://community.e.foundation/t/e-easy-installer-samsung-galaxy-s7-no-teamwin-help-please/27887/3


If it still blocked, and if you have a windows computer, you can try to flash TWRP (the recovery) by using [ODIN tools](https://samsungodin.com/) instead of heimdall. Once TWRP is installed, you can restart easy-installer and finish the process.

## Unlock code from Fairphone website doesn't work
Your smartphone must have the latest version of Fairphone OS to make the oem unlock code to work.
If you are still unable to unlock your Fairphone, please [contact Fairphone directly](https://www.fairphone.com/en/about/contact-us/) 


## On which OS did you test the easy-installer ?
We developed the Easy-installer on Ubuntu (20.04) and Windows (10).
It should work without any issue on Windows 7, 8, 10, Ubuntu, ElementaryOS and Linux Mint.


## I want to install the Easy Installer on a Windows without admin access
You will be able to install, but you won't get a shorcut in your non-admin account.

You have some knowledge in NSIS (tools to create windows installer)? Feel free to give us an hand at <the issue>
## I want to see log files
The installer creates a log file with a randomly generated name (e.g. 45761d08-711f-435a-881d-a236949654bb.log) that might contain useful information if something doesn’t work as expected.

Location of the log file:

- **Linux:** ~/snap/easy-installer/common
- **Windows:** %LOCALAPPDATA%\easy-installer

The log file is too large to upload to the forum directly. If you report a problem, please paste the contents of the logfile to https://haste.tchncs.de/ and click the save icon there which will generate a link which you can then use here.

If the log is too large even to be pasted and you already have an Murena Workspace account, you can ZIP the log and upload it to your Murena Workspace storage.

## My device is not detected on Windows

The Easy Installer requires drivers to be installed on Windows OS for device detection. You can download vendor specific drivers [here](https://developer.android.com/studio/run/oem-usb). For Google Nexus devices check [this guide](https://developer.android.com/studio/run/win-usb). Please note both are external links.

## My question is not listed here
If you don't find an answer to your question, a solution to your issue, please contact the [community support](https://t.me/joinchat/D1jNSmJHKo4u70ha).
You can also check on the [community forum](https://community.e.foundation/c/community/easy-installer/) or directly ask for help there.

You should also check [this page](https://community.e.foundation/t/howto-troubleshoot-issues-with-the-easy-installer/23290) which contains many hints.
