
## What is adb?
**Android Debug Bridge** oder `adb` ist ein Werkzeug, um Befehle über die Kommandozeile auf einem angeschlossenen Android-Gerät auszuführen. 

`adb` ermöglicht einen Benutzerzugang zu einer gewissen Anzahl an Geräte-Aktion, einschließlich

- Installation
- Fehlersuche (debugging) 

`adb` stellt eine [UNIX](https://unix.org/)-Shell für den Nutzer bereit, mit dem Befehle ausgeführt werden können.

`adb` ist ein Client-Server-Programm. Der Benutzer schreibt die Befehle in einer Konsole auf dem Gerät, worauf ein Hintergrundserver diese Befehle ausführt und mit dem Gerät kommuniziert. Selbst wenn der Benutzer die Konsole schließt, würde der Server im Hintergrund weiterlaufen. Um den Server anzuhalten, müssen bestimmte Befehle ausgeführt werden.

Das `adb`-Werkzeug läuft so lange wie der Benutzer eingeloggt ist. - Das heißt, wenn der Benutzer den PC herunterfährt, hält auch der Server an. Beim nächsten Mal muss `adb` wieder mit den entsprechenden Befehlen gestartet werden. 

`adb` hat drei Komponenten:
 - **Client**

   Hier gibt der Benutzer die Befehle ein. Dieser Prozess läuft auf der Entwicklungsmaschine. Der Client wird mit einem Befehl in der Kommandozeile gestartet.
 - **Daemon (adbd)**

   der Befehle auf einem Gerät ausführt. Der Daemon läuft als Hintergrundprozess auf jedem einzelnen Gerät

 - **Server**

   Der Hintergrundprozess, der die Kommunikation zwischen dem Gerät und dem Daemon steuert.

## Wo kann ich adb herunterladen?

`adb` ist bei einigen Linux-Distributionen (wie Ubuntu) schon eingeschlossen. Nichtsdestoweniger empfehlen wir die neuesten adb-Werkzeuge herunterzuladen und zu installieren.
Falls nicht die neueste Version von `adb` benutzt wird, können ernstliche Probleme bei gewissen Geräten auftreten. 

Wie man `adb` installiert, findest du im nächsten Abschnitt heraus.

## Wie man adb installiert

- [Linux-basierte Systeme](/pages/install-adb)
- [Windows-Betriebssystem](/pages/install-adb-windows)


## Zusätzliche Referenzen
Um mehr über adb zu erfahren, lies in diesem [Führer](https://developer.android.com/studio/command-line/adb) nach