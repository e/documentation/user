Alle /e/OS-Projekte heißen Mitwirkende willkommen! Suchen Sie nach Ihrem bevorzugten Projekt und registrieren Sie sich als Benutzer [auf dieser Website] (https://gitlab.e.foundation), um einen Beitrag zu leisten!

## Offene Stellen bei /e/OS

Positions are full time, and remote working.

Look for the list of open positions and how to apply [on the Jobs page](https://murena.com/jobs/).

## Specific projects looking for contributors

We have several projects that need contributors:

* Help the [microG project](https://microg.org/)! microG is helping us to get rid of the Google services. It needs good developers to help and accelerate the development and fix issues. Looking for a first developer challenge with microG? Help fixing issues and contribute [on GitHub](https://github.com/microg/).

* Join the test team, get the latest builds and help us hunt all bugs! [Apply from here](/testers).

* Hilf uns, Probleme zu beheben! Probleme zu melden ist großartig für /e/OS, sie zu beheben **verdient unsere ewige Anerkennung**. Möchtest du [ein Problem auswählen und daran arbeiten] (https://gitlab.e.foundation/groups/e/-/issues)?

* OpenCamera: some issues have been detected on some devices, probably related to some hardware drivers/firmware. For instance, OpenCamera is randomly crashing, depending on some settings. Also, we'd like to have the best ever open-source camera application in term of usability and picture result. If interested in this project please contact us, and we will start a fork (+ contribute upstream). [contact us](#how-to-contact-us)!

* Persönlicher Assistent: dies wird ein Schlüsselprojekt für /e/OS sein: wir brauchen einen persönlichen Assistenten, der deine Privatsphäre respektiert. Das Projekt hat mit einem ersten PoC begonnen! Wir brauchen ASR-Spezialisten, Konversationsmodell-Spezialisten, KI-Spezialisten (Deep Learning/Neuronale Netze). Schauen Sie sich [elivia](https://gitlab.e.foundation/e/elivia) an! Es ist unser erster Vorschlag für einen persönlichen Assistenten... Wenn Sie an einer Mitarbeit interessiert sind, kontaktieren Sie uns bitte (#how-to-contact-us).

* ROM developer/maintainer for Qualcomm (Snapdragon) and Mediatek chipsets: we want to provide support for newest devices available on the market. If you are interested in working on this, please go through the  details [here](rom-maintainer).

* Help improve the [/e/OS-related contents on Wikipedia](https://en.wikipedia.org/wiki//e/_(operating_system))! Some /e/OS-related pages are available in 10 different languages already, but they have suffered from various vandalisms and biased or poor redactions, and are often outdated. Therefore those pages are sometimes misleading users and hurting our reputation. Contribute to honnest, factual and neutral contents there!

## Major /e/OS-related projects looking for contributors

/e/OS is using, modifying, integrating a lot of different open source software components, including those ones that are key for /e/OS:

* LineageOS: /e/OS is forking LineageOS for different needs, different users. Therefore, better hardware support for LineageOS means a better hardware support for /e/OS. [Contribute to LineageOS](https://lineageos.org/)!

* Searx: searx is a great open source meta-search engine that we are forking to make our default search engine ([spot.murena.io](https://spot.murena.io)). [Contribute to Searx](https://asciimoo.github.io/searx/)!

* Exodus Privacy: Exodus Privacy is a non-profit organization that creates software tools to inform people about privacy issues in Android applications, such as abusive permissions & trackers. They [welcome software development and financial](https://exodus-privacy.eu.org/) contributions!

* NextCloud: several /e/OS online services heavily rely on NextCloud (a fork of OwnCloud), like cloud storage, online calendar, notes etc. [Contribute to NextCloud](https://nextcloud.com/contribute/)!


## Is it paid?

Projects here are open source projects and we know that many people really appreciate voluntary contributions to open source projects, for the benefit of all.

However, if you think, as a specialist, that the project will require a huge amount of work and that you are ready to make it within a limited timeframe, we can consider a bounty or a paid-project. Please contact us.

## How to contact us?

Please send an email to <join@murena.io>, with a clear subject line (like: OpenCamera development ).

Tell us why you are interested in participating, how your skills are related to this project, and any other useful information.
