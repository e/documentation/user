## Der Wiederherstellungsprozess
Nach dem Start des Wiederherstellungsprozesses für den Murena Workspace werden Ihre fehlenden Online-Einträge und -Daten allmählich wieder erscheinen. Folgendes wird Sie erwarten:

1. **Dateieintrag**
   Dies wird der erste Eintrag sein, der zurückkehrt, zunächst ohne Daten.

2. **Andere Einträge**
   Einträge wie Notizen und Galerie werden nach und nach wieder erscheinen.

3. **Datenwiederherstellung**
   Ihre wiederhergestellten Daten werden in einem Ordner namens `MurenaRecovery` im Dateieintrag platziert. Einige Dateien könnten fehlen oder leer sein, überprüfen Sie daher gründlich. Da sich Ihre wiederhergestellten Dateien in einem neuen dedizierten Ordner befinden, sind die Dateifreigaben, die Sie vor dem Ausfall eingerichtet haben, nicht mehr wirksam: Sie müssen sie erneut einrichten, damit sie funktionieren.

Murena Workspace wird Anfragen von den folgenden Nextcloud-Clients blockieren:
- Carnet
- EasySync
- eDrive (nur für /e/OS vor 1.13)
- /e/OS-Notizen
- Nextcloud-App (Windows, macOS, Linux, Android, iOS)
- Nextcloud-Notizen

Der Grund für diese Entscheidung ist, dass diese Apps einige Dateien löschen könnten, wenn wir die Cloud wieder eröffnen, da der Client einen Unterschied erkennt zwischen dem, was lokal vorhanden ist, und dem, was auf dem Murena Workspace vorhanden ist. Durch das Blockieren der Anfragen geben wir jedem Benutzer Zeit, seine lokalen Dateien zu sichern. Die folgende Dokumentation erklärt, Client für Client, wie Sie als Benutzer Ihre Dateien sichern können.

## Sichern Sie Ihre lokalen Daten

### eDrive
Überprüfen Sie zunächst, ob eDrive auf Ihrem Gerät läuft. Gehen Sie zu `Einstellungen` > `Konten` > Ihre Murena Workspace-E-Mail-Adresse > `Kontosynchronisation`. Wenn `Bilder und Videos` deaktiviert ist, bedeutet dies, dass eDrive nicht läuft und Sie diesen Teil ignorieren können.

Wenn Sie **/e/OS unter 1.13** verwenden ([wie überprüfe ich meine /e/OS-Version?](https://community.e.foundation/t/howto-give-complete-e-os-version-info-easily-for-support-answers-comparison-etc/45030)):
- Bitte aktualisieren Sie auf die neueste /e/OS-Version (`Einstellungen` > `Updates`)
- ODER, wenn Sie nicht aktualisieren können, kopieren Sie die /e/OS-Gerätedaten aus den folgenden Ordnern auf ein anderes Gerät:
    - DCIM
    - Documents
    - Movies
    - Music
    - Pictures
    - Podcasts
    - Recordings
    - Ringtones

Wenn Sie **/e/OS 1.13 und höher** verwenden, müssen Sie nichts tun! eDrive löscht niemals Dateien, daher keine Sorge wegen Datenverlust. Die Dateisynchronisation ist ab dieser Version von Anfang an aktiviert.

> **Hinweis:** Standardmäßig wird eDrive nicht alle lokalen Dateien, die Sie auf Ihrem Smartphone haben, erneut hochladen. Ein bevorstehendes eDrive-Update wird dieses Verhalten beheben und das Hochladen der Dateien, die Sie auf Ihrem Gerät haben, auf Murena Workspace erzwingen.

### Nextcloud-App (Android, iOS, Linux, macOS, Windows)
Nextcloud wird Dateien löschen, die lokal, aber nicht mehr auf dem Server vorhanden waren. Standardmäßig werden Nextcloud-Apps nicht synchronisiert, wenn wir die Murena Workspace-Datei-App wieder öffnen. Bitte kopieren Sie Ihre Dateien an einen anderen Ort.

#### Windows, macOS und Linux
Um ein Backup zu erstellen:
1. **Öffnen Sie die Nextcloud-App**: Starten Sie die Nextcloud-App auf jedem Gerät, auf dem sie installiert ist.
2. **Zugriff auf den lokalen Ordner**: Klicken Sie auf "Lokalen Ordner öffnen", um Ihre synchronisierten Dateien anzuzeigen.
3. **Dateien auswählen**
   - **macOS** Verwenden Sie `Cmd (⌘) + A`, um alle Dateien auszuwählen, und kopieren Sie sie mit `Cmd (⌘) + C`
   - **Windows/Linux** Verwenden Sie `Strg + A`, um alle Dateien auszuwählen, und kopieren Sie sie mit `Strg + C`
4. **Backup-Ordner erstellen**: Erstellen Sie einen neuen Ordner (z. B. `Nextcloud-Backup`) an einem sicheren Ort.
5. **Dateien einfügen**
   - **macOS** Fügen Sie die kopierten Dateien mit `Cmd (⌘) + V` in den Backup-Ordner ein
   - **Windows/Linux** Fügen Sie die kopierten Dateien mit `Strg + V` in den Backup-Ordner ein

{% include alerts/warning.html content="Sie müssen diesen Vorgang auf jedem Gerät anwenden, auf dem Sie einen Nextcloud-Client installiert haben." %}

#### Android
1. Öffnen Sie die `Dateien`-App und klicken Sie auf die *Nextcloud-Schaltfläche* im Seitenbereich (wenn Sie dieses Symbol nicht sehen, gehen Sie zu Schritt 9).

    ![image](/images/murena-workspace-recovery/nextcloud-1.png){: height="400" }
2. Halten Sie nun einen beliebigen Eintrag gedrückt, um ihn auszuwählen, klicken Sie auf das *Drei-Punkte-Menü (⋮)* oben rechts und wählen Sie *Alle auswählen*.

    ![image](/images/murena-workspace-recovery/nextcloud-2.png){: height="400" }
3. Dadurch werden alle restlichen Dateien ausgewählt.

    ![image](/images/murena-workspace-recovery/nextcloud-3.png){: height="400" }
4. Klicken Sie erneut auf das *Drei-Punkte-Menü (⋮)* oben rechts und wählen Sie *Kopieren nach...*.

    ![image](/images/murena-workspace-recovery/nextcloud-4.png){: height="400" }
5. Es wird eine Seite geöffnet, die zum Stammordner des Geräts führt. Klicken Sie auf die Schaltfläche *Ordner hinzufügen*, um einen neuen Ordner zu erstellen.

    ![image](/images/murena-workspace-recovery/nextcloud-5.png){: height="400" }
6. Es wird eine Textaufforderung geöffnet, in der Sie nach dem Ordnernamen gefragt werden. In unserem Fall haben wir ihn `Nextcloud.bak` genannt.

    ![image](/images/murena-workspace-recovery/nextcloud-6.png){: height="400" }
7. Öffnen Sie nun den neu erstellten Ordner und drücken Sie die *Kopieren*-Schaltfläche unten auf dem Bildschirm. Dadurch wird der Kopiervorgang gestartet.

    ![image](/images/murena-workspace-recovery/nextcloud-7.png){: height="400" }
8. Möglicherweise sehen Sie eine Benachrichtigung, wenn die Kopiergröße größer ist.

    ![image](/images/murena-workspace-recovery/nextcloud-8.png){: height="400" }

    {% include alerts/success.html content="Ihr Backup-Vorgang ist abgeschlossen. Wenn Sie bei Schritt 1 gescheitert sind, folgen Sie bitte den nachstehenden Schritten:" %}

9. Öffnen Sie die `Nextcloud`-App und wählen Sie im linken Seitenbereich `Einstellungen`.

    ![image](/images/murena-workspace-recovery/nextcloud-9.png){: height="400" }
10. Klicken Sie auf die Option `Datenspeicherordner`, um ein Popup zu öffnen. Notieren Sie sich das ausgewählte Element im Popup.

    ![image](/images/murena-workspace-recovery/nextcloud-10.png){: height="400" }
11. Öffnen Sie die `Dateien`-App und navigieren Sie zu dem zuvor notierten Pfad (standardmäßig sollte dies sein: `Android/media/com.nextcloud.client`). Hier finden Sie einen Ordner namens `nextcloud`. Halten Sie ihn gedrückt, um ihn auszuwählen.

    ![image](/images/murena-workspace-recovery/nextcloud-11.png){: height="400" }
12. Folgen Sie nun den Schritten 4 bis 8, um den Backup-Vorgang abzuschließen.

#### iOS

1. **Öffnen Sie die Nextcloud-App** auf Ihrem iPhone oder iPad.
2. **Navigieren Sie zu den Dateien/Ordnern**, die Sie sichern möchten.
3. **Tippen Sie auf das Drei-Punkte-Menü (⋮)** neben dem Datei- oder Ordnernamen.

    ![image](/images/murena-workspace-recovery/Nextcloud-Backup-iOS1.jpeg){: height="400"}
4. **Wählen Sie "Als offline verfügbar festlegen"**, um die Dateien für den Offline-Zugriff herunterzuladen.

    ![image](/images/murena-workspace-recovery/Nextcloud-Backup-iOS2.jpeg){: height="400"}
5. **Öffnen Sie die Dateien-App** auf Ihrem iPhone.

    ![image](/images/murena-workspace-recovery/Nextcloud-Backup-iOS3.jpeg){: height="400"}
6. **Gehen Sie zu "Auf meinem iPhone"** oder **"iCloud Drive"** unter **Orte**.

    ![image](/images/murena-workspace-recovery/Nextcloud-Backup-iOS4.jpeg){: height="400"}
7. **Tippen Sie auf das Drei-Punkte-Menü (⋮)** in der oberen rechten Ecke und wählen Sie **"Neuer Ordner"**.
8. **Benennen Sie den Ordner** (z. B. `Nextcloud-Backup`).

    ![image](/images/murena-workspace-recovery/Nextcloud-Backup-iOS5.jpeg){: height="400"}
9. **Öffnen Sie die Dateien-App** auf Ihrem iPhone.
10. **Gehen Sie zu "Nextcloud"** unter **Orten**.
11. **Tippen Sie auf das Drei-Punkte-Menü (⋮)** in der oberen rechten Ecke und wählen Sie **"Auswählen"**.
12. **Wählen Sie alle Dateien** aus, die Sie sichern möchten.

    ![image](/images/murena-workspace-recovery/Nextcloud-Backup-iOS6.jpeg){: height="400"}
13. **Tippen Sie auf das Drei-Punkte-Menü (⋮)** unten und wählen Sie **"Kopieren"**.

    ![image](/images/murena-workspace-recovery/Nextcloud-Backup-iOS7.jpeg){: height="400"}
14. **Navigieren Sie zum Ordner "Nextcloud-Backup"**.
15. **Halten Sie eine Weile innerhalb des Ordners gedrückt**, dann tippen Sie auf **"Einfügen"**, um die Dateien zu kopieren.

    ![image](/images/murena-workspace-recovery/Nextcloud-Backup-iOS8.jpeg){: height="400"}

### EasySync
EasySync löscht Dateien, die lokal, aber nicht auf dem Server vorhanden sind. Standardmäßig wird die EasySync-App nicht synchronisiert, wenn wir den Murena Workspace-Dateieintrag wieder aktivieren. Bitte kopieren Sie Ihre Dateien an einen anderen Ort.

1. Öffnen Sie die `Dateien`-App und halten Sie die Elemente gedrückt, die Sie sichern möchten.

   ![image](/images/murena-workspace-recovery/easysync-1.png){: height="400" }
2. Klicken Sie nun auf das *Drei-Punkte-Menü (⋮)* oben rechts und wählen Sie *Kopieren nach...*.

   ![image](/images/murena-workspace-recovery/easysync-2.png){: height="400" }
3. Es wird eine Seite geöffnet, die zum Stammordner des Geräts führt. Klicken Sie auf die Schaltfläche *Ordner hinzufügen*, um einen neuen Ordner zu erstellen. Es wird eine Textaufforderung geöffnet, in der Sie nach dem Ordnernamen gefragt werden. In unserem Fall haben wir ihn `easySync.bak` genannt.

   ![image](/images/murena-workspace-recovery/easysync-3.png){: height="400" }
4. Öffnen Sie nun den neu erstellten Ordner und drücken Sie die *Kopieren*-Schaltfläche unten auf dem Bildschirm. Dadurch wird der Kopiervorgang gestartet.

   ![image](/images/murena-workspace-recovery/easysync-4.png){: height="400" }

### /e/OS-Notizen
Die /e/OS-Notizen-App löscht die Notizen, die auf dem Gerät, aber nicht auf dem Server vorhanden waren. Der Murena Workspace-Notizeintrag ist derzeit deaktiviert, um dieses Verhalten zu verhindern. Um Ihre Notizen zu sichern:

1. **Kurzfristig: Verwenden Sie die Freigabeschaltfläche**
   - Öffnen Sie die Notizen einzeln
   - Verwenden Sie die Freigabeschaltfläche, um jede Notiz zu exportieren

2. **Demnächst: Verwenden Sie die neueste /e/OS-Notizen-App**
   Wir werden Ihnen eine APK zur Verfügung stellen, die Ihnen hilft, alle Ihre Notizen zu extrahieren. Bleiben Sie dran für Aktualisierungen, die insbesondere über die App Lounge veröffentlicht werden. Um das neueste /e/OS Notes-Update zu erhalten, sobald es verfügbar ist:
   - Aktualisieren Sie auf die neueste /e/OS-Version
   - ODER installieren Sie zumindest die neueste Version von App Lounge, indem Sie [dieser Dokumentation](https://doc.e.foundation/support-topics/app_lounge.html#download-app-lounge-apks) folgen

### Nextcloud-Notizen
- Öffnen Sie die Notizen einzeln
- Verwenden Sie die Freigabeschaltfläche, um jede Notiz zu exportieren

### Carnet
1. Erstellen Sie das Backup
   - Verwenden Sie das *Drei-Linien-Menü (☰)* oben links
   - Wählen Sie die *Einstellungsschaltfläche*

       ![image](/images/murena-workspace-recovery/carnet-Backup1.jpeg){: height="400" }
   - Verwenden Sie die *Export-Schaltfläche*

       ![image](/images/murena-workspace-recovery/carnet-Backup2.jpeg){: height="400" }
   - Gehen Sie zu dem Ordner, den Sie für das Backup verwenden möchten, und drücken Sie die *Speichern-Schaltfläche* unten rechts

       ![image](/images/murena-workspace-recovery/carnet-Backup3.png){: height="400" }

2. Überprüfen Sie das Backup
   - Öffnen Sie die Dateien-App
   - Navigieren Sie zu dem zuvor ausgewählten Ordner
   - Öffnen Sie `carnet_archive.zip`
   - Sie sollten `.sqd`-Dateien sehen, die Ihren Notizen entsprechen, sowie Ordner, falls Sie welche hatten

## FAQ-Bereich
- **F: Warum ist die Synchronisation deaktiviert?**
  - A: Die Synchronisation ist deaktiviert, um Datenverlust während des Wiederherstellungsprozesses zu verhindern.

- **F: Wann kann ich die Synchronisation wieder aktivieren?**
  - A: Wir werden Sie benachrichtigen, wenn es sicher ist, die Synchronisation wieder zu aktivieren.
