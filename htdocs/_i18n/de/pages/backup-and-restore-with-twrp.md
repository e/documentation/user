## Was ist TWRP?
[TWRP](https://twrp.me/) ist ein benutzerdefiniertes Recovery, das zum Installieren von benutzerdefinierter Software auf Android-Geräten verwendet wird. Diese benutzerdefinierte Software kann kleinere Modifikationen wie das Rooten des Gerätes enthalten, oder aber das Ersetzen der Firmware des Gerätes mit einer komplett benutzerdefinierten "ROM" wie z.B. [/e/OS](https://e.foundation/).

## TWRP für Ihr Gerät erhalten
Suchen Sie das `TWRP`-Build für Ihr Gerät auf [dieser Seite](https://twrp.me/Devices/)
- Sie benötigen Ihren `Devicecode`, um das richtige TWRP-Abbild für Ihr Gerät zu finden.

## TWRP-Sicherungen verstehen

TWRP erstellt`Nandroid`-Sicherungen, die ein nahezu vollständiges Abbild Ihres Systems sind.
- Mit Nandroid-Sicherungen können Sie Ihr Telefon in genau dem Zustand wiederherstellen, in dem es sich zum Zeitpunkt der Sicherung befand: die Android-Version, das Hintergrundbild, der Startbildschirm, bis hin zu den ungelesenen Textnachrichten, die Sie hinterlassen haben.

{% include alerts/tip.html content="Versichern Sie sich, dass die Sicherung auf ein externes Speichermedium gemacht wird. In manchen Fällen können Geräte komplett formatiert werden - das würde Ihre Sicherung zunichte machen. "%}

## Flashe TWRP auf deinem Gerät

Bei dieser Anleitung gehen wir davon aus, dass Sie TWRP auf Ihrem Gerät installiert haben. Falls nicht, bitte schauen [Sie hier nach](install-twrp).

## Mache eine `nandroid`-Sicherung deines Geräts

- In die TWRP-Wiederherstellung (recovery) booten
  Die Tastenkombination, um in den Wiederherstellungsmodus zu gelangen, ist von Gerät zu Gerät verschieden. Lies den Boot-Modi-Abschnitt auf der jeweiligen [Geräte-Infoseite]({% tl devices %})
- Für einige Geräte könnte es

  `Ausschalten` und `Lautstärke leiser` gleichzeitig, dann benutze die Lautstärketasten, um in den `Wiederherstellungsmodus` neu zu starten

- Sobald dein Gerät neu gestartet wurde, sollte – wie unten gezeigt – der TWRP-Hauptbildschirm angezeigt werden

![](/images/TWRP_Backup1.png)
- Tippe im nächsten Bildschirm oben die `Name`-Leiste, um deiner Sicherung einen wiedererkennbaren Namen zu geben.
- Hake die Boot-, System- und Daten-Kästchen ab und wische Leiste entlang des unteren Randes, um die Sicherung zu starten.
  > Sicherungen sind ziemlich groß; wenn du also eine Fehlermeldung über nicht ausreichenden Speicherplatz bekommst, musst du gegebenenfalls einige Dateien vom internen Speicher oder der SD-Karte löschen, bevor du fortfahren kannst

![](/images/TWRP_Backup1.png)

- Die Sicherung benötigt ein paar Minuten zur Fertigstellung, also sei geduldig.
- Wenn sie fertig ist, kannst du auf `Zurück`tippen, um zum TWRP Hauptmenü zu gelangen
- Alternativ kannst du auf `Reboot System` (System neustarten) tippen, um in Android neuzustarten

## Von einer TWRP-Sicherung wiederherstellen

- Starte erneut in TWRP und tippe auf die `Wiederherstellen`-Schaltfläche auf dem Startbildschirm

![](/images/TWRP_Restore.png)

- TWRP zeigt dir eine Liste deiner bisherigen Sicherungen
- Tippe die Sicherung deiner Wahl an, und dir wird folgender Bildschirm angezeigt.

![](/images/TWRP_Restore1.png)

- Stelle sicher, dass alle Kästchen abgehakt sind, und wische die Leiste zum Wiederherstellen.

![](/images/TWRP_Recovery2.png)

- Das Wiederherstellen dauert ein paar Minuten, aber wenn es fertig ist, kannst du dein Smartphone zurück in Android neu starten.

## Empfohlene Lektüre
[Was sollte ich in TWRP sichern?](https://twrp.me/faq/whattobackup.html)
