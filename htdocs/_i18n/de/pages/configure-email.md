## E-Mail-Konfiguration

 - Sie können Ihr @e.email oder @murena.io E-Mail-Konto verwenden, um sich unter [https://murena.io/](https://murena.io/) anzumelden  

Viele verschiedene E-Mail-Programme können Ihr E-Mail-Konto automatisch konfigurieren – Sie müssen nur Ihre E-Mail-Adresse und Ihr Passwort angeben, um sich zu verbinden.

- Zum Beispiel:
Murena Mail, Mozilla Thunderbird 3.1 oder neuer, Evolution, KMail, Kontact und Outlook 2007 oder neuer.

- Wenn Sie eine aktuelle Version von Apple Mail verwenden (sowohl unter macOS als auch unter iOS), können Sie [https://autoconfig.murena.io/](https://autoconfig.murena.io/) besuchen und Ihre Daten eingeben, um ein Konfigurationspaket herunterzuladen, das Ihr E-Mailprogramm automatisch konfiguriert.


## Manuelle Einrichtung

Für die manuelle Einrichtung verwenden Sie bitte die folgenden Einstellungen:


   - SMTP/IMAP Server: mail.ecloud.global

   - SMTP/IMAP-Benutzername: Ihre vollständige Murena-Adresse (Beispiel: john.doe@e.email oder john.doe@murena.io)

   - SMTP/IMAP-Passwort: Ihr Murena-Passwort

   - SMTP-Port: 587, Passwort, normal, STARTTLS, Authentifizierung erforderlich

   - IMAP-Port: 993, SSL/TLS, Authentifizierung erforderlich
