{% include alerts/warning.html content=" Ein neues Installationsprogramm, [der WebInstaller](https://e.foundation/installer/) ist nun als Alpha-Version verfügbar. Dieser Webinstaller funktioniert in Browsern, die auf Chrome basieren, weil er WebUSB benötigt.
Der EasyInstaller wird bald nicht mehr unterstützt. " %}

## Voraussetzungen
Du musst die Treiber für dein Telefon schon installiert haben
{% include alerts/tip.html content="Um Treiber für dein Telefon zu installieren, schau bitte in die Installationsanleitung deines Gerätes [hier](easy-installer#list-of-devices-supported-by-the-easy-installer)" %}

## Einrichtungsschritte für Windows OS

### Herunterladen

Um den Easy-installer beta exe für Windows herunterzuladen, klicke [bitte hier](https://images.ecloud.global/easy-installer/Easy-installer-setup.exe )

- Speichere ihn in einem Ordner auf der Festplatte

![](/images/win_setup_easyinstaller_download_1.png)

- Überprüfe die [sha256sum](https://images.ecloud.global/easy-installer/Easy-installer-setup.exe.sha256sum)

### Ausführen

- Die heruntergeladene .exe-Datei kann durch Doppelklicken ausgeführt werden


{% include alerts/warning.html content="Der Windows Defender Smartscreen wird wahrscheinlich folgende Fenster öffnen und nach der Bestätigung der Installation des Easy-installer-setup.exe fragen" %}

![](/images/win_setup_easyinstaller_popup_1.png)

Klicke auf `Mehr Infos`
Das Fenster sieht jetzt so aus

![](/images/win_setup_easyinstaller_popup_2.png)

Klicke auf `Trotzdem ausführen`

Auf manchen Computer kann dieses Fenster erscheinen

![](/images/win_setup_easyinstaller_warning_1.jpg)


Klicke auf `Ja`

Dies wird das Installationsprogramm starten, und ein ähnliches Willkommensfenster öffnen

![](/images/win_setup_easyinstaller_3.1.png)

### Lese die Lizenzvereinbarung und stimme ihr zu

- Lese die Lizenzvereinbarung und klicke auf `Zustimmen`, falls du den Bedingungen für die Installation zustimmst.


![](/images/win_setup_easyinstaller_3.2.png)

### Wähle einen Ordner aus

- Wähle aus, wo du die Anwendung installieren willst.

![](/images/win_setup_easyinstaller_3.3.png)

Standardmäßig wird unter Windows ein Ordner namens `easy-installer` in den Programmdateien auf dem Laufwerk C erstellt.

- Nun kannst du die Standardauswahl bestätigen und auf `Installieren` klicken, oder ein anderer Ordner auswählen

![](/images/win_setup_easyinstaller_3.3.1.png)

### Die Installation starten

- Folgendes Bild wird während der Installation angezeigt. Es sollte nur einige Minuten bis zum Abschluss der Installation dauern.

- Den PC bitte nicht ausschalten und den Installationsprozess nicht unterbrechen.

![](/images/win_setup_easyinstaller_3.4.png)

### Installation abgeschlossen

- Wenn die Installation abgeschlossen ist, wird folgendes Fenster angezeigt

![](/images/win_setup_easyinstaller_3.5.png)

### Wo der Easy-Installer gefunden werden kann

- Der Easy-Installer kann im Startmenü gefunden werden

![](/images/windows_easy_installer_icon.png)

- Klicke darauf und probiere den Easy Installer aus!

### Fehlerbehebung

- Wenn du ein Antiviren-Programm auf deinem Windows-PC laufen hast, kann dies in manchen Fällen den Easy-Installer blockieren. In diesem Fall musst du ihn in den Einstellungen des Antivirus-Programmes freischalten.


- Wenn du ein **Samsung**-Gerät flashst, siehst du vielleicht dieses Fenster. Klicke auf `Ja`

<br>Kindly click on `Yes`" %}

wdi-simple ist einWerkzeug zur Treiber-Installation. Der Quellcode ist einsehbar unter [libwdi-Projekt](https://github.com/pbatard/libwdi)


