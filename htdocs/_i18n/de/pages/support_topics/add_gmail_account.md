## Purpose of this guide
This guide will explain how you can add a Gmail or Google account to your /e/OS smartphone. 

### Advantages 
- You can directly access your Gmails from the `Mail` app.
- You can access your Google Contact from the `Contacts` app
### Disadvantages
- Please be aware that by granting Google access to your /e/OS Smartphone you are opening up the device to Google.
### Recommendation

- Aktiviere oder füge Google-Konten nur dann hinzu, wenn du sie unbedingt auf deinem Gerät benötigst



## Add a Google Account to your smartphone

1. Click on `Settings` icon on the main screen.

    ![](../images/howtos/gmail/add_gmail_account_1.png)

1. Browse to `Accounts` in the Settings screen

    ![](../images/howtos/gmail/add_gmail_account_2.png) 

1. In the `Accounts` screen all the accounts you have already created will be displayed. Select `Add Account`
1. This will open the `Add an Account` screen as shown in the second screen shot. Select `Google`

    ![](../images/howtos/gmail/add_gmail_account_3.png)
    ![](../images/howtos/gmail/add_gmail_account_4.png)

1. Daraufhin wird eine Pop-up-Warnung angezeigt, wie unten dargestellt. Der Pop-up-Text weist darauf hin, dass das Gerät einen gefälschten Gerätenamen an Google melden wird. Dies wird aus Datenschutz-Gründen gemacht. Du kannst den Namen des Geräts, der an Google gemeldet wird, auf der Google-Website unter Geräteaktivität überprüfen. Er wird sich von dem tatsächlichen Namen Deines Telefons unterscheiden. Wähle `OK`

    ![](../images/howtos/gmail/add_gmail_account_5.png)

1. This will open up the screen to log in to your Google Account as shown below. Add your Google ID credentials. Click `Next`

    ![](../images/howtos/gmail/add_gmail_account_6.png)

1. Enter your Google ID password in this screen. Click `Next`

    ![](../images/howtos/gmail/add_gmail_account_7.png)
1. At this stage if you have enable 2-step verification on your Google ID you will need to complete it. A notification is sent to one of your devices where it will ask you to verify if you are adding the account. Tap `Yes` on that device. Please note unless you tap `Yes` this screen will not go ahead.

    ![](../images/howtos/gmail/add_gmail_account_8.png)

1. Once you have verified your account, in the next screen Google will want access to your account. Click `Allow` only if you want Google to have access to these options. You can stop at this point if you are not comfortable giving access. In that case, the account will not be added to your phone. 

    ![](../images/howtos/gmail/add_gmail_account_9.png)![](../images/howtos/gmail/add_gmail_account_10.png)
1. If in the previous step you have granted access,the account manager will now take a few seconds to save and add the new account. 

    ![](../images/howtos/gmail/add_gmail_account_11.png)![](../images/howtos/gmail/add_gmail_account_12.png)
1. Once the Google account has been added to your phone it will open up the Browser with Google in the Search bar. 

    ![](../images/howtos/gmail/add_gmail_account_13.png)
1. To verify that you have added the Gmail / Google account to your phone. Browse to the `Settings` >> `Accounts` screen. Here you would see a new addition in the list. It will now display your Google Account ID. 

    ![](../images/howtos/gmail/add_gmail_account_14.png)
## Remove the Google Account

You can always remove the Google Account that you have added to your /e/OS Smartphone

- Click on `Settings`
- Browse to `Accounts`
- In the Accounts screen select the Google Account you want to delete
- This will open a screen like shown below

    ![](../images/howtos/gmail/add_gmail_account_15.png)

- Here click on the `Remove Account` button to delete the Google account you have added. You can always add the account back at a later stage.
