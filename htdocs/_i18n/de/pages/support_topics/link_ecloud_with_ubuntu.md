## Link

{% include alerts/tip.html content="We have taken the screenshots on a Ubuntu 22.04 LTS system. The process should be the same for other Ubuntu versions as well." %}

### Schritt 1
Öffne auf deinem Ubuntu-System im Browser 

`Anwendungen anzeigen` >> `Einstellungen`
Scrolle nach unten bis zu `Online-Konten`

![](../images/howtos/ecloud/ecloud_link_ubuntu_1.jpg)


### Schritt 2
Auf die `NextCloud`-Option klicken, um den Dialog zu öffnen

![](../images/howtos/ecloud/ecloud_link_ubuntu_2.jpg)

### Schritt 3 
Trage hier die Anmeldedaten ein, wie folgt

- `server` : `murena.io`
- `Username` : Your murena ID username. Write the complete username in the format username@e.email or username@murena.io
- `Password` : Your murena ID password
- Press `Connect`

![](../images/howtos/ecloud/ecloud_link_ubuntu_3.jpg)

### Schritt 4
Bei erfolgreicher Verbindung siehst du folgenden Bildschirm

{% include alerts/tip.html content="Click the `X` icon on top right of the pop-up to close this screen. Clicking the `Remove Account` will remove the account you just created"%}
![](../images/howtos/ecloud/ecloud_link_ubuntu_4.jpg)
## Verify
- In the Settings

Du solltest nun die neue Verbindung sehen können unter `Einstellungen` >> `Online-Konten`

![](../images/howtos/ecloud/ecloud_link_ubuntu_5.jpg)
- In the Files App

You should be able to see a link to the Murena Workspace account on the left side in the links. It would be in the format yourusername@murena.io@murena.io as seen in the screenshot

![](../images/howtos/ecloud/ecloud_link_ubuntu_6.jpg)

- Clicking on the link will display the folders on your Murena Workspace. 
{% include alerts/tip.html content="It may take a couple of seconds to open up the folder view as now it is accessing a folder across the network. Please be patient."%}

![](../images/howtos/ecloud/ecloud_link_ubuntu_7.jpg)

## Benutzung

You can now copy folders and files from your local PC to the Murena Workspace. In this screenshot we show two windows one displaying the local folders and the other the Murena Workspace folders. 

Bitte beachte, dass der ordnerübergreifende Kopiervorgang von deiner Netzwerkgeschwindigkeit abhängt. Darum bitte etwas Geduld, bis der Kopiervorgang abgeschlossen ist. 

![](../images/howtos/ecloud/ecloud_link_ubuntu_8.jpg)


## Verbindung trennen

To remove or delink the Murena Workspace connection you have two options

### Von der Dateimanager-App 

- Ein Rechtsklick auf den Link in der Dateimanager-App, dann Auswerfen auswählen 

![](../images/howtos/ecloud/ecloud_link_ubuntu_9.jpg)

### In den Einstellungen

- `Settings` >> `Online Accounts` >> Select the Murena Workspace account >> Click the `Remove account` button

![](../images/howtos/ecloud/ecloud_link_ubuntu_4.jpg)