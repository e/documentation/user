
## Worum geht es hier
Diese Anleitung erklärt, wie man einen PC mit der Murena Workspace verbinden kann.
## Warum verbinden? 

Die Murena Workspace ist dein persönlicher Speicherort für Daten wie Dokumente, Musik und Bilddateien. 

Mit /e/OS und deiner /e/OS ID ist es möglich, einfach Daten von deinem Smartphone zu deiner persönlichen Murena Workspace zu übertragen. 

Wenn deine Murena Workspace mit deinem PC verbunden ist, kannst du einfach auf deine Dokumente und Bilder sowohl von deinem PC wie von deinem Smartphone zugreifen. 


In dieser Anleitung zeigen wir dir, wie du deinen PC mit deiner Murena Workspace verbinden kannst. 

Die Verbindungsmethode ist je nach Betriebssystems deines PCs unterschiedlich. Wir decken einige der bekanntesten Betriebssysteme ab.

## Wie man mit verschiedenen Betriebssystemen verbindet

[Ubuntu](/support-topics/link-ecloud-with-ubuntu)

[Windows](/support-topics/link-ecloud-with-windows)

Es werden noch mehr Betriebssysteme zu dieser Liste hinzugefügt werden


## Du hast noch keine Murena Workspace oder /e/-Konto? 

Schau dir [diese Anleitung](/create-an-ecloud-account) an
