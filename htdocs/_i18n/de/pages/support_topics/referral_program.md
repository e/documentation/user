# Murena Workspace „Freunde werben“-Programm

Erhalten Sie bis zu 40 € Guthaben für Ihren Cloud-Speicher, indem Sie Ihre Freunde zu Murena Workspace einladen!

Für jeden Freund, der ein Murena Workspace Konto eröffnet, erhalten Sie 2 € Guthaben für Speicherplatz auf der Murena Workspace.

Murena Workspace ist ein persönliches E-Mail-Konto mit Kalender und Kontakten, mit Cloudspeicher und Online-Office - und das alles in einem einzigen Service mit unkomplizierter Bedienung. Murena Workspace läuft auf bewährter Software wie NextCloud und OnlyOffice.

Ihr Murena Workspace Konto enthält 1 GB kostenlosen Speicherplatz. Unsere Speichertarife beginnen bei 1,99 € pro Monat für 20 GB.

## Wie funktioniert das?

Wenn Sie einen verifiziertes murena.io oder e.email-Konto haben, dann ist es sehr einfach.

Sie müssen sich nur noch bei unserer E-Commerce-Plattform [https://murena.com/my-account/](https://murena.com/my-account/) anmelden, damit wir Ihnen das Guthaben zuweisen können.

Gehen Sie in Ihrem murena.com Kontenbereich auf den Abschnitt ‘Freunde werben’ und folgen Sie dem eindeutigen und anonymen Link.

Sie können diesen eindeutigen Link einfach kopieren und woanders verwenden, z.B. Ihren Freunden mailen oder in einem Tweet teilen.

Für jeden Freund, der diesen Link benutzt, um ein Gratis-Konto zu erstellen, erhalten Sie 2 € Cloud-Guthaben auf Ihren murena.com Konto.

## Wie lade ich meine Freunde ein?

Es ist sehr einfach, Sie müssen nur Ihren eindeutigen Registrierungslink versenden, der in Ihren 
Murena.com-Konto zu finden ist. Der Link ist komplett anonym, er sammelt keine identifizierenden 
Informationen wie Ihren Namen oder Ihre E-Mail-Adresse.

## Wie kann ich das Guthaben benutzen?

Ihr Guthaben kann benutzt werden, sobald Sie für die Speicherplatzerweiterung angemeldet sind. Das Guthaben wird dann in einen Rabatt für den Bezahltarif umgewandelt.

## Wie lange ist mein Guthaben gültig?

Ihr Guthaben ist für 24 Monate gültig.

## Wie lang dauert diese „Freunde werben“-Aktion?

Die Murena Workspace „Freunde werben“-Aktion ist ein dauerhaftes Programm.

## Kann ich mein Guthaben auch bei einem Kauf auf murena.com benutzen, z.B. für ein Telefon oder anderes?

Dein Guthaben kann nur für die Erweiterung oder Verlängerung des Bezahl-Abos der Murena Workspace benutzt werden.

## Wie funktioniert das für meine Freunde?

Wenn Ihre Freunde auf den „Freunde werben“-Link klicken, werden sie auf die Kontoeröffnungs-Seite weitergeleitet, wo sie eine Benutzer-ID und ein Passwort erstellen können.

Wenn die Kontoeröffnung abgeschlossen ist, werden Ihre Freunde einen Link zu einem Murena Workspace Konto erhalten, womit sie auf Zugriff auf das Konto und den Cloud-Speicher haben und mit der OnlyOffice Suite starten können.

## Wie erstelle ich ein Murena Workspace Konto?

Es ist sehr einfach. Besuchen Sie [https://murena.io/signup](https://murena.io/signup), um eine Einladung anzufordern. Sie werden aber nur in das „Freunde-werben“-Programm aufgenommen, wenn Sie sich auch unter [https://murena.com/my-account/](https://murena.com/my-account/) registrieren und Ihre eindeutigen Werbe-Links benutzen.

## Was ist die Murena Workspace genau?

Murena Workspace ist ein vollständiges Online Ökosystem, welches „ent-google-t“ ist.

Murena Workspace ist ein persönliches E-Mail-Konto unter den murena.io und e.email-Domains, mit Kalender und Kontakten, mit Cloudspeicher und Online-Office - und das alles in einem einzigen Service mit unkomplizierter Bedienung. Murena Workspace läuft auf bewährter Software wie NextCloud und OnlyOffice.

## Ist der „Freunde werben“-Link anonym?

Der Link ist völlig anonym, er sammelt keinerlei identifizierende Informationen wie den Namen oder E-Mail-Adresse. Die Person, welche den Link versendet, hat keinen Überblick darüber, wer diesen Link benutzt hat.

## Ich kann mein Guthaben auf murena.com nicht sehen, was soll ich tun?

Bitte kontaktieren Sie uns unter [helpdesk@murena.com](mailto:helpdesk@murena.com), damit wir dies untersuchen und für Sie lösen können.
