Wir haben kürzlich entdeckt, dass /e/OS-Nutzer ihre Fairphone 3+ nicht mehr starten konnten, nachdem sie von Android 10 auf 11 aktualisiert hatten. Wenn du unter dieses betroffenen Nutzern bist, kannst du dein /e/OS wie folgt reparieren.

Erforderlich:
1. Ein Computer mit Windows oder Linux
1. Ein Datenkabel, um dein Telefon mit dem PC zu verbinden. Ein reines Ladekabel funktioniert nicht.

**Schritt 1**: Lade die Archiv-Datei vom untenstehenden Link herunter.

[https://images.ecloud.global/stable/FP3/fix_fp3.zip](https://images.ecloud.global/stable/FP3/fix_fp3.zip)

**Schritt 2**: Entpacke die heruntergeladene Archivdatei, indem du rechts draufklickst und „Entpacken“ auswählst. Dies kann auch mit folgenden Befehlen in einem Terminal gemacht werden.

```
unzip PFAD_ZU_ARCHIVNAME
```

Nach dem Entpacken wirst du einige Dateien und Ordner sehen.

1. Der `bin-linux-x86` -Ordner enthält Linux-Werkzeuge wie fastboot und adb.
1. Der `bin-msys` Ordner enthält Windows-Werkzeuge wie fastboot und adb.
1. Eine flashbare Zipdatei namens `e-1.7-r-20230110250404-stable-FP3.zip`, die auf dein Fairphone geflasht werden wird.

**Schritt 3**: Starte dein Fairphone in den Wiederherstellungs-Modus (recovery mode) , indem du lange auf `Ausschalten` und `Lautstärke lauter` drückst, bis du ein /e/-Logo mit „recovery mode“ darunter geschrieben siehst.

**Schritt 4**: Verbinde dein Telefon mit einem PC über ein Datenkabel.

**Schritt 5**: Navigiere zu `Apply update` (Aktualisierung anwenden), indem du kurz auf `Lautstärke leiser` drückst und mit `Ausschalten` bestätigst. Drücke dann nochmals `Ausschalten`, um die adb-Aktualisierung zu bestätigen.

**Schritt 6**: Folge nun der untenstehenden Anleitung, je nach deinem Betriebssystem.

Für Windows-Nutzer:

- Navigiere zu dem entpackten Ordner aus Schritt 2
- Öffne die Eingabeaufforderung aus diesem Ordner. Du kannst der Anleitung hier folgen [Eingabeaufforderung aus Ordner starten](https://www.tippscout.de/kommandozeile-ordner_tipp_3658.html).
- Kopiere diesen Befehl `bin-msys\adb.exe sideload e-1.7-r-20230110250404-stable-FP3.zip` und drücke Enter.

Für Linux-Nutzer:

- Navigiere zu dem entpackten Ordner aus Schritt 2
- Öffne ein Terminal aus diesem Ordner. Unter Ubuntu und Derivate: Rechtsklick auf den Ordner, dann „Im Terminal öffnen“ auswählen
- Kopiere diesen Befehl `chmod +x bin-linux-x86/adb && ./bin-linux-x86/adb sideload e-1.7-r-20230110250404-stable-FP3.zip`und bestätige mit Enter

Die Befehlsaufforderung oder das Terminal wird den Fortschritt anzeigen. Warte, bis er abgeschlossen ist.

**Schritt 7**: Wenn der Vorgang abgschlossen ist, wirst du auf den Wiederherstellungs-Bildschirm (recovery) von deinem Telefon geleitet werden. Dort kannst du auf `Zurück` tippen, um ins Wiederherstellungs-Hauptmenü zu gelangen. Dort ist die „Neustarten“-Option (reboot) schon ausgewählt. So musst du nur kurz auf `Ausschalten` drücken, um in das /e/OS zu starten.

Viel Spaß!
