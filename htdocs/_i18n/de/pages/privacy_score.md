In der `App Lounge`, dem Standard /e/OS App Store, kann jeder seine Lieblingsapps auf Datensicherheit überprüfen. Eingebaute Tracker und notwendige Berechtigungen werden angezeigt, damit jeder sie einsehen und selbst entscheiden kann, ob er die App installieren möchte.

## Was sind Tracker und Berechtigungen?

`Tracker` sind in Anwendungen eingebettete Softwareteile, die Daten darüber sammeln sollen, was Sie mit der Anwendung tun, wo Sie sie verwenden oder Sie mit maßgeschneiderter Werbung versorgen.

`Berechtigungen` sind Aktionen, die Anwendungen auf dem Gerät ausführen können, z.B. auf die Kontakte und SMS zugreifen oder bestimmte Funktionen wie die Kamera oder das Internet nutzen.

Jede App in der App Lounge erhält eine Datenschutzbewertung. Damit kann sich jeder selbst ein Urteil über die Sicherheit einer App bilden, bevor er sie auf seinem Smartphone installiert. Es geht um unkontrollierten Datenfluss und Berechtigungen.

Die Bewertung setzt sich aus zwei Kriterien zusammen:

  - die in die Anwendung eingebetteten Tracker
  - von der Anwendung benötigte Berechtigungen

## Der Berechnungsalgorithmus

Die Bewertung ist eine Zahl von 1 bis 10, die mit einem Farbcode von rot bis grün verbunden ist. Je höher der Wert, desto sicherer ist die App für den Benutzer.
Berechnet werden die Anzahl an Berechtigungen und Tracker, die in der App gefunden werden, gemeinsam mit der Datenschutzbewertung der App:

{% include alerts/tip.html content="Datenschutzbewertung = Wert, berechnet aufgrund Trackern + Wert, berechnet aufgrund der Berechtigungen" %}


- die Anzahl der Tracker macht den größten Teil der Berechnung aus. Sie begründet sich auf den [Befund von Exodus Privacy](https://reports.exodus-privacy.eu.org/en/reports/list/), der mittels einer API abgerufen wird.

- die Anzahl an Berechtigungen macht den Rest der Berechnung aus. Sie begründet sich ebenfalls auf die [Befunde von Exodus Privacy](https://reports.exodus-privacy.eu.org/en/reports/list/).

### Werteberechnung nach Trackeranzahl.
  -	0 Tracker: Trackingwert = 9
  -	1 Tracker: Trackingwert = 8
  -	2 Tracker: Trackingwert = 7
  -	3 Tracker: Trackingwert = 6
  -	4 Tracker: Trackingwert = 5
  -	5 Tracker: Trackingwert = 4
  -	6 oder mehr Tracker: Trackingwert = 0


### Wert, aus den Berechtigungen berechnet.
  - 1 Punkt: eine bis fünf Berechtigungen
  -	0 Punkte: mehr als fünf Berechtigungen

Der Quellcode des Berechnungs-Algorithmus der Datenschutzbewertung ist [hier einsehbar](https://gitlab.e.foundation/e/os/apps/-/blob/main/app/src/main/java/foundation/e/apps/PrivacyInfoViewModel.kt#L78).
Die Berechnungslogik für die Datenschutzbewertung wird weiterhin verbessert, durch Rückmeldungen und Beobachtungen.

## Die Grenzen der Datenschutzbewertung

Die Datenschutzbewertung ist eine Hilfe für App Lounge-Nutzer, aber sie hat ihre Grenzen. Zum Beispiel:
 -	Wenn eine App keine eingebauten Tracker hat, aber die Daten anderweitig sammelt und weiterschickt, so kann diese App eine tolle Datenschutzbewertung haben, aber der Datenschutz des Nutzers ist nicht gewährleistet.
 -	Sie trifft keine Aussagen über die legitime Anforderung der Berechtigungen.


## Rückmeldungen und Vorschläge
Wir freuen uns über Rückmeldungen und Vorschläge, um /e/OS zu verbessern. Du kannst Verbesserungsvorschläge [auf GitLab](https://gitlab.e.foundation/e/backlog/-/issues) aufzeichnen oder deine Bemerkungen an <helpdesk@murena.com> senden.


{% include alerts/tip.html content="Exodus Privacy ist nicht verantwortlich für unsere Datenschutzbewertung." %}
