## Über das /e/OS-Projekt

> Entdecke mehr über das /e/OS-Projekt

- [Was ist /e/OS? Lese die Produktbeschreibung!]({% translate_link what-s-e %})
- [Installiere /e/OS auf unterstützten Smartphones!]({% translate_link devices %})
- [Erstelle ein gratis Murena.io Arbeitsplatz-Konto]({% translate_link create-a-murena-cloud-account %})
- [Anleitungen]({% translate_link support-topics %})
- [Häufig gestellte Fragen über /e/OS]({% translate_link support-topics %})
- [Teile deine /e/rfahrung mit!]({% translate_link testimonies %})

## Erfahre mehr darüber

> Informationen über das /e/OS-Projekt und seine Apps

- [/e/OS easy-installer]({% translate_link easy-installer %})
- [Karten-App]({% translate_link maps %})
- [Verschiedene Build-Arten]({% translate_link build-status %})
- [App Lounge ]({% translate_link app-lounge-main %})
- [Advanced Privacy]({% translate_link support-topics/advanced_privacy_main %})
- [e.foundation Impressum & Datenschutz](https://e.foundation/legal-notice-privacy/)

## Hilf bei /e/OS mit!

> Möchtest du mitmachen?

- [Projekte, die noch Mitwirkende suchen]({% translate_link projects-looking-for-contributors %})
- [Stellenauschreibungen bei Murena](https://murena.com/jobs/)
- [ROM Maintainer]({% translate_link rom-maintainer %})
- [Übersetzer]({% translate_link translators %})
- [Tester]({% translate_link testers %})
- [Freiwillige]({% translate_link volunteers %})

## Kurzverweise

> Links zu den /e/OS-Webseiten, Foren und Telegram-Kanälen

- [/e/OS Webseite](https://e.foundation/)
- [/e/OS Quellcode](https://gitlab.e.foundation/e)
- [Hilfe benötigt? Support?](https://e.foundation/contact/)
- [Melde einen Fehler/Bug bei /e/OS oder in den Apps](https://gitlab.e.foundation/e/backlog/-/issues/new)
- [Frage nach Unterstützung für dein Smartphone](https://community.e.foundation/c/e-devices/request-a-device)

