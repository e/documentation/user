We recently discovered that /e/OS users were unable to boot their Fairphone 3+ after upgrading from Android 10 to Android 11. If you are one among these affected users, you can fix your /e/OS by following the steps below.

Requirements:
1. A computer that runs Linux or Windows
1. A data cable for connecting your phone to your computer. A charging-only cable will not work.

**Step 1**: Download the archive file using the link below.

[https://images.ecloud.global/stable/FP3/fix_fp3.zip](https://images.ecloud.global/stable/FP3/fix_fp3.zip)

**Step 2**: Unzip the downloaded archive by right-clicking the archive and selecting the extract option. You can also do it by entering the following command into a terminal.

```
unzip PATH_TO_NAME_OF_ARCHIVE
```

After archive extraction, you will be able to see some files, and folders/directories.

1. The `bin-linux-x86` folder/directory containing Linux tools such as fastboot and adb.
1. The `bin-msys` folders/directory containing Windows tools such as fastboot and adb.
1. A flashable zip file named `e-1.7-r-20230110250404-stable-FP3.zip` which will be flashed to your phone.

**Step 3**: Boot your Fairphone into recovery mode by long pressing `Power` and `Volume up` buttons together until you see an /e/ logo with recovery mode written below it.

**Step 4**: Connect your phone to your computer using a data cable.

**Step 5**: Navigate to the 'Apply update' section by short pressing 'Volume down' and then 'Power' button. Short press the 'Power' button again to confirm the ADB update.

**Step 6**: Follow the instructions below, depending on your operating system.

For Windows users:

- Navigate to the unzipped folder from Step 2
- Open a command prompt in that folder. You can follow this article on [How to open command prompt in a folder](https://www.lifewire.com/open-command-prompt-in-a-folder-5185505).
- Paste this command `bin-msys\adb.exe sideload e-1.7-r-20230110250404-stable-FP3.zip` and press Enter.

For Linux users:

- Navigate to the unzipped folder from Step 2
- Open a terminal in that folder. You can follow this article on [How to open terminal in a directory](https://www.howtogeek.com/270817/how-to-open-the-terminal-to-a-specific-directory-in-linux/).
- Paste this command `chmod +x bin-linux-x86/adb && ./bin-linux-x86/adb sideload e-1.7-r-20230110250404-stable-FP3.zip`

The Command prompt or terminal will indicate the progress. Wait for it to complete the tasks.

**Step 7**: When the procedure is finished, you will be taken to the recovery screen on your phone, where you can tap the `Back` button to return to the recovery home screen. On the home screen, you will notice that the reboot option is already selected, so you can just short press the `Power` button to boot into /e/OS.

Enjoy!
