1. Meld je aan bij jouw account van [Murena.io](https://murena.io).

1. Click on your profile image located at the upper right corner.

1. Click on `Settings` in the dropdown menu.

   ![Navigate to settings from Murena dashboard](/images/tips/navigate-to-settings-on-murena-dashboard.png)

1. Go to the `Security` tab using the menu on the left.

   ![Click on security tab](/images/tips/click-on-security-tab.png)

1. Enter an email address of your choice in the Recovery Email section and click `Change recovery email`.

   ![Enter your preferred recovery email](/images/tips/update-recovery-email.png)

Congratulations, the email address you entered will be used for account recovery.
