### User report about Nextcloud security scan

One of our users got concerned when the Nextcloud server security check resulted in an F grade. However, this does not imply that Murena Workspace is insecure. Rather, the low Nextcloud security scan scores are due to differences in how Murena Workspace is deployed and how Nextcloud ranks servers.

### How does Nextcloud security scanner work?

The Nextcloud security scanner evaluates the security score of Nextcloud servers by checking factors like the version of Nextcloud version installed, the configuration of the server used, and some other factors. If the configuration used by a Nextcloud server is not as per the recommendations by Nextcloud, then the server's security score is reduced. As an example, one of Nextcloud's recent low security scans was because Murena Workspace was only one minor version behind and did NOT lack any major security patches. Warnings issued by Nextcloud security scanner like "likely trivial to break in" is not applicable to Murena Workspace, because of the security measures and monitoring on our infrastructure.

### Security measures deployed on Murena Workspace

We follow security news and also have automated systems in place to spot strange behavior in traffic and stop it before it can be exploited against Murena Workspace. Other small issues that are unrelated to the Murena Workspace or Nextcloud are handled at the infrastructure level as well. We follow Nextcloud's suggested security settings and other common security practices to keep Murena Workspace secure. This different configuration environment may confuse the Nextcloud security scan, leading to inaccurate scores. This indicates that Nextcloud security scan is an effective tool to test vanilla Nextcloud setups, but does not accurately reflect the security of Murena Workspace.

We are now receiving an A+ on the security scan. Nevertheless, the Nextcloud release cycle can occasionally cause a dip in the score, even when Murena Workspace is secure.
