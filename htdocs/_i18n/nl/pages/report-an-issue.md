What to do when you encounter an issue in /e/OS?

## Connect to our GitLab server

   * If you have an account, [login](https://gitlab.e.foundation/users/sign_in)
   * If not, [create one](https://gitlab.e.foundation/e)

## Go to our issue dashboard

   - /e/OS issues are listed and created [here](https://gitlab.e.foundation/e/backlog/-/issues)

   - Before creating a new issue, please search if it has previously been reported. To check for this

   - Type the bug in the search text box on the top of the screen as shown below

   ![New Search](/images/new_issue_search.png)

   - The screen will also display some other similar worded issues

   - If this search does not return any results create a new issue

## Create a new issue

   - Select a template for the issue from the drop down marked 'Choose a template'
   - 'Apply template'
   - Add a Title
   - Fill in all the details as requested in the template
   - Always [add logs](/support-topics/create-a-log) where possible. This helps the developers debug the issue better.
   - Once you have finished, click `Submit issue`.



## Getting an error message in new ID creation on /e/OS Gitlab?

   - Some users may get an error message, while attempting to create a new ID on the /e/OS Gitlab
   - The error message may ask you to contact Murena support. You can {% translate content.contact_helpdesk %} 
   - In case you run into such an error, please send a mail with the following details
      - Full Name:
      - Desired username:
      - Email:
   - <a href="mailto:helpdesk@murena.com?subject=Open%20GitLab%20account&body=Hi%2CI%20want%20to%20create%20a%20Gitlab%20acccount%20to%20report%20issue.%0A%0AFull%20Name%3A%20%0ADesired%20username%3A%20%0AEmail%3A%20">Create a Helpdesk ticket for GitLab account creation</a>



   - Let us explain the reason behind this error. Some time back we had an issue with a large number of dummy IDs swamping our servers. As a preventive measure we now allow registrations from a limited number of email domains. Users wanting to register with their private email address or say their Gmail address, can {% translate content.contact_helpdesk %} and the team will create an account for them. 



## Syntax and Source

   * [Source](https://docs.gitlab.com/ee/user/project/description_templates.html#setting-a-default-template-for-issues-and-merge-requests)
   * [Markdown (GitLab)](https://docs.gitlab.com/ee/user/markdown.html)

