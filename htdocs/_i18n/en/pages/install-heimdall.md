You can build the Heimdall suite yourself by following our documentation available [here](build-heimdall)
Once you have built the suite follow the below steps.

## Install

### On Ubuntu

Run the following commands to install heimdall for all users:

```
$ unzip /path/to/heimdall_ubuntu.zip -d /tmp
$ cp /tmp/bin/heimdall* /usr/bin/
$ rm -rf /tmp/bin
```

### On MacOSX

Run the following commands to install heimdall for all users:

```
$ unzip /path/to/heimdall_macos.zip -d /tmp
$ cp /tmp/bin/heimdall /usr/local/bin
$ rm -rf /tmp/bin
```

## Validate

In a terminal, you can run the following command to check that everything is working properly:

```
$ heimdall info
Heimdall v1.4.2

Copyright (c) 2010-2017 Benjamin Dobell, Glass Echidna
http://www.glassechidna.com.au/

This software is provided free of charge. Copying and redistribution is
encouraged.

If you appreciate this software and you would like to support future
development please consider donating:
http://www.glassechidna.com.au/donate/

Heimdall utilises libusbx for all USB communication:
    http://www.libusb.org/

libusbx is licensed under the LGPL-2.1:
    http://www.gnu.org/licenses/licenses.html#LGPL
```
