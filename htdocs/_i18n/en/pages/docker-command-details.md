## Objective of the document
When running the Docker command to build an /e/OS image we need to pass a few parameters. 

These environment variables can at times be confusing to the user.

Here we explain what some of the important commands mean. 

{% include alerts/tip.html content="Parameters marked with an * are mandatory " %}

## List of environment variables

**DEVICES**: the device codenames, separated by a space. This parameter is mandatory.

**BRANCH_NAME**: the android version to build (v1-q, v1-pie, v1-oreo or v1-nougat). If not set, it will build for all android version available for each device.

**INCLUDE_PROPRIETARY**: to vendors from the TheMuppets (not used since we have our own)

**MINIMAL_APPS**: Used to ship a device with Minimal Apps. Set to false by default.

**ENG_BUILD**: run an engineer build. Will enable adb by default Set to false by default

**OTA_URL** : To run an OTA server for your build. 

> Format would be `OTA_URL=ota-server-url`


## Conclusion

This list is not complete. A few parameters which are not required or relevant to the users or ROM builders are not included here

Let us know if you need more clarification on some of these or other parameters and we will add them to this document. 

To checkout the full list of available parameters have a look at [this document](https://gitlab.e.foundation/e/os/docker-lineage-cicd/-/blob/master/Dockerfile.community)

To write to us use the {% translate content.contact_helpdesk %} with your suggestions.