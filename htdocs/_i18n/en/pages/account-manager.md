
![](/images/ic_launcher_round.png)

## One application to rule them all!

/e/OS comes with Account Manager. This application allows you to add your account(s) from any provider and sync your data on your /e/OS device.

This way you can check your new emails in real time, see when is your next meeting, call your new neighbour whose number you had saved on another device and update your tasks in real time.

And all this while [respecting your privacy](https://e.foundation/legal-notice-privacy/#account-manager): your data is stored locally on your device and is not shared with any third party servers except of course as required by Murena Workspace, Google, Yahoo or any other provider for authentication and synchronization purpose.
