### Advantages of a Murena Workspace account

* having a new brand email address @murena.io
* having a single identity to access all Murena Workspace services :

   - Murena Workspace Email : username@murena.io
   - Murena Workspace Drive : photos, videos, documents...
   - Murena Workspace Calendar
   - Murena Workspace Contacts
   - Murena Workspace Notes
   - Murena Workspace Tasks

* in your /e/OS smartphone and online at : [https://murena.io](https://murena.io)

### How to get your free Murena Workspace account ?

* Go to [https://murena.io/signup](https://murena.io/signup) you should see a screen like this

![](images/murena-cloud-account-request.png)

* And follow the below steps :
  - enter your current email address
  - reenter your current email address
  - solve the math equation for human verification
  - on success, you should see a screen like this

![](images/murena-cloud-account-successful-request.png)

* Wait for an invitation in your mailbox ! Usually it takes < 2 minutes. Check your SPAM box !

### Get Started

* After you clicked the link in the invitation email you can proceed to the actual account creation :

![](images/create-murena-cloud-account.png)

### Creating your own Murena Workspace account

* Fill the fields correctly! This example will create a Murena Workspace identity as John Doe <john.doe@murena.io>
* Your username may contain Latin letters \(a-z\), digits \(0-9\) and special characters (\- . \_)
* Your username must be at least 3 characters long and at most 30 characters long
* Your username must not contain special characters in succession
* Your username must not start or end with special characters
* Please note that your username will be automatically converted to lowercase during account creation \(i.e. "John.Doe@murena.io" becomes "john.doe@murena.io"\)

![](images/your-murena-cloud-account.png)

* On successful account creation you should see this screen

![](images/murena-cloud-account-success.png)

### Using a Murena Workspace account

* Now that your Murena Workspace account is created, you can use it in your /e/OS smartphone 
  - In the first time usage wizard just after /e/OS installation
  - Or you can add account by going to:
     ````
     Settings >> Accounts >> Add account >> Murena.io
     ````

* On the web at [https://murena.io](https://murena.io) to retrieve all the pictures, videos you took with your /e/OS smartphone
* To access on the [Murena Workspace](https://murena.io) all you emails and check your calendar

### Having trouble creating an Murena Workspace account?

* If you never received the invitation on your current email, check your email client SPAM folders
* If you cannot login in your /e/OS smartphone or at [https://murena.io](https://murena.io) try to reset your password with the reset link available at [https://murena.io](https://murena.io)
* Make sure that you use your username@murena.io as email for the reset process, NOT your recovery email.
* If you are still facing issues you can {% translate content.contact_helpdesk %}

### How can I delete my Murena Workspace account?

* Log in to your [Murena Workspace account](https://murena.io) using the ID you want to delete
* Take a backup of your data and emails (if required) before proceeding
* Click on your profile image
* This will open up a menu
* Click on Settings
* This will display the Setting options as shown in the screenshot below

![](images/delete-murena-cloud-account.png)

* Click on "Delete Account"
* This will open up a screen with the text "Check this to confirm the deletion request"
* Select the check box
* Click "Delete my Account" button
* The operation could take up to a minute. Please wait
  >  This action will remove your Murena Workspace account, data stored on the Murena Workspace if any and all emails.

* You will be logged out of [murena.io](https://murena.io)
* The Murena Workspace account deletion is complete
> If you are still facing issues you can {% translate content.contact_helpdesk %}

### I have a Murena Shop account created with this Murena ID. What happens if I delete my Murena Workspace account?

{% include alerts/tip.html content="Please note that in case you have an active subscription, the Murena Workspace account cannot be deleted. You have to wait for the subscription to expire or cancel it." %}


With an expired or cancelled subscription if you choose to delete your Murena Workspace account you have two options:

`Option 1:` Delete your Murena Workspace account only, while retaining your Murena Shop account

![](/images/howtos/murena_account_deletion_option_1.png)

 - You can delete your Murena Workspace account but retain the Murena Shop account
 - The recovery email address you had set will be your new Murena Shop account's login
 - You will be sent a link to reset your password.
 - Future invoices will be sent to this recovery email address
 - Your Murena Workspace account will be deleted.
 - You can continue using your Murena Shop account with this recovery email address. 
 
`Option 2:` Delete your Murena Workspace account as well as your Murena Shop account.

![](/images/howtos/murena_account_deletion_option_2.png)
  - If you have any orders, you can check them and download invoices using the links `[0]`(for murena.com) and `[1]`(for murena.com/america)
  - Both your Murena Workspace account and your Murena Shop accounts will be deleted

### Some more tips on your Murena Workspace account

* Your Murena Workspace identity at murena.io is not (yet) compatible with our various websites and forums.

* This means that you will have to create specific login/password at [community.e.foundation](https://community.e.foundation) and [gitlab.e.foundation](https://gitlab.e.foundation)
* Do not try to login in your /e/OS smartphone with your community or gitlab identity. It will not work.

### Want to manually configure your mail client for Murena Workspace?

Most mail clients should be able to configure your Murena Workspace ID automatically, once you input your Murena Workspace username and password. In case that is not possible, please use these settings to manually configure:

SMTP/IMAP server: mail.ecloud.global

SMTP/IMAP username: your Murena Workspace email address in full (example: john.doe@e.email or john.doe@murena.io)

SMTP/IMAP password: your Murena Workspace email password

SMTP port: 587, plain password, STARTTLS, requires authentication

IMAP port: 993, SSL/TLS, requires authentication


### Storage Options
* Your free account will be **limited to 1GB** online storage. 

* We now have some premium storage plans available. You can also **upgrade it to 20GB** while supporting the /e/OS project by becoming an [/e/ Early Adopter](https://e.foundation/donate).

**PLEASE KEEP IN MIND** that /e/OS Free drive and mail accounts are supported by donations! 

Please [support us now and receive a gift](https://e.foundation/support-us).

### Is a Murena Workspace account mandatory to use /e/OS ?
A Murena Workspace account at murena.io is not mandatory to use /e/OS, but is highly recommended if you want to benefit from the full Murena experience as described in this guide.


### What about e.email accounts, are they still valid?

Murena Workspace is the new name of Murena Workspace. Users with an e.email account, or legacy accounts, can still use their e.email account with no issues on Murena Workspace.

Legacy e.email accounts have both e.email address (by default) and now a new murena.io alias (needs to be enabled in account settings).

Also, legacy e.email account benefit too from the [Hide my email](/support-topics/hide-my-email) feature and [new features on Murena Workspace](/support-topics/additional-ecloud-apps).

To add an e.email account on a phone with /e/OS, just follow the standard process: go to the account manager, add an Murena Workspace account, enter the e.email ID and your account password.

To sign in to your Murena Workspace account, just browse to [https://murena.io](https://murena.io) with your e.email user ID. [https://murena.io](https://murena.io) replaces [https://ecloud.global](https://ecloud.global)


### Some /e/OS email specific guides

- [Manually configure /e/OS email](/support-topics/configure-email)
- [Adding contacts to /e/OS emails](/support-topics/add-contact-to-email)
- [Adding a Gmail account](/support-topics/add-a-gmail-account)
- [Setting up /e/OS email filters](/support-topics/email-filters)
- [Bypassing incorrect spam filtering](/support-topics/bypass-false-positive-spam-filtering)