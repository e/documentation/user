{% include alerts/tip.html content="We have used screenshots from windows 10 OS for this guide." %}

{% include alerts/tip.html content="adb and fastboot come as part of Android SDK Platform-Tools" %}

## Download
Get the latest version of the **Platform Tools** specific to your Operating system from the [Android developers  website](https://developer.android.com/studio/releases/platform-tools)


{% include alerts/tip.html content=" Windows users also will need the proper drivers installed on their computer. You can check the options available [here](https://adb.clockworkmod.com/)" %}

## Create a folder

Create a folder in a location which would be easy to access.

Ideally you should do it on your c: drive and give it an easy to remember name

In this screenshot we have created a folder named platform-tool in the c: drive

![](/images/howtos/adb/create_folder.png)

## Extract the zip
- Next we will extract the zip file downloaded in the previous step into the newly created folder

    The extraction ( you do this by double clicking the zip file and showing it the location where to place the extracted files )

    ![](/images/howtos/adb/browse_to_folder.png)

- Wait for the files to extracted completely. You would see a green progress line as shown in the screenshot  while the extraction happens.

    ![](/images/howtos/adb/extract_adbfiles.png)

## Run the commands

- Browse to the folder where you have extracted the files. In our example it would be the `Platform-tools` folder in `C:` drive

- You will need to open a command prompt window inside this folder for adb or fastboot to work.

### Open a command prompt window inside a folder in windows

- One of the ways to open a command prompt window inside a folder in windows is to check the address bar

  - A normal address bar in Windows will show you the file path of the current folder

    ![](/images/howtos/adb/start_cmd_1.png)

  - Select the address bar and type `command prompt`

    ![](/images/howtos/adb/start_cmd_2.png)


- This will open a command prompt window as shown here

    ![](/images/howtos/adb/cmd.png)

    In this command prompt window you execute your adb or fastboot commands.

    ![](/images/howtos/adb/adb_executed.png)

## Adding adb path to the environment variables

{% include alerts/danger.html content="Please note making changes to environment variables, if not correctly done can cause serious stability issues to your computer. Proceed with caution." %}

One of the problems with the method given above is that the every time you will need to open the command window inside the Platform-tools folder.

A way to improve this process is to add the address of this folder in your `environment variables`


- Click on the `Start` icon ..this will open the list of applications as shown here
    ![](/images/howtos/adb/desktop.png)

- Scroll to the `Settings` icon...this is gear shaped icon as seen highlighted in this screenshot ..click it

    ![](/images/howtos/adb/settings.png)

- This opens the below screen . Go to the `System` settings. This would be the first option in the list as seen in the screenshot here

    ![](/images/howtos/adb/system_in_list.png)

- Clicking `System` Settings opens this screen
- Scroll down on the list on the left to `About`

    ![](/images/howtos/adb/about.png)


- In the `About` screen scroll down on the right until you see `Advanced System Setting`


    In the Properties screen scroll down to the Advanced System Settings . Click the link

    ![](/images/howtos/adb/advanced_system_settings.png)

    This will open a dialog box titled System Properties as shown here.

    One of the buttons is captions `Environment Variables`  . Click the button

    ![](/images/howtos/adb/env_variables.png)

- Clicking `Environment Variables`  opens the below screen .. In the lower box select `Path` ...as shown in this screenshot

    ![](/images/howtos/adb/env_path.png)

- Click the `Edit` button
- This will open the below screen

    {% include alerts/danger.html content="Ensure none of the option in this screen are selected when you make edit now. You have to create a new variable NOT edit existing variables. To un select click anywhere outside the list.  "%}
- To repeat do not select any of the existing variables.

    ![](/images/howtos/adb/env_path_1.png)

- Click on the `Browse` button and search for the folder where you extracted the adb files

    ![](/images/howtos/adb/env_path_2.png)

- You should have a new entry in the list as shown here.

- This entry would have the location of the folder where you placed the adb extracted files.

    In our case we had created a folder called platform-tools and that shows up in these screen shots

    ![](/images/howtos/adb/env_path_3.png)

- Select the folder and click `Ok` three times to return to the Desktop

- Now you should be able to execute the adb commands from any where on the system.

    You can test this by opening a command prompt window and typing the adb commands.
