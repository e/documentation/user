## Where do the Apps in /e/OS installer application come from?

Apps available in the /e/OS Installer are open source apps that you can find on Fdroid, and free Android apps, same as you can find on  Google Play Store. The /e/OS app installer relies on a third-party application repository called [cleanapk.org](https://info.cleanapk.org)

Some mobile web apps and progressive web apps (PWA) are also available through the /e/OS application installer.

## Where is the source code base of the /e/OS installer application?

You can check the code here [https://gitlab.e.foundation/e/apps/apps](https://gitlab.e.foundation/e/apps/apps)

## Is there an easy way to find if an application I want is available in the /e/OS App Store ?

You can search for the name of the application in the [Application Checker](https://e.foundation/e-os-available-applications/)

## How can I request an application to be added to the library?

Users can request  a specific app  to be listed by making a query directly in the /e/OS application installer (**Settings** > **Request App**)

## What information is provided for each app within the /e/OS application installer ?

At the moment, each app features a Privacy rating. In the future, user rating and energy score will also be available. We will also provide classic user ratings to help user make a better choice

Privacy ratings provide a list of trackers and requested permissions that come in as part of installing these apps (Note: those are computed by Exodus Privacy software). Energy rating will provide details of how much energy is consumed by the app to work. Please note  some of these features are currently under development.

## How can I make sure  apps in the installer are not tampered with but ‘original'

Apps are checked either using a PGP signature check, or a checksum.

In case you suspect an app  to be tampered, we would request you to please {% translate content.contact_helpdesk %} and we will take appropriate action keeping you informed.

### Do the phones with /e/OS  support fingerprint, banking apps and Android Auto?

### Would I still be able to use apps like Uber? 
### Is there a away to use apps that I've purchased from the Google play store, or maybe have in app purchases?

Most apps are compatible and work. We're working with microG's founder to improve the support and compatibility.

You can check app availability [here](https://e.foundation/e-os-available-applications/)

You can see a list of what apps work and what do not [here](https://community.e.foundation/t/editable-list-of-apps-that-work-or-do-not-work-with-microg/21151)

Some users also install Aurora Store to access any free Android app at Play Store.
## What should you do  if you don't find an app you would like to use in the /e/OS app  installer ?

If you can't find an Android app you would like to use, you can first request it in the app installer

The process to request an app on /e/'s App Store is as under
- Tap the Apps Icon
- On the Apps task bar

    you will see `Settings` > `Request App`

    and in the text box provide the `package name` of the app you want to be added.
- To get the package name you can
    - Go to the Google Play store.
    - Select the app download page of your app.

      > For e.g. let us assume you want to add the [Radar Covid app](https://play.google.com/store/apps/details?id=es.gob.radarcovid) to the /e/OS Apps Store

    - When you open the app download page on the Google Play Store check the page url - the package name will be the last couple of words which in this case would be  `es.gob.radarcovid`
    - Copy this and paste it in the text box
    - Press the `Submit` button

    > You will see a success message displayed as the request is accepted.

    > In case the app has already been requested you will see a message mentioning that it has already been requested.

- An option is also to look for a web alternative . For instance, several services  are available with  progressive web apps that can be used on the mobile.: Uber, Twitter, Starbucks, Some banks are alos offering web services as  an alternative to native apps. For  easier access, a shortcut to the web service can easily be added to the /e/OS BlissLauncher, using the web browser.

## How long would it take for an app I requested to show up in /e/OS Apps Store

It takes about 1 week for the app to show up in the /e/OS Apps Store after the request.

If an app does not show up in the /e/OS Apps Store even after a couple of weeks of submitting the request, please raise an issue in [/e/ Gitlab](https://gitlab.e.foundation/e/backlog/-/issues/new) with all the details.

## What is the Privacy Score and how is it calculated ?

To understand what the Privacy Score is and how it is calculated, Please refer [this guide](/privacy_score)

## The /e/OS Application Installer downloads packages from cleanapk.org Who is behind that, and why would a privacy-conscious person trust it?

They probably want to stay private. Our strategy with this is:

  1. To ask our users to report any issue with some package delivered by CleanAPK, for instance if a package is suspected to be modified from the original source. So far (over 2 years) the only issue we have seen have been some outdated packages.

  1. We are trying to find (technical) ways to prove that an APK has been unmodified from source. This is easy with packages that come from F-Droid because there is a signature, but it's not for packages that come from Play Store.

  1. We are evaluating different options for apps in the future. That could be a new "store" where publishers could push their apps, an alternative to Google Play. We also support a lot of PWAs in /e/OS.

  1. We are waiting for authorities to come out with more regulations on this. For instance in the EU, it is likely that Google and Apple will have to allow users to use alternative stores of applications.

## How did /e/OS learn about CleanAPK? Never heard of it before and it doesn't look like they spend a lot of effort on advertising :)

People contact us all the time about new projects, and we decided to go ahead with CleanAPK as a default apps installer with the best possible user experience for the largest number of users.

## Where can I find more information about CleanApk?
You can browse for more details on their [website information page](https://info.cleanapk.org/)

## Does /e/OS have any plans to launch an app store for paid apps? It seems like you could attract many developers/publishers by taking a smaller cut of the purchase price than Google does on their app store.
Yes, we may plan something like that in the future

## Progressive Web Apps (PW)

Another option is to look for a web alternative. Several services  are available with  progressive web apps that can be used on the mobile.: Uber, Twitter, Starbucks....  

Some banks are also offering web services as an alternative to native apps. 

For easier access, a shortcut to the web service can easily be added to the /e/OS BlissLauncher, using the web browser.


## Alternate Application Stores

Some users report some success using applications downloaded using the Yalp or Aurora Store. Please note these stores will require the user to authorize third-party application installation in /e/OS settings. 

Other options users can try out include UpToDown, APKpure...

