
## What is adb?
**Android Debug Bridge** or `adb` is a command line tool which allows a user to communicate with a device. 

`adb` provides a user access to a number of device actions including

- installing
- Debugging 

`adb` provides a [UNIX](https://unix.org/) shell for the user to run its list of commands.

`adb` is a client-server program. It has a console where the user types the commands and a server which works in the background executing these commands and interacting with the device. Even if the user closes the client - the console, the server would continue to function in the background. To stop the server the user has to execute specific commands.

`adb` tool once executed runs for the duration of the user log in. Which means if the user closes the PC the server will stop. The next time the user will have to again start `adb` using the relevant commands. 

`adb` has three components:
 - **client**

   The user executes the commands here. This part runs on the development machine. The client is initiated by running the command on a command line.
 - **deamon (adbd)**

   which runs commands on a device. The daemon runs as a background process on each device

 - **server**

   The background process which manages the communication between the client and deamon

## Where can I download adb from ?

`adb` comes with as part of the installation in some linux flavors like Ubuntu. That being said it is always recommended to download and install the latest adb tools. 
Not using the latest or compatible version of `adb` can cause serious stability issues on some devices. 

You will find the details on how to install `adb` in the links shared in the next section

## How to Install adb

- [Linux based systems](/pages/install-adb)
- [Windows OS](/pages/install-adb-windows)


## Additional References
To read in detail about adb refer this [external guide](https://developer.android.com/studio/command-line/adb)