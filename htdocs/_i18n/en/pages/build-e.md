## /e/OS Build Types

{% include alerts/tip.html content=" To understand all about the various /e/OS Build types check out [this guide](../build-status) " %}


## Requirements for different builds:

### Custom builds

   * The source code has been modified and if the ROM is redistributed, we have several requirements:
   * Any reference to /e/OS, including “/e/OS”, and our logo must be removed from the build
   * The redistribution must not let people think that it is /e/OS.
   * **The ROM name must be changed** and possibly say that it’s been forked from /e/OS
   * Calls to our OTA server infrastructure should be removed
   * Custom ROMs can be built on /e/OS servers only for exceptional reasons (special development…).

### Unofficial /e/OS builds
   *  Source code can be modified only to reach full support on the target device.
   *  /e/OS features, list of installed apps etc. shouldn’t be modified
   *  Unofficial builds can be published on the /e/OS website at a specific place with warnings and a list of what works/what doesn’t work
   *  It doesn’t have nightly builds nor OTA updates.

### Community builds
  *  The level of quality is considered high, security updates are applied if possible,
  *  There may or may not be a ROM maintainer
  *  Community builds have nightly builds and can be updated OTA
  *  Source code have to be hosted on our GitLab instance, or on trusted sources (LineageOS GitHub group, AOSP).

### Official /e/OS builds
  * The level of quality must have reached our conditions.
  * It must have an official maintainer.
  * Official builds have nightly builds and can be updated OTA.
  * Source code have to be hosted on our GitLab instance, or on trusted sources (LineageOS GitHub group, AOSP).

Any question about this? [contact us](https://e.foundation/contact/)

## How to build the ROM?

> WARNING : This process will only work on **case-sensitive** filesystems!
> * Windows: will not work
> * macOS: doesn't work either for HPS+ filesystem - adfs not tried
> * Linux: work on Ubuntu and CentOS

> System requirement: Your computer needs to be 64-bit and running a 64-bit operating system with at least 400GB of spare hard drive space and 16GB of RAM.

### 1. Install docker

If you have not already done so, [install docker](https://docs.docker.com/install/)

### 2. Get our docker image

{% include alerts/tip.html content="Please run this step before each build, to be sure to get the last docker image."%}

```shell
$ sudo docker pull registry.gitlab.e.foundation:5000/e/os/docker-lineage-cicd:community
```
### 3. Find your DeviceCode
The device code can be found on [/e/OS devices list](../devices) or by running the following command from adb:

 `$ adb shell getprop ro.product.device`
### 4. Create directories

```shell
$ sudo mkdir -p \
/srv/e/src \
/srv/e/zips \
/srv/e/logs \
/srv/e/ccache \

```
### 5. Extract proprietary blobs

Some device vendors for example Xiaomi have blocked access to their vendor files as some of the files may be proprietary.

To get around this you will have to manually extract vendor files from your device.

{% include alerts/tip.html content="This step requires to have a device already running the latest /e/OS, based on the branch you wish to build for. If you don’t have access to such a device, refer to [Extracting proprietary blobs](extracting-proprietary-blobs) from an installable zip. " %}

Now ensure your device
- is connected to your computer via the USB cable
- has [ADB and root enabled](/pages/install-adb)
- and that you are in the `/srv/e/src/<version>/device/<vendorname>/<my-device>` folder
- now run the `./extract-files.sh` script

The blobs should be pulled into the `/srv/e/src/<version>/vendor/<vendorname>/<my-device>` folder.

Once you have fetched the vendor blobs, you can (re)run the build.

### 6. Start build

Run the following command. Don't forget to replace `<my-device>` with your device code !

```shell
$ sudo docker run \
-v "/srv/e/src:/srv/src" \
-v "/srv/e/zips:/srv/zips" \
-v "/srv/e/logs:/srv/logs" \
-v "/srv/e/ccache:/srv/ccache" \
-e "BRANCH_NAME=<tag>" \
-e "DEVICE_LIST=<my-device>" \
-e "REPO=https://gitlab.e.foundation/e/os/releases.git" \
registry.gitlab.e.foundation:5000/e/os/docker-lineage-cicd:community
```

List of tags to use for `BRANCH_NAME` is available at [https://gitlab.e.foundation/e/os/releases/-/tags](https://gitlab.e.foundation/e/os/releases/-/tags).



{% include alerts/tip.html content="We now use git tags in addition to a specific manifest for each release. It let us to know exactly what's inside each build." %}

> If you want to build a test version, you can use:
> - `BRANCH_NAME` set to v1-nougat, v1-oreo, v1-pie, v1-q, v1-r, v1-s, v1-t
> - `REPO=https://gitlab.e.foundation/e/os/android.git`

---

To understand what these parameters mean, check the document given [here](docker-command-details)

Example for Google Pixel 5
```shell
$ sudo docker run \
-v "/srv/e/src:/srv/src" \
-v "/srv/e/zips:/srv/zips" \
-v "/srv/e/logs:/srv/logs" \
-v "/srv/e/ccache:/srv/ccache" \
-e "BRANCH_NAME=v1-t" \
-e "DEVICE_LIST=redfin" \
-e "REPO=https://gitlab.e.foundation/e/os/releases.git" \
registry.gitlab.e.foundation:5000/e/os/docker-lineage-cicd:community
```

### 7. Build options

You can now customize applications installed by default in /e/OS.

* if you want to add extra applications to the default applications: add your APK to the android_prebuilts_prebuiltapks/ directory, and set the `CUSTOM_APPS` environment variable accordingly in the Docker image, before building.
* if you want to keep a minimal /e/OS build, set the `MINIMAL_APPS` environement variable to true (default is false). For now it's removing LibreOffice viewer, PDFViewer, Maps and Weather.

### 8. Get your image!

When your build is finished, please find your images inside `/srv/e/zips/<my-device>` folder. To install, please refer to our [documentation](../../devices).



## Need more help

If you need help, please check with other ROM builders who are active [on our forum](https://community.e.foundation/c/e-devices/unofficial-builds/92) and build /e/OS unofficial ROM's. Internet search for guides on android ROM building would also turn up sites which should be helpful.

To find more information about our docker image and its environment variables click [here](https://gitlab.e.foundation/e/os/docker-lineage-cicd).

To report an issue about a build, please refer to [this guide](report-an-issue)
