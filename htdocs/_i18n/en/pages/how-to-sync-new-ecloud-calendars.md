When a new calendar is created on Murena Workspace, it is not automatically available on the phone. Here the process to sync it:

1. From the device, open `Settings` > `Accounts`

    ![](/images/screenshot_20200624_15_22_06.png)
    ![](/images/screenshot_20200624_15_22_09.png)
1. Tap on your device account (not the Contacts entry!)

    ![](/images/screenshot_20200624_15_22_12.png)
1. Tap `Account Settings`

    ![](/images/screenshot_20200624_15_22_16.png)
1. In the new Window, on your /e/OS email address

    ![](/images/screenshot_20200624_15_22_20.png)
1. Go into `Calendar` tab

    ![](/images/screenshot_20200624_15_22_23-bis.png)
1. Refresh the calendar list by clicking on the `3 dots button` (to right) > `Refresh calendar list`

    ![](/images/screenshot_20200624_15_22_26.png)
1. When few seconds. Your new calendar is now available!

    ![](/images/screenshot_20200624_15_22_23.png)
1. The new calendar is also available in the Tasks app

    ![](/images/screenshot_20200624_15_22_35.png)
