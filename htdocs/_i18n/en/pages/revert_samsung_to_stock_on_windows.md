{% include alerts/danger.html content="These instructions are only for Windows PC users."%}
{% include alerts/tip.html content="The installation will wipe all data on your phone. Take necessary backups of your data if required."%}

## Assumptions
- The user has a working samsung device with /e/OS on it.
- The user wishes to revert their Samsung device to Stock ROM
> Stock ROM here refers to the ROM that is provided by the vendor.

{% include alerts/tip.html content="The process of installation was tested on the S9 and S9+ . It should work on all Samsung devices." %}

## Downloads

1. Samsung stock ROM
    > We downloaded the ROM from the [sammobile site](https://www.sammobile.com/firmwares/)

    > The site may require an account to be created. You can sign up for a free account.
{% include alerts/warning.html content="Ensure you download the latest stock ROM build specific to your country or location. "%}

1. [ODIN](https://odindownload.com/)
> The version we used for this test was [3.14.4](https://www.androidfilehost.com/?fid=14943124697586351949)

1. Samsung [USB drivers](https://developer.samsung.com/mobile/android-usb-driver.html)

## Pre Requisites
- Windows 10 PC
> We tested this on a windows 10 PC.
- Odin installed and working on the PC
> We tested with ODIN version 3.14
- Internet connection on the PC
> Required to download any device specific drivers to make it run on the PC
- Working data cable
> There are cables which are used only for charging. Ensure the cable you use allows data and file transfer

**IMPORTANT**
- Keep the phone fully charged **before** starting the installation   process
- Disable Samsung Kies if installed on your Windows PC
- Disable any anti virus programs running on the Windows PC. This includes the default

## Steps
1. - Opening up the Sammobile site to download the Samsung stock ROM will show a long list of downloads.  
    - Choose the download specific to your country

    ![](/images/0_sammobile_site.png)

1.  This is how your Samsung smartphone would appear at this moment.
    - You have not yet started the installation
    - In this screenshot we have a /e/OS oreo build already installed on a s9
    - Remove any passwords or PIN you may have set to unlock your device.
      > You can always set these up later after the installation

    ![](/images/1_s9_with_oreo.jpg)

1. Disconnect the phone if attached by the device cable from the PC

1. Create a folder and unzip the downloaded Stock ROM inside this folder
    - You should see files in the folder similar to the screenshot below

    ![](/images/1a_s9_unzip_stock_rom.png)

1. Extract ODIN from the zip file.
1. Browse to the folder where you have extracted ODIN and run Odin.exe
1. Power off your phone.
1. Power on the phone into Download mode.

    > For example if it is the S9 then press `Bixby` + `Power` + `Volume down`. The `Bixby` button is the one below the volume button on the s9

1. On the blue screen you will be asked to `Confirm` by clicking the `Volume +` button
1. Connect the phone to the PC using the data cable
1. ODIN should detect your device and assign a COM Port to it. In the screen shot below you can see COM5 assigned to the attached s9

    > The port number may change from PC to PC and on individual USB ports.

    ![](/images/2_s9_odin_start.png)

1. In the ODIN UI you would see  buttons with the text `BL`, `AP`, `CP`, `CSC`, `USERDATA`
    - Select the AP tab from the Odin and import AP file
    - Select the BL tab and import BL file.
    - Select CP tab and import CP file there.
    - Click on the CSC tab and import the Home_CSC file that is already in the firmware folder.
1. In Odin, tick on Auto Reboot and F.Reset.Time options for your Odin options

    Now that you have set up ODIN to install you need to start the installation

1. Click on `Start` button

    Your phone screen will look like this while the installation is running. Notice a thin white line marking the completion of the download and installation

    ![](/images/3_s9_screen_whie%20downloading.jpg)

1. ODIN will also mark the progress and completion with a `PASS` showing up as visible in the screenshot below

   ![](/images/4_s9_odin_complete.png)

1. This marks the completion of the installation.
1. Wait for a few minutes as your phone will now reboot and you should be at this screen which marks the start of the Stock ROM Set wizard on the Samsung smartphone

   ![](/images/5_s9_stock_installed.jpg)

{% include alerts/tip.html content="On some devices if you are stuck at the boot screen, you can try to do a factory reset from within the recovery to get the phone to boot."%} 
1. You can remove the cable at this point as there is no further requirement for the PC or ODIN. You can start setting up your smartphone.
  
