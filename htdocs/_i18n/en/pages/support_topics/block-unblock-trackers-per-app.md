1. Head to the system settings.
   
   ![](/images/tips/navigate-to-settings.png)
   
1. Scroll down a bit and tap on Advanced Privacy.

   ![](/images/tips/navigate-to-advanced-privacy.png)

1. Tap on Manage apps' trackers.
   
   ![](/images/tips/tap-manage-app-trackers.png)

1. Select the app for which you want to allow or block trackers.
   
   ![](/images/tips/tap-on-app-name-to-manage-trackers.jpg)

1. Advanced Privacy blocks trackers found within all apps by default when you first activate it. You can allow a tracker/domain using a toggle located next to the tracker's name. The tracker/domain will be allowed only for the app for which you change the settings, other apps remain protected. If you choose to stop blocking all trackers, your custom settings for trackers will be saved for next time you activate the tracker toggle on main page/widget.
   
   ![](/images/tips/allow-or-block-trackers-using-toggles.jpeg)
