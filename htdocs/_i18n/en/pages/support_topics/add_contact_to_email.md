Murena Email uses contacts from your Contacts application when sending emails. 

Here you will find some of the methods in which contacts can be added to emails.
### When writing an email

- If you start writing an email address for example in `to:` field for a new email, on typing the first couple of alphabets,it will auto-complete the full email address. 
- If multiple emails ID start with the same alphabets it will show you the different email IDs and you can choose the correct email address.

### If you do not remember the email
When you do not remember how the name or email of a contact starts, you can: 
-  Go to `Contacts` page
-  Find the right contact
-  Then click on the icon with an arrow inside a box
-  Next to their email address. That will open the email link.
<br>

<div class="text-center">
  <img src="/images/email_link_button.png" alt="">
</div>
<br>

### If your computer has not been set up to open email links

To set up your computer, to open email links with murena.io
- Go to [murena.io](https://murena.io) email page in your browser
- Next click on the icon that will let you set this page as default email application
<br>

<div class="text-center">
  <img src="/images/allow_open_email_links.png" alt="">
</div>