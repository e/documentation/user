# Murena Workspace referral program

Get up to 40€ of credits for your cloud storage by inviting your friends to Murena Workspace!

For every friend who joins and creates a Murena Workspace account, you will earn 2€ to be used for
cloud storage on Murena Workspace.

Murena Workspace is your personal email account, your agenda and contacts, your drive on the cloud and
your online office suite, all combined into one single service, simple to use. Murena Workspace is powered by
proven open-source software like NextCloud and OnlyOffice.

Your Murena Workspace account is free up to 1GB of storage. Our paid plans start at 1,99€ per month for
20GB.

## How does this work?

Once you have a valid murena.io/e.email account, it is very easy.

All you have to do is sign up at [https://murena.com/my-account/](https://murena.com/my-account/), our ecommerce platform, so we can assign your cloud credits.

In your murena.com account space, locate the section ‘Referral’ and check your unique and
anonymous link.

You can copy your unique link easily and use it elsewhere, for example, emailing it to your
friends or even sharing it in a tweet.

For every friend who uses this link to create a free account, you will receive 2€ of cloud
credits in your murena.com account.

## How do I invite my friends?

It is very simple, you just have to send the unique registration link located in your murena.com
account. The link is completely anonymous, it does not collect identifying information such as
name or email address.

## How can I use the credits?

Your credits can be used once you subscribe to an extended storage plan. They will be
converted into discount towards your paid subscription.

## How long are my credits valid?

Your credits will be valid for 24 months.

## How long will this referral program last?

The Murena Workspace referral program is a permanent program.

## Can I redeem my credits against the purchase of a phone or something else at murena.com?

Your credits are only valid towards the subscription of paid Murena Workspace plan.

## How does it work for my friends?

Upon clicking on the referral link, your friend will be taken to the account creation page where
she/he will be able to create a user ID and password.

Upon completing the account creation process, your friend will have a link to a Murena Workspace account where she/he can access the newly created murena.io account, the cloud storage or get started with the OnlyOffice suite.

## How do I create a Murena Workspace account?

It is very simple. Visit [https://murena.io/signup](https://murena.io/signup) to request your invitation. You will only be included in the referral program once you also sign up at [https://murena.com/my-account/](https://murena.com/my-account/) and use your referral links.

## What is Murena Workspace?

Murena Workspace is a complete, fully “deGoogled”, online ecosystem.

Murena Workspace is your personal email account with the murena.io/e.email domain, your agenda and contacts, your drive on the cloud and your online office suite, all combined into one single service, simple to use. Murena Workspace is powered by proven open-source software like NextCloud and OnlyOffice.

## Is the referring link anonymous ?

The link is completely anonymous, it does not collect identifying information such as name or
email address. The person sending the link has no visibility into which people have used the
referring link.

## I don’t see my credits in my murena.com space, what shall I do ?

Contact us using this [contact form](https://murena.com/contact-us/) so we can investigate and see how to solve this issue
for you.
