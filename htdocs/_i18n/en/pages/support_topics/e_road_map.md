### Do you have plans to work with other privacy focused companies like Purism or Pine?

We have been in touch with both project teams.

We were interested in creating a /e/OS Librem but nothing came out of our discussions.

On Pine, we have a post with details of how to [port /e/OS](https://edevelopers-blog.medium.com/e-os-ports-for-the-pinebook-and-pinephone-596139c76479) to the PineBook. 

Based on our test of the /e/OS on the PinePhone, we found the current hardware was too slow to offer a decent user experience.


### Any plans on building a smartphone of your own?

A range of Murena smartphones that run /e/OS is available for sale [at https://murena.com](https://murena.com)

For most of these devices, we work with hardware partners like Fairphone and Teracube to offer the smoothest possible user experience.

### Is the /e/OS compatibility with Asiapac networks in Australia or New Zealand ?

You can check supported bands on Murena smartphones product pages. For other /e/OS smartphones, please refer to their specifications.

### Is it possible to see /e/OS on tablets that you make?

It is possible to install /e/OS on tablets. You can check some of the device builds [here](/devices).

A Murena tablet is now available for sale [at https://murena.com](https://murena.com)

### Are there any plans on offering a desktop mode like Samsung Dex

That is something we have in mind since the beginning of the /e/OS story. It is also regularly requested by some users and potential business partners.

This mode will be soon available on several devices. Please check our updates.

### Will you be adding e-ink support
We would love to support "slower experience devices" with e-ink once the team has more bandwidth to handle the tasks.

### Would the installation wizard offer different levels of configurations for user like default, intermediate, expert ....?

We plan to eventually offer the ability to uninstall default apps. 

### Will there be more phone models added to the Easy Installer in the future?

The development on the /e/OS Installer is also a community effort. This means that users with development skills contribute to the code. Addition of more devices depends on efforts from the community.  

### Would you be interested in aligning with other like minded project teams

Yes we are always ready to discuss. Feel free to get in touch with them and share the details with us on this [contact form](https://murena.com/contact-us/)

### Will /e/OS project launch more web services, such as Spot?

First we have to improve Spot (our default search engine, meta-search engine forked from Searx) as it is too slow. We have other services and products in mind. You can subscribe to our newsletter to receive latest news and updates from us.

### Does /e/OS have ASHA support for hearing aids?

Not yet unfortunately
