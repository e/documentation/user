This workaround is for /e/OS users on the French carrier SFR. 

Some of you may be facing an issue where using SFRs 2G network may cause the phone to freeze and become totally unresponsive with a black screen.

Please try this workaround:

- Open phone app
- Click the button for dialing, enter: `##4636##`
- Choose Phone Information
- Under `Set Preferred Network type`: choose `LTE/WCDMA`
- Click back 4 times
  
This will limit the network to be used to 4G (LTE) and 3G (WCDMA), so your phone won't be using 2G network.


The negative side to this workaround is that if you are very far away from cities, you won't be able to use 2G signal, however you can manually switch to 4G preferred (which can use 3G and 2G) from SIM card settings.