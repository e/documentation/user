<div class="text-center">
  <img src="/images/app_lounge_banner.png" alt="">
</div>

App Lounge is the second iteration of the application store embedded within /e/OS. It allows everyone to access millions of applications directly from their phone home screen.

It combines common Android apps, open source apps and even progressive web apps in one single repository. It is the only app store that does this today. Like with its predecessor, Apps, you don’t need to sign in to an account to download apps.

## What are we talking about?

When we speak about applications, we can refer to 2 types of apps: native applications that can be either “commercial applications” or open source applications, and progressive web apps (PWA).

Commercial applications are the usual free or paid Android apps you can find in the Google Play store or alternative app stores like Aptoide or UpToDown. These applications cover your most common needs like banking, travel, productivity, etc.... Free native apps often include advertising. Both free and paid apps often come with embedded trackers to profile you and follow you around.

Open source apps are Android applications that are released with an open source license . You can find open source apps in an alternative store like F-droid.

Finally, progressive web app or PWA is a type of application delivered through the web. It is intended to work on any platform that uses a standards-compliant browser, including both desktop and mobile devices. It is free from proprietary software from Google or Apple as these apps aren’t native apps. Unlike a native application that will need to be downloaded on your phone, a PWA works with a shortcut, similar to a web bookmark that you can store on your home screen and they run seamlessly on /e/OS.

## How does it works?

<div class="text-center">
  <img src="/images/app_lounge_sources.png" alt="">
</div>

App Lounge, formerly called Apps, now provides these apps, native android apps and web apps into a single service. There is no need any longer to install several app stores if you want to install commercial applications, Open Source apps or Progressive Web Apps (PWA) on your phone.

With this evolution, you can now access millions of apps from different catalogues in one single store. Just open App Lounge and get ready to browse for your favorite apps.

These three types of apps are installed from different sources.

1. **Commercial applications**: It’s the main improvement in App Lounge. It now gets commercial applications directly from the Google Play Store, relying on the Google Play API. You can install different types of commercial apps from this source. All applications are now available within App Lounge: no more “this app is missing” request! You can also, from now, install purchased applications, under certain conditions (check below).
The app list is shown depending on device architecture/hardware configuration and selected locale from the device. So users will be able to install the apps that are supported by their devices and preferred locale of the user.
2. **Open Source applications**: We continue to offer these applications within our store. The Open Source applications, coming from F-Droid, are currently delivered using the CleanAPK API. When it detects an application is available on both Google Play Store and F-Droid (based on the package name), we promote the Open Source version.
3. **Progressive Web Apps (PWA)**: In addition to usual Android apps, we also offer to users the possibility to install PWAs in /e/OS, like any other application. We want to promote this new way to deliver applications, which reduces the dependency regarding the operating system it runs on. PWA are delivered through the CleanAPK API.
Users will receive updates for all their applications. They will be notified when there are new updates available, and they will be downloaded and installed automatically in background.

## Native Apps from the Google Play Store catalog without a Google account

<div class="text-center">
  <img src="/images/app_lounge_sign_in.png" alt="">
</div>

It may seem counter intuitive for a deGoogled operating system to offer downloading apps from the Play store.

Well not really. The Play store is the widest catalog available. Distributing apps from the Play store ensures that applications are coming directly from the source and that they haven’t been tampered with.

As we want to offer users a way to access apps without tracking, we have an option to sign in to App Lounge Anonymously.  With this option, we are able to offer access to commercial applications without requiring a personal Google account: it means no tracking! This option gives only access to the free catalog.

In case a user had purchased paid apps in a previous life with a Google Android phone, we also provide him/her the option to sign in with a Google Account.  Please note: In order to purchase a paid app, users will need to use a Google account linked to another device that has Google Play installed.

## Be informed regarding privacy

<div class="text-center">
  <img src="/images/app_lounge_privacy_score.png" alt="">
</div>

We kept one of the key feature from Apps: the Privacy Score.

In a blink of an eye, we decipher the app code for you: you can see  which and how many trackers are in the app. It also documents the number  of permissions the app requires to operate. With an easy to read scoring, you can see which apps are safe and which ones should be avoided.

Available on the search result and app detail pages, it still relies on Exodus Privacy data.

The Privacy Score is computed based on trackers and users permissions used for the app. The Privacy Score is a sum of Tracker score and Permission score.

**Privacy Score = Trackers score + Permissions score**

If the number of Tracker is higher than 5, then score for trackers is 0. Otherwise the score for trackers will be the value of (9 – Number of trackers).
- if number of trackers > 5 : **0**
- if number of trackers < 5 : **9 - number of trackers**

If the number of permissions is more than 9, then score for permissions is 0. Otherwise, the score for permissions will be the rounded value of an equation given below.
- if number of permissions > 9 : **0**
- if number of permissions < 9 : **0.2 x ( (10 – number of permissions) / 2 )**

The source code link of the Privacy Score calculation will be found at [https://gitlab.e.foundation/e/os/apps/-/blob/main/app/src/main/java/foundation/e/apps/PrivacyInfoViewModel.kt](https://gitlab.e.foundation/e/os/apps/-/blob/main/app/src/main/java/foundation/e/apps/PrivacyInfoViewModel.kt#L78)

## All the features users can expect from an application store

App Lounge provides all of them:
-	Automatic update,
-	Homepage with recommendations,
-	Search feature to find the app you are looking for,
-	App detail page with all the information regarding the app, including some privacy related ones.

## What’s next?

App Lounge is already bringing great improvements in /e/OS but we don’t want to stop there. We plan to keep improving the service and make it more efficient and useful.

We thought you could be interested in some upcoming features:

### Fetching Open Source applications from F-Droid (and get rid off CleanAPK)

With the same idea we had for commercial application, we are working on fetching Open Source applications directly “from the source”. One of the challenges will  be to handle alternative F-Droid repositories, that are sometimes more up-to-date than the official one.

### Delivering /e/OS default applications update

Delivering /e/OS default application updates via App Lounge would allow users to receive updates as soon as they are ready, without waiting for the next OS update.

### No-Google mode

We are working on an App Lounge optional mode which would allow users to fetch only Open Source applications and PWAs. This would reduce the catalog size, but would give some peace of mind to people concerned by any network connections made to Google servers, even anonymized ones.

### Improved Privacy Score

The current Privacy Score computation shows some limitations. We are looking into ways to improve its accuracy regarding the privacy compliance of an application.

### Why can the size displayed in App Lounge and the one displayed in GPlay be different?

App size varies based on CPU architecture/Hardware configuration or Locale/Region and Google play has more dynamic feature as well. The Google Play Store API does not provide access to this data. That is why the app size displayed in App Lounge and the one display in Google Play can sometimes appear different.

## Download App Lounge APKs

Some devices which are on /e/OS but not updated to v1.0 are still on the legacy "Apps" application. Here we offer the signed apk builds for such devices to update Apps to the new "App Lounge" application.

**How to choose which apk to download?**

Simplest way is to download and try to install apks one by one. Only the correct apk for your device will be successfully installed, the rest will fail due to signature mismatch. Once you have one of the below apks installed, you don't need to try the other apks.

App Lounge latest APKs (community and official): [GitLab release](https://gitlab.e.foundation/e/os/apps/-/releases/permalink/latest)  

**How to revert back to the old "Apps" application?**

You may need to setup Apps from start.

1. Open the device's `Settings` from the home screen.
2. Go to `Apps and notifications` -> `See all xxx apps` -> `App Lounge"`
3. Click the top right three dots -> `Uninstall updates`

Now you can use the old "Apps" application if you want.

⚠️ To be used only on an /e/OS device! The app will not work on other Android platforms.

## Still have some questions?
Find the answers in the [App Lounge FAQ](/app-lounge)

## Many thanks!

All those features would have been much more complicated to deliver without the wonderful Open Source community! We would like to give a special thanks to:
-	The [Aurora OSS](https://auroraoss.com/) project behind the well known Aurora Store. App Lounge is using their GPlay API library
-	The [Exodus Privacy](https://reports.exodus-privacy.eu.org/fr/) project, for their continued efforts to provide data related to app trackers
-	The [/e/OS community](https://community.e.foundation/), which helped us a lot and provided valuable feedback during the development of App Lounge
