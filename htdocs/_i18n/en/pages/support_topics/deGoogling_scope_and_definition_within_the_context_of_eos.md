## Purpose

This document  proposes to set the scope of the term ‘deGoogling’ as defined by /e/OS.
Ever since the first /e/OS ROM’s rolled out, the term ‘unGoogling’ and then ‘deGoogling’ has gained currency. The term is often loosely defined as a complete removal of all Google related code from the Android Smartphone operating system code base, leading to a dogmatic definition.  This document seeks to correct some of these incorrect assumptions behind such ideas and instead attempts to focus on the expected benefits of deGoogling. Here we share some points to clarify each question. There could be more points.

### The Reasons Behind Google's Use of Personal Data

- To improve user experience
- Help local business
- Connect business and the customer
- Integrate businesses across the globe

### What information about the user is usually leaked

Leaked from a non deGoogled smartphone and also across various devices such as tablets, computers and smartwatches:

- Location in real time and location history
- Search History
- Browsing history
- Email contents
- Application usage: which application, usage monitoring
- Spending ability
- Address books
- Digital footprint
- Bank information

### Why is it dangerous for the user to share this information

- Loss of privacy
- Sensitive personal information getting into the hands of unscrupulous elements
- Breach of trust. Data pilfering without user approval
- Identity theft

### What deGoogling does to the user

- Returns control of data to the user
- User is aware of what information is being leaked
- Any information shared is with users approval
- Transparency in user data usage

### What deGoogling will not do

- Protect users from Government security agencies when someone is targeted
- Protect users from big Criminal organizations when someone is targeted

### How  /e/OS deGoogle’s Android

- Clean up of the source code by removing most Google server calls
- Connectivity checks traditionally done against Google servers, replaced with our servers
- Replace Google NTP servers for network time protocol
- Removed default Google DNS and offers more choices for DNS settings
- Replaced all default Google Apps by privacy-safe and feature-equivalent apps
- Adds an anonymity layer to Google services that are difficult to live without such as push notifications and access to Play Store applications

### Additional benefits for a Smartphone user on /e/OS

- App Lounge offers the full catalog of Play Store applications, open source applications at F-droid and Progressive Web Apps
- Advanced Privacy lets users know about the number of trackers triggered by mobile applications, and cut them. It also adds IP-masking and Geo location-faking features out of the box
- Offers a privacy-safe search engine

## Conclusion  

deGoogling is about making users’  digital life safer. /e/OS with its deGoogled codebase, default applications and unique features, protects users’ smartphones from permanent data collection and surveillance, while ensuring an extremely high level of usability.
