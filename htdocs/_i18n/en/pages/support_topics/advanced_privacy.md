<div class="text-center">
  <img src="/images/advanced_privacy_homepage.jpg" alt="">
</div>

Advanced Privacy is a specific tool we have developed to limit your data exposure once you have installed third party apps.

When an application snoops in the background, it will use trackers to log your activity even if you are not using the app. It will also collect the IP address, so it can potentially link internet activity to a specific device and to a persona, and finally it will try to pinpoint your exact location.

Advanced Privacy lets you manage in app trackers, IP address and location. It’s available as a widget and within the operating system settings.

## *Trackers Blocker*

<div class="text-center">
  <img src="/images/advanced_privacy_tracker_blocker.jpg" alt="">
</div>

### What are we talking about ?

A typical tracker consists of a software initiating requests from the device to send personal data to specific endpoints or APIs which collect connections' metadata while providing a service. The collection and aggregation of these data are used to track users' activity and behavior.

Tracing a tracker means having a look at which applications (or system components) are connecting to which endpoints (when, how much data, which data, ...).


### How does it work

*Trackers blockers* detect and block trackers when they perform DNS (Domain Name System) requests. The DNS is the first step of an Internet request: it transforms the name of a service (for instance `e.foundation`), to its current real address on the internet (for instance: `157.90.154.178`).  

We build and maintain a list of all URLs used by trackers, merging Exodus list, Adaway list and a set of custom /e/OS rules. Adaway list is used to establish a list of domains to block, Exodus to identify those domains with a clean name. Then, the low level DNS service of /e/OS sends each requested URL to Advanced Privacy. Advanced Privacy then logs each request to URL in the trackers list, and blocks them if the user required to block trackers in Advanced Privacy settings.


### Side effects

1. Some trackers URL could be mandatory to use services. In order to solve potential issues, we provide the ability to whitelist any tracker individually for any application.
2. There are more and more innovations around the DNS technology, like DoH (DNS Over HTTPS) or DoT (DNS Over TLS) which can bypass the low level DNS service of the system, and then be ignored by the trackers manager.
3. The Tracker Blocker may sometimes indicate that a system app is loading trackers. This is not a false positive, but it also does not imply that a system app is tracking you. It happens when system apps use third-party plugins, such as [Mapbox](https://www.mapbox.com/) to display maps, which then may load its trackers.

   Sometimes Advanced Privacy indicates a tracker being loaded by the System. It's because in /e/ OS Progressive Web Apps (PWA) are managed by PWAPlayer, which is a system app. PWAPlayer does not load any trackers by itself but the websites browsed using PWAPlayer may load their trackers, which are ultimately blocked by Advanced Privacy.

   If any app (including system apps) load any trackers, the Tracker Blocker will block it. We do not intend to use/promote any services that attempt to track you, and we are actively developing better solutions to address these challenges.
4. App Lounge downloads apps from Google anonymously, so there are no trackers embedded in App Lounge. However, if Advanced Privacy detects any trackers in it, it is because you chose to sign in with your Google account (like for downloading paid apps). 
5. Over time, trackers (i.e. tracking domains) may stop tracking users or change their behavior. These changes are adopted into filter lists (like Exodus) during their updates. However, these trackers won't disappear from Advanced Privacy to reflect the tracking domain's historical nature.
6. Advanced Privacy in /e/OS uses AdAway and Exodus filter lists to identify tracking domains. That means Advanced Privacy will block requests to domains present in those filter lists across all apps on your device. In rare cases, this process of blocking trackers may lead to some inconvenience. As an example, one of Microsoft's subdomain `login.microsoftonline.com` serves multiple purposes. It allows Microsoft users to log in to Microsoft apps, but according to Exodus, [the same subdomain is used by Microsoft to track users in third-party apps](https://reports.exodus-privacy.eu.org/en/trackers/63/). In such cases, Advanced Privacy can't determine if a user is willingly using a service or is being tracked. We are currently evaluating options for dealing with situations like this, but in the meantime, [you can allow specific domains for specific apps in the Advanced Privacy settings](/support-topics/block-unblock-trackers-per-app).



## *Hide My IP*

<div class="text-center">
  <img src="/images/advanced_privacy_hide_ip.jpg" alt="">
</div>

### What are we talking about ?

IP (Internet Protocol) addresses are the origin and destination addresses of the communications performed between clients and servers on the internet.

A smartphone device is practically never directly connected to internet, with "its" IP address. Its visible IP address on internet is the one actually attributed by the cellular provider, the one of the home internet connection, or the one of the current Wi-Fi hotspot. In each of these situations, the same IP address may be used by many users at a same time (using the Network Address Translation protocol - NAT) and many IP addresses may be used the same day by one device just because the user has moved.

IP addresses are the way to link the internet activity to an individual in the real world, including for legal purposes. For example, leaving a comment on a forum:
* The forum's hosting service has to store the IP address of who has posted the comment (for a legal period of time)
* The Internet Service Provider (ISP) has to store the IP address (also IMEI, cell ID) and to which customer it is linked (for a legal period of time)

In the end, the customer potentially becomes a well identified person.

Typical legal uses of the IP addresses:

* Find back an illegal torrent user,
* Find back authors of unappropriate contents on the web, ...

IP addresses generally are very volatile in mobile use. In other situations, they can be very stable: many ISP provide a fixed IP address to their customers  (home connection), so that an IP address can potentially become another user identifier. They are used in various cases like:

* Tracking users for profiling them and delivering ads
* Preventing identity theft: blocking connections to personal accounts from unexpected IP addresses,
* Restricting contents delivery based on Geographic information,
* Banning user for game server, or from Wikipedia, for instance, after vandalization,

Internet users have been using IP scrambling for years to bypass those strategies.

### How does it work

Advanced Privacy is using an implementation of the Tor project. We extracted the core functionality from the [Orbot](https://github.com/guardianproject/orbot) application, and added a user interface on the top of it.

> *Tor directs Internet traffic through a free, worldwide, volunteer overlay network, consisting of more than seven thousand relays, to conceal a user's location and usage from anyone performing network surveillance or traffic analysis.* [Wikipedia](https://en.wikipedia.org/wiki/Tor_(network))  

When *Hide My IP* is activated, all the device's internet traffic, or just the one of some selected app, is redirected through the Tor network. In the end, the user's primary (and read) IP address is masked by a random IP address that belongs to the TOR network.

### Side effects


The implementation of Orbot bridges all the devices's Internet traffic through the Tor network. This was initially designed for [VPN Services](https://developer.android.com/guide/topics/connectivity/vpn). That's why *Hide My IP* appears as a VPN in /e/OS. We are working on another solution to route the devices's traffic directly through Tor, to avoid this confusion.

*Hide My IP* also reduces bandwidth and increases latency (like 200ms - 500ms). This is a side effect of the Tor network architecture and behavior. Each Internet request goes through many Tor relays through the Internet to reach the final server, and also to come back to the device. Wandering on the internet makes them anonymous, but it also takes some time.

Some Internet services can detect traffic coming from Tor, and block it. That's why users may face some strange behaviors or non-working features while using *Hide My IP*.

Finally, some services are using the IP address to compute users location. The user experience may be affected using those services, considering they will use the Tor output node IP address, and not the real one from users, as the Tor output node may be located anywhere in the world.


## *Fake My Location*

<div class="text-center">
  <img src="/images/advanced_privacy_fake_location.jpg" alt="">
</div>

Smartphones provide technical tools to compute their geographical position, using satellite-based radio navigation systems such as GPS, Galileo..., or by mapping visible networks (cell network, Wi-Fi networks, ...). This is a key feature of smartphones that provides users with the ability to use maps applications and services.

But it is also used to track users: to know where they live, which store they go to, in which area they work and so to have precise profiles to push targeted advertising to them. Some other digital services can also use the location to limit the functionality against the location of users.  

### How does it work

In Advanced Privacy, *Fake my location* takes advantage of existing low-level operating system features that we have connected to the Advanced Privacy User Interface to make it easy to use and hidden from applications. *Fake my location* bypasses the real location provided by the satellite radio navigation system or the network, and instead sends the one set by users to applications that are requiring location.

### Side effects

After enabling *Fake my location*, users may face some unexpected behavior:
1. the Weather widget data will be based on the fake location, not the real one. A workaround could be to define manually a fake location, not so far away from the real one.
2. the navigation applications (MagicEarth, OsmAnd, Maps.ME, Waze...) will use the fake location. In order to users properly those applications, the users have to temporarily disable *Fake my location*.
3. We use [Mapbox](https://www.mapbox.com/) in order to display a map to the users while they play with location configuration. It will be detected as a tracker by the Tracker blocker feature. We are looking for an alternative.
4. automatic dark mode will trigger at wrong times, if the fake location is not on the same time zone as the user.
