This documentation intends to help /e/OS users, backup and restore their device

A difference in the encryption system between Android 9 and subsequent android versions prevents this device from being upgraded to Android 10+ without a factory reset.

{% include alerts/tip.html content="This guide is assuming that you don't synchronize your phone, for example by using a Murena Workspace account. If you do use such an account, please first check what data is available on the server. You don't need to back up the data already available on the Murena Workspace. After adding your Murena Workspace account on the phone with freshly installed /e/ OS, the data should automatically appear on the phone." %}

## Requirements

- /e/ OS phone
- Computer (Linux, Windows, macOS)
- USB cable
- Some time : 2-3 hours, if you want to re-install /e/ OS

## Step 1: back up your data

### Task 1: back up files (Photos, videos and documents)

You need to transfer your files from the phone to a computer.

Connect your phone to a laptop using a USB cable. 

![screenshot_20230525_210036](/images/howtos/upgrade-fp3-from-a9/screenshot_20230525_210036.jpg)

Slide your finger from top of the screen downwards, and click on “Android System. Charging this device via USB” 2 times.You will see USB preferences menu. Select File transfer. If you don't see this option, try using another cable or another USB port on your computer.

![screenshot_20230525_225425](/images/howtos/upgrade-fp3-from-a9/screenshot_20230525_225425.jpg)

You will now see the phone storage as a folder in the file explorer of your PC. You can browse the files on your phone. You can copy all folders, or you can choose which folders to copy, e.g.:
DCIM
Music
Pictures
etc.

note: If you are using MacOS, you will need to download and install the Android File Transfer app for macOS. The Android File Transfer can be found at this address: https://www.android.com/filetransfer/  

### Task 2: back up your calendar(s)

It is not possible to back up a local calendar. If you are using an online calendar, such as if you use a Murena Workspace account or Nextcloud based account, the online calendar will appear again after adding your account in the /e/ OS after the installation of the OS.

### Task 3: back up your contacts

Open Contacts app.

Click on 3 lines icon.

Click on Settings.

![screenshot_20230525_235151](/images/howtos/upgrade-fp3-from-a9/screenshot_20230525_235151.jpg)

Click on _Export_.

Click on _Export to .vcf file_.

![screenshot_20230525_235346](/images/howtos/upgrade-fp3-from-a9/screenshot_20230525_235346.jpg)

Choose a folder where you want to save the .vcf file, for example the "Download" folder. Click save, then copy the file to a computer, like in Step 1: "Photos, videos and documents".

![screenshot_20230525_235516](/images/howtos/upgrade-fp3-from-a9/screenshot_20230525_235516.jpg)

### Task 4: back up your SMS/MMS and call logs

It is not possible to save SMS/MMS, neither call logs, with integrated default apps. That's why we advise users to use `SMS Backup and Restore`:
1. Open `Apps`
2. Search and install `SMS Backup & Restore`
3. Open the app, and create a backup
4. Copy the backup files (*calls-[date].xml* and *sms-[date].xml*) on your computer

### Task 5: back up Signal

Please refer to the [official Signal documentation](https://support.signal.org/hc/en-us/articles/360007059752-Backup-and-Restore-Messages)  


### Task 6: back up Whatsapp

The official Whatapps documentation relies on a Google account to backup data: [Whatsapp backup and restore documentation](https://faq.whatsapp.com/481135090640375/?helpref=hc_fnav&cms_platform=android)

You can refer to the [community discussion](https://community.e.foundation/t/howto-transfer-whatsapp-messages-from-google-android-to-e-backup-and-restore/12389) which provide useful guidelines.

### Task 7: back up applications and application data

It is not possible to make a direct backup of applications with the app data. We can offer an alternative to back up user installed apps. 
- For apps that sync data without an account, your settings won't be saved, so you will have to configure them again after the upgrade.
- For apps that sync data with an account, you will just have to connect your account again. 

{% include alerts/warning.html content="With encrypted messaging applications such as Signal, WhatsApp, Olvid etc. **if you do not back up your conversations they will be lost**." %}

To back your apps up, you can use a third party app called _Package Manager_ from Apps.

Once you have launched _Package Manager_, you will see apps organized by type (All, System, User). You want to focus on User apps, which are the ones you installed on the phone.   

![Screenshot_20230526-094546_Package_Manager](/images/howtos/upgrade-fp3-from-a9/Screenshot_20230526-094546_Package_Manager.jpg)

To select all `User Apps`, tick one app to select it then tap on `Batch Options` and `Select All`. The `Batch options` button will now have a number next to it, counting the number of selected apps.
![Screenshot_20230526-094616_Package_Manager](/images/howtos/upgrade-fp3-from-a9/Screenshot_20230526-094616_Package_Manager.jpg)

Now press the `Batch options` button again and select `Export`. 

![Screenshot_20230526-094632_Package_Manager](/images/howtos/upgrade-fp3-from-a9/Screenshot_20230526-094632_Package_Manager.jpg)

You will be prompted to confirm before you export the files.

![Screenshot_20230526-094641_Package_Manager](/images/howtos/upgrade-fp3-from-a9/Screenshot_20230526-094641_Package_Manager.jpg)

Once you have confirmed, the export will start.   
![Screenshot_20230526-095031_Package_Manager](/images/howtos/upgrade-fp3-from-a9/Screenshot_20230526-095031_Package_Manager.jpg)
![Screenshot_20230526-094711_Package_Manager](/images/howtos/upgrade-fp3-from-a9/Screenshot_20230526-094711_Package_Manager.jpg)

The exported apps will be located under the folder `/Package_Manager/` on your device.   
Please copy all files to your computer. 

![Screenshot_20230526-095056_Files](/images/howtos/upgrade-fp3-from-a9/Screenshot_20230526-095056_Files.jpg)
![Screenshot_20230526-095113_Files](/images/howtos/upgrade-fp3-from-a9/Screenshot_20230526-095113_Files.jpg)

## Step 2: restore your data

### Task 1: restore files (photos, videos and documents)

Transfer files from the computer onto the phone.

You can copy the data back to your phone, by doing the same steps in the “Transfer files from the phone to a computer” section, then copying the files or folders from the computer and pasting them onto the phone in the right directory.

### Task 2: restore calendar(s)

You cannot restore a local calendar. If you add a Murena Workspace account, related calendars should appear automatically on your phone. You can also create a new local calendar if you like. To do so, click on 3 lines on top left > Settings > Add offline calendar > Add calendar.

![screenshot_20230526_010313](/images/howtos/upgrade-fp3-from-a9/screenshot_20230526_010313.jpg)
![screenshot_20230526_010446](/images/howtos/upgrade-fp3-from-a9/screenshot_20230526_010446.jpg)
![screenshot_20230526_010504](/images/howtos/upgrade-fp3-from-a9/screenshot_20230526_010504.jpg)
![screenshot_20230526_010516](/images/howtos/upgrade-fp3-from-a9/screenshot_20230526_010516.jpg)

### Task 3: restore contacts 

Copy the contacts backup file to the phone, for example in "Download" folder.
Open Contacts app, click on 3 lines > Settings > Import : .vcf file , then select the backup file.

![screenshot_20230526_011043](/images/howtos/upgrade-fp3-from-a9/screenshot_20230526_011043.jpg)
![screenshot_20230526_011119](/images/howtos/upgrade-fp3-from-a9/screenshot_20230526_011119.jpg)
![screenshot_20230526_011146](/images/howtos/upgrade-fp3-from-a9/screenshot_20230526_011146.jpg)

### Task 4: Restore your SMS/MMS and call logs

We advise to use `SMS Backup & Restore`:
1. Copy the backup files (*calls-[date].xml* and *sms-[date].xml*) from your computer on the Fairphone 3
1. Open `Apps`
2. Search and install `SMS Backup & Restore`
3. Open the app
4. Tap `Restore` on homepage, and follow the process

### Task 5: Restore Signal

Please refer to the [official Signal documentation](https://support.signal.org/hc/en-us/articles/360007059752-Backup-and-Restore-Messages) 

### Task 6: Restore Whatsapp

The official Whatapps documentation relies on a Google account to restore data: [Whatsapp backup and restore documentation](https://faq.whatsapp.com/481135090640375/?helpref=hc_fnav&cms_platform=android)

You can refer to the [community discussion](https://community.e.foundation/t/howto-transfer-whatsapp-messages-from-google-android-to-e-backup-and-restore/12389) which provide useful guidelines.

### Task 7: Restore applications and application data

To reinstall the apps, install Package Manager again.
Connect your phone to your computer and copy the exported apps to your phone in the folder `Package_Manager`. 

Then open the menu on the top right and select `Installer`.  
![Screenshot_20230526-095154_Package_Manager](/images/howtos/upgrade-fp3-from-a9/Screenshot_20230526-095154_Package_Manager.jpg)

Check the instructions on screen and tap on `+`.  
![Screenshot_20230526-095202_Package_Manager](/images/howtos/upgrade-fp3-from-a9/Screenshot_20230526-095202_Package_Manager.jpg)

Search for the folder `Package_Manager`.  
![Screenshot_20230526-095326_Package_Manager](/images/howtos/upgrade-fp3-from-a9/Screenshot_20230526-095326_Package_Manager.jpg)

Tick the app you want to install and press the blue button `Select`.

A new screen will open giving you the option to install the app or cancel. Select `Install` and confirm the installation with the on-screen prompt.
Apps without the tick box will need to be installed individually.   

You can now exit Package Manager

{% include alerts/tip.html content="You will have to repeat the step where installer comes in picture, as it is not able to recover all applications at once" %}