
- Tap to Open Contacts app
- Click on the `three dots` on the top right
- Next click or tap on `Import/export`
- Choose `Export to .vcf file`
- Click on `0 selected` 
- Next click `Select all`.
- Press `OK` 
> Here choose the location where you want to save the .vcf file. 


 
{% include alerts/tip.html content="We recommend you choose the <code>Downloads</code> folder." %}
    
    