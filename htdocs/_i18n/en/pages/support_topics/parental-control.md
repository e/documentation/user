Parental Control on /e/OS allows parents to protect children from inappropriate contents. This is done by restricting access to some mobile applications and web contents, depending on the child's age. 

## Importance of Parental Control

* The internet offers access to information, culture, and social relationships, but the cost is huge if children are exposed to activities and content that affect their health and development. 

* Long screen time: Children's early exposure to screens can harm their proper development. Overexposure or inappropriate management can lead to risks and effects on children's health, including sleep problems, vision problems, and weight issues. It's important for parents to monitor and limit children's screen time.

* Inappropriate content: Children are at risk of exposure to inappropriate content online. Especially from non-family friendly sites and apps that display pornographic content, from social media apps, and from sharing platforms that facilitate the dissemination of explicit content. Parents should block access to apps and websites inappropriate for children to limit their exposure to inappropriate content.

* Online harassment: Online harassment poses serious risks to kids, including emotional distress, social isolation, and decreased academic performance. To minimize these risks, parents should educate their kids about online safety, monitor their activities, encourage reporting of incidents, and foster open communication.

* Parents can play a crucial role by setting up parental control on their child's phone to help children have a safe and responsible digital experience. It's also important that parents openly communicate with their children on how to safely navigate the digital space.

## Activating Parental Control

* Parental Control can be activated during the `First Time Setup Wizard` of the phone or from `Settings > Parental Control` after initial setup.   
![alt text](/images/parental-control/parental-control-activate-1.png)   

* Toggle `Activate Parental control`   
![alt text](/images/parental-control/parental-control-activate-2.png)   

* Select the `Age group of the child`  and tap `Next`.   
![alt text](/images/parental-control/parental-control-activate-3.png)   

* Set up a `Pin` or `Password` to manage Parental Control's settings. This PIN/Password is required when changing or disabling Parental Control's settings. Your child should not know this PIN/Password.   
![alt text](/images/parental-control/parental-control-activate-4.png)   

## Requirements to activate Parental Control

* Parental restrictions, such as `Family safe DNS` can be bypassed if Parental Control is set up in a multi-user setup. We recommend removing extra users when asked.   
![alt text](/images/parental-control/multiple-users-warning.jpg)

* Parental Control cannot be setup if another admin app (like Shelter) is in use. Please deactivate/uninstall the app to proceed.   
![alt text](/images/parental-control/other-admin-app-disable.jpg)

## Restrictions applied when Parental Control is on

* DNS queries are intercepted on device to deny access to adult websites and block trackers.   

* Private DNS is set to Cloudflare's family friendly DNS, and cannot be changed.   
{% include alerts/tip.html content="Cloudflare Family DNS helps keep kids safe online by:<br>
    - Blocking adult content<br>
    - Filtering out malware and phishing sites<br>
    - Preventing access to violent or harmful content<br>
    - Limiting access to age-inappropriate content"%}   
![alt text](/images/parental-control/private-dns-cannot-be-changed1.png) ![alt text](/images/parental-control/private-dns-cannot-be-changed2.png)

* Apps can only be installed via [App Lounge](/support-topics/app_lounge.html), which gives access to apps from Google Play and F-droid.     
![alt text](/images/parental-control/app-lounge-ss2.png) ![alt text](/images/parental-control/app-lounge-ss1.png)   

* Apps can only be installed if the app's content rating matches the child's age set during Parental Control setup. Apps' content ratings are published by Google Play or F-Droid depending on the app.   
![alt text](/images/parental-control/age-restricted-apps-cant-install-1.png) ![alt text](/images/parental-control/age-restricted-apps-cant-install-2.png)

* Apps which require parental guidance cannot be installed for the 0-3 age group. We plan to make it customizable in the next few updates.   
![alt text](/images/parental-control/parental-consent-required1.png) ![alt text](/images/parental-control/parental-consent-required2.png)   

* Already installed apps whose age ratings are more than the age set by parent are greyed out and cannot be launched.   
![alt text](/images/parental-control/greyed-out.png) ![alt text](/images/parental-control/cant-open-app.png)   

* After setting up Parental Control with only one user, your child won't be able to create new users.   
![alt text](/images/parental-control/multiple-user-disabled.png)   

* `Clear data`, `Clear cache` or `Force stop` won't work for any apps.   
![alt text](/images/parental-control/force-stop-clear-cache-greyed-out.jpeg)   

* Device cannot be Factory reset.   
![alt text](/images/parental-control/factory-reset-disabled1.png) ![alt text](/images/parental-control/factory-reset-disabled2.png)   

## Disabling or Changing Parental Control

* To disable Parental Control, change the age group, or change the PIN/password, the parent needs to enter the PIN/password that was set during setup.   