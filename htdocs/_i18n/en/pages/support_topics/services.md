### How to check if the services offered by /e/OS are online?

You can check the status of /e/OS services at [status.e.foundation](https://status.e.foundation)
and Murena Workspace services at [status.murena.io](https://status.murena.io/).

### Is my data on the /e/OS servers encrypted?

  - All Murena Workspace files are encrypted by **server-side encryption**.
  - This means the keys to decrypt the data are only available in the servers hosting the application.
  - Only a handful of senior administrators and lead developers have access to those servers to perform maintenance and troubleshooting.
  - Decryption is only authorized as part of a disaster recovery procedure, initiated by a user request submitted using this [contact form](https://murena.com/contact-us/) or as part of a legal obligation to answer a binding request coming from legal authorities in the European Union.
  - All backups are encrypted with a different key, not stored on the application servers. Only a few administrators have access to the servers hosting the backups.
  - True end-to-end encryption for Murena Workspace files, full database encryption, and encryption for mails is in our plans as long-term features.

### Can I delete my Murena Workspace ID?
Yes you can. Refer [to this document](/create-an-ecloud-account#how-can-i-delete-my-e-account-eemail) to understand how this can be done.

### What can I do if I lost my phone?

If you have access to the credentials and can log into Murena Workspace:
 - **Revoke** all sessions and application passwords except the one you are currently using on [Murena.io security settings](https://murena.io/settings/user/security#security), _Devices and sessions_.
 - Immediately change your Murena Workspace password and store it in a safe place (typically, a password manager).
 - Monitor for a few hours to make sure no other device or application appears in the list of sessions on murena.io.

If you lost your 2FA system, don't have your credentials at hand or cannot do this on time:
 - Request an **account deactivation** by sharing details on this [contact form](https://murena.com/contact-us/). This is reversible and your data will remain safe in our cloud.
 - For the request use your Murena Workspace ID or the recovery email you set to recover your password.
 - **DO NOT delete your account** as this will also delete all your data on the [Murena Workspace](https://murena.io) as well.

As you may know Murena Workspace is forked from [Nextcloud](https://github.com/nextcloud/server) which offers a "Wipe Device" functionality. This implementation has not been added as yet into /e/OS so unfortunately it cannot be used.

### Can the Murena Workspace admin access my data

On any Nextcloud instance, an administrator can see your files and all the information in the database.
Not only Nextcloud admins, but also server admins (i.e. root) have this access. In our case this means a handful of trusted administrators and lead developers.
The only way to make files inaccessible is by using end-to-end encryption.

Nextcloud has no plans for database encryption as per our understanding. We are planning to provide an additional service down the line, maybe based on [EteSync](https://www.etesync.com/). The team needs to investigate its feasibility and the User Experience to provide a seamless operation between the phone and the web interface.

Our Murena Workspace admins need to make backups, perform upgrades, reset passwords, etc.  We have implemented Nextcloud's server side encryption on our servers. 
We do not intend to enable end to end encryption (E2EE) [plugin from Nextcloud](https://apps.nextcloud.com/apps/end_to_end_encryption) because it is not satisfactory in its current state and can lead to data loss, as shown on the user reviews.

You can also host your own [Murena Workspace](https://gitlab.e.foundation/e/infra/ecloud-selfhosting) or a [plain Nextcloud](https://nextcloud.com/athome/) (no e-mail server included in that case).

### Can the Murena Workspace admin read our emails ?

Only a few team members can read your emails, unless some encryption mechanism is used like [PGP](https://en.wikipedia.org/wiki/Pretty_Good_Privacy). This is the case on any e-mail provider except custom non-standard solutions. Our goal at Murena is also to set up something that works out of the box including key creation, discovery and seamless experience between /e/OS and Murena webmail. This will need specific R&D.
For now, you can already configure PGP on /e/OS Mail and on Murena Workspace webmail.

### I want to know more about the security on Murena Workspace

Since this is a common query from /e/OS users, we would like to clarify a few points:

  -  murena.io is a modified version of [murena-cloud-selfhosting](https://gitlab.e.foundation/e/infra/ecloud-selfhosting), which is based upon several open source projects.
  -  We have implemented [Nextcloud's server side encryption](https://nextcloud.com/blog/encryption-in-nextcloud/) on our servers. As you maybe aware SSE is a requirement for E2EE.
  -  We have a long-standing relationship with a security expert in charge of hardening and monitoring our systems, including murena.io.
  -  [Nextcloud security scanner's score doesn't accurately reflect the security of Murena Workspace](/support-topics/security-difference-nextcloud-murena) 
  -  Read more about it on your [Murena Workspace instance](https://murena.io/settings/user/privacy)

A few of the improvements applied to murena.io in regards to the base murena-cloud-selfhosting instance:

  -  Performance tuning adapted to the scale of the service (number of users, storage size, etc).
  -  High availability for all core services: nextcloud, mariadb and redis, behind a HAProxy load balancer.
  -  We try to always keep the infrastructure and applications in compliance with the best available security hardening guidelines available such as [DevSec Hardening Framework](https://github.com/dev-sec) and CIS Benchmark for Ubuntu.
  -  We apply process confinement techniques and mitigation provided by [AppArmor](https://apparmor.net/) and systemd.
  -  We use [Wazuh](https://wazuh.com/) monitoring for threat detection.

If you think (or can prove) that there is a security vulnerability in murena.io or the murena-cloud-selfhosting project, please contact us directly: [security@murena.io](mailto:security@e.email). Please **do not use** this address for any other purpose. 

Kindly use the following key to encrypt any sensitive disclosure:
[https://keys.openpgp.org/vks/v1/by-fingerprint/F242DB4B0F002ED0AB73A5D06E25E121E5939DAF](https://keys.openpgp.org/vks/v1/by-fingerprint/F242DB4B0F002ED0AB73A5D06E25E121E5939DAF)

### How can I contact Murena Workspace Data Protection Officer (DPO)?

Please write your inquiry to [dpo@murena.com](mailto:dpo@murena.com) and the person currently performing this role will coordinate the necessary action and reply in the shortest delay.
If you have a general question or concern about the services and their implementation, please write using this [contact form](https://murena.com/contact-us/) .

### Can a custom cloud be configured to connect to /e/OS?
You can configure /e/OS to talk to another Nextcloud instance, though you won't get all the features like common username for all services including mail etc. You can also [selfhost Murena Workspace](https://gitlab.e.foundation/e/infra/ecloud-selfhosting) on your own servers if you want.

### Where is Murena Workspace data stored?
The Murena Workspace servers are located in Finland and run on renewable energy. You can read more [here](https://e.foundation/ecloud/).

### How are backups handled on Murena Workspace?
Murena Workspace user data is stored in a replicated storage cluster at Hetzner's data center in Helsinki, Finland (code named - HEL1) with server side encryption provided by Nextcloud server application. Every day, a snapshot (copy) of the data is stored in the same datacenter and every copy is kept for seven days.

Additionally, we create a second copy of these snapshots and synchronize them every day to Hetzner's data center in Falkenstein, Germany (code named - FSN1). These incremental cold backups are kept for up to three months to serve as a safe off-site backup in case of a disaster at Finland's data center.
  
