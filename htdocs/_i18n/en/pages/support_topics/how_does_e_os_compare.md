If you are looking for an OS with hardened security, use Graphene, if you are searching for an OS that helps you keep your data safe from Google, use /e/OS . The choice depends on your needs.

Personal privacy is a commonly accepted need. You can have the most secure device, but that will not change the fact that your data will be constantly streaming your data to Google or Facebook .
Zero-privacy can be achieved in a very secure way.

There can be cases where security supports privacy, like if your device gets stolen or if you are targeted by an organization etc.

So the ideal world is a mix of security and privacy.


