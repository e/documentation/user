

### Coming from a Google Android Phone or Custom ROM with Google account

#### Using a Murena Workspace account
- [Create an Murena Workspace account ](/create-an-ecloud-account)
- [Import contacts from Google to Murena Workspace](https://community.e.foundation/t/howto-import-gmail-contacts-into-ecloud/2809)

#### Migrate without creating a Murena Workspace account
- [Import contacts from Google to /e/ OS]()


#### Backing up your existing phone data

- [Backup your contacts](/support-topics/backup-your-contacts)
- [Backup your sms](/support-topics/backup-your-sms)
- [Backup your photo,videos and files](/support-topics/backup-your-files)
- [Migrate your data from Google Cloud to Murena Workspace](/support-topics/migrate-data-from-google-cloud)
- [Move contacts from Google account to /e/OS (export, import)](https://community.e.foundation/t/howto-move-contacts-from-google-account-to-e-export-import/12390)



### Coming from iPhone

#### Using a Murena Workspace account

- [Create an Murena Workspace account ](/create-an-ecloud-account)
- [Import contacts from Apple to Murena Workspace](https://community.e.foundation/t/howto-import-contacts-from-apple-to-e-cloud/5532)

#### Migrate without creating a Murena Workspace account
- [ Import contacts from Apple to /e/ OS]()


### On your /e/OS phone

- [Transferring Data from iPhone and iCloud to /e/OS and Murena Workspace](/transfer-data-from-apple-account)
- [Add a Gmail account](/support-topics/add-a-gmail-account)
- [Restore your photo,videos and files](/support-topics/restore-your-files) 
- [Import Google Keep notes into /e/](https://community.e.foundation/t/how-to-import-google-keep-notes-into-e/3165/3)
- [Transfer WhatsApp messages from Google Android to /e/OS (backup and restore)](https://community.e.foundation/t/howto-transfer-whatsapp-messages-from-google-android-to-e-backup-and-restore/12389)
