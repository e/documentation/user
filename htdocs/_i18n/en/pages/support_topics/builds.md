### Why is debug mode enabled on /e/OS ? 


UserDebug is a heritage we carry from LineageOS from where we have forked our source code. It is also needed on many older devices to boot and work properly.

From a security perspective, it may be safer to have user mode instead of debug mode, though an attacker can also decompile an APK to understand how it works. So the real benefit might not be that huge.

However, we plan to enable user mode for newer devices on official builds.

### I am interested in building and maintaining the ROM for my device. How can I help?

Thanks for offering to help with the builds. You can refer the [document here](/rom-maintainer) for more details.
