## Introduction

In modern mobile devices, understanding and managing internal storage space is crucial for efficient device usage. However, users often encounter discrepancies in the reported size of internal storage between the device's system settings and file manager applications downloaded from app stores. This document aims to elucidate the reasons behind such inconsistencies and provide users with a clearer understanding of the issue.

## Reasons for Discrepancies

Several factors contribute to the disparities in internal storage size between Settings and file manager applications:

### 1. System Usage

Mobile devices allocate internal storage for various functions, including system operations and user data. Understanding how system storage is managed sheds light on discrepancies in reported sizes:

- **Partitioning**: Mobile devices use a sophisticated partitioning scheme. The system partition is reserved for system files, OS components, and pre-installed apps, inaccessible to users.
  
- **Reserved Space**: Operating systems set aside storage for critical functions like updates, temporary files, and system logs. This space isn't visible to users but ensures system stability.
  
- **File System Overhead**: File systems incur overhead for managing structures, metadata, and journaling. While minor, it affects storage utilization and varies by file system format.

- **A/B Partitioning Devices**: Some newer Android devices utilize A/B partitioning, which involves having two separate system partitions, along with various other partitions, known as Slot A and Slot B. This setup allows seamless updates without interrupting the user experience. However, due to this nature, devices with A/B partitioning may allocate almost twice the amount of space compared to non A/B devices.

**Example**:

Consider a mobile device with a total storage capacity of 64GB. The system partition may consume 10-15%, leaving the rest for user data. However, users may not see this breakdown, leading to discrepancies between reported sizes.

### 2. Flawed Calculation Methods

Android devices employ flawed methods for calculating storage space utilization by system components, leading to discrepancies in reported storage sizes. These discrepancies may arise from the misattribution of storage space to system components, including erroneous categorization of user-generated files as system files.

**Example**:

Android's calculation of storage space consumed by system components exhibited inconsistencies. A test scenario was devised where a dummy 5GB file was created and pushed to the directory /data/media/0, a non-system directory representing internal storage. Despite the file's explicit location outside the system partition, the system storage breakdown erroneously attributed the increased storage utilization to the Android OS System, resulting in inflated system storage figures. This shows inherent flaws in Android's storage space calculation logic, where user-generated files outside the system directory are inaccurately categorized as system files, leading to misleading storage utilization metrics.

**System storage info test results**

| Before Test (Clean Install) | After Test (5GB Dummy File) |
| ----------------------- | ------------------------------- |
| ![Android OS (Using GB)](/images/support-topics/incorrect-storage-info/Settings-Storage-Info-Before.png) | ![Other File Managers (Using GiB)](/images/support-topics/incorrect-storage-info/Settings-Storage-Info-After.png) |

### 3. Unit of Measurement Discrepancy

Storage capacity is often measured using different measuring systems. Most devices and systems, like Windows and Linux, use a method called binary notation (base 2). In this system, 1GB is calculated as 1,024 * 1,024 * 1,024 bytes. However, to simplify things and avoid confusion, some applications or utilities may display storage space using a different system, called decimal notation (base 10). In base 10, 1GB equals 1,000 * 1,000 * 1,000 bytes.

In binary notation, storage units are named using binary prefixes such as kibibyte (KiB), mebibyte (MiB), gibibyte (GiB), etc. These units represent powers of 2. For example, a kibibyte represents 1,024 bytes, a mebibyte represents 1,048,576 bytes, and a gibibyte represents 1,073,741,824 bytes.

Conversely, in base 10 notation, storage units are named using metric prefixes such as kilobyte (KB), megabyte (MB), gigabyte (GB), etc. These units represent powers of 10. For example, a kilobyte represents 1,000 bytes, a megabyte represents 1,000,000 bytes, and a gigabyte represents 1,000,000,000 bytes.

Android's utilization of gigabytes (GB) as a unit of measurement contrasts with some File Manager apps that utilize use of gibibytes (GiB) for showing storage capacities. This discrepancy in measurement units contributes to confusion among users and may lead to an overestimation of the space occupied by the Android OS and other system files.

**Example**:

For instance, in a mobile device with a storage capacity of 64GB:

- The storage capacity is represented as 51.108839 Gigabytes (GB) in decimal notation (base 10).
- The same storage capacity is represented as 48.74118709564209 Gibibytes (GiB) In binary notation (base 2).

This discrepancy in advertised versus reported storage capacities may lead users to believe that the Android OS consumes a larger portion of storage space than it actually does.

| Android OS (Using Gigabytes) | Other File Managers (Using Gibibytes) |
| ----------------------- | ------------------------------- |
| ![Android OS (Using GB)](/images/support-topics/incorrect-storage-info/File_Manager_GB_1.png) | ![Other File Managers (Using GiB)](/images/support-topics/incorrect-storage-info/File_Manager_GiB_1.png) |
| ![Android OS (Using GB)](/images/support-topics/incorrect-storage-info/File_Manager_GB_2_Files.png) | ![Other File Managers (Using GiB)](/images/support-topics/incorrect-storage-info/File_Manager_GiB_2.png) |

## Recommendations for Users

- **Consider System Limitations**: Recognize that the total storage capacity reported in Settings may be higher than the usable storage space visible in file manager applications due to system partitioning, reserved space, and file system overhead.

- **Regularly Monitor Storage Usage**: Regardless of the disparities, regularly monitoring storage usage through both Settings and file manager applications enables users to identify and address storage-intensive files and applications.

- **Optimize Storage Usage**: Utilize built-in storage management tools and file manager applications to identify and remove unnecessary files, clear cache, and uninstall unused applications to optimize storage usage.
