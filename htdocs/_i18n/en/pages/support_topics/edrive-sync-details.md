eDrive is a system app that synchronizes user data to Murena Workspace or a self-hosted cloud. You have complete control over which directories are synchronized to the cloud, but by default, eDrive syncs the following from device to cloud:

| Device location                      | Cloud location                         | Category     |
| ------------------------------------ | -------------------------------------- | ------------ |
| /storage/emulated/0/DCIM             | /Photos                                | Images       |
| /storage/emulated/0/Pictures         | /Pictures                              | Images       |
| /storage/emulated/0/Movies           | /Movies                                | Movies       |
| /storage/emulated/0/Music            | /Music                                 | Music        |
| /storage/emulated/0/Ringtones        | /Ringtones                             | Ringtones    |
| /storage/emulated/0/Documents        | /Documents                             | Documents    |
| /storage/emulated/0/Podcasts         | /Podcasts                              | Podcasts     |
| /data/system/users/0/                | /Devices/\<id\>/rom_settings/          | Rom settings |
| /data/data/foundation.e.drive/files/ | /Devices/\<id\>/rom_settings/app_list/ | Rom settings |

## Customize eDrive synchronization settings

To choose the directories to be synchronized to the cloud, navigate to System settings > Passwords & Accounts > Select your Murena.io account > Account sync. You can toggle synchronization settings for the following categories:

- `Application settings` toggle will affect synchronization for device settings.
- `Pictures and videos` toggle will affect synchronization for Images, Movies, Music, Ringtones, Documents and Podcasts directories.

In here, you can also enable/disable "Allow files sync on metered network". If this option is disabled, eDrive will not synchronize files over a metered network, such as mobile data.
