This guide will help you install /e/OS on officially supported devices based on the Mediatek platform.

{% include alerts/tip.html content="This guide only works for devices **officially** supported by us. [Check if your device is supported](https://doc.e.foundation/devices)." %}

{% include alerts/warning.html content="Following this guide to flash /e/OS on your device will erase all data stored on your device. Please back up your important data before proceeding." %}

## Requirements

- A supported device based on Mediatek. [Check if your device is officially supported](https://doc.e.foundation/devices).
- A host machine (Linux or Windows).
- A data cable to connect your device to the host machine. A charging-only cable won't work.

## Downloads

- SPFlashTool
   - `Windows`: [SP_Flash_Tool_v5.2316_Win](https://spflashtools.com/windows/sp-flash-tool-v5-2316)
   - `Linux`: [SP_Flash_Tool_v5.2228_Linux](https://spflashtools.com/linux/sp-flash-tool-v5-2228-for-linux)

We can confirm that the above-linked versions of SPFlashTool work as intended. Using the latest versions is neither encouraged nor discouraged.

## Set up the host machine (Linux or Windows)

1. Download SPFlashTool on the host machine.
1. Unzip the downloaded file.
1. Navigate to the unzipped folder.
1. Launch SPFlashTool by running or executing the `flash_tool` file.
1. Enable `Auto Reboot` in SPFlashTool by navigating to `Options -> Download -> Auto Reboot after Download`.

## Steps to flash /e/OS

### Step 1: Download the Latest Firmware

- Find the codename for your device by searching your device's brand and model in our [Smartphone Selector](https://doc.e.foundation/devices). For example, the codename for Gigaset GS290 smartphone is `GS290` and the codename for Teracube 2e smartphone is `emerald`

- Download the latest firmware package for your device by appending the codename to this link - https://images.ecloud.global/stable/. For example, The firmware download link for Teracube 2e (`emerald`) would be - https://images.ecloud.global/stable/emerald.

- Unzip the downloaded firmware.

### Step 2: Configure SPFlashTool

1. In SPFlashTool, choose the scatter file (`*_Android_scatter.txt`) from the firmware directory unzipped in the previous step. See the screenshot below for reference.
   ![Scatter File Selection.](/images/support-topics/spflash/Screenshot_from_2023-11-30_11-30-30.png)

1. Set SPFlashTool's mode to `Firmware Upgrade`. This mode resets verity, flashes the necessary images, and re-locks the device. See the screenshot below for reference.
   ![Flash Tool Configuration.](/images/support-topics/spflash/flash_tool_m2_firmware_upgrade.png)

### Step 3: Flash the Firmware

{% include alerts/tip.html content="It might seem counterintuitive, but you must unplug the device from the host machine before following the next action. You will plug it back in the second action." %}

1. Start the flashing process by clicking `Download` in SPFlashTool.
   ![Download Button.](/images/support-topics/spflash/flash_tool_m2_download.png)

2. Power off the device and connect it to the host machine using a cable that supports data transfer.

   {% include alerts/tip.html content="Use a USB3 port to avoid detection issues." %}

3. The flashing process should start automatically once it detects the device. This process may take several minutes to finish, please be patient.

4. You'll see a big green tick in SPFlashTool once the process is complete, and the device should reboot automatically. At this point, it's safe to unplug the device from the host machine.
   ![Download Completion.](/images/support-topics/spflash/Screenshot_from_2023-11-30_11-39-36.png)

### Step 4: Verify the Installation

- **Automatic Reboot**: The device should reboot automatically after the flashing process.
- **Welcome Screen**: The device may take a few extra minutes to boot after a flash. Please be patient, and eventually you should see the /e/OS welcome screen.

### Step 5: Enjoy!
Congratulations! Your Mediatek device is now running /e/OS. :tada: 