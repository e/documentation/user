### Do you offer /e/-preloaded phones?

Yes - New and refurbished smartphones flashed with /e/OS, [are available now](https://e.foundation/e-pre-installed-smartphones/).

### How do I access my country of choice on your website?

To see this list of countries
- Browse to our [murena shop](https://murena.com/)
- On the navigation bar you would see a drop down  as shown in the screenshot below

![](/images/eshop_country_selection.png)

The nation wise drop-down list displays the following information
- Latest list of nations we sell our devices in 

On selecting a particular nation you would be able to see
- Devices available for sale in that particular country.

### Is it possible to change the language of [murena shop](https://murena.com/)
Yes, this is possible. On the navigation bar you will see the option to change the language

At present we support the following languages
- English
- French
- German

### Is it possible to install and use Google Play store apps on the /e/OS ?

A wide range of applications can be installed on the /e/OS. We ensure this to retain maximal compatibility as well as to cater to a wider range of users.

That being said not all of the available applications may work. Applications that require the complete Google code base to function may not work on /e/OS. 

You can check [this list](https://community.e.foundation/t/editable-list-of-apps-that-work-or-do-not-work-with-microg/21151)  of apps that work or do not work on /e/OS. 

Here you will find a list focused on [banking applications](https://community.e.foundation/t/list-banking-apps-that-work-on-e-os/33091) that work with /e/OS. 

We are working on improving the application coverage.
