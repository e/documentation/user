We recommend using Spot, our metasearch tool for the /e/OS browser. Spot combines web search results from various sources to provide the most comprehensive results. It is a fork of Searx. Keep your activity private using Spot and our /e/OS browser's built-in ad-blocker.

You are, however, free to use any other search engine. Please follow the steps below to change the default search engine.

**Step 1**: Launch the /e/OS browser.

![](/images/tips/navigate-to-browser.png)

**Step 2**: In the top right corner, tap on the three-dot menu.

![](/images/tips/tap-three-dots-top-right.png)

**Step 3**: Tap on settings.

![](/images/tips/tap-settings.png)

**Step 4**: Tap on 'Search engine'.

![](/images/tips/tap-search-engine.png)

**Step 5**: Choose your preferred search engine.

![](/images/tips/choose-the search-engine-of-your-choice.png)

The search engine you selected will be used the next time you search.