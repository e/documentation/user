{% include alerts/warning.html content="This page is a work in progress. We are adding more details with time." %}

/e/OS is intended to be a privacy-focused system, so we strive to be as transparent as possible about any outbound, inbound connections made by /e/OS to first-party, third-party domains and servers. First-party domains and servers adhere to our policy's privacy guidelines, but Third-party domains and servers may keep logs and may not be as private as ours. You can check their privacy policies on their respective websites.

## First-party connections:

1. An outbound connection to `gitlab.e.foundation` is used to fetch domain filter lists for Advanced Privacy.

## Third-party connections:

1. [Some Google domains](https://doc.e.foundation/calls_to_google_servers.html) when microG is enabled.
1. An outbound connection to 'gitlab.com' is being looked into.
1. Outbound connections to `SUBDOMAIN.mediatek.com` are used to download **Extended Prediction Orbit** and **Quarter Extended Prediction Orbit** along with respective **md5sum checksum files** for GPS on Mediatek devices. 'SUBDOMAIN' part varies depending on your OS version.
