/e/OS is a deGoogled mobile OS designed for the non technical users. 

## What does "deGoogling" mean? 

The term "deGoogling" may perplex some, as it doesn't imply a complete disassociation from Google. The core Android project (AOSP) is under Google's stewardship, and Android mobile applications frequently depend on Google Play Services. Thus, "deGoogling" should be interpreted as enabling users to evade Google's incessant data gathering.

/e/OS has historically been (since 2018) the first mobile OS to solve this complicated equation by replacing all the proprietary Android components that communicate with Google Play services by an open source layer called "microG". This layer, which can be disabled, substitutes certain Google Play Services and anonymizes any remaining calls to them, such as Push Notifications. Read more about deGoogling in /e/OS, Advanced Privacy features in /e/OS (mobile apps trackers killer, fake gelolocation, fake IP address...) and online services we provide (personal workspace, search engine...): https://doc.e.foundation/what-s-e#degoogling--ungoogling-in-eos

The standard setup of /e/OS is designed to provide an optimal user experience, particularly when using third-party applications. Certain applications need to communicate with Google servers to function correctly. However, in /e/OS, **any data transmitted to Google servers is systematically anonymized as a default setting**, ensuring the safeguarding of your privacy. 

We have created this page for transparency: like on any other Android-based mobile OS, you can detect some network calls to Google servers on your device running on /e/OS. This page explains where they are coming from, what they are used for and what information may be shared with Google.

Our goal is to focus on personal data protection while allowing users to enjoy a normal life with their phone. Our approach is to avoid sending identifiable information when we have to send data, while not degrading the user experience.

The other thing that is key to note is that /e/OS doesn't capture any logs, any location or app activity. With /e/OS, Google is not able to profile users and leverage data from the OS for its own purpose, nor to sell this data to third parties. In other words: with the default /e/OS setup, Google is not able to capture any personal information from users.

## Device registration (microG)

In order to get access to the Google API (mainly to get access to notifications), microG has to register the device at Google.


- domain: `android.clients.google.com`
- when: once per day in background
- how to disable: **Settings** > **System** > **Advanced** > **microG** > **Google device registration** > disable
- known side effect: applications using any Google Play service may not work
- data shared
    - stripped device identifier (MAC addresses, IMEI): not fully anonymized to keep some brand and device model identifier
    - IP address
    - returns and Android ID

Please note you can define another profile from **Settings** > **System** > **Advanced** > **microG** > **Google device registration** > **Select profile**

## Push Notifications (microG)

A large number of Android applications are using and embedding the Google Push notifications framework to have notification messages received by users. As this framework is relying on the Google servers infrastructure, connecting to Google servers is needed to have Google Push notifications work. However, since we have totally replaced the proprietary Google Play Services piece of software from /e/OS by microG, connections to Google servers for the purpose of push notifications feature are done anonymously (by default on /e/OS). This means that the only thing Google knows is that they got a connection from a specific IP that is related to push notifications, but no more. So even if that is not totally perfect (an IP address can be used to track users) it’s considered to be good enough in terms of personal data privacy.

- domains:
    - `android.clients.google.com`: register the application for push notifications
    - `mtalk.google.com`: server used for push notifications
- when
    - when you first start an application that uses push notifications (`android.clients.google.com`)
    - then persistent connection to receive notification (`mtalk.google.com`)
- how to disable: **Settings** > **System** > **Advanced** > **microG** > **Cloud messaging** > disable
- known side effect: applications using Google Cloud Messaging (GCM) only for notifications may not receive notifications (at best) or not work at all (at worst)
- known side effects:
  - for applications using any Google Play services, some features may not work (at best) or the application will not work at all (at worst)
  - higher battery consumption
- data shared
    - stripped device identifier (MAC addresses, IMEI): not fully anonymized to keep some brand and device model identifier
    - IP address
    - the Android ID
    - the applications subscribing to notifications

## Safetynet (microG)

Safetynet is a security feature that Google suggests to app developers to ensure that their app is not running on a non-GoogleAndroid device (e.g. commercial Android you find on smartphones in stores, with the Google stamp on it). This check needs a connection to some Google servers. It’s an anonymous call (by default on /e/OS), so it doesn’t allow Google to track the user.

Please note Safetynet also has a reCAPTCHA feature, different from the [Firebase reCAPTCHA](https://developer.android.com/training/safetynet/recaptcha).

**Safetynet Parameters**

- domain: `www.googleapis.com`
- when: whenever an application requests a Safetynet attestation
- how to disable: **Settings** > **System** > **Advanced** > **microG** > **Google Safetynet** > disable
- known side effect: for applications requesting Satetynet attestation, some features may not work (at best) or the application will not work at all (at worst)
- data shared
    - stripped device identifier (MAC addresses, IMEI): not fully anonymized to keep some brand and device model identifier
    - IP address
    - the Android ID
    - the applications requesting Safetynet attestations
    - some others info from the build.prop, such as at least he following ones.
        - ro.boot.verifiedbootstate
        - ro.boot.veritymode
        - ro.build.version.security_patch
        - ro.oem_unlock_supported
        - ro.boot.flash.locked
        - ro.build.version.security_patch
        - ro.build.fingerprint
        - ro.product.model
        - ro.product.brand

{% include alerts/warning.html content="Please note this list may be updated in the future, according to what we will discover."%}    



## Firebase Authentication (microG)

Firebase is a Google backend providing features like realtime database, authentication and cloud messaging.

- domains:
    - `www.gstatic.com`
    - `securetoken.googleapis.com`
- when: when an application requests a Firebase Authentication
- how to disable: **Settings** > **System** > **Advanced** > **microG** > **Google device registration** > disable
- known side effect: for applications requesting Firebase Authentication, some features may not work (at best) or the application will not work at all (at worst)
- data shared
    - stripped device identifier (MAC addresses, IMEI): not fully anonymized to keep some brand and device model identifier
    - IP address
    - the Android ID
    - the applications requesting Firebase Authentication

## Firebase Authentication reCaptcha (microG)

- domains:
    - `www.gstatic.com`
    - `www.google.com`
- when: when an application requests a Firebase Authentication reCaptcha
- how to disable: **Settings** > **System** > **Advanced** > **microG** > **Google device registration** > disable
- known side effect: for applications requesting Firebase Authentication reCaptcha, some features may not work (at best) or the application will not work at all (at worst)
- data shared
    - stripped device identifier (MAC addresses, IMEI): not fully anonymized to keep some brand and device model identifier
    - IP address
    - the Android ID
    - the applications requesting Firebase Authentication reCaptcha

## Google Account management (not by default on /e/OS installation) (microG)

The following calls happen only when a user decides to sign in within microG

- domains:
    - `www.googleapis.com`
    - `android.googleapis.com`
    - `waccounts.google.com`
- when: when a user decides to sign in within microG
- how to disable: **Settings** > **System** > **Advanced** > **microG** > **Account** > logout
- known side effect: user will be authenticated anonymously when using microG
- data shared
    - stripped device identifier (MAC addresses, IMEI): not fully anonymized to keep some brand and device model identifier
    - IP address
    - the Android ID
    - the user Google account

## Google Account sign in (not by default on /e/OS installation) (microG)

microG gives you the possibility to log in with your Google account. It's required for some third party applications in order to work properly.

- domain: `android.googleapis.com`
- when: when an application requests a Google Account sign in
- how to disable: **Settings** > **System** > **Advanced** > **microG** > **Account** > logout
- known side effect: for applications requesting Google Account sign in, some features may not work (at best) or the application will not work at all (at worst)
- data shared
    - stripped device identifier (MAC addresses, IMEI): not fully anonymized to keep some brand and device model identifier
    - IP address
    - the Android ID
    - the user Google account

## App Lounge

App Lounge is fetching data from Google Play Store directly to get access to the whole catalog of Android applications. We offer two options for this: anonymous access and real Google account. It’s the user’s choice, but in any case there is an option to avoid being tracked by Google. The only issue here remains the IP address, but it’s a smaller issue.

- domain: `android.clients.google.com`
- how to disable: Reset `App Lounge` app data, and don't open it after
- Known side effect: You will not be able to access applications from the Google Play Store within App Lounge nor will you be able to get updates from App Lounge for your installed apps.
- data shared
    - The Google account if setup, otherwise the anonymous one
    - The list of install application (for updates)
    - Device properties
      - Build.RADIO
      - Build.BOOTLOADER
      - Screen.Density
      - GL.Extensions
      - HasFiveWayNavigation
      - Build.BRAND
      - Build.ID
      - Platforms
      - TouchScreen
      - Build.FINGERPRINT
      - Vending.version
      - Screen.Width
      - Build.HARDWARE
      - Build.VERSION.RELEASE
      - Build.VERSION.SDK_INT
      - Build.MODEL
      - Locales
      - SharedLibraries
      - GL.Version
      - GSF.version
      - Screen.Height
      - Vending.versionString
      - HasHardKeyboard
      - Features
      - Navigation
      - UserReadableName
      - Build.MANUFACTURER
      - Keyboard
      - Build.DEVICE
      - ScreenLayout
      - Build.PRODUCT

##  A-GPS and SUPL servers

⚙️ Under investigation with the /e/OS development team.

## More about deGoogling in /e/OS


- deGoogling scope and definition within [the context of /e/OS](https://doc.e.foundation/support-topics/deGoogling-scope-and-definition-within-the-context-of-eos)


## Additional reference:
- [Clarifications on the GmsCore](https://github.com/microg/GmsCore/issues/1508#issuecomment-876269198)
- [Why is /e/OS connecting to google](https://community.e.foundation/t/e-page-says-e-is-ungoogled-degoogled-why-is-e-then-connecting-to-google/40707/59)
- [Security identifiers](https://calyxos.org/docs/guide/security/identifiers/)
