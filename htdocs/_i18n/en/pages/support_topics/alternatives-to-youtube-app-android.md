We've received reports from /e/OS users where YouTube App stops loading videos after one minute. Currently there are no working fixes, but there are some workarounds.

{% include alerts/tip.html content="We will update this page once we find a working solution to this issue" %}

## Workarounds

1. Watch YouTube videos in the Browser.

2. Use Third-Party apps to watch YouTube videos. We've verified that the following apps work well - 

- [NewPipe](https://f-droid.org/packages/org.schabi.newpipe/) ([Website](https://newpipe.net/))
- [LibreTube](https://f-droid.org/en/packages/com.github.libretube/) ([Website](https://libre-tube.github.io/))
- [Clipious](https://f-droid.org/en/packages/com.github.lamarios.clipious/) ([Website](https://github.com/lamarios/clipious))

You can download these apps from the App Lounge or from their official websites.

## Pros of using workarounds

Our Browser app and the above third-party apps block Ads and Trackers from YouTube. LibreTube and Clipious even block the in-video sponsors section using their integrated [SponsorBlock](https://sponsor.ajay.app/) extension.