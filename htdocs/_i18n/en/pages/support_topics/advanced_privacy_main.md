<div class="text-center">
  <img src="/images/advanced_privacy_main.png" alt="">
</div>

&nbsp;

Advanced Privacy is a tool we have developed to limit your data exposure once you have installed third party apps.
When an application snoops in the background, it will use trackers to log your activity even if you are not using the app. It will also collect the IP address, so it can potentially link internet activity to a specific device and to a persona, and finally it will try to pinpoint your exact location. 

- #### [Learn all about Advanced Privacy and it features](/support-topics/advanced_privacy)
  
### Some more privacy related topics

- #### [How to block or unblock tracker per application](/support-topics/block-unblock-trackers-per-app)
- #### [Does a work profile break features ?](/work-profile-breaks-features)