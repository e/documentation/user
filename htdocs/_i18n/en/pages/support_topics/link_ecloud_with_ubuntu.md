## Link

{% include alerts/tip.html content="We have taken the screenshots on a Ubuntu 22.04 LTS system. The process should be the same for other Ubuntu versions as well." %}

### Step 1
On your Ubuntu system browse to 

`Show Applications` >> `Settings` 
Here scroll down to `Online Accounts`

![](../images/howtos/ecloud/ecloud_link_ubuntu_1.jpg)


### Step 2
Clicking the `NextCloud` option opens this dialog box

![](../images/howtos/ecloud/ecloud_link_ubuntu_2.jpg)

### Step 3 
Here enter the credentials as under

- `server` : `murena.io`
- `Username` : Your murena ID username. Write the complete username in the format username@e.email or username@murena.io
- `Password` : Your murena ID password
- Press `Connect`

![](../images/howtos/ecloud/ecloud_link_ubuntu_3.jpg)

### Step 4
On succesfully connecting you will be show this screen

{% include alerts/tip.html content="Click the `X` icon on top right of the pop-up to close this screen. Clicking the `Remove Account` will remove the account you just created"%}
![](../images/howtos/ecloud/ecloud_link_ubuntu_4.jpg)
## Verify
- In the Settings

You should now be able to see the newly created connection under `Settings` >> `Online Accounts`

![](../images/howtos/ecloud/ecloud_link_ubuntu_5.jpg)
- In the Files App

You should be able to see a link to the Murena Workspace account on the left side in the links. It would be in the format yourusername@murena.io@murena.io as seen in the screenshot

![](../images/howtos/ecloud/ecloud_link_ubuntu_6.jpg)

- Clicking on the link will display the folders on your Murena Workspace. 
{% include alerts/tip.html content="It may take a couple of seconds to open up the folder view as now it is accessing a folder across the network. Please be patient."%}

![](../images/howtos/ecloud/ecloud_link_ubuntu_7.jpg)

## Use

You can now copy folders and files from your local PC to the Murena Workspace. In this screenshot we show two windows one displaying the local folders and the other the Murena Workspace folders. 

Note that the copy process across the folders will depend on your network speed so be patient for the copy process to complete. 

![](../images/howtos/ecloud/ecloud_link_ubuntu_8.jpg)


## Delink

To remove or delink the Murena Workspace connection you have two options

### From the Files App 

- Right click on the link in the Files App and choose Unmount 

![](../images/howtos/ecloud/ecloud_link_ubuntu_9.jpg)

### From Settings

- `Settings` >> `Online Accounts` >> Select the Murena Workspace account >> Click the `Remove account` button

![](../images/howtos/ecloud/ecloud_link_ubuntu_4.jpg)