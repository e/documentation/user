- [Build the /e/OS ROM ](build-e)
- [How much space do I need to build for /e/?](https://community.e.foundation/t/howto-how-much-space-do-i-need-to-build-for-e/3014)
- [Some useful Docker commands](https://community.e.foundation/t/howto-some-useful-docker-commands/3075)
- [Find files in your source tree](https://community.e.foundation/t/howto-find-files-in-your-source-tree/4794)
- [An unofficial / e / ROM built with Docker – in pictures](https://community.e.foundation/t/howto-an-unofficial-e-rom-built-with-docker-in-pictures/2445)
- [I am trying to build /e/OS but the download seems to be stuck](https://community.e.foundation/t/howto-i-am-trying-to-build-e-but-the-download-seems-to-be-stuck/3013)
- [Build /e/OS without docker](https://community.e.foundation/t/howto-build-e-without-docker/3149)
- [Build /e/OS without docker for non LineageOS supported devices](https://community.e.foundation/t/howto-build-e-without-docker-for-non-lineageos-supported-devices/3991)
- [Building unofficial /e/OS rom on a cloud VM - various options](https://community.e.foundation/t/howto-building-unofficial-e-rom-on-a-cloud-vm-various-options/6877)
- [Build /e/OS the full classic way no docker, no scripts, just ‘make’](https://community.e.foundation/t/howto-build-e-the-full-classic-way-no-docker-no-scripts-just-make/19364)
- [First steps if your build won’t boot](https://community.e.foundation/t/how-to-first-steps-if-your-build-wont-boot/5480)
