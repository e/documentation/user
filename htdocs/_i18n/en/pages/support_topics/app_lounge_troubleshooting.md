## Tips and tricks to navigate around issues in App Lounge

### I can't find an App  
`Solution:` Turn OFF Fake location or/and IP spoofing while you search for the app, and you'll be able to find it.

### I am unable to install or update apps  
`Solution:` Log out and log in to your account via the settings in App Lounge.  

### I am unable to install or update apps even after logging out and logging in  
`Solution:` Clear App Lounge's data by navigating to Settings > Apps > App Lounge > Storage & cache > Clear storage.

### I am unable to login in App Lounge via an Anonymous account  
`Solution:` Clear App Lounge's data by navigating to Settings > Apps > App Lounge > Storage & cache > Clear storage, then wait for a while before you use the Anonymous account login again.  

### I am still unable to login in App Lounge with an anonymous session
`Solution:` Instead of login anonymously, login with a dumb **Google account that you will use only for downloading apps with App Lounge**. Note: it is recommended to use an authenticator app such as FreeOTP/FreeOTP+ along with this Google account: first add "authenticator" (they call it Google Authenticator) as second step in your Google account security configuration, and then enable 2FA using Authenticator. You can most probably avoid adding a phone number in this case.
