With the launch of /e/OS v2.0, we've added support for Android Auto. Follow along to setup Android auto with /e/OS.

{% include alerts/tip.html content="The current implementation is so far compatible only with Android 13 version of /e/OS. In addition, it requires to install and use some applications coming from Google. We are exploring ways to bypass those requirements."%}

## Install required applications

- Install the `Google` app ([open in app lounge](https://play.google.com/store/apps/details?id=com.google.android.googlequicksearchbox)).

- Install `Google Maps` ([open in app lounge](https://play.google.com/store/apps/details?id=com.google.android.apps.maps)):

- Install `Google Speech Recognition & Synthesis` app ([open in app lounge](https://play.google.com/store/apps/details?id=com.google.android.tts)):

- Finally, install the `Android Auto` app ([open in App Lounge](https://play.google.com/store/apps/details?id=com.google.android.projection.gearhead)):

## Setup Android Auto on your Car

- Connect your /e/OS device with a Car that's compatible with Android Auto. Only a USB cable that supports data transfer will work.

- If everything goes well, you should see the following screen. Tap `OK` if you agree:

    ![](/images/support-topics/android-auto/Capture d’écran du 2024-05-16 10-40-26.png){: height="400" }

- Tap on `Continue` if you agree:

    ![](/images/support-topics/android-auto/Capture d’écran du 2024-05-16 10-40-57.png){: height="400" }

- Android Auto will ask you to continue the setup on your /e/OS device. Tap on `Go to phone`:

    ![](/images/support-topics/android-auto/Capture d’écran du 2024-05-16 10-41-17.png){: height="400" }


- On your /e/OS device you'll be asked to turn ON notification access for Android Auto. If you agree, tap on `Android Auto` -> `Allow notification access` -> `Allow`:

    ![](/images/support-topics/android-auto/photo_2024-04-10_15-11-32__6_.jpg){: height="600" }
    ![](/images/support-topics/android-auto/photo_2024-04-10_15-11-32__7_.jpg){: height="600" }
    ![](/images/support-topics/android-auto/photo_2024-04-10_15-11-32__8_.jpg){: height="600" }


- Once all permissions are granted, you'll be able to see the Android Auto interface on your Car: 

    ![](/images/support-topics/android-auto/Capture d’écran du 2024-05-16 10-42-08.png){: height="400" }

## Setup Google Maps

- On your /e/OS device: Navigate to `Settings` -> `Apps` -> `Google Maps` -> `Permissions` -> `Location`:

    ![](/images/support-topics/android-auto/photo_2024-04-10_15-11-32.jpg){: height="600" }
    ![](/images/support-topics/android-auto/photo_2024-04-10_15-11-32__2_.jpg){: height="600" }

- Set the location permission as `Allow while using the app`:

    ![](/images/support-topics/android-auto/photo_2024-04-11_16-28-34.jpg){: height="600" }

- Once the permission is granted, Google Maps will work with Android Auto.

    ![](/images/support-topics/android-auto/Capture d’écran du 2024-05-16 10-43-03.png){: height="400" }

## Troubleshoot apps not working with Android Auto

Sometimes apps like Spotify don't work Android Auto (despite them being compatible). If you're face this issue, follow the steps below:

- Launch the `Android Auto` app on your /e/OS device.
- Scroll to the bottom and tap `Version` 10 times.
- Navigate to Developer settings by tapping the `three-dot button` at the top-right corner and taping `Developer settings`.
- Scroll down to `Unknown sources` and turn it `ON`.