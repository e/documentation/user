Murena has its own search engine, "Spot". It is built on Searx. Of course, it does not track your searches and also has some great features. One of those features includes quick answers, which detect when a search term involves conversion of currencies, units, or arithmetic operations, among other questions.

1. Currency converter

    Spot's currency conversion feature is enabled by default and comes in handy when converting amounts from one currency to another. Spot will provide you with instant conversion results when you search for something like `100 USD to EUR`, how much is `10 USD to INR`, or even in languages other than English, such as `100 EUR en USD`. 

2. Arithmetic Calculator

    Quick question… what is the square root of 4729? No worries, Spot can help you here. Simply enter an arithmetic you want to perform and Spot will answer it.

    - 10*32+320-3238 
    - 100^8

    Spot can also perform advanced calculations like `tan(90)` and `sqrt(100)`. The full list of supported operations is available in the [Math.js docs](https://mathjs.org/docs/expressions/syntax.html#operators). 

3. Units conversion

    Spot can also help you with those pesky unit conversions. It supports conversions to and from imperial, SI/metric and even handles arithmetic operations within the conversions.

    Here are some examples:
    - 10 km to miles
    - 100 kmph to mph
    - 1 gallon to liter
    - 100 liters to gallon
    - 10V * 5A

    Along with conversions, you can quickly perform operations involving constants such as `2*pi`, `3*avogadro`, and `speedOfLight in km/h`. Constants are case-sensitive, so check the [Math.js docs](https://mathjs.org/docs/datatypes/units.html#reference) for a complete list. 
