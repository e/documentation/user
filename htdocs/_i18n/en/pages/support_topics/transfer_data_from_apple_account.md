## Introduction
Switching from iOS to /e/OS can seem daunting, but with the right steps, you can ensure a smooth transition. This guide will walk you through transferring your data from your iPhone and iCloud to /e/OS and Murena Workspace.

## Prerequisites

- An iPhone with all your data.
- Access to your iCloud account.
- A device with /e/OS installed.
- Murena Workspace account (not mandatory).
- A computer (for certain steps).

## Step-by-Step Guide

### 1. Back up your iPhone data
 - On your iPhone, go to `Settings` > `[your name]` > `iCloud`.
 - Touch `iCloud Backup`.
 - Touch Back up now. Wait for the backup process to complete.


### 2. Export iCloud data (computer required)


a. Contacts:
- On your computer, go to `iCloud.com` and log in.
- Click on `Contacts`
- Select `all contacts` (Command+A for Mac, Ctrl+A for Windows).
- Click on the square icon with the arrow pointing upwards and choose `Export vCard`.

![](/images/howtos/apple_contacts.png)

b. Photos and videos :

- On your computer, go to `iCloud.com` and log in.
- Click on `Photos`.
- Select the photos and videos you want to export.
- Click on the icon with the dots in a circle at the top right to access `More download options`
- Select `Better compatibility`.

c. Calendar :

- On your computer, go to `iCloud.com` and log in.
- Click on `Calendar`.
- Select the calendar you want to access and click on the icon that looks like a person.

![](/images/howtos/apple_calendar.png)

- Share the calendar publicly and copy the URL.
- Replace `webcal` with http in the copied URL and paste it into a new browser tab.
- The `ICS file` will be downloaded automatically.


### 3. Transfer data to /e/OS (some steps require a computer)

a. Activate file transfer on /e/OS :

- Connect your /e/OS device to your computer using a USB cable.
- Swipe down from the top of the screen to access the notification panel.
- Tap on the USB notification and select `File Transfer` or `MTP` (Media Transfer Protocol) mode.

b. Contacts :

- On your /e/OS device, go to `Contacts`.
- Tap on the menu (three dots) > `Settings` > `Import`.
- Choose the `vCard` file you exported from iCloud.

c. Photos and videos (computer required):

- Connect your /e/OS device to your computer.
- Drag photos and videos from your computer to the `DCIM/Camera` folder on your /e/OS device.

d. Calendar :

- On your /e/OS device, go to `Calendar`.
- Tap on the menu (three dots) > `Settings` > `Import`.
- Choose the `ICS file` you have downloaded from iCloud.

e. SMS/MMS and call logs:

Unfortunately, Apple doesn't provide a direct way to export SMS/MMS and call logs. You may need third-party software on your computer to extract this data from an iPhone backup. Once extracted, you can import them into /e/OS using appropriate applications.

f. Encrypted messaging applications - Signal and WhatsApp :

Signal: Signal does not support transferring chats between platforms (iOS to Android). Refer this [external article](https://support.signal.org/hc/en-us/articles/360007059752-Backup-and-Restore-Messages)


WhatsApp: Use the WhatsApp `Export Chat` feature on your iPhone to export individual chat histories as text files. On /e/OS, install WhatsApp, verify your number, then use the `Import Chat` function.

g. Applications and application data:

Direct transfer of apps and their data from iOS to Android is not possible. You will need to manually download Android versions of your iOS apps from the /e/OS app shop or other Android app shops. For app data, check if the app supports cloud backup or cross-platform data transfer.

### 4. Upload data to the Murena Workspace (computer required):

a. Contacts:

- On your computer, sign in to your `Murena Workspace account`.
- Go to `Contacts`.
- Click `Settings` > `Import` and choose the `vCard` file.

b. Photos and videos:

- On your computer, log into your `Murena Workspace account`.
- Go to `Photos` or `Files`.
- Drag and drop or use the upload button to upload your photos and videos.

c. Calendar:

- On your computer, sign in to your `Murena Workspace account`.
- Go to `Calendar`.
- Click on `Settings` > `Import Calendar` and choose the `ICS file`.

## Conclusion


Transferring data from iPhone and iCloud to /e/OS and Murena Workspace might take some time, but following these steps will ensure you don't miss out on any important data. Once you've completed the transfer, make sure to double-check if all your data (contacts, photos, videos, calendar events, SMS/MMS, call logs, and app data) is present on your new device and Murena Workspace.