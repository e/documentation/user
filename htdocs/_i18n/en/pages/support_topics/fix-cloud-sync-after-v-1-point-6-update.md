Murena Workspace sync doesn't work for Murena Workspace account setup on /e/OS v.1.15. This issue mostly affects those who received a new device with /e/OS v1.15 pre-installed.

## Solution - Log out and log in

Luckily, the solution to this issue is to just log out and log in to your Murena Workspace account after you upgrade /e/OS from v1.15 to v1.16. So, just log out and log back in to your Murena Workspace account if you are unsure about why cloud sync is not working.

## Steps to Log out and log in

Follow the steps below to log out and log in to your Murena Workspace account:

1. Open `System settings`

![](/images/howtos/fix-cloud-sync-after-update/open-sys-settings.jpg)

2. Scroll down to find the `Accounts` menu

![](/images/howtos/fix-cloud-sync-after-update/tap-on-accounts.jpg)

3. In the `Accounts` menu tap on your Murena Workspace account

![](/images/howtos/fix-cloud-sync-after-update/tap-on-your-murena-account.jpg)

4. Tap `Remove account`, after which you will be taken back to the `Accounts` menu

![](/images/howtos/fix-cloud-sync-after-update/tap-on-remove-account.jpg)

5. Tap on `+ Add account`

![](/images/howtos/fix-cloud-sync-after-update/tap-add-new-account.jpg)

6. Select `Murena account` and follow the log in process.

![](/images/howtos/fix-cloud-sync-after-update/select-murena-account-to-add-account.jpg)
