Murena One supports all network bands required to work with AT&T in the USA. It is fully compatible with their network and should work with no issues. That being said, it is likely that AT&T will fail to automatically detect the device. In such cases, you will have to contact AT&T with a request to manually activate their services for your phone.

Our beta testers and many AT&T customers have solved this issue by following these steps:

1. Visit an AT&T store or chat with an agent on ATT.com.
2. Request a new line or an update to an existing line for a BYOD phone.
3. The agent may request you test the device's compatibility with the phone IMEI.

If it still does not detect the device, you will have to ask the agent to take a manual action and 'trick' the system into thinking it is an AT&T phone (like a Pixel 6a). This will allow you to use calls and VoLTE services of AT&T on your Murena One.
