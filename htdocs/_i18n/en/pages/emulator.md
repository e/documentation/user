
This page aims to give details to use an /e/ OS Emulator.

## Download

1. Install Android Studio from [https://developer.android.com/studio](https://developer.android.com/studio)
2. Download the emulator image from [https://images.ecloud.global/dev/sdk_phone_x86_64/](https://images.ecloud.global/dev/sdk_phone_x86_64/)

## Extract the emulator image 

Extract the emulator archive into the following directory:

```plaintext
SDK_PATH/system-images/android-33/lineage/
```

with `SDK_PATH` corresponding to your Android SDK path (i.e. $ANDROID_HOME)

{% include alerts/tip.html content="We use `android-33` as the current emulator runs Android 13"%}

Once you extracted the archive, you will end up with something like this:

```shell
$ ls ~/Android/Sdk/system-images/android-30/lineage/x86
advancedFeatures.ini  build.prop  data  encryptionkey.img  kernel-ranchu-64  NOTICE.txt  package.xml  ramdisk.img  source.properties  system.img  userdata.img  vendor.img  VerifiedBootParams.textproto
```

## Using the emulator from Android Studio

1. Open the **AVD Manager** with the icon located at the top right of the screen
2. Click on **Create Virtual Device**
3. Select the **Nexus 5X** phone entry for instance and click Next
4. In the **x86 Image**s tab, select the **LineageOS** corresponding to your version of Android API and click Next
5. Name your AVD device **/e/OS** and click Finish
6. In the **AVD Manager** launch your /e/ OS virtual device

   ![emulator](/images/emulator.png)
