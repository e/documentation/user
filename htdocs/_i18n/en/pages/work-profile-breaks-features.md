We have observed that some /e/OS features won't function properly with a work profile set up. This was tested by adding a work profile using the Shelter app.

In our tests, Advanced Privacy's Fake my location wasn't able to change the device location because the work profile setup using shelter app broke Advanced Privacy's access to system interfaces, including /e/OS' location providers.

We are presently looking into the problem, so a fix might not be available soon. Until then, you will have to remove the work profile and uninstall shelter to avoid any bugs. If you only uninstall shelter, but continue to use a work profile, an unexpected process related to Advanced Privacy continues to run even after a restart and might introduce bugs like the one above.
