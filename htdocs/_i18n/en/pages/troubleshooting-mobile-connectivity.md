## Check your mobile data settings

Go to `Settings > Network & internet > Mobile Network > tap on the tab of your provider if you have 2 SIM cards > tap on Advanced > Access Point Names`. This will show you the APN settings on your device.

Tap the `Access Point` itself to `edit` the settings or `add new settings` by tapping the `+` sign. Note that these settings are provided by network operators. Look For APN settings on your mobile operator website or contact your network operator to get the correct and up to date settings.

## Try different connection protocols (4G/3G/2G)

If the issues persist, the phone might be trying to connect using a network protocol that is not supported by your contract.

Go to `Settings > Network & internet > Mobile Network > tap on the tab of your provider if you have 2 SIM cards > Advanced , then Preferred network type`.

Cycle through the options available and see if you manage to get some data connection using at least one of the protocols.

## Set preferred SIM card for mobile data

Keep in mind that your phone connects using only the default SIM Card so, if you use two SIM Cards, make sure to set the preferred SIM Card for mobile data.

Go to Go to `Settings > Network & internet > Mobile Network > tap on the tab of your provider if you have 2 SIM cards > turn mobile data` off and on. You may need to confirm switching, as you can use only one SIM card for mobile data.

## Network settings reset

Go to S`etings > System > Advanced > Reset Options > Reset Wi-Fi, mobile & Bluetooth`. For older devices the path is: `Setings > System  > Reset Options > Reset Wi-Fi, mobile & Bluetooth. Then choose which SIM card if more than one > Reset settings`.  Go back and do it for the other SIM card as well if you have 2 cards. Then try step 3 again.

## Try a couple of different SIM cards

Borrow a couple of SIM cards from family or friends and test them with your device. Make sure to go through all the steps you already tried with your own SIM card.
If you get a data connection with other SIM cards, it is likely that something is wrong with either your SIM card or your contract/data plan. Please get in touch with your mobile service provider.

## Check your network operator

Make sure the phone connects to the right network.

Go to `Settings > Network & internet > Mobile Networks` and select the tab related to the SIM card you are troubleshooting.

Disable the option `Automatically select network`. The phone will scan for all available operators. This might take a bit.

Manually select your operator (provider). If everything goes as planned and your data connection starts working again, you might have been registered on the wrong network.

## Issue specific to 4G (AT&T) Straight Talk

In `Settings` >> `Network Settings` >> `Mobile and Internet` >> `Advanced` >> `Access points names` >>  `Add access point` ( click the `+` button on the top)
- Set these fields:
  - Name: ims
  - APN: ims
  - APN type: ims
  - APN protocol IPV4/IPV6
  - APN roaming protocol IPV4/IPV6
  - Hit three dots menu on the top right and click  `Save`
  - Restart the phone

## Collect data and send it to us

Please make a screenshot of your APN settings, from step 1. Then, go to your dialer (the  Phone app), type `*#*#4636#*#*` on the keypad to access a phone test menu.  Click on `Phone information` and make screenshots with all of the information.

You can compare your signal strength with below data to see if your signal is good enough.

![](/images/signal_strength.png)

If you cannot resolve the issue. Please send all screenshot(s), including your country and the names of your mobile operator(s) and {% translate content.contact_helpdesk %}