{% include alerts/warning.html content=" A new installer the [WebInstaller](https://e.foundation/installer/) is now available as an alpha version. The Webinstaller works on Chrome based browsers as it requires WebUSB.
The EasyInstaller will be deprecated soon, hence moving forward we will not proceed with any error fixes on it " %}

## Installation steps for MacOS

### Download

1. To download the EasyInstaller beta for MacOS click [here](https://images.ecloud.global/easy-installer/EasyInstaller.dmg).

2. Save it to a folder on your Hard Drive.

3. Verify the [sha256sum](https://images.ecloud.global/easy-installer/EasyInstaller.dmg.sha256sum).

### Execute

1. Double click to open.

   ![](/images/mac_setup_easyinstaller_open_dmg.png)

1. Drag & drop the Icon on your desktop.

   ![](/images/mac_setup_easyinstaller_drag_and_drop.png)

1. Move the icon you have just dropped on the desktop into the applications folder. A double click on the icon should open the EasyInstaller application.

Now enjoy the EasyInstaller on macOS! Kindly report any issues with the EasyInstaller [here](https://gitlab.e.foundation/e/backlog/-/issues/new).

## Troubleshooting

### If you get an Install `Rosetta` pop up


![](/images/mac_setup_easyinstaller_popup_rosetta.png)

1. You will get a warning pop up.

    ![](/images/mac_setup_easyinstaller_popup_keystrokes.png)

1. Click on `Open System Preferences`

1. Enter your credentials

    ![](/images/mac_setup_easyinstaller_keystrokes_credentials.png)

1. Look for `Java` under `Privacy` tab and tick the checkbox beside to it

    ![](/images/mac_setup_easyinstaller_keystrokes_java.png)

1. Restart the Easy-installer

### If you get an "EasyInstaller cannot be opened" message


![](/images/mac_setup_easyinstaller_cantopen.png)

1. Open `Security and privacy` preferences.

1. Click on `Open anyway`

    ![](/images/mac_setup_easyinstaller_openAnyway.png)

1. Restart the EasyInstaller
