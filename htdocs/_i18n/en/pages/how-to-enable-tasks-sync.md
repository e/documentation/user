Following the deployment of /e/OS v0.8, the Tasks synchronisation have to be enabled again. Here how to proceed.

## Nougat

1. Go into **Settings**

    ![](images/screenshot_20200415_15_38_03.png)

2. Scroll to find **Accounts**


    ![](images/screenshot_20200415_15_38_10.png)


    ![](images/screenshot_20200415_15_38_17.png)

3. Select your account **/e/** (first entry)

    ![](images/screenshot_20200415_15_38_24.png)

4. Tap on your **e.email** address

    ![](images/screenshot_20200415_15_38_29.png)

5. If **Tasks** is unchecked, check it to enable again tasks synchronisation

    ![](images/screenshot_20200415_15_38_37.png)

    ![](images/screenshot_20200415_15_38_41.png)

## Oreo

1. Go into **Settings**

    ![](images/screenshot_20200415_15_32_49.png)

2. Tap on **Users & Accounts**


    ![](images/screenshot_20200415_15_32_56.png)


3. Select your **e.email** account (first entry)

    ![](images/screenshot_20200415_15_33_31.png)

4. Tap on **Account Sync**

    ![](images/screenshot_20200415_15_33_34.png)

5. If **Tasks** is unchecked, check it to enable again tasks synchronisation

    ![](images/screenshot_20200415_15_33_38.png)

    ![](images/screenshot_20200415_15_33_41.png)

## Pie

1. Go into **Settings** and scroll to find on **Accounts**

    ![](images/Screenshot_20200415-191315_Settings.png)


3. Select your **e.email** account (first entry)

    ![](images/Screenshot_20200415-191324_Settings.png)

4. Tap on **Account Sync**

    ![](images/Screenshot_20200415-191341_Settings.png)

5. If **Tasks** is unchecked, check it to enable again tasks synchronisation

    ![](images/Screenshot_20200415-191358_Settings.png)

    ![](images/Screenshot_20200415-191410_Settings.png)
