## Purpose of the document
This guide will share details on rooting and how to do it on devices with /e/OS.


{% include alerts/danger.html content="Rooting is not required to install or run /e/OS. Root your device only if you know what you are doing and do it at your own risk."%}


## What is Rooting?

Rooting is a manual process executed on an android device. It is done by a user to gain access to the root or core level of the device. By Rooting an android device, a user is able to:
- add, remove or alter system applications available on the device
- modify system settings
- install or run application that require administrator level privileges
- perform activities that are normally restricted to the user by device vendors
- monitor or enhance the smartphone functionality

## FAQ on rooting (<sub>From /e/OS perspective</sub>)

### Is it possible to root an /e/OS smartphone?

Yes, you can root a smartphone which has /e/OS installed.

### Are users required to root their device to install /e/OS?
No. Rooting is not required to install or run /e/OS. 

### Do you recommend /e/OS users to root their devices?
No. We do not recommend rooting. User who want to root their device should do it at their own risk.

### Why is rooting not recommended?

 - Rooting should only be done by experts
 - If not done correctly, rooting can brick your device
- Rooting should only be done by experts
- If not done correctly, rooting can brick your device
- Rooting will also void the warranty of your device


### Is rooting an /e/OS device different from rooting other smartphones
The concepts remain the same. There may be minor variations depending on the OS installed on the device.


## What has changed in /e/OS v1.0

The v1.0 source code includes a change which impacts the ability of the user to root. Previously there was an option to enable `adb root` through the `Developer Options`. This option will not be available with the code of v1.0 and greater. 

The reason behind this is 
- Use of `ih8sn`
- move to user builds


Lets learn more about these two points

### ih8sn
`ih8sn` is a tool developed by [LineageOS developers](https://github.com/luk1337/ih8sn) which helps to mask a rooted device . Please note LineageOS does not officially endorse this tool.


### user builds

- Android ROM's can be built with different configuration settings

  - `eng` or Engineering builds are used for development purpose. These builds have `adb` enabled by default.

  - `user-debug` builds are meant to help device developers understand the performance of in-development releases. It does not include root access by default, but it can be added. Includes extra logging features

  - `user` builds are meant for the final release to the end user. These builds do not allow root access.

- For v1 we will go ahead with the following configurations:

  - test builds would continue to be `eng` builds
  - community builds will be `userdebug` builds
  - official builds will be `userdebug` builds with ih8sn which fakes a user build



- Post v1 (what we plan to achieve)

  - test builds will continue to be `eng` builds
  - community builds will continue to be `userdebug` builds


## Why can't I root from developer options on official builds?

`official` builds intend to be offer the better user experience for our users. That's why we worked hard in order to offer Safetynet support for those devices, in order to improve third party app support. To do so, we had to fake the build type, moving from `userdebug` to `user` build (awaiting the move on `user` builds)

As a side effect, it disabled the `adb root` option, available under `Developer Options`. In order to recover it, we advise our users to move on a `/dev` build.

## How to root v1.0 /e/OS ROMs

### By enabling `adb rooting` in the developer options (only for community builds)

It is the safest way to root your device. This method only works on `community` builds. 

To root your /e/OS device
- Enable [Developer options](/pages/enable-usb-debugging) on the device and enable `adb root`
- You should see options similar to the screenshot show below

![](/images/adb_root.png)

- Enable the `Rooted Debugging` option
- Once done return from this screen back to the main

{% include alerts/tip.html content="If you are on a `official` build of /e/OS, enabling Developer Options will not display the adb rooting option. If you still want to root your device you will have to switch on a `/dev` build (or use Magisk)."%}


### Rooting by Using Magisk

The steps on how to install Magisk is not in the scope of this document. Interested users can check the guide linked below in the 'Additional References' section. Rooting with Magisk may create some side effects and strange behaviors on your installation.


{% include alerts/danger.html content="Rooting is not required to install or run /e/OS. Root your device only if you know what you are doing and do it at your own risk."%}


## Additional references

- [How to root](https://web.archive.org/web/20230831175328/https://www.xda-developers.com/root/)
- [ih8sn Github repo](https://github.com/luk1337/ih8sn)
- [Magisk](https://web.archive.org/web/20230910103619/https://www.xda-developers.com/how-to-install-magisk/)