{% include alerts/danger.html content="These instructions are only for Windows PC users."%}
{% include alerts/tip.html content="The installation will wipe all data on your phone. Take necessary backups of your data if required."%}

## Assumptions
- The user can't reboot to recovery or fastboot.


## Downloads

- OnePlus EDL ROM
    > [Global variant](https://androidfilehost.com/?fid=2188818919693750599)

    > [European variant](https://androidfilehost.com/?fid=8889791610682920993)

    > [Indian variant](https://androidfilehost.com/?fid=2188818919693750595)

{% include alerts/warning.html content="Ensure you download the latest stock ROM build specific to your country or location. "%}


## Steps
1. Extract the zip.

2. Launch `MsmDownloadTool` exe from the extracted zip.

3. Set the `user` type as `Other`.

4. Choose your variant from the `Target` box next to `Enum` button on the top left corner.

5. Plug in your phone.

6. Click `start` (So your device gets captured once its in EDL mode).

7. Click on the volume buttons and the power button at the same time.

8. Once the phone starts flashing leave the buttons and be careful not to unplug the device until flashing is done.

9. After the flashing process is done your device will reboot, and your bootloader will be locked.
