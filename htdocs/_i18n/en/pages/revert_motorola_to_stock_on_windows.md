{% include alerts/danger.html content="These instructions are only for Windows PC users."%}
{% include alerts/tip.html content="The installation will wipe all data on your phone. Take necessary backups of your data if required."%}

## Assumptions
- The user has a working motorola device with /e/OS on it.
- The user wishes to revert their Motorola device to Stock ROM
- This guide can also be used where user wants to retrieve a Motorola device which is soft bricked.
> Stock ROM here refers to the ROM that is provided by the vendor.

## Downloads

- [`Rescue and Smart Assistant Tool`](https://motorola-global-portal.custhelp.com/app/answers/prod_answer_detail/a_id/158726)

## Steps
- Enable Developer Mode by clicking on `Settings >> About Phone >> Build Number` multiple times.  A popup will let you know how many clicks are pending. 
- Enable ADB Debugging under `Settings >> System >> Advanced >> Developer Options` menu item
- Enable OEM unlocking under the Debugging menu.
- Ensure to enable `fastboot flashing unlock` otherwise you may get an error "Fastboot Flash Single Partition".
- Put your phone into fastboot mode, and then plug the USB cable in.

The tool should download the latest compatible ROM by scanning the following
 - phones Model
 -  IMEI
 -  Serial Number.  

You can choose to flash an older one manually, if you so desire

## Additional references

View the video of this process on the [motorola site](https://www.motorola.com/us/rescue-and-smart-assistant/p)