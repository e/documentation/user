## The Recovery Process

After the  Murena Workspace recovery process begins, your missing online entries and data will gradually reappear. Here's what to expect:
1. **Files entry** This will be the first entry to return, in a first time without any data
2. **Other entries** Entries like Notes and Gallery will reappear progressively.
3. **Data Recovery** Your recovered data will be placed in a folder named `MurenaRecovery` in the Files entry. Some files may be missing or empty, so please check thoroughly. Moreover as your recovered files are in a new dedicated folder the file sharings you may have set prior to the outage will not be effective: you need to set them again if you want them to work.

Murena Workspace will block requests coming from the following Nextcloud clients
- Carnet
- EasySync
- eDrive (only for /e/OS before 1.1.13)
- /e/OS Notes
- Nextcloud App (Windows, macOS, Linux, Android, iOS)
- Nextcloud Notes

The reason behind this choice is that those apps might delete some files when we will reopen, as the client will detect a difference between what is present locally and what is present on Murena Workspace. By blocking the requests, we let time for each users to back up his local file. The documentation below explains, client by client, how as a user you can back up your files.

## Back-up Your Local Data

### eDrive

First, you have to check if eDrive is running on your device. Go to `Settings` > `Accounts` > your Murena Workspace email address > `Account sync`. If `Pictures and videos` is disabled, it means eDrive is not running and that you can ignore this part.

If you are running **/e/OS below 1.13** ([how do I check my /e/OS version?](https://community.e.foundation/t/howto-give-complete-e-os-version-info-easily-for-support-answers-comparison-etc/45030)): 
- Please upgrade to the latest /e/OS version (`Settings` > `Updates`)
- OR if you can't upgrade, copy the /e/OS device data from the following folder to another device
    - DCIM
    - Documents
    - Movies
    - Music
    - Pictures
    - Podcasts
    - Recordings
    - Ringtones

If you are running **/e/OS 1.13 and later**, nothing to do! eDrive never deletes files, so no need to worry about data loss. File sync will be enabled from the beginning with this version.

> **Note:** By default, eDrive will not push again all the local files you have on your smartphone. An upcoming eDrive update will fix this behavior, and force push on Murena Workspace the files you have on your device.

### Nextcloud App (Android, iOS, Linux, macOS, Windows)

Nextcloud will delete files that were present locally but not anymore on the server. By default, Nextcloud apps will not sync when we reopen Murena Workspace Files app. Please copy your files to another location using one of the procedures below.

#### Windows, macOS and Linux

In order to back up:
1. **Open the Nextcloud App**: Launch the Nextcloud app _on every device it is installed on_
2. **Access Local Folder**: Click on "Open Local Folder" to view your synced files.
3. **Select Files**
    - **macOS** Use `Cmd (⌘) + A` to select all files, then copy them using `Cmd (⌘) + C`
    - **Windows/Linux**  Use `Ctrl + A` to select all files, then copy them using `Ctrl + C`
4. **Create a back up Folder**: Create a new folder (e.g., `Nextcloud-Backup`) in a safe location.
5. **Paste Files**
    - **macOS** Paste the copied files into the back-up folder using `Cmd (⌘) + V`
    - **Windows/Linux** Paste the copied files into the back-up folder using `Ctrl + V`

{% include alerts/warning.html content="You have to apply this process on every device where you installed a Nextcloud client." %}

#### Android


1. Open `Files` app & click on the *Nextcloud Button* on the side panel (if you don't see this icon go to step - 9)

    ![image](/images/murena-workspace-recovery/nextcloud-1.png){: height="400" }
2. Now long press in any item, it will select the item, click the *3 dot button (⋮)* from top right corner, & preses *select all*

    ![image](/images/murena-workspace-recovery/nextcloud-2.png){: height="400" }
3. It will select all the rest of the files

    ![image](/images/murena-workspace-recovery/nextcloud-3.png){: height="400" }
4. Now click the *3 dot button (⋮)* from top-right corner again & click *copy to...*

    ![image](/images/murena-workspace-recovery/nextcloud-4.png){: height="400" }
5. It will open a page pointing to the device's root folder. Click on the *add folder* button to create new folder.

    ![image](/images/murena-workspace-recovery/nextcloud-5.png){: height="400" }
6. It will open a text prompt, asking for folder name, in our case we named it `Nextcloud.bak`.

    ![image](/images/murena-workspace-recovery/nextcloud-6.png){: height="400" }
7. Now open the newly created folder & press *copy* button from the bottom of the screen. It will start the copy process.

    ![image](/images/murena-workspace-recovery/nextcloud-7.png){: height="400" }
8. You might see a notification going on if the copy size is bigger.

    ![image](/images/murena-workspace-recovery/nextcloud-8.png){: height="400" }

    {% include alerts/success.html content="Your backup process is done. In any case, if you failed on step-1, please follow below:" %}
9. Open `Nextcloud` app, select `Settings` from left side panel.

    ![image](/images/murena-workspace-recovery/nextcloud-9.png){: height="400" }
10. Click on the `Data storage folder` option, it will open a popup. Note down the selected item on the popup.

    ![image](/images/murena-workspace-recovery/nextcloud-10.png){: height="400" }
11. Open the `files` app & navigate to the previously noted path (by default it should be: `Android/media/com.nextcloud.client`). Here you will find a folder named `nextcloud`. Long press to select it.

    ![image](/images/murena-workspace-recovery/nextcloud-11.png){: height="400" }
12. Now follow step 4 to step 8 to complete the backup process.

#### iOS

1. **Open the Nextcloud App** on your iPhone or iPad.
2. **Navigate to the Files/Folders** you want to back up.
3. **Tap the three-dot menu (⋮)** next to the file or folder name.

    ![image](/images/murena-workspace-recovery/Nextcloud-Backup-iOS1.jpeg){: height="400" }
4. **Select "Set as Available Offline"** to download the files for offline access.

    ![image](/images/murena-workspace-recovery/Nextcloud-Backup-iOS2.jpeg){: height="400" }
5. **Open the Files App** on your iPhone.

    ![image](/images/murena-workspace-recovery/Nextcloud-Backup-iOS3.jpeg){: height="400" }
6. **Go to "On My iPhone"** or **"iCloud Drive"** under **Locations**.

    ![image](/images/murena-workspace-recovery/Nextcloud-Backup-iOS4.jpeg){: height="400" }
7. **Tap the three-dot menu (⋮)** in the top-right corner and select **"New Folder"**.
8. **Name the folder** (e.g., `Nextcloud-Backup`).

    ![image](/images/murena-workspace-recovery/Nextcloud-Backup-iOS5.jpeg){: height="400" }
9. **Open the Files App** on your iPhone.
10. **Go to "Nextcloud"** under **Locations**.
11. **Tap the three-dot menu (⋮)** in the top-right corner and select **"Select"**.
12. **Select All Files** you want to back up.

    ![image](/images/murena-workspace-recovery/Nextcloud-Backup-iOS6.jpeg){: height="400" }
13. **Tap the three-dot menu (⋮)** at the bottom and choose **"Copy"**.

    ![image](/images/murena-workspace-recovery/Nextcloud-Backup-iOS7.jpeg){: height="400" }
14. **Navigate to the "Nextcloud-Backup" Folder**.
15. **Long press inside the folder**, then tap **"Paste"** to copy the files.

    ![image](/images/murena-workspace-recovery/Nextcloud-Backup-iOS8.jpeg){: height="400" }

### EasySync

EasySync deletes files that are present locally but not on the server. By default, EasySync app will not sync when we reenable Murena Workspace Files entry. Please copy your files to another location.

1. Open `Files` app & long prese items which you want to make backup

    ![image](/images/murena-workspace-recovery/easysync-1.png){: height="400" }
2. Now click the *3 dot button (⋮)* from top-right corner & click *copy to...*

    ![image](/images/murena-workspace-recovery/easysync-2.png){: height="400" }
3. It will open a page pointing to the device's root folder. Click on the *add folder* button to create new folder. It will open a text prompt, asking for folder name, in our case we named it `easySync.bak`.

    ![image](/images/murena-workspace-recovery/easysync-3.png){: height="400" }
4. Now open the newly created folder & press the *copy* button from the bottom of the screen. It will start the copy process.

    ![image](/images/murena-workspace-recovery/easysync-4.png){: height="400" }

### /e/OS Notes

The /e/OS Notes app will delete the notes that were existing on the device but not on the server. The Murena Workspace Notes entry is currently disabled to prevent this behavior. To back up your notes:

1. **Short term: Use the Share Button** 
    - Open notes one by one
    - Use the share button to export each note
2. **Coming soon: Use the lastest /e/OS Notes app** We will provide an APK to help you extract all your notes. Stay tuned for updates which will be released through App Lounge in particular. To be able to receive the latest /e/OS Notes update when it will be released:
    - update to the latest /e/OS version 
    - OR, at very minimum, install the latest App Lounge version by following [this documentation](https://doc.e.foundation/support-topics/app_lounge.html#download-app-lounge-apks)

### Nextcloud Notes

- Open notes one by one
- Use the share button to export each note

### Carnet


1. Create the backup
    - Use the three-line menu (☰) at the top left
    - Select the *Settings* button

        ![image](/images/murena-workspace-recovery/carnet-Backup1.jpeg){: height="400" }
    - Use the *Export* button

        ![image](/images/murena-workspace-recovery/carnet-Backup2.jpeg){: height="400" }
    - Go to the folder you want to use for back-up, and press the *Save* button at the bottom right

        ![image](/images/murena-workspace-recovery/carnet-Backup3.png){: height="400" }

2. Check the back-up
- Open the Files app
- Browse to the previously selected folder
- Open `carnet_archive.zip`
- You should be able to `.sqd` files corresponding to your notes as well as folders if you had any



## FAQ Section

- **Q: Why is sync disabled?**
  - A: Sync is disabled to prevent data loss during the recovery process.

- **Q: When can I re-enable sync?**
  - A: We will notify you when it is safe to re-enable sync.
