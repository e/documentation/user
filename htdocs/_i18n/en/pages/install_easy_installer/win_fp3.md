## FP3/FP3+
### Solution 1: Using Windows update


1. Go to Windows Update
  - Click on the Start menu in the lower left corner.
  - Click on `Settings`
  - Go to `Update and Security`

   <img src="../../images/windows-update-menu.png" width="400" alt="screenshot of windows update menu">

1. Install the necessary driver
  - Click on `Search for updates`

   <img src="../../images/windows-searchForUpdate.png" width="400" alt="screenshot of windows looking for update">

1. A new section named `Show optional updates` should appear. Click on it.

   <img src="../../images/windows-showOptionnalUpdate.png" width="400" alt="Scrrenshot of window optionnal update menu">

1. Click on `Driver updates` to display the list.

   <img src="../../images/windows-clickOnDriverUpdates.png" width="400" alt="screenshot of windows drivers update menu">

1. Select the driver named `Google, Inc. - Other hardware - Android Bootloader Interface` and click on `Download` and install. 
  {% include alerts/tip.html content="If the driver does not work, there are other drivers listed. Feel free to install and try them." %}
  

   <img src="../../images/windows-selectDriverToUpdate.png" width="400" alt="Screenshot of driver selection of 'Google, Inc. - Other hardware - Android Bootloader Interface'">

  [source from community forum](https://community.e.foundation/t/howto-fairphone-3-3-stuck-in-fastboot-bootloader-mode-with-the-easy-installer-on-windows/24914)


If the previous solution does not work, you can try this one:
### Solution 2: Manual installation of fastboot driver for FP3/FP3+

1. Download the driver. Unfortunately, Fairphone does not provide an official "fastboot interface driver".
You can use [the Android one](https://developer.android.com/tools/releases/platform-tools) but there can be cases where it may not work.
1. Start your phone in Fastboot mode (turn it off then keep pressing Power + Volume down until it start in fastboot mode)
1. Plug your phone to your windows computer
1. Open device manager from windows settings
1. Select the android device with the warning icon
1. Right click to open details of the device
1. Move to Driver section, and select install/update driver
1. A message prompt will open. Choose to use local file
1. Select the folder where you downloaded the driver
1. Wait for the driver to install

{% include alerts/tip.html content="You can follow the [official android documentation about driver installation](https://developer.android.com/studio/run/oem-usb)" %}
  
{% include alerts/tip.html content="You may have to reboot your computer after driver installation, to make them available." %}

