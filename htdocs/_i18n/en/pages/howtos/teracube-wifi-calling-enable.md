{% include alerts/tip.html content="Wi-Fi calling will only work on Teracube devices running  /e/OS v2.1 and above."%}

1. Navigate to `System settings`

    ![go to settings](/images/howtos/teracube-wifi-calling-enable/teracube-wifi-calling-enable-1.png) 

2. Tap on `Network and Internet`

    ![tap on network and internet](/images/howtos/teracube-wifi-calling-enable/teracube-wifi-calling-enable-2.png) 

3. Tap on `Internet`

    ![tap on internet](/images/howtos/teracube-wifi-calling-enable/teracube-wifi-calling-enable-3.png) 

4. Tap on `Gear symbol` beside your SIM card name

    ![tap on gear symbol beside your SIM card name](/images/howtos/teracube-wifi-calling-enable/teracube-wifi-calling-enable-4.png) 

5. Tap on `Wi-Fi Calling`

    ![tap on Wi-Fi calling](/images/howtos/teracube-wifi-calling-enable/teracube-wifi-calling-enable-5.png) 

6. Toggle Wi-Fi calling `ON`

    ![toggle Wi-Fi calling ON](/images/howtos/teracube-wifi-calling-enable/teracube-wifi-calling-enable-6.png) 

7. Tap on the new `Calling preference` setting that will appear after Wi-Fi calling is turned ON.

    ![tap on Calling preference](/images/howtos/teracube-wifi-calling-enable/teracube-wifi-calling-enable-7.png) 

8. Select `Call over Wi-Fi`

    ![select call over Wi-Fi](/images/howtos/teracube-wifi-calling-enable/teracube-wifi-calling-enable-8.png) 

9. Go back one screen using the `Back button`. Then, tap on `Access point names`

    ![tap on Access point names](/images/howtos/teracube-wifi-calling-enable/teracube-wifi-calling-enable-9.png) 

10. Tap on `three dots` at `top right`

    ![tap on three dots at top right](/images/howtos/teracube-wifi-calling-enable/teracube-wifi-calling-enable-10.png) 

11. Tap on `Reset to default`

    ![tap on reset to default](/images/howtos/teracube-wifi-calling-enable/teracube-wifi-calling-enable-11.png) 

12. Your APN settings will be reset and Wi-Fi calling should start working. If it still doesn't work, try restarting your device.

    ![your APN settings will be reset](/images/howtos/teracube-wifi-calling-enable/teracube-wifi-calling-enable-12.png)