### Connect your Google account
1. Login to your [Murena Workspace](https://murena.io) account using your credentials
1. Navigate to Settings and then Data Migration settings
![Screen showing how to navigate to settings](/images/howtos/ecloud/navigate-to-settings.jpg)
![Screen showing how to navigate to data migration settings](/images/howtos/ecloud/navigate-to-data-migration-settings.jpg)
1. Use the "Sign in with Google" button to authenticate your Google account at Murena Workspace
![Screen showing how to sign in with Google](/images/howtos/ecloud/click-on-sign-in-with-google.jpg)
1. You will then be prompted by Google to give permissions for access to services such as Calendar, Drive, Photos and Contacts. You may choose to give only specific permissions to unlock specific parts of the app
![Screen showing how to give consent to Murena Workspace](/images/howtos/ecloud/give-consent-google.jpg)
1. After signing in and giving permissions, you will be able to import your data


### Import Contacts
1. Click on the "Import Google Contacts" button to import your contacts to Murena Workspace
![Screen showing Import Google Contacts button](/images/howtos/ecloud/import-contacts.jpg)
1. Once your contacts are imported, you will see a notification
![Notification after contacts are imported](/images/howtos/ecloud/contacts-import-successful.jpg)
1. You can now access your imported contacts through the Murena Workspace contacts app or sync to your  /e/OS device

### Import Calendars
1. Please choose the calendar that you want to import from the listed calendars from your Google Account (Here, we have chosen "my-tasks-calendar")
1. Click on the "Import calendar" button to import chosen calendar
![Screen showing Import calendar button](/images/howtos/ecloud/import-calendar.jpg)
1. Once your calendar is imported, you will see a notification
![Notification after calendar is imported](/images/howtos/ecloud/calendar-import-successful.jpg)
1. You can now access your imported calendar through the Murena Workspace calendar app or sync to your /e/OS device

### Import Photos
1. To import your Google Photos into Murena Workspace, add your preferred import directory
1. Once your import directory is set, click on the "Import Google photos" button
![Screen showing Import Google photos button](/images/howtos/ecloud/import-photos.jpg)
1. As your photos are imported in the background, you can now leave this page and come back to check progress of import from time to time
1. Upon successful import, you will receive a notification  
![Notification after successful photos import](/images/howtos/ecloud/photos-import-success-notification.jpg)  
1. You can now access your photos using the Murena Workspace photos app

### Import files from Drive
1. Choose your preferred document import format
1. Set your preferred import directory
1. Click on the "Import Google Drive files" button
![Screen showing Import Google Drive files button](/images/howtos/ecloud/import-drive.jpg)
1. As files from your Drive are imported in the background, you can now leave this page and come back to check progress of import from time to time
1. Upon successful import, you will receive a notification  
![Notification after successful drive import](/images/howtos/ecloud/drive-import-success-notification.jpg)  
1. You can now access your files from the Murena Workspace files app