Two factor Authentication is a security mechanism to give you an extra layer of security on your account. More information can be found [here](https://en.wikipedia.org/wiki/Multi-factor_authentication).

## Enable 2FA using time-based one-time passwords (TOTP):

To enable a TOTP Password you will need a compatible client. You can find some proposals [here](https://github.com/nextcloud/twofactor_totp#readme).

1. Download one [compatible TOTP client](https://github.com/nextcloud/twofactor_totp#readme)

1. Connect to your Murena Workspace account

1. Go to your [user security settings](https://murena.io/settings/user/security) and make sure "Enable TOTP" is checked 

   ![Security screen showing where is the location of "Enable TOTP"](/images/howtos/2fa/security_screen.png "Security Screen")

1. The TOTP secret and QR code will be right below it 

   ![Enabled TOTP showing the QR code and the TOTP secret](/images/howtos/2fa/enabled_TOTP.png)

1. Open the TOTP app

1. Inside your TOTP client, read the QR code shown at step 3 OR type the TOTP secret exactly as it is written.

1. To make sure everthing is working, place the generated code at the verification field box and click on "Verify"
   - Remember: For security reasons those generated codes expire in about 30 seconds and will not work after that.
   - If the verification fails, a small popup will appear alerting you of that.
   
     ![Popup alerting of wrong code of verification](/images/howtos/2fa/popup_fail.png "Popup on the right top")
1. A new code will be calculated every 30 seconds (depending on the TOTP configuration) and will be needed every time you connect to your Murena Workspace account

🎉 2FA is now enabled on your account 🎉

## Disabling two-factor authentication (2FA) 🔓

1. Connect to your Murena Workspace account
1. Go to your [user security settings](https://murena.io/settings/user/security)
1. Make sure "Enable TOTP" is unchecked

## Manually setup Mail application
1. Go to your Mail app
1. You will be prompted to write your credentials there
   
   ![Add mail screen showing empty login and password fields](/images/howtos/2fa/eos_mail_empty.png "Add mail screen")
1. Using your Murena Workspace **main** password, fill in the fields. 
   
   ![Add mail screen showing filled login and password fields](/images/howtos/2fa/eos_mail_filled.png "Add mail screen filled")

- It may be necessary to manually setup the mail application. For this, tap on "MANUAL SETUP" and fill in the fields such as the image below and tap on next. Do not forget to change the email address! 
   
   ![Incoming mail settings configuration for the mail application](/images/howtos/2fa/eos_maybe_manual_settings.png "Incoming settings")
   
- If necessary, more information to configure your mail IMAP settings can be found [here](https://e.foundation/email-configuration/)

{% include alerts/warning.html content="The IMAP account is not protected with 2FA" %}

## Using an application password for the eOS account manager

1. Connect to your Murena Workspace account

1. Go to your [user security settings](https://murena.io/settings/user/security)

1. In the section "Devices & sessions", type the application name and click on "Create new app password"

   ![List of apps names](/images/howtos/2fa/apps_names.png "Apps name")

1. The login and password to your application will be shown as the image below. Make sure to anotate it for the next steps.

   ![Example of Login and password created](/images/howtos/2fa/login_pass.png "Login and password")

1. Now on your eOS smartphone, go to "Settings"

   ![Settings screen with all menu options](/images/howtos/2fa/eos_settings.png "Settings screen")

1. Tap on "Accounts" and then "Add account"

   ![Accounts screen](/images/howtos/2fa/eos_accounts.png "Accounts screen") ![Add new account screen](/images/howtos/2fa/eos_add_account.png "Add new account")

1. Complete the field boxes with the login and password saved before

   ![Login and password field boxes](/images/howtos/2fa/eos_apptoken.png "Login and password")

1. Tap "Login" to complete the process
