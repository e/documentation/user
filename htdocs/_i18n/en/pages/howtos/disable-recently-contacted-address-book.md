## Why am I seeing a **HTTP 501 Not Implemented** error notification?

Some users are getting an **HTTP 501 Not Implemented** error, as shown in the screenshot below. It is caused due to the **Recently contacted** feature of Nextcloud.

![](/images/howtos/disable-recently-contacted-address-book/501-error.png)

## How can I get rid of this Error?

This functionality is already disabled on murena.io, and the local address book removed on /e/OS 1.0 and later.

If you are a user of a version lower than /e/OS 1.0.0 (typically based on Android 9 or earlier), please follow this guide to remove this address book manually from your phone.

## Steps for manual deactivation:

### 1. Open system settings

![](/images/howtos/disable-recently-contacted-address-book/bliss-launcher.png)

### 2. Go to the Accounts section

![](/images/howtos/disable-recently-contacted-address-book/system-settings.png)

### 3. Tap Recently Contacted and Turn off the toggle

![](/images/howtos/disable-recently-contacted-address-book/account-section.png)

![](/images/howtos/disable-recently-contacted-address-book/recently-contacted-toggle.png)

Congrats! Now, the recently contacted error notification will not bother you again.
