## Upgrade your Murena Workspace storage

### Log into Murena Shop (Only required for some users)

If you purchased a subscription prior to October 13, 2022, you must first login to [Murena Shop](https://murena.com) with your existing account and then proceed with the steps below.

![](/images/howtos/ecloud/login-with-shop-account.png)

After you've logged in, visit [Murena Shop subscriptions page](https://murena.com/ecloud-subscriptions/). You will be prompted to link your Murena Workspace and Murena Shop accounts. To authorize this change, you must enter your Murena Workspace password.

![](/images/howtos/ecloud/connect-shop-with-cloud.png)

You can now jump straight to Step 6 below.

### Log into Murena Workspace

1. Log in to [Murena Workspace](https://murena.io).

2. You can either click **UPGRADE STORAGE** on your Dashboard or click **Increase storage size** in the Murena Workspace files app. Both options will redirect you to the subscriptions page.
![](/images/howtos/ecloud/murena-dashboard.png)
![](/images/howtos/ecloud/files-app-murena.png)

3. On the subscriptions page, scroll down and click Login with Murena ID. You may be asked to re-enter your password. This login ensures that the benefits will be applied to the account of your choice.
![](/images/howtos/ecloud/subscriptions-page-no-login.png)

4. Login with your Murena ID.
![](/images/howtos/ecloud/murena-cloud-login.png)

5. Grant access to Murena shop.
![](/images/howtos/ecloud/grant-shop-access.png)

6. After granting access, you'll be taken back to the subscriptions page to choose one of our plans.
You have the option of paying monthly or annually. 
![](/images/howtos/ecloud/subscriptions-page-login.png)

7. Enter your billing information and complete the purchase. Following a successful payment, we will automatically apply the new storage benefits to your account. Enjoy! 
![](/images/howtos/ecloud/checkout-murena.png)
