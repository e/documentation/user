Murena Workspace supports use of email filters that automatically organize your incoming emails.

{% include alerts/tip.html content="Before you follow the steps below, decide which filters you wish to setup and create new folders for it."%}

![Setup and create new folders](/images/setup-and-create-new-folders.png)

## Setup an email filter on Murena Workspace
{:.mt-5}

- Visit [this link to the Filters section](https://murena.io/apps/snappymail/#/settings/filters) or navigate to `Murena dashboard` -> `Email` -> `Settings` (gear icon at bottom) -> `filters`.

- If asked, login with your Murena Workspace account e-mail and password.

- Click on `rainloop.user` as shown in the screenshot below.

![Click on rainloop.user](/images/click-on-rainloop-user.png)

- Click on `+ Add a Filter`

![Click on add a filter](/images/click-on-add-a-filter.png)

- Next, setup the filter as per your requirements. For example, the configuration of filter in the screenshot below will move all incoming emails from `mybankemail@example.com` to the folder named `Banking`.

![Setup the filter](/images/setup-the-filter.png)

- Don't forget to click on `Done` and then `Save`, otherwise your filter will not be saved.

![Click done and save](/images/click-done-and-save.png)

- Finally, activate your filter by selecting the `Dot` beside `rainloop.user` option.

![Activate your filter](/images/activate-your-filter.png)

## Use email filters with Hide my email
{:.mt-5}

- Visit [this link to view your Hide my email](https://murena.io/apps/snappymail/#/settings/hide-my-email) alias email address.

- If asked, login with your Murena Workspace account username and password.

- Click on `COPY` to copy your Hide my email address.

![Copy hide my email alias](/images/copy-hide-my-email.png)

- Use the copied email in the `Conditions` section when setting up an filter.

For example, the configuration of filter in the screenshot below will move all incoming emails from `r4ndom.alia5.m4il@murena.io` to the folder named `Alias emails`.

![Setup the filter with hide my email](/images/setup-the-filter-with-hide-my-email.png)

- Don't forget to click on `Done`, then `Save`, and then select the `Dot` beside `rainloop.user` option, otherwise your filter will not be saved.