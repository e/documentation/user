The Teracube 2e is failing to update itself automatically. If you are familiar with the command line, you can flash the update using the latest build files and `adb` tools. If you can't, we also have an easier SD card approach. More information on that is available on its [doc page](/support-topics/teracube-2e-sdcard-update) and its [Gitlab issue](https://gitlab.e.foundation/e/backlog/-/issues/5766).

## Requirements

* A backup of your phone's important data on an external storage device (optional, but recommended).
* A phone with at least 50% charge.
* A computer with `adb` installed. [How to set up ADB?](https://doc.e.foundation/pages/install-adb)
* A functional data cable to connect your phone to your computer (A charging only cable won't work).
* Download all the files mentioned in the download section.

## Downloads

* [2e official](https://ota.ecloud.global/builds/full/stable/2e/e-1.5-q-20221028230220-stable-2e.zip)
* [2e community](https://ota.ecloud.global/builds/full/dev/2e/e-1.5-q-20221031230909-dev-2e.zip)

It is important to note the path of downloaded files. You can check your browser's download section to find the file. For more help on file paths, you can search for [how to find a file path](https://spot.murena.io/search?q=how%20to%20find%20file%20path) on **YOUR** OS.

## Applying the /e/OS update via recovery

1. Download the OTA with the hotfix (See downloads section).
1. Reboot into recovery mode:
   * With the device powered off, long press the `Power` button and `Volume up` button together, until a text menu appears on the screen.
   * In this menu, the `Volume up` button is used to navigate through the options and the `Volume down` button is used to confirm a selected option. The selected option is indicated by an arrow `<<==`.
   * Select `Recovery Mode` using the `Volume up` button, then use `Volume down` to confirm the choice.
1. Sideload the /e/ .zip package:
   * On your phone, select “Apply Update”, then “Apply from ADB” to begin sideload.
   * Plug your phone into the computer. It's ok, if you have already done that.
   * On your computer, sideload the package using: `adb sideload <path to downloaded OTA zip file>`.

   > **NOTE 1** At this point the update is being applied and can take a while to complete. Don't use/unplug your device. Don't worry if the update is stuck at some x%. Be patient and wait for feedback from your device.

   > **NOTE 2**: Normally, ADB will report `Total xfer: 1.00x`, but in some cases, even if the process succeeds, the output will stop at 47% and report `adb: failed to read command: Success`. In some cases, it will report `adb: failed to read command: No error` or `adb: failed to read command: Undefined error: 0` which is also fine.

1. Once you have installed everything successfully, tap the back arrow at the top left corner of the screen (currently it's not very visible). You can also go back by swiping right to left anywhere above the the /e/ logo.
1. Select “Reboot system now”.
