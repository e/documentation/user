## 1. Requirements

1. Take a backup of your data (recommended).
1. Charge your phone to at least 50%.

## 2. Setup the SDCard

1. With your phone's screen turned on, long press `Power` button till you see a side menu.
![Teracube 2e power button location](/images/howtos/teracube-sd-card-update/2.png)
1. Tap on `Power off`.
![Tap on power off in side menu](/images/howtos/teracube-sd-card-update/3.png)
1. Wait for at least 10 seconds after your phone is completely turned off.
1. Open your phone's backcover and insert the SD card sent to you by Murena.
![Teracube 2e SD card slot location](/images/howtos/teracube-sd-card-update/5.png)
1. Press `Power` to boot your Teracube 2e
1. Open the Browser

    ![](/images/howtos/teracube-sd-card-update/18.png)
1. In the URL bar, enter [https://lstu.fr/2e15q](https://lstu.fr/2e15q) the shortened URL or [https://ota.ecloud.global/builds/full/stable/2e/e-1.5-q-20221028230220-stable-2e.zip](https://ota.ecloud.global/builds/full/stable/2e/e-1.5-q-20221028230220-stable-2e.zip) the full URL.

    ![](/images/howtos/teracube-sd-card-update/19.png)
1. Choose SDCard as the place to download the /e/OS package

    ![](/images/howtos/teracube-sd-card-update/22.png)![](/images/howtos/teracube-sd-card-update/23.png)![](/images/howtos/teracube-sd-card-update/24.png)![](/images/howtos/teracube-sd-card-update/25.png)
1. With your phone's screen turned on, long press `Power` button till you see a side menu.
![Teracube 2e power button location](/images/howtos/teracube-sd-card-update/2.png)
1. Tap on `Power off`.
![Tap on power off in side menu](/images/howtos/teracube-sd-card-update/3.png)
1. Wait for at least 10 seconds after your phone is completely turned off.

## 3. Install the update

1. Long press `Power` and `Volume up` buttons at the same time, until a text menu appears on the screen.
![](/images/howtos/teracube-sd-card-update/6.png)
1. This text menu cannot be used via touch screen. `Volume up` button is used to `Navigate` the options and `Volume down` button is used to enter the selected option. An arrow `<<==` indicates the selected option.
![Navigate the text menu using volume up and confirm your option using volume down](/images/howtos/teracube-sd-card-update/7.png)
1. Navigate to `Recovery Mode` using `Volume up` button and enter the option using `Volume down` button.
![Navigate to recovery mode using volume up button and enter the option using volume down button](/images/howtos/teracube-sd-card-update/8.png)
1. Once the device boots into recovery mode, you will be able to see the /e/OS logo and `Recovery` title below it.
![](/images/howtos/teracube-sd-card-update/9.png)
1. Tap on `Apply update`.
![Tap on apply update](/images/howtos/teracube-sd-card-update/10.png)
1. Tap on `Choose from sdcard1`.
![Tap on choose from SD Card](/images/howtos/teracube-sd-card-update/11.png)
1. Navigate with the `Volume down` and `Volume up` buttons and **tap** on the following folders successively when they are highlighted in orange `Android`, `data`, `foundation.e.browser`, `files` and `Download`. Select `e-1.5-q-20221028230220-st...` when it is highlighted **tap** on it.
![Tap on e-1.5-q-2022...](/images/howtos/teracube-sd-card-update/12.png)
1. If you see a message saying `Signature verification failed`, you can still tap `Yes` and safely continue with the process (at least for this guide).
![Tap yes for verification](/images/howtos/teracube-sd-card-update/13.png)
1. At this point, the update is being applied. This can take a while to finish, and the progress % might even get stuck at some point. Don't worry and have some patience, the update will ultimately succeed.
![](/images/howtos/teracube-sd-card-update/14.png)
1. Once the update completes, you will be able to see `Script succeeded: result was [1.000000]`.
![](/images/howtos/teracube-sd-card-update/15.png)
1. Swipe left in the empty area above the /e/OS logo (shown in the video).
1. Tap on 'Reboot system now', and wait for your device to restart.
![](/images/howtos/teracube-sd-card-update/17.png)

## 4. Validate the installation

1. Go to `Settings` > `System update`, and confirm the /e/OS version at top is `/e/OS 1.5-20221028230220`
1. Congrats! An updated version of /e/OS was successfully installed on your Teracube 2e. Your system settings will reflect the updated `Version number`. Have fun with the update!

