
# Ad block filters

Since the release of `/e/OS v0.15` it is possible to use custom ad block filters.

Change the default filters URL from `Settings` > `Legacy AdBlock settings`, and specify a valid URL for the filters file.

The default URL is: [https://images.ecloud.global/apps/browser/filters.dat ](https://images.ecloud.global/apps/browser/filters.dat)

# How to create a custom filters file

See [Chromium documentation](https://github.com/chromium/chromium/blob/main/components/subresource_filter/FILTER_LIST_GENERATION.md).

You will need to use the `ruleset_converter` command-line tool as in:
```
ruleset_converter --input_format=filter-list \
		--output_format=unindexed-ruleset \
                --input_files=easyprivacy.txt,easylist.txt \
		--output_file=filters.dat
```

A pre-built `ruleset_converter` binary for Linux/amd64 is available here [Browser Filters](https://gitlab.e.foundation/e/os/browser_filters/-/blob/master/ruleset_converter).

# Requirements

The webserver serving the file must provide a consistent `Last-Modified` header.

# Automatic filters updates

The filters URL will be automatically checked 5 seconds after the browser is started and then once per week.

# Limitations

You will need to close and re-open Browser in order for the new URL to be in effect.
