What to do when you encounter an issue in /e/OS?

## Connect to our GitLab server

   * If you have an account, [login](https://gitlab.e.foundation/users/sign_in)
   * If not, [create one](https://gitlab.e.foundation/e)

## Go to our issue dashboard

   - /e/OS issues are listed and created [here](https://gitlab.e.foundation/e/backlog/-/issues)

   - Before creating a new issue, please search if it has previously been reported. To check for this

   - Type the bug in the search text box on the top of the screen as shown below

   ![New Search](/images/new_issue_search.png)

   - The screen will also display some other similar worded issues

   - If this search does not return any results create a new issue

## Create a new issue

   - Select a template for the issue from the drop down marked 'Choose a template'
   - 'Apply template'
   - Add a Title
   - Fill in all the details as requested in the template
   - Always [add logs](/support-topics/create-a-log) where possible. This helps the developers debug the issue better.
   - Once you have finished, click `Submit issue`.



## Getting an error message in new ID creation on /e/OS Gitlab?

   - Some users may get an error message, while attempting to create a new ID on the /e/OS Gitlab
   - Let us explain the reason behind this error. Some time back we had an issue with a large number of dummy IDs swamping our servers. As a preventive measure we now allow registrations from a limited number of email domains. 
   - The error message you may get while attempting to create an /e/OS Gitlab ID, may ask you to contact Murena helpdesk.
   - To contact helpdesk:
     - Paying customers can write in [here](https://murena.com/contact-us/) with issue details
     - Users who use the /e/OS or Murena cloud for free can write on [the forum](https://community.e.foundation/) to seek assistance





## Syntax and Source

   * [Source](https://docs.gitlab.com/ee/user/project/description_templates.html#setting-a-default-template-for-issues-and-merge-requests)
   * [Markdown (GitLab)](https://docs.gitlab.com/ee/user/markdown.html)

