
### Official
 
 - These builds are currently only available on the Murena smartphones
> Murena smartphone are phones purchase through the [murena Shop](https://murena.com/)
 - Are built based on source code from the Community build branch which has been released and tested
 - This build undergoes additional and longer testing cycles
 - Builds are made by /e/OS using the /e/OS build infrastructure and signed by /e/OS official keys
 - Official builds have a maintainer
 - Get regular OTA updates
 - Users can also download the official builds and manually install them if they so desire
 - Builds format are IMG files with a build script to help with the installation.
 - Release Frequency: Monthly or bimonthly
 - Earlier referred to as stable builds
 - Build Naming format: IMG-e-1.20-s-20240220382012-stable-FP4.zip

Naming format explanation:

`IMG-e-codeversionnumber-osversion-YYYYMMDDbuildid-stable-devicename.zip`

### Community
 - Builds are made by /e/OS using the /e/OS build infrastructure and signed by /e/OS dev keys
 - Includes the latest /e/OS development code for the device
 - Is released after testing but may still contain non critical bugs
 - Get regular OTA updates
 - Available for download on /e/OS websites
 - Release Frequency: Monthly if possible
 - Earlier referred to as dev builds
 - Build Naming format: e-1.20-t-20240223382228-dev-mata.zip

Naming format explanation:

`e-codeversionnumber-osversion-YYYYMMDDbuildid-dev-devicename.zip`



### Test
  - Builds are made by /e/OS using the /e/OS build infrastructure and signed by Android test keys
  - Builds are made to test some new features or an upgrade
  - May or may not get regular OTA updates
  - These builds are only accessible to testing team members  
  - Are not available for download on /e/OS websites
  - Release Frequency: Depending on testing requirement
  - Build Naming format: e-1.21-rc-r-20240313386629-stable-FP2.zip

Naming format explanation:

`e-codeversionnumber-testphasecode-osversion-YYYYMMDDbuildid-dev/stable-devicename.zip`

### Unofficial
 - Builds are made by /e/OS or external users
 - Builds are not supported by /e/OS
 - Made using /e/OS sources code with no modifications in any form
 - At times may not have a maintainer as in made by a user who does not own the device
 - Do not receive OTA updates
 - Available for download on /e/OS websites and are tagged as "UNOFFICIAL"
 - Release Frequency: Monthly or biMonthly
 - Build Naming format: e-0.7-n-2020021341116-UNOFFICIAL-mata.zip
 - Install these builds at your own risk

Naming format explanation:

`e-codeversionnumber-osversion-YYYYMMDDbuildid-UNOFFICIAL-devicename.zip`

### Custom
   - Builds are made by /e/OS or external users
   - The build may have possible changes to /e/OS sources and include code developed for specific device support
   - Builds are not supported by /e/OS
   - Will not be publicly available for download on /e/OS websites
   - Install these builds at your own risk
   - Release Frequency: Dependent on the client or ROM Builder
   - Build Naming format: Format to be set by client or ROM Builder

Naming format explanation:

`Depends on the convention followed by the custom ROM builder`