## What's in /e/OS?

/e/OS is a complete mobile ecosystem which includes a mobile operating system (ROM) and online-services. The /e/OS project was launched at the end of 2017. It has historically been the first mobile OS to focus on deGoogling.

/e/OS is:
- **Open source**
- **Pro-privacy**
- Compatible with existing **Android applications**
- Cares about **usability**
- Running on more than 200 different smartphone models

/e/OS is an **alternative to the Apple/Google duopoly** for smartphone users.

/e/OS consists of:
*  An installable mobile operating system for smartphones, which is forked from LineageOS/AOSP, and completely "deGoogled"
*  A default set of open source applications that have been improved and optimized for the user
*  Various online services that are linked to the mobile operating system, such as: a meta search engine for the web, a cloud drive with data synchronization, mail, calendar, notes, tasks.
* Advanced Privacy: cuts mobile applications trackers, ads, and much more.
* Parental Control: protects children from inappropriate contents (apps and web)

/e/OS can be downloaded and installed on more than 200 smartphone models, and is also available pre-installed on a range of [Murena smartphones](https://murena.com).

### DeGoogling / UnGoogling in /e/OS

The goal of "deGoogling" is 
- To remove or disable any feature or code that is sending data to Google servers, or at least to anonymize those accesses
- To offer non-Google default online services, including for search.

- The Google default search engine is removed and replaced by other services (see below in [default apps](/what-s-e/#e-default-applications) and services)
- Google Services are replaced by microG and alternative services (see below for more details)
- All Google apps are removed and replaced by equivalent Open Source applications. The one exception is the [Maps Application](/maps)
- No use of Google servers to check connectivity  
- NTP servers are not Google NTP servers anymore
- DNS default servers are not Google anymore, and their settings can be enforced by the user to a specific server
- Network Geolocation was using Mozilla Location Services in addition to GPS. Since Mozilla Location Services has been closed mid-2024, e Foundation has set up an anonymous proxy to the HERE location service (https://www.here.com/docs/bundle/network-positioning-api-developer-guide-v2/page/README.html) as a short term plan to ensure continuity of service for /e/OS users. So far the service has proven to work well while protecting user's privacy. However, since the HERE API service has a significant cost for e Foundation, we're working on more long term options. 
- CalDAV/CardDAV management and synchronization application (DAVDroid) is fully integrated with the user account and calendar/contact application

For a detailed response please go through the document given [here]({% translate_link support-topics/deGoogling-scope-and-definition-within-the-context-of-eos %})


### Advanced Privacy features in /e/OS

![](../images/advanced_privacy_mainscreen.png)![](../images/advanced_privacy_trackers.png)![](../images/advanced_privacy_location.png)![](../images/advanced_privacy_ip.png)

Starting from /e/OS V1, /e/OS features a new "Advanced Privacy" module that offers the following features:
- Tracker management: users know in real time the number of active trackers used by installed applications. All trackers' activity can be cut, with some granularity by application
- Location faking: /e/OS users can choose to have their location faked for applications that use location. Faking goes from "random plausible locations" to a specific, chosen, location.
- IP address faking: /e/OS users can chose to fake their IP address, for all application, or specific applications. This is made possible by redirecting the internet traffic to the TOR network.

[Read more about Advanced Privacy here](/support-topics/advanced_privacy).

Also, an "hide my email" address alias is provided to each user, for users who don't want to disclose their real email address.

### /e/OS Parental Control

Parental Control protects children from inappropriate contents by restricting access to a range of applications and websites, depending on their age. 

![](../images/parental-control/age-restricted-apps-cant-install-2.png)

[Read more about Parental Control here](/support-topics/parental-control).

### /e/OS User interface

![](../images/blisslauncher_v3_0.png)

- A new launcher "BlissLauncher" was developed to offer a simple and attractive look and feel. Its features include: 

  - Easy to understand User Interface with almost no learning curve for the new user
  - Application widget support 
  - Uses the "Spot" search engine by default
  - Icons that were designed specifically for the project. 
  - Improvements made in settings to offer a better looking interface

### Services

- A specific file synchronization service has been developed for multimedia and file contents.
 
It can use [Murena Workspace services server at https://murena.io](https://murena.io) (formerly "ecloud.global) or [self-hosted cloud services](https://gitlab.e.foundation/e/infra/ecloud-selfhosting).
- The weather provider is the LOS weather provider using data fetched from [Open Weather Map](https://openweathermap.org/)
- Account Manager has been modified to support /e/OS/-Murena user accounts, with a single identity, thus providing /e/OS users
   -  Mail
   -  Content sync
   -  Calendar
   -  Contacts
   -  Notes
   -  Tasks 

/e/OS users can decide where they want to store their data. It could be either the [Murena Workspace](https://murena.io) based on NextCloud, or [self-hosted](https://gitlab.e.foundation/e/infra/ecloud-selfhosting) by the user.

### How is the promise 'Privacy by design' fulfilled?

On /e/OS:
 - Connecting a cloud service is not mandatory.
 - We strive to provide the maximum functionality while remaining anonymous, like App Lounge or instant notifications.
 - We onboard privacy-enhancing apps like an ad-free Browser and Advanced Privacy, with sensible defaults balancing privacy and usability.
 - Our Mail app is integrated with Open Keychain to provide the possibility to send end to end encrypted e-mails from your device.
 - We continuously monitor /e/OS new builds to ensure /e/OS does not make unwanted requests to third party servers. When that is unavoidable, we provide our own replacement server.
 - /e/OS is open source which means it is auditable by anyone with code knowledge

On Murena Workspace:
 - We change the way Nextcloud typically works, with [privacy-enhancing patches](https://gitlab.e.foundation/e/infra/ecloud/nextcloud/-/tree/main/patches).
 - We carefully review which apps we offer; for instance, we cannot offer Talk or Nextcloud Mail as they store the content of your conversations in clear text in the database.
 - We disable some features like shared tags or sharing to groups.
 - We onboard a PGP-ready webmail and explain [how to set it up](/support-topics/setup-use-mailvelope-with-murena.html).
 - We follow security best practices, perform regular external security audits and follow through with their findings.
 - We are doing long-term R&D to achieve usable but reliable end-to-end encryption.

## /e/OS default applications

All pre-installed applications are open source applications except for the Maps app (read details about this in the [Maps FAQ](/maps)).

- [Web-browser](https://gitlab.e.foundation/e/apps/browser): an deGgoogled fork of Chromium, built from Bromite patch-sets, with specific /e/OS settings. It features an ad-blocker by default.
- [Mail](https://gitlab.e.foundation/e/apps/mail): a fork of K9-mail for better user experience, some bugfixes and support for oauth, with OpenKeyChain for PGP encryption support
- [Message](https://gitlab.e.foundation/e/apps/Message): a fork of QKSMS
- [Camera](https://gitlab.e.foundation/e/apps/camera): a fork of OpenCamera
- [Dialer](https://gitlab.e.foundation/e/os/android_packages_apps_Dialer): default Android dialer application
- [Calendar](https://gitlab.e.foundation/e/apps/calendar): a fork of Etar calendar, that fixes crashes with calendar invitations
- [Contact](https://gitlab.e.foundation/e/os/android_packages_apps_Contacts): default Android contact application
- [Clock](https://gitlab.e.foundation/e/os/android_packages_apps_DeskClock): a fork of Android deskclock application
- [Gallery](https://gitlab.e.foundation/e/os/android_packages_apps_Gallery2): default Android gallery3d application
- [Filemanager](https://gitlab.e.foundation/e/os/android_packages_apps_DocumentsUI): Android documentsui application
- [Sound recorder](https://gitlab.e.foundation/e/os/android_packages_apps_Recorder): default LineageOS sound recorder
- [Calculator](https://gitlab.e.foundation/e/os/android_packages_apps_ExactCalculator): default Android calculator2 application
- [Keyboard](https://gitlab.e.foundation/e/os/android_packages_inputmethods_LatinIME): default Android keyboard
- [App Lounge](https://gitlab.e.foundation/e/apps/apps) is the /e/OS application installer, that offers [millions applications](https://e.foundation/e-os-available-applications/). Applications are accessed from Google Play Store (commercial apps), F-Droid (open source apps) and also Progressive Web Apps, all at a single place. App Lounge features a "Privacy Score" for each application, computed from the number of trackers found by Exodus Privacy in each application, and from the number of permissions.

Other preinstalled applications:
- [Maps](/maps#how-magicearth-is-added-into-eos): MagicEarth (read the [Maps FAQ](/maps) for details about its license)
- [PDF reader](https://gitlab.e.foundation/e/apps/pdfviewer): PdfViewer Plus
- [Notes](https://gitlab.e.foundation/e/apps/notes): a fork of NextCloud Notes to support Murena online accounts
- [Tasks](https://gitlab.e.foundation/e/apps/tasks): OpenTasks

## Online services

![online_services_login_eos_ecloud](../images/online_services_login_eos_ecloud.png)

- "spot" at [https://spot.murena.io](https://spot.murena.io) search engine is a fork of [SearX](https://searx.me/)  meta-search engine. Improvements made include:
  
  - Peformance enhancements
  - User interface improvement

- [Murena Workspace](https://murena.io) includes 
  -  Drive
  -  Mail
  -  Calendar
  -  Contacts
  -  Notes
  -  Tasks
  -  Office

It is built upon NextCloud, Postfix, Dovecot and OnlyOffice. 

It has been integrated to offer a single login identity in /e/OS as well as online through a web interface.

Users can retrieve their data at [https://murena.io](https://murena.io) services, or [self-hosted](https://gitlab.e.foundation/e/infra/ecloud-selfhosting) on their own server

## /e/OS Community, Official and Legacy (Discontinued)

/e/OS is a fork of Android and in particular of the LineageOS flavor of Android. 

### Community, Official and Legacy

At present we have builds based on three Android versions:

**Officially Supported: Community and Official**
 
 - T or Android 13
 - S or Android 12
 - R or Android 11
 

 **Discontinued or Legacy**

 - Q or Android 10
 - Pie or Android 9
 - Oreo  or Android 8.x
 - Nougat  or Android 7.x
  
  
### What  does a 'discontinued' or 'Legacy' device mean

  -  Newly released applications like App Lounge and Advanced privacy are not customized for these OS versions.
  -  Lack of dedicated ROM Maintainers
  -  No support upstream on Lineage
  -  Device trees will continue to exist in the [Gitlab](https://gitlab.e.foundation/e) 
  -  Ysers with build skills can [create unofficial or custom ROMs]({% translate_link build-status %}) on these versions
  -  Security patches and vendor patches for the Pie, Oreo and Nougat code will be updated based on availability.
  - Google stops security patches for OS after some time. Similarly, Vendors drop support for older models when they release newer models in the market. Then it depends on developers from various communities to [backport](https://github.com/LineageOS/android_build/commit/4e4d9b094bc8df3bd06c74a993261058745c7733) the patches.
  - OS versions which will not receive support are marked as 'Legacy' in our [Supported Devices list](https://doc.e.foundation/devices). 
  - Devices marked as 'Legacy' will no longer receive any updates from the /e/OS team. 

## /e/OS Development Updates

### 2019 
We [addressed a number of issues](https://gitlab.e.foundation/search?group_id=&project_id=&repository_ref=&scope=issues&search=Infosec+Handbook+Review)  with ungoogling. 

This included among others: 
 - Removed connectivity check against Google servers
 - Replaced Google NTP servers by pool.ntp.org servers
 - Replaced Google DNS servers by 9.9.9.9 by default and offer users to set the DNS servers of their choice

More details about the state of /e/OS deGooglisation can be found in [this white paper](https://e.foundation/wp-content/uploads/2020/09/e-state-of-degooglisation.pdf).



### 2020 
- We introduced the "easy installer" . It lets users install /e/OS without using obscure tools or the command line. 
  
  Currently available in Linux and Windows. Development is now community driven.
- Working on pro-privacy features such as a "privacy dashboard"
- Added more supported smartphones (FairPhone 3 and 3+, PinePhone, GigaSet...) 
- Worked on adding more features such as "my SMS to cloud" and "my geolocation to cloud" (Work in Progress)
- Added Progressive Web Apps (PWAs) to the application installer V2


###  2021

- Add the capability to un-install some of the system applications where possible
- Offer corresponding Google-free SDKs for app publishers, because we believe that web apps is the future for most mobile applications.
- Make /e/OS available in two versions:
  - standard /e/OS with default applications
  - /e/OS with minimal number of default/pre-installed applications 
   > This is partially implemented as some devices which have less disk space get a [MINIMAL_APPS](/support-topics/build-e#5-build-options) version of the /e/OS

### 2022

- /e/OS V1 is available
- App Lounge with access to the full catalog of Android apps is available
- Advanced Privacy is added to /e/OS

### 2024

- /e/OS V2 is available
- /e/OS Parental Control 
- /e/OS new BlissLauncher v3, with tablet mode

## Checkout Gaël's roadmap for /e/OS and Murena

- [2024](https://community.e.foundation/t/your-2024-journey-with-e-os-and-murena-and-a-v2/56577)
- [2023](https://community.e.foundation/t/murena-e-os-roadmap-for-2023-and-beyond/49507)
- [2022](https://community.e.foundation/t/murena-e-os-a-product-roadmap-for-2022-towards-our-v1/39800)
- [2021](https://community.e.foundation/t/e-2021-the-year-everything-is-going-to-change-2-2-a-product-roadmap/31253)

## What to expect in future releases?
Check out the [eRoadmap section]({% translate_link support-topics/e-road-map %}) in our FAQ
  

## Want to give /e/OS a try?

[Install it now](/devices/)!

Or 

Purchase [a Murena smartphone](https://murena.com).
