When running `adb devices` in bash a device is shown as `unauthorized`

1. Disconnect USB between PC and device.
1. Stop adb server by entering `adb kill-server`  in command window.
1. On device use **Revoke USB debugging authorizations**  in **Developer Options**
1. On PC delete `adbkey`  file in user directory, eg. `.android`
1. Reconnect the device to the PC.
1. Check the box on the popup on the device.
1. Open a bash and enter `adb devices`

Device should be shown as shown in this screenshot

![](/images/adb_Device_Detected.png)
