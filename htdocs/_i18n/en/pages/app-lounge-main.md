
<div class="text-center">
  <img src="/images/app_lounge_banner.png" alt="">
</div>

&nbsp;

The App Lounge is the second iteration of the application store embedded within /e/OS. It allows everyone to access millions of applications directly from their phone home screen.

It combines common Android apps, open source apps and even progressive web apps in one single repository. It is the only app store that does this today. Like with its predecessor, Apps, you don’t need to sign in to an account to download apps. There is more...

- #### [Learn all about the App Lounge](/support-topics/app_lounge)
- #### [Still have questions about the App Lounge? Read the FAQ](/app-lounge)
- #### [Troubleshooting App Lounge](/support-topics/app_lounge_troubleshooting)