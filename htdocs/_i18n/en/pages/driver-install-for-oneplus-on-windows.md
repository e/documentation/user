## Purpose of this Document
To explain to OnePlus users, how to install drivers on Windows OS

## Download

Lets start by downloading the device driver


[Driver file](https://dl.google.com/android/repository/usb_driver_r13-windows.zip)

- Extract the zip into a folder on your PC

## Installation Steps


1. Connect the device with a Data USB cable. Please note some cables are only for charging and cannot be used to transfer data from your PC to the phone.


1. Go to Windows `Device Manager` 
   If the device is undetected by your PC, you will find the device with a yellow triangle next to its title in the `Device Manager`

1. Right click it and choose
   `Update` > `Browse my computer for drivers` >  `Let me pick from a list of available drivers on my computer` > `Next` > `Have disk` >  go to folder where you have extracted the zip and click on the `inf` file > choose `Bootloader interface` > `next`

1. Wait for the installation to complete

1. Once the installation is complete, reboot your PC and now when you connect your Phone it should be detected by the PC.


