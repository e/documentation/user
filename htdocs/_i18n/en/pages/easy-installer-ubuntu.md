{% include alerts/warning.html content=" A new installer the [WebInstaller](https://e.foundation/installer/) is now available as an alpha version. The Webinstaller works on Chrome based browsers as it requires WebUSB.
The EasyInstaller will be deprecated soon, hence moving forward we will not proceed with any error fixes on it " %}

## Installation steps for Linux

### Using the command line

> Depending of your configuration, you may need to prefix the commands with `sudo`

- Open a console and type the below command to install the Easy Installer app through the snap store.

    ```shell
    snap install easy-installer --channel=latest/beta
    ```

### Using the App store (the easy way)

> Here we use the Ubuntu Software Center

  - Open the Ubuntu Software application. It should open a screen like this
![](/images/ubuntu-software.png)

  - Go to the 'Explore' tab or click on the search icon
  - Type in 'easy-installer' . This should fetch the /e/OS easy installer as shown below

![](/images/easy-installer_search.png)

 - Click on the 'install' button to start the installation process
 ![](images/easy-installer_2.png)
 -
 - Depending on your configuration you may be asked to provide the admin user password to complete the installation
 - Once the installation is complete you will see this screen  

 ![](images/easy-installer_4.png)

- After the installation is complete a new icon should appear in the apps section as shown in the screenshot below. Click on it to open and try out our Easy Installer !!

![](/images/easyinstaller-in-apps.png)

{% include snippets/easy_installer.html
   title="Which permissions are required for Easy Installer to work? "
   details=site.data.easy-installer.list
 %}

### How to install EasyInstaller using snap? (Works for most Distros/OS)
Don't use Ubuntu? You can still install the EasyInstaller on other operating systems by following [our EasyInstaller Guide](https://snapcraft.io/easy-installer).
