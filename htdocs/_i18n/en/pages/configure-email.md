## Email Configuration

 - You can use your @e.email or @murena.io email account to log in at: [https://murena.io/](https://murena.io/)  

Lots of different email clients are able to automatically configure your email account – you just need to provide email address and password in order to connect.

- For example:
Murena Mail, Mozilla Thunderbird 3.1 or newer, Evolution, KMail, Kontact and Outlook 2007 or newer.

- If you are using a current Apple Mail version (both on macOS or iOS) you can visit [https://autoconfig.murena.io/](https://autoconfig.murena.io/) and fill in your details to download a configuration package that automatically configures your client.


## Manual Configuration

For manual configuration, please use the following settings:


   - SMTP/IMAP server: mail.ecloud.global

   - SMTP/IMAP username: your Murena address in full (example: john.doe@e.email or john.doe@murena.io)

   - SMTP/IMAP password: your Murena password

   - SMTP port: 587, plain password, STARTTLS, requires authentication

   - IMAP port: 993, SSL/TLS, requires authentication
