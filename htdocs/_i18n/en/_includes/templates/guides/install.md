{% assign guides_titles = guides_titles | push: "Command line" %}

{% capture install_content %}
For advanced users, installing /e/OS with Android platform tools.
{% endcapture %}
{% assign guides_contents = guides_contents | push: install_content %}

{% capture install_link %}{{ site.baseurl | append: "/devices/" | append: device.codename | append: "/install" }}{% endcapture %}
{% assign guides_links = guides_links | push: install_link %}
{% capture build_link_label %}Install{% endcapture %}
{% assign guides_link_labels = guides_link_labels | push: build_link_label %}
