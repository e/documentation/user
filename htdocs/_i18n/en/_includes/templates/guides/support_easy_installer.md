{% for el in device.install %}
    {%- if el.mode == "Easy Installer" %}
        {% assign guides_titles = guides_titles | push: "Easy-Installer" %}

        {% capture install_with_easyinstaller %}
        A software application compatible with Windows, Linux and MacOS that will help install /e/OS on your phone following simple steps on your screen.
        {% endcapture %}
        {% assign guides_contents = guides_contents | push: install_with_easyinstaller %}

        {% capture install_with_easyinstaller_link %}{{ "/easy-installer" | relative_url }}{% endcapture %}
        {% assign guides_links = guides_links | push: install_with_easyinstaller_link %}
        {% capture build_link_label %}Install{% endcapture %}
        {% assign guides_link_labels = guides_link_labels | push: build_link_label %}
    {%- endif %}
{% endfor %}
