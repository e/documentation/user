{% assign guides_titles = guides_titles | push: "Build" %}

{% capture build_content %}
Build /e/OS for yourself with your own settings.
{% endcapture %}
{% assign guides_contents = guides_contents | push: build_content %}

{% capture build_link %}{% translate_link support-topics/build-e %}{% endcapture %}
{% assign guides_links = guides_links | push: build_link %}
{% capture build_link_label %}Build{% endcapture %}
{% assign guides_link_labels = guides_link_labels | push: build_link_label %}
