{% for el in device.install %}
  {%- if el.mode == "Roll-back" %}
    {% assign guides_titles = guides_titles | push: "Roll back to stock OS" %}

    {% capture stock_revert_content %}Return to the default operating system.{% endcapture %}
    {% assign guides_contents = guides_contents | push: stock_revert_content %}

    {%- if el.url != null %}
      {% assign guides_links = guides_links | push: el.url %}
    {% else %}
      {% if device.codename == "GS290" %}
        {% capture stock_revert_link %}
          {{ "/pages/revert_e_GS290_windows.html" | relative_url }}
        {% endcapture %}
        {% assign guides_links = guides_links | push: stock_revert_link %}
      {% endif %}
      {% if device.codename == "starlte" or device.codename == "star2lte" or device.codename == "dreamlte" or device.codename == "dream2lte" or device.codename == "herolte" or device.codename == "hero2lte" %}
        {% capture stock_revert_link %}
          {{ "pages/revert_samsung_to_stock_on_windows" | relative_url }}
        {% endcapture %}
        {% assign guides_links = guides_links | push: stock_revert_link %}
      {% endif %}
      {% if device.codename == "lake" %}
        {% capture stock_revert_link %}
          {{ "pages/revert_motorola_to_stock_on_windows" | relative_url }}
        {% endcapture %}
        {% assign guides_links = guides_links | push: stock_revert_link %}
      {% endif %}
      {% if device.codename == "avicii" %}
        {% capture stock_revert_link %}
          {{ "/pages/revert_oneplus_to_stock_on_windows" | relative_url }}
        {% endcapture %}
        {% assign guides_links = guides_links | push: stock_revert_link %}
      {% endif %}
    {% endif %}
    {% capture build_link_label %}Roll back{% endcapture %}
    {% assign guides_link_labels = guides_link_labels | push: build_link_label %}
  {% endif %}
{% endfor %}
