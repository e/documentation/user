{% include set_title_var.md %}
*This documentation "{{ title }}" is a derivative of ["install LineageOS on {{ device.codename }}"](https://wiki.lineageos.org/devices/{{ device.codename }}/install) by The LineageOS Project, used under [CC BY](https://creativecommons.org/licenses/by-sa/3.0/).*
