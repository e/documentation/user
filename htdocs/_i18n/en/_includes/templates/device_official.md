{% assign device = page.device %}

{% assign title = device.vendor %}
{% assign subtitle = device.codename %}
{% assign device_title = device.name %}
{% assign details_title = "Device details" %}
{% assign image_url = "/images/devices/" | append: device.codename | append: ".jpg" %}
{% assign image_alt = device.codename %}
{% assign e_os_build_versions_status = "" | split: ',' %}
{% if device.build_version_stable %}
    {% assign t = device.build_version_stable | append: ' (official)' %}
    {% assign e_os_build_versions_status = e_os_build_versions_status | push: t %}
{% endif%}

{% assign e_os_supported_models = device.models %}

{% assign guides_titles = "" | split: ',' %}
{% assign guides_contents = "" | split: ',' %}
{% assign guides_links = "" | split: ',' %}
{% assign guides_link_labels = "" | split: ',' %}

{% assign details_topics_titles = "" | split: ',' %}
{% assign details_topics_contents = "" | split: ',' %}

{% assign maintainers = "" | split: ',' %}

{% if device.maintainers and device.maintainers.first %}
    {% assign maintainers = device.maintainers %}
{% else %}
    {% assign maintainers = maintainers | push: "/e/OS Build Team" %}
{% endif %}
{% assign document_type = "Official" %}
{% tf _includes/templates/guides/support_easy_installer.md %}
{% tf _includes/templates/guides/install.md %}
{% tf _includes/templates/guides/build.md %}
{% tf _includes/templates/guides/stock_revert.md %}

{% tf _includes/templates/details_topics/release.md %}
{% tf _includes/templates/details_topics/soc.md %}
{% tf _includes/templates/details_topics/nand.md %}
{% tf _includes/templates/details_topics/ram.md %}
{% tf _includes/templates/details_topics/cpu.md %}
{% tf _includes/templates/details_topics/architecture.md %}
{% tf _includes/templates/details_topics/gpu.md %}
{% tf _includes/templates/details_topics/network.md %}
{% tf _includes/templates/details_topics/storage.md %}
{% tf _includes/templates/details_topics/sd_card.md %}
{% tf _includes/templates/details_topics/screen.md %}
{% tf _includes/templates/details_topics/device_tree.md %}
{% tf _includes/templates/details_topics/bluetooth.md %}
{% tf _includes/templates/details_topics/wi_fi.md %}
{% tf _includes/templates/details_topics/peripherals.md %}
{% tf _includes/templates/details_topics/cameras.md %}
{% tf _includes/templates/details_topics/dimensions.md %}
{% tf _includes/templates/details_topics/battery.md %}

{% include device/product_page.html %}
