{% assign details_topics_titles = details_topics_titles | push: "Screen" %}

{% capture screen_content %}
    <span class="span-boot">
        {{ device.screen }} <br>
        {{ device.screen_res }} ({{ device.screen_ppi }} PPI)<br>
        {{ device.screen_tech }} (max {{ device.screen_refresh_rate }} Hz)
        
    </span>
{% endcapture %}

{% assign details_topics_contents = details_topics_contents | push: screen_content %}