{% assign details_topics_titles = details_topics_titles | push: "SoC" %}

{% capture soc_content %}
    {{ device.soc }}
{% endcapture %}

{% assign details_topics_contents = details_topics_contents | push: soc_content %}