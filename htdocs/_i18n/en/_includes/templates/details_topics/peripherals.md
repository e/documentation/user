
{% assign details_topics_titles = details_topics_titles | push: "Peripherals" %}

{% capture peripherals_content %}

    {%- if device.peripherals != null -%}

        {%- if device.peripherals.first.size == 1 %}
            {%- for model in device.peripherals %}
                <ul>
                    <li>{{ model.first[0] }}
                        {% assign peripherals = model.first[1] %}
                        {% include snippets/peripherals.html %}
                    </li>
                </ul>
            {%- endfor %}
        {%- else %}
            {%- assign peripherals = device.peripherals %}
            {%- include snippets/peripherals.html %}
        {%- endif %}

    {%- else %}
        –
    {%- endif %}
{% endcapture %}

{% assign details_topics_contents = details_topics_contents | push: peripherals_content %}