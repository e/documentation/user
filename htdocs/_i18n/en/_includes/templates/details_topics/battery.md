{% assign details_topics_titles = details_topics_titles | push: "Battery" %}

{% capture battery_content %}
    {% if device.battery.first.size == 1 -%}
        {%- comment %}The size of "first" will be 1 when there is a list of models{% endcomment -%}
        {% for model in device.battery -%}
            {%- assign modelname = model.first[0] %}
            {%- assign battery_data = model.first[1] %}
            {% include snippets/battery.html %}
            {%- unless forloop.last -%}
                <br/>
            {%- endunless %}
        {% endfor -%}
    {%- else %}
            {%- assign battery_data = device.battery %}
            {% include snippets/battery.html %}
    {%- endif -%}
{% endcapture %}

{% assign details_topics_contents = details_topics_contents | push: battery_content %}