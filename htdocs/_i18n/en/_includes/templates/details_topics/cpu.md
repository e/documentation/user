{% assign details_topics_titles = details_topics_titles | push: "CPU" %}

{% capture cpu_content %}
    <span class="span-boot">
        {% case device.cpu_cores %}
            {% when '1' %}
                Single-core
            {% when '2' %}
                Dual-core
            {% when '4' %}
                Quad-core
            {% when '6' %}
                Hexa-core
            {% when '8' %}
                Octa-core
            {% else %}
                {{ device.cpu_cores }}x
        {% endcase %}
        {{ device.cpu }}<br>{{ device.cpu_freq }}
    </span>
{% endcapture %}

{% assign details_topics_contents = details_topics_contents | push: cpu_content %}