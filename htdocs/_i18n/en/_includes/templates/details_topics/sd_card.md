{% assign details_topics_titles = details_topics_titles | push: "SD card" %}

{% capture sd_card_content %}
    {%- if device.sdcard != null -%}
        {{ device.sdcard }}
    {%- else %}
        –
    {%- endif %}
{% endcapture %}

{% assign details_topics_contents = details_topics_contents | push: sd_card_content %}