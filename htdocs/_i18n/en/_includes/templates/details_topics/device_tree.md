{% assign details_topics_titles = details_topics_titles | push: "Device Tree" %}

{% capture device_tree_content %}
    {%- if device.tree != null -%}
        {{ device.tree }}
    {%- else %}
        –
    {%- endif %}
{% endcapture %}

{% assign details_topics_contents = details_topics_contents | push: device_tree_content %}