{% assign details_topics_titles = details_topics_titles | push: "Bluetooth" %}

{% capture bluetooth_content %}
    {% if device.bluetooth.profiles -%}
        {{ device.bluetooth.spec }} with {{ device.bluetooth.profiles | join: ', ' }}
    {% elsif device.bluetooth != null -%}
        {{ device.bluetooth.spec }}
    {% else -%}
        –
    {% endif -%}
{% endcapture %}

{% assign details_topics_contents = details_topics_contents | push: bluetooth_content %}