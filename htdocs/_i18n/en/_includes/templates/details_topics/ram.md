{% assign details_topics_titles = details_topics_titles | push: "RAM" %}

{% capture ram_content %}
    {{ device.ram }}
{% endcapture %}

{% assign details_topics_contents = details_topics_contents | push: ram_content %}