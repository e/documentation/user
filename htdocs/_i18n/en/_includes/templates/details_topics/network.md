{% assign details_topics_titles = details_topics_titles | push: "Network" %}

{% capture network_content %}

    {%- if device.network != null -%}
        <ul>
        {% for el in device.network %}
            {% if el.bands != '' %}
                <li>{{ el.tech }} bands: {{ el.bands }}</li>
            {% else %}
                {% continue %}
            {% endif %}
        {% endfor %}
        </ul>
    {%- else %}
        –
    {%- endif %}

{% endcapture %}

{% assign details_topics_contents = details_topics_contents | push: network_content %}
