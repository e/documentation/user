### Requisiti di Base

* adb e fastbooot devono essere installati e funzionanti sul PC.
> Per configurare adb segui i passi indicati [qui](../../pages/install-adb)

* adb e fastboot richiedono i permessi di root su Linux e i diritti di amministratore su Windows.
  > In Linux anteponi sudo a qualsiasi comando adb o fastboot, ad esempio "sudo fastboot reboot", e
   in Windows avvia la finestra di comando come amministratore.

{% include alerts/danger.html content="Prima di seguire le istruzioni, verifica che sul tuo smartphone sia installato l'ultimo firmware Android disponibile. Ad esempio **se disponibile**, prima di flashare /e/S, installa Android Stock 12, In maniera similare, prima di installare /e/OS 'R', dovresti avere installato Andoid Stock 11, prima di /e/OS 'Q', doresti aver installato Android Stock 10. Controlla il sito web del produttore per scaricare la corretta versione della ROM Stock." %}