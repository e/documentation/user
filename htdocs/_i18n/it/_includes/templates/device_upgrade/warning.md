{% assign device = page.device %}

{% include alerts/warning.html content="Si prega di notare che questa build di upgrade non ha potuto essere testata in quanto non erano disponibili tester specifici per il dispositivo.
Per alcuni dispositivi potrebbe essere necessario ripristinare la ROM stock prima dell'aggiornamento. Per eseguire il flash della ROM stock è necessario utilizzare lo strumento fornito dal produttore. Potrebbe anche essere necessario scaricare un'immagine di ripristino creata appositamente per il dispositivo.
" %}

  