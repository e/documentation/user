## Il processo di recupero
Dopo l'inizio del processo di recupero di Murena Workspace, le tue voci e i tuoi dati online mancanti ricompariranno gradualmente. Ecco cosa aspettarsi:

1. **Voce File**
   Questa sarà la prima voce a tornare, inizialmente senza dati.

2. **Altre voci**
   Voci come Note e Galleria ricompariranno progressivamente.

3. **Recupero dei dati**
   I tuoi dati recuperati verranno posizionati in una cartella denominata `MurenaRecovery` nella voce File. Alcuni file potrebbero mancare o essere vuoti, quindi controlla attentamente. Inoltre, poiché i tuoi file recuperati si trovano in una nuova cartella dedicata, le condivisioni di file che potresti aver impostato prima dell'interruzione non saranno efficaci: devi impostarle di nuovo se desideri che funzionino.

Murena Workspace bloccherà le richieste provenienti dai seguenti client Nextcloud:
- App Nextcloud (Windows, macOS, Linux, Android, iOS)
- Carnet
- EasySync
- eDrive (solo per /e/OS prima di 1.13)
- Note /e/OS
- Note Nextcloud

Il motivo di questa scelta è che queste app potrebbero eliminare alcuni file quando riapriremo, poiché il client rileverà una differenza tra ciò che è presente localmente e ciò che è presente su Murena Workspace. Bloccando le richieste, diamo tempo a ciascun utente di eseguire il backup dei propri file locali. La documentazione seguente spiega, client per client, come puoi eseguire il backup dei tuoi file come utente.

## Esegui il backup dei tuoi dati locali

### eDrive
Prima di tutto, controlla se eDrive è in esecuzione sul tuo dispositivo. Vai su `Impostazioni` > `Account` > il tuo indirizzo email Murena Workspace > `Sincronizzazione account`. Se `Immagini e video` è disabilitato, significa che eDrive non è in esecuzione e puoi ignorare questa parte.

Se stai eseguendo **/e/OS inferiore a 1.13** ([come faccio a controllare la mia versione /e/OS?](https://community.e.foundation/t/howto-give-complete-e-os-version-info-easily-for-support-answers-comparison-etc/45030)):
- Aggiorna alla versione più recente di /e/OS (`Impostazioni` > `Aggiornamenti`)
- OPPURE, se non puoi aggiornare, copia i dati del dispositivo /e/OS dalle seguenti cartelle su un altro dispositivo:
    - DCIM
    - Documents
    - Movies
    - Music
    - Pictures
    - Podcasts
    - Recordings
    - Ringtones

Se stai eseguendo **/e/OS 1.13 e versioni successive**, non devi fare nulla! eDrive non elimina mai i file, quindi non preoccuparti della perdita di dati. La sincronizzazione dei file sarà abilitata fin dall'inizio con questa versione.

> **Nota:** Per impostazione predefinita, eDrive non invierà nuovamente tutti i file locali che hai sul tuo smartphone. Un aggiornamento imminente di eDrive correggerà questo comportamento e forzerà l'invio su Murena Workspace dei file che hai sul tuo dispositivo.

### App Nextcloud (Android, iOS, Linux, macOS, Windows)
Nextcloud eliminerà i file che erano presenti localmente ma non più sul server. Per impostazione predefinita, le app Nextcloud non si sincronizzeranno quando riapriremo l'app File di Murena Workspace. Copia i tuoi file in un'altra posizione.

#### Windows, macOS e Linux
Per eseguire il backup:
1. **Apri l'app Nextcloud**: avvia l'app Nextcloud su ogni dispositivo su cui è installata.
2. **Accedi alla cartella locale**: fai clic su "Apri cartella locale" per visualizzare i tuoi file sincronizzati.
3. **Seleziona i file**
   - **macOS** Usa `Cmd (⌘) + A` per selezionare tutti i file, quindi copiali usando `Cmd (⌘) + C`
   - **Windows/Linux** Usa `Ctrl + A` per selezionare tutti i file, quindi copiali usando `Ctrl + C`
4. **Crea una cartella di backup**: crea una nuova cartella (ad esempio, `Nextcloud-Backup`) in una posizione sicura.
5. **Incolla i file**
   - **macOS** Incolla i file copiati nella cartella di backup usando `Cmd (⌘) + V`
   - **Windows/Linux** Incolla i file copiati nella cartella di backup usando `Ctrl + V`

{% include alerts/warning.html content="Devi applicare questo processo su ogni dispositivo su cui hai installato un client Nextcloud." %}

#### Android
1. Apri l'app `File` e fai clic sul pulsante *Nextcloud* nel pannello laterale (se non vedi questo icona, vai al passaggio 9).

    ![image](/images/murena-workspace-recovery/nextcloud-1.png){: height="400" }
2. Ora mantieni premuto un elemento, selezionerà l'elemento, fai clic sull'icona *tre punti (⋮)* in alto a destra e premi *seleziona tutto*.

    ![image](/images/murena-workspace-recovery/nextcloud-2.png){: height="400" }
3. Selezionerà tutti i file rimanenti.

    ![image](/images/murena-workspace-recovery/nextcloud-3.png){: height="400" }
4. Ora fai clic sul pulsante *tre punti (⋮)* in alto a destra e fai clic su *copia in...*.

    ![image](/images/murena-workspace-recovery/nextcloud-4.png){: height="400" }
5. Ti porterà a una pagina diretta alla cartella principale del dispositivo. Fai clic sul pulsante *aggiungi cartella* per creare una nuova cartella.

    ![image](/images/murena-workspace-recovery/nextcloud-5.png){: height="400" }
6. Ti chiederà il nome della cartella, nel nostro caso l'abbiamo chiamata `Nextcloud.bak`.

    ![image](/images/murena-workspace-recovery/nextcloud-6.png){: height="400" }
7. Ora apri la nuova cartella creata e premi il pulsante *copia* in basso. Questo avvierà il processo di copia.

    ![image](/images/murena-workspace-recovery/nextcloud-7.png){: height="400" }
8. Potresti vedere una notifica se la dimensione della copia è grande.

    ![image](/images/murena-workspace-recovery/nextcloud-8.png){: height="400" }

    {% include alerts/success.html content="Il tuo processo di backup è completato. In ogni caso, se hai fallito al passaggio 1, segui quanto segue:" %}

9. Apri l'app `Nextcloud`, seleziona `Impostazioni` dal pannello sinistro.

    ![image](/images/murena-workspace-recovery/nextcloud-9.png){: height="400" }
10. Fai clic sull'opzione `Cartella di archiviazione dati`, aprirà un popup. Prendi nota dell'elemento selezionato nel popup.

    ![image](/images/murena-workspace-recovery/nextcloud-10.png){: height="400" }
11. Apri l'app `File` e vai al percorso annotato in precedenza (per impostazione predefinita dovrebbe essere: `Android/media/com.nextcloud.client`). Qui troverai una cartella chiamata `nextcloud`. Mantienila premuta per selezionarla.

    ![image](/images/murena-workspace-recovery/nextcloud-11.png){: height="400" }
12. Ora segui i passaggi dal 4 all'8 per completare il processo di backup.

#### iOS

1. **Apri l'app Nextcloud** sul tuo iPhone o iPad.
2. **Vai ai file/cartelle** che desideri eseguire il backup.
3. **Tocca il menu a tre punti (⋮)** accanto al nome del file o della cartella.

    ![image](/images/murena-workspace-recovery/Nextcloud-Backup-iOS1.jpeg){: height="400"}
4. **Seleziona "Imposta come disponibile offline"** per scaricare i file per l'accesso offline.

    ![image](/images/murena-workspace-recovery/Nextcloud-Backup-iOS2.jpeg){: height="400"}
5. **Apri l'app File** sul tuo iPhone.

    ![image](/images/murena-workspace-recovery/Nextcloud-Backup-iOS3.jpeg){: height="400"}
6. **Vai su "Sul mio iPhone"** o **"iCloud Drive"** sotto **Posizioni**.

    ![image](/images/murena-workspace-recovery/Nextcloud-Backup-iOS4.jpeg){: height="400"}
7. **Tocca il menu a tre punti (⋮)** in alto a destra e seleziona **"Nuova cartella"**.
8. **Dai un nome alla cartella** (ad esempio, `Nextcloud-Backup`).

    ![image](/images/murena-workspace-recovery/Nextcloud-Backup-iOS5.jpeg){: height="400"}
9. **Apri l'app File** sul tuo iPhone.
10. **Vai su "Nextcloud"** sotto **Posizioni**.
11. **Tocca il menu a tre punti (⋮)** in alto a destra e seleziona **"Seleziona"**.
12. **Seleziona tutti i file** che desideri eseguire il backup.

    ![image](/images/murena-workspace-recovery/Nextcloud-Backup-iOS6.jpeg){: height="400"}
13. **Tocca il menu a tre punti (⋮)** in basso e scegli **"Copia"**.

    ![image](/images/murena-workspace-recovery/Nextcloud-Backup-iOS7.jpeg){: height="400"}
14. **Vai alla cartella "Nextcloud-Backup"**.
15. **Premi a lungo all'interno della cartella**, quindi tocca **"Incolla"** per copiare i file.

    ![image](/images/murena-workspace-recovery/Nextcloud-Backup-iOS8.jpeg){: height="400"}

### EasySync
EasySync elimina i file che sono presenti localmente ma non sul server. Per impostazione predefinita, l'app EasySync non si sincronizzerà quando riattiveremo la voce File di Murena Workspace. Copia i tuoi file in un'altra posizione.

1. Apri l'app `File` e mantieni premuti gli elementi di cui desideri eseguire il backup.

   ![image](/images/murena-workspace-recovery/easysync-1.png){: height="400" }
2. Ora fai clic sul pulsante *tre punti (⋮)* in alto a destra e fai clic su *copia in...*.

   ![image](/images/murena-workspace-recovery/easysync-2.png){: height="400" }
3. Ti porterà a una pagina diretta alla cartella principale del dispositivo. Fai clic sul pulsante *aggiungi cartella* per creare una nuova cartella. Ti chiederà il nome della cartella, nel nostro caso l'abbiamo chiamata `easySync.bak`.

   ![image](/images/murena-workspace-recovery/easysync-3.png){: height="400" }
4. Ora apri la nuova cartella creata e premi il pulsante *copia* in basso. Questo avvierà il processo di copia.

   ![image](/images/murena-workspace-recovery/easysync-4.png){: height="400" }

### Note /e/OS
L'app Note /e/OS eliminerà le note che erano presenti sul dispositivo ma non sul server. La voce Note di Murena Workspace è attualmente disabilitata per prevenire questo comportamento. Per eseguire il backup delle tue note:

1. **A breve termine: usa il pulsante Condividi**
   - Apri le note una per una
   - Usa il pulsante di condivisione per esportare ogni nota

2. **Prossimamente: usa l'ultima app Note /e/OS**
   Forniremo un APK per aiutarti a estrarre tutte le tue note. Resta sintonizzato per gli aggiornamenti. Nel frattempo:
   - Aggiorna alla versione più recente di /e/OS
   - OPPURE, almeno, installa l'ultima versione di App Lounge seguendo [questa documentazione](https://doc.e.foundation/support-topics/app_lounge.html#download-app-lounge-apks)

### Nextcloud Notes
- Apri le note una per una
- Usa il pulsante di condivisione per esportare ogni nota

### Carnet
1. Crea il backup
   - Usa il pulsante con le tre linee in alto a sinistra
   - Seleziona il pulsante Impostazioni
       
       ![image](/images/murena-workspace-recovery/carnet-Backup1.jpeg){: height="400" }
   - Usa il pulsante Esporta
       
       ![image](/images/murena-workspace-recovery/carnet-Backup2.jpeg){: height="400" }
   - Vai alla cartella che desideri utilizzare per il backup e premi il pulsante Salva in basso a destra
       
       ![image](/images/murena-workspace-recovery/carnet-Backup3.png){: height="400" }

2. Verifica il backup
   - Apri l'app File
   - Vai alla cartella selezionata in precedenza
   - Apri carnet_archive.zip
   - Dovresti essere in grado di vedere i file `.sqd` corrispondenti alle tue note e alle cartelle, se ne avevi

## Sezione FAQ
- **D: Perché la sincronizzazione è disabilitata?**
  - R: La sincronizzazione è disabilitata per prevenire la perdita di dati durante il processo di recupero.

- **D: Quando posso riabilitare la sincronizzazione?**
  - R: Ti informeremo quando sarà sicuro riabilitare la sincronizzazione.
