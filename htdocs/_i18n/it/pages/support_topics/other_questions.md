## Info su /e/OS

### Quando ricevono le build di /e/OS gli aggiornamenti di sicurezza di AOSP e le patch del produttore?

Quando costruiamo /e/OS, applichiamo le patch di sicurezza disponibili da AOSP per la corrispondente versione di Android (cioè: Android 10, Android 11, etc...). AOSP è open-source e ci lavorano sia sviluppatori di Google, che di una grande community, compresi quelli di /e/OS.

La disponibiltà di una versione di AOSP per un dispositivo dipende principalmente dal tempo per il quale la community open-source è disposta a supportarlo. A volte dei blocchi tecnologici richiedono il passaggio ad una versione Android più recente.
A volte una community, per esempio quella di /e/OS, LineageOS o altre, esegue il back-port delle ultime patch di sicurezza su una verione AOSP meno recente.

With /e/OS, we sometimes have the option to make an /e/OS version based on newer Android version to provide a longer lifetime to key devices. For example, your device is initially based on Android 11, but there is a new OS version for your device based on Android 12, this will mean that your device will get security updates for a longer period. This is different from vendor provided device patches. Which end, when the last stock ROM update is published by the device manufacturer.


### When are future released of /e/OS scheduled

For future releases, check the link [here](https://gitlab.e.foundation/groups/e/-/milestones)


### Quali cartelle di /e/OS vengono sincronizzate su murenacloud

Per informazioni su quali cartelle verranno sincronizzate vedi [questa guida](https://gitlab.e.foundation/e/os/eDrive#map-of-synced-directories)

### Come posso sapere se il mio dispositivo è supportato?

Trovi l'elenco dei dispositivi supportati ufficialmente (e i relativi link per i download) [qui](https://doc.e.foundation/devices)

### Devo installare /e/OS manualmente?
Per installare /e/OS, su alcuni dei modelli supportati, si può usare [Easy Installer](https://doc.e.foundation/easy-installer)

    

### Che fare se il mio dispositivo non è presente nell'elenco di quelli supportati?

Nel caso in cui il tuo dispositivo non sia in elenco, potrebbe essere compatibile con il [Generic System Image (GSI) di /e/OS](https://doc.e.foundation/support-topics/understand-and-use-gsi.html)

(Assicurati di sapere come reinstallare il software originale nel caso in cui qualcosa vada male.)

Per tua informazione, spediamo dispositivi con /e/OS pre-installato a Europa, Canada e USA sul [nostro shop online](https://murena.com/products/smartphones/)

### Esiste una community per gli utenti /e/OS?

Nel caso in cui ti serva aiuto o voglia discutere di /e/OS con altri utenti, puoi leggere e usare [il forum](https://community.e.foundation)


### E' possibile trasferire i miei file sullo smartphone da macOS

To access your smartphone files from a mac, it's a 2 step process:

Step 1: you will need to download the Android File Transfer tool for macOS.
The Android File Transfer tool can be found at [this address](https://www.android.com/filetransfer/)

Step 2: Once you have installed Android File Transfer, you will need to enable file transfer from your phone.

To enable file transfer, follow these steps:
- connect your phone to your computer via USB,
- drag the notification bar from the top of the phone screen,
- tap twice on the Android System : USB notification...
- open the USB preferences and select File Transfer.

Your phone should now be visible from your mac.


### What do I do if I am facing mobile connectivity problems

Check out [this guide](https://doc.e.foundation/pages/troubleshooting-mobile-connectivity.html)

### I am facing issues sending SMS messages

Regarding sending text messages, it could be an issue with missing SMS message center.

Access this number, please dial `*#*#4636#*#*` from the phone dial pad, then tap on 'Phone Information', scroll down to the bottom of the screen to locate SMSC and hit refresh. If after hitting refresh still nothing is displayed, you need to enter this number manually. You can search SMSC for your operator on the internet.

### Do you offer Mail in service (sending a phone) to be installed?

We do not offer a mail-in installation service. That being said, if your phone is supported, you might have the option to use our easy installer that supports a few models. [Read more](https://doc.e.foundation/easy-installer)

You might also find people in the community that could help locally. You can connect with [the community](https://community.e.foundation)

### Using Easy Installer with Samsung phones using TWRP ADB

<details>

<summary>Segui questi passi</summary>


<ul>
    <li>Install the  <a href="https://doc.e.foundation/easy-installer">Easy Installer</a> on your computer</li>
    <li>Open the Easy Installer, and skip all the steps until the Easy Installer is searching for your device.</li>
    <li>Now let's try to make the Easy Installer detect your device. Boot your S9 to the recovery mode (TWRP). Once you are on the main menu of TWRP, plug your S9 to your computer.</li>
    <li> Once done, on your S9 in TWRP go to <strong>Advanced</strong> > <strong>ADB Sideload</strong>  >  Swipe to enable ADB Sideload > wait for a few seconds > <strong>Cancel</strong> .</li>
    <li>The Easy Installer should now have detected your device. If not, retry step 3 and 4 another time. If it still doesn't work, try with another cable and on another USB port. As a last resort, retry from the beginning on another computer.</li>
    <li>Now that the Easy Installer has detected your device, simply follow instructions provided. This way, /e/OS will be reinstalled, and you will be able to use your phone again.</li>
    <li>In case you are facing any issue, please share the exact step you are at, and what you already tried to do. Also share a picture of what the Easy Installer is displaying and what the phone is showing, so we can see the issue.</li>
</ul>
</details>

### Samsung bootloop (TWRP) old phones

You can enter into the recovery mode (called TWRP) by following the beginning of the part of the tutorial in the link below. Then once in TWRP (in case a password is asked simply click on `Cancel`, follow the part to wipe the data.

Please have in mind that TWRP is replaced by /e/OS Recovery on new phones.


Here is the [tutorial](https://community.e.foundation/t/howto-the-system-doesnt-boot-bootloop/28266)
### How do I enable or disable the OTA Test channel server?

<details>

<summary>Segui questi passi</summary>

<ul>

<li>Enable the developer mode: From Settings, open About phone, and tap 7 times on Build number</li>
<li>Enable access to the /e/OS update test channel: From <strong>Settings</strong> , open <strong>System</strong>, show <strong>Advanced</strong>, and open <strong>Developer options</strong>, scroll bottom, and enable <strong>Connect to /e/OS test channel</strong></li>
<li>Check for updates: From Settings, open System updates, refresh if required with the arrow at top right</li>
<li>Download, then install the latest update to test.After the reboot, you can disable the options enabled previously:</li>
<li>Disable access to the /e/OS update test channel: From Settings, open System, show Advanced, and open Developer options, scroll bottom, and disable Connect to /e/OS test channel</li>
<li>Disable developer mode: From  <strong>Settings</strong>, open <strong>System</strong>, show <strong>Advanced</strong>, and open <strong>Developer options</strong>, uncheck On at top</li>
</ul>
</details>

### How do I get my device on the supported list?
First check if your device is in the supported devices list, which can be seen here: https://doc.e.foundation/devices

You can check if there is an Unofficial build available, made by the community. Those are visible [here](https://community.e.foundation/c/e-devices/unofficial-builds)

If you don’t find your phone in these lists, you can still request support for it on our community forum, where we keep track of requests. We evaluate phone support based on most popular requests and feasibility. More at [this URL](https://community.e.foundation/c/e-devices/request-a-device)

In case you want to get a feel of /e/OS and if your device is compatible, you could also try our GSI. A GSI is a generic image that can be installed on Android smart-phones that support project Treble. It is meant for testing and not as a daily driver. More info about GSI [here](https://doc.e.foundation/how-tos/understand-and-use-gsi)

### How to get logs and create an issue

To create a GitLab issue record, you can open a GitLab account with your /e/OS (Murena) email. If you use another email, and it is not accepted, let us know, so we can whitelist your email. Getting logs is very helpful for the developers for finding the issues. To get the logs using a computer use [this method](https://community.e.foundation/t/howto-get-a-log-from-your-phone/2366)


If this is too complicated for you, you can also try generating logs without a computer use [this method](https://community.e.foundation/t/howto-create-a-log-a-bug-report-easily-without-computer/14072)


### How to collect full logs?

If you are looking at reporting issues to our helpdesk use the ID helpdesk@e.email
1. Enable developer options with `Settings`> `About Phone`> tap `Build Number` (at the bottom) 7 times.
2. Generate the bug report with `Settings` > `System`> `Developer Options` > `Bug report` > `Full report` > `Report` 
3. After that you have to wait for a couple of minutes, until the phone notifies you that the logs are ready.
4. Open `Files` > `Hamburger Menu` (Top Left) > `Bug Reports` > Context Menu (3 dots, Top Right) > `Select All` > `Share`

    You can share use the email address to send them, then let us know that you have sent them, as they will likely be received in a new ticket.
Please Note: Logs **should not be shared publicly** as they may contain sensitive data.

### How do I factory reset my phone ?

Please make sure you are running the latest /e/ OS version: Click Settings > System Updates, click the curved arrow at top right to see the latest version and install it.

If this does not help, please back up your data, and perform a factory reset, which will delete all data on the phone.

To back up your data, you can use different backup options from [this page](https://doc.e.foundation/support-topics/migrate-to-e) 

To perform factory reset, you can use: `Click Settings` > `System` > `Advanced` > `Reset options` > `Erase all data (factory reset)`


### Factory reset using recovery

- Method 1: Please turn off the phone, then press and hold power + volume up buttons. You will see /e/ recovery screen. From the recovery menu, select factory reset, format data/factory reset, then format data. Your phone will erase all data, restart, and will act like a new device.

- Method 2: Please turn off the phone, then press and hold volume up button and insert the charging cable, you can leave the button after vibration. You will see /e/ recovery screen. From the recovery menu, select factory reset, format data/factory reset, then format data. Your phone will erase all data, restart, and will act like a new device.

### How do I update the OS to the latest /e/OS build of the same version?

Please make sure you are running the latest /e/OS version. Also ensure you are connected to the internet, ideally on an wifi network. /e/OS builds usually will be greater than 500mb in size and downloading on a paid data connection will be expensive.
Click `Settings` > `System Updates`, click the curved arrow at top right to see the latest version and install it.

### Troubleshooting basic wifi connectivity issues

Please make sure you are running the latest /e/ OS version.

`Settings` > `System Updates`, click the curved arrow at top right to see the latest version and install it.

Please try forgetting the Wi-Fi connection, and then reconnecting.
`Settings` > `network & internet` > `Wi-Fi` > click the related Wi-Fi name > `Forget`. Then click the Wi-Fi name again and connect to it.

If this does not help, reset the Wi-Fi, Bluetooth and mobile data settings:

`Settings` -> `System` -> `Advanced` -> `Reset options` -> `Reset Wi-Fi`, `mobile & Bluetooth` -> `Reset settings` -> `Reset settings` , then try connecting again to Wi-Fi.

Also, after the reset, please try connecting to different Wi-Fi networks. You could ask people to enable a Wi-Fi hotspot on their phone, for example. If the Wi-Fi at your home is the only one with this issue, it might not be related to your phone.

If you get an error message indicating the connection failed, please send us a screenshot.

Go to your dialer (the  Phone app), type `*#*#4636#*#*` on the keypad to access a phone test menu.  Click on `Wi-Fi information` > `Wi-Fi status` > `Refresh stats`, then run ping test and take a screenshot with the information, then send it to us.

### How to sideload the /e/OS ROM image

* Have the OTA zip linked downloaded
* Enable USB debugging
* Connect phone to PC
* Type `adb reboot sideload` on PC's terminal and phone will reboot to sideload mode
* then type adb wait-for-sideload sideload "add the path to .zip file", usually dragging and dropping the file works in most OS's

### GPS Location not working?

You can try this:
`settings`> `apps` > see all `xxx apps`> 3 dots on top right> show `system`> `Fused Location` > `storage` & `cache`> `clear storage`> OK .

Please check in Advanced privacy that you don't use fake location option in the settings. Also disable and enable location for the whole phone (you can do it from drop down menu or from settings).

Please make sure you are outside, and wait for a minute, to make sure you get good GPS signal when testing.

Please download an app, such as GPS Test, to test your GPS signal, and if reporting an issue send us a screenshot. Mobile signal and Wi-Fi signals also help in finding your location. If you have these disabled, and especially if you are inside, it is hard to get GPS signal.


### Microsoft related connection issues (Outlook, To Do etc.)

Outlook app for Android includes a tracker called: Mobile Engagement , which can be used to track you, thus is being disabled by default by advanced privacy's tracker blocker. This tracker can be used to track you, and it is being also used in other apps to collect information about you.
All you need to do is to whitelist this tracker for Outlook app, or if you don't know the name of the tracker, just enable tracking for the whole Outlook app.
Please go to: settings> advanced privacy> manage apps trackers > Outlook , then here you can either disable the slider for "Mobile Engagement" or disable the slider "block trackers". If you choose the latter option, no tracker included in Outlook app, will be stopped from collecting your data.


### How do we transfer files using USB

To transfer data from your phone to a computer:

- Connect your phone to a laptop using a USB cable. If the phone is not detected, try using another cable or another USB port.
- Slide your finger from top of the screen downwards, and click on "Android System. Charging this device via USB" 2 times. 
- You will see USB preferences menu. Select `File transfer`. 
- The phone will now be available in the file explorer of your laptop, and you can browse the files of your phone from your laptop. Copy what you need from the phone. 
- You can copy the data back to your phone (or another phone), by doing the same steps, and copying the files from the computer, then pasting them into the phone folders.

### How do I clear app cache

Could you please try clearing the cache of the app.
settings> apps&notifications> see all `xxx apps` > `appname` > `storage & cache`> clear `storage`> `OK`
for system apps:
`settings`> `apps&notifications`> see all `xxx apps`> 3 dots on top right> show `system`> `appname` > `storage & cache`> clear `storage`> `OK`


### How do I sell my Murena phone on community forum

You can try selling your phone on the [community forum](https://community.e.foundation/c/e-devices/sell-swap/91)