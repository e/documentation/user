## Situazione dello sviluppo di microG

Lo sviluppo di microG è un progetto in corso. Discutiamo regolarmente con il suo manutentore e sosteniamo il progetto microG anche finanziariamente.

mentati. Da qualche tempo abbiamo introdotto il sistema FCM e ottimizzato il codice.

E' stata aggiunta anche l'API contact-tracing (anche se non è abilitata di Defaul su /e/OS, vedi sotto).

There are more updates in the pipeline. 
The project is complex and it would be great if more users contribute to it.

## microG e microG EN

- microG è una re-implementazione open source di app utente e librerie proprietarie di Google Android

- microG EN è una build di microG necessaria soltanto agli utenti che vogliono usare app di contact tracking per il COVID-19.

## Tutto sul Contact tracing

### Un avviso del Development team di /e/OS
- L'app di contact tracing non è necessaria per il funzionamento di /e/OS e può essere facilmente rimossa dall'utente a suo piacere.
- /e/OS non conserva i dati generati dall'app o dall'API Exposure Notifications
- Questa funzione è stata prevista nel caso in cui gli utenti di /e/OS si trovino ad essere obbligati ad utilizzare un'app Governativa di contact tracing per il Covid-19.

### Maggiori informazioni sull'API microG EN
L'API Exposure Notifications è richiesta dallo smartphone per interagire con le app tracking covid-19 usate in alcune nazioni.
- L'API è disabilitata di default
- L'implementazione richiede l'intervento diretto dell'utente per essere attivata.
  Ciò include
   - Installare l'API
   - Installare le app specifiche per la tua nazione
   - Abilitare l'API quando serve

Questa guida illustra i passaggi necessari per attivare il framework su /e/OS

{% include alerts/tip.html content="Not all covid tracing apps require the Exposure Notifications API to function. For example, the TousAntiCovi app of France does not require the Exposure Notifications API. Check the app specific to your country works on the /e/OS, before enabling the API"  %}

### Come funziona il contact tracing Covid-19?

Il tracciamento dei contatti COVID-19 avviene tramite l'API Exposure Notifications. L'API ha le quattro funzioni seguenti

- Trasmissione Bluetooth: il telefono trasmette un Rolling Proximity Identifier (RPI) via Bluetooth. Ogni dieci minuti viene pubblicato un nuovo RPI, per rendere impossibile rintracciarti. Gli RPI sono derivati da una chiave privata generata casualmente una volta al giorno dal dispositivo. Questa derivazione garantisce crittograficamente che gli RPI non possano essere collegati tra loro, tranne nel caso in cui si condivida la chiave privata.
- Scansione Bluetooth: ogni 5 minuti circa il telefono esegue una scansione dei dispositivi Bluetooth vicini che trasmettono RPI e memorizza l'RPI insieme a un timestamp in un database locale.
- Condivisione delle chiavi: se sei risultato positivo al test COVID-19, puoi chiedere al tuo telefono di condividere le chiavi private degli ultimi 14 giorni con l'app COVID-19 per la ricerca dei contatti. Solo con questa chiave privata gli altri potranno vedere se sono stati in contatto con te per più di 10 minuti. L'implementazione di microG farà in modo che questa condivisione di chiavi non possa avvenire senza esplicito consenso dell'utente.
- Calcolo del rischio: in base all'elenco delle chiavi private condivise dalla App companion e all'elenco degli RPI raccolti negli ultimi 14 giorni, il telefono verifica se si è stati a stretto contatto con una persona che è risultata positiva al test. Il telefono calcola un punteggio di rischio totale e fornisce all'App companion informazioni su data e ora, durata del contatto e potenza del segnale dell'esposizione. L'App companion non saprà quali chiavi o persone sono state a stretto contatto, né otterrà informazioni sul luogo in cui è avvenuto il contatto.

{% include alerts/tip.html content="microG costituisce l'unica implementazione open source di questa API."%}

### Passi per installare l'API Exposure Notifications in /e/OS

1. Scarica l'ultima versione di [microG EN](https://gitlab.e.foundation/e/apps/GmsCore/-/releases)

     - I download di entrambe (`/dev` e `/stable`) le build sono disponibili a questo indirizzo. Verifica la versione e scarica l'apk corrispondente

1. Installalo manualmente
1. Verifica regolarmente la presenza di eventuali aggiornamenti e, se disponibili, applicali manualmente


### Configurare una app per funzionare con microG EN

Useremo l'esempio della app Covid Radar (spagnola)
 - Dopo aver installato l'app, aprila sul telefono

     ![](/images/Covid_3.png)
 - Mostrerà dei pop up che richiedono le autorizzazioni per
   - Abilitare il Bluetooth
   - Abilitare l'attivazione delle API di Exposure Notification
 - Dai `ok` a entrambi i pop up
 - Ora la app, che nel nostro caso era Covid Radar, inizierà a funzionare.
 - Le API Exposure Notification a questo punto verranno abilitate, come puoi vedere sullo screenshot seguente

     ![](/images/Covid_7.png)


### Come aggiornare il Framework microG Exposure Notification

La nuova **versione di microG EN** presenta alcune migliorie nelle API del Framework Exposure Notification.

Exposure Notification Framework è stato sviluppato da Apple e Google per consentire il tracciamento dei contatti su iOS e Android. Le API Exposure Notification Framework sono necessarie per poter utilizzare la maggior parte delle app di tracciamento dei contatti COVID-19.

{% include alerts/tip.html content="Not all covid tracing apps require the Exposure Notifications API to function. For example, the TousAntiCovi app of France does not require the Exposure Notifications API. Check the app specific to your country works on the /e/OS, before enabling the API"  %}

Purtroppo, l'aggiornamento di microG EN che condividiamo su Apps non si installa o si aggiorna automaticamente e deve essere installato manualmente.

Per installarlo manualmente, devi:

- Scarica l'ultimo microG EN

I download per entrambe le build (`/dev` and `/stable`) sono disponibili a [questo link](https://gitlab.e.foundation/e/apps/GmsCore/-/releases). Verifica la tua versione e scarica l'apk corrispondente.

- Installa manualmente l'apk
- Controlla spesso se esistono agiornamenti e, se disponibili, installali manualmente.

Nel caso in cui microG EN divenisse indisponibile sul tuo dispositivo dopo un aiornamento di /e/OS, non preoccuparti: lo puoi reinstallare seguendo i passi forniti in questa guida.


## Come disabilitare l'App o il framework?

Puoi disabibilitare il contact tracing COVID-19 da /e/OS molto facilmente. Tutto quel che devi fare è...
- Disinstalla l'App di contact tracing che hai scaricato
- Disabilita l'API Exposure Notifications da `Impostazioni` > `Sistema` > `microG` > `Exposure Notifications`

Inoltre, è anche possibile disinstallare la versione di microG con Exposure Notifications. Per farlo, vai su `Impostazioni` > `App & notificche` > `microG Services Core`, tocca i `3 puntini` in alto a destra e scegli `Disnstalla aggiornamenti`.

## Ulteriori Informazioni

- [Progetto microG](https://microg.org/)
- [microG su Github](https://github.com/microg)
- [Progetto microG sul Gitlab /e/OS](https://gitlab.e.foundation/e?filter=microg)
