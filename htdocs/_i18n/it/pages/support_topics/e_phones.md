### Vendete telefoni con /e/ preinstallato?

Sì - Smartphone nuovi e ricondizionati flashati con /e/OS, [sono disponibili ora](https://e.foundation/e-pre-installed-smartphones/).

### Come faccio a scegliere la mia nazione sul vostro sito web?

Per vedere l'elenco delle nazioni
- Browse il nostro [shop murena](https://murena.com/)
- Nella barra di navigazione, dovresti vedere un menu a comparsa simile allo screenshot seguente

![](/images/eshop_country_selection.png)

L'elenco a comparsa delle nazioni visualizza le seguenti informazioni
- Elenco aggiornato delle nazioni nelle quali vendiamo i nostri dispositivi 

Dopo aver scelto la nazione dovresti poter vedere
- Quali dispositivi siano disponibili alla vendita in quella nazione.

### E' possibile cambiare la lingua mostrata nello [shop murena](https://murena.com/)
Si, lo è. L'opzione per la modifica della lingua utilizzata è presente sulla barra di navigazione

Al momento, puoi scegliere tra queste lingue
- Inlese
- Francese
- Tedesco

### E' possible installare ed usare le app del Google Play store su /e/OS ?

Su /e/OS possono essere installate un gran numero di app. Lo facciamo per mantenere la massima compatibilità e per soddisfare una gamma più ampia di utenti.

Ciò detto, non tutte le app disponibili possono funzionare. Le app che richiedono l'intera base del codice Google per funzionare, potrebbero non funzionare su /e/OS. 

Puoi verificare [questo elenco](https://community.e.foundation/t/editable-list-of-apps-that-work-or-do-not-work-with-microg/21151)  di app che funzionano o meno su /e/OS. 

Qui trovi un elenco di [banking app](https://community.e.foundation/t/list-banking-apps-that-work-on-e-os/33091) che funzionano con e/OS. 

Stiamo lavorando per migliorare la copertura delle app.
