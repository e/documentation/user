### Se trovo un bug, devo inviarvi una e-mail?

No - segnala i bug e invia suggerimenti come [spiegato qui](/support-topics/report-an-issue). Aggiungi il maggior numero di dettagli e, se possibile, un [log](/support-topics/create-a-log). In questo modo è più facile per i nostri sviluppatori eseguire il debug del problema. Considera che gli sviluppatori assegnati potrebbero contattarti attraverso la sezione commenti di Gitlab nel caso in cui avessero bisogno di ulteriori input da parte tua.

Evita di utilizzare l'e-mail o la messaggistica istantanea per segnalare i problemi, non è un processo efficiente.

### Ho dei suggerimenti per voi. Come posso contattarvi?

Suggerimenti e richieste di nuove funzionalità sono ben accetti. È possibile creare un topic [qui] (https://community.e.foundation/c/e-smartphone-operating-system/request-a-feature/82). Sul forum gli altri utenti possono esaminare i vostri suggerimenti e fornire il loro feedback. 
Se il numero di richieste degli utenti è elevato, le condivideremo con il nostro team di sviluppo che valuterà se il suggerimento è tecnicamente possibile. In base alle loro indicazioni, potremo pianificarne l'inclusione nella nostra roadmap di sviluppo.

Se hai suggerimenti per migliorare la privacy sul nostro sistema operativo, puoi inviare un'e-mail a <privacychallenge@murena.io>

