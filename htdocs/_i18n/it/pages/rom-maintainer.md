Prima di arrivare alla parte in cui ci si candida per diventare Maintainer ROM, rispondiamo ad alcune delle domande più frequenti su questo argomento.
## Chi è un Maintainer di ROM?
Una semplice definizione del termine " Maintainer ROM" è quella di un utente che esegue la build di una ROM che viene flashata su un particolare dispositivo. I Maintainer della ROM sono responsabili dei problemi che gli utenti incontrano durante il flashing o l'utilizzo della ROM.
- Questa definizione cambierà nel contesto di /e/OS... più avanti in questo documento

## Avete dei Maintainer ROM in /e/OS ?
Si, abbiamo dei Maintainer ROM che seguono le build per dispositivi dedicati

## Avete dei manutentori per le ROM di tutti i dispositivi supportati in elenco?
No. Per i dispositivi per i quali non disponiamo di un ROM Maintainer dedicato, eseguiamo le build utilizzando la nostra infrastruttura di build automatica

## Ci sono differenze tra i manutentori di altre ROM personalizzate e /e/OS?
Sì e queste differenze sono piuttosto importanti 
 - I manutentori delle ROM di /e/OS sono responsabili dei bug specifici del loro dispositivo.
 - I nomi dei manutentori delle ROM di /e/OS non vengono divulgati o condivisi con altri utenti. Questo viene fatto per rispettare la privacy dei manutentori delle ROM, a meno che non vogliano condividere personalmente i loro dettagli con gli utenti.

## Quali sono le responsabilità dei manutentori della ROM? 
I manutentori della ROM sono tenuti a fare quanto segue
- Build della /e/OS ROM utilizzando la stessa base di codice disponibile su [/e/Gitlab](https://gitlab.e.foundation/e/os/android).
- Se il dispositivo non è supportato da LineageOS, il manutentore della ROM dovrà fare il porting dei tree del dispositivo e sincronizzarli con il codice sorgente di /e/OS.
- Non saranno apportate modifiche di alcun tipo alla ROM o al codice sorgente così come è condiviso sul [/e/Gitlab](https://gitlab.e.foundation/e/os/android)
- Questo significa specificamente che lo stesso insieme di applicazioni disponibili su [/e/Gitlab](https://gitlab.e.foundation/e/os/android) sarà utilizzato nella compilazione.
- Il manutentore della ROM deve prima testare la ROM sul proprio dispositivo, assicurarsi che funzioni correttamente e poi rilasciarla per tutti gli utenti.
- Se durante il test emergono problemi specifici del dispositivo, il manutentore del dispositivo deve risolverli prima di condividere la build finale con gli utenti.

## Come posso diventare un Maintainer ROM?

### Prerequisiti

  Se sei interessato a diventare un manutentore di ROM, si presume che tu abbia una vasta esperienza nella build e nel flashing di ROM personalizzate. Si presume inoltre che sia esperto nel debug dei problemi incontrati durante il processo di creazione.
  

### Passi

  1. First you should build an unofficial /e/OS ROM using the same code as available on our [/e/Gitlab](https://gitlab.e.foundation/e/os/android)
  You are free to choose the build mode you prefer. You can either use Docker or the traditional repo sync method to build the ROM.
  1. Flash it on your own device and test it. Check that the device is functioning as expected. Which basically means you are able to make and receive calls, send and receive text messages ...
  1. Share this unofficial build with users on [our forum](https://community.e.foundation/c/e-devices/unofficial-builds/92). Create a new topic for this and mark it as an unofficial build.
  1. Once the users test the build they will share their feedback on the same
  1. Based on a reasonable number of positive feedback you can send us a  mail to <join@murena.io>, with a clear subject line (like: ROM Maintainer Mediatek devicename) and we will take it up from there.

## Avete un ruppo o un canale di supporto dedicato per i Mantainer ROM
Si, abbiamo un canale telegram dedicato esclusivamente ai Maintainer ROM. Quando sarai parte dei Maintainer ROM, le tue credenziali verranno aggiunte al gruppo.

## Posso uscire dal ruppo dei Maintainer ROM?
Si, essendo un compito su basi volontarie, puoi uscire dal ruppo dei Maintainer ROM in qualsiasi momento. Chiediamo solo di comunicarci per tempo la decisione in modo che possiamo trovare soluzioni alternative per far si che le build continuino a essere disponibili agli utenti. 

## Cosa fare nel caso abbia altre domande da porre
Nel caso in cui abbia ulterioridubi, ti invitiamo a inviare una email a <helpdesk@murena.com> e ti risponderemo il prima possibile.
