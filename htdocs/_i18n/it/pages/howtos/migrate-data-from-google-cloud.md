### Connetti il tuo account Google
1. Accedi all'account [Murena Workspace](https://murena.io) usando le tue credenziali
1. Naviga su Impostazioni e quindi Migrazione Dati
![Schermata che mostra come andare su Impostazioni](/images/howtos/ecloud/navigate-to-settings.jpg)
![Schermata che mostra come andare su impostazioni Migrazione Dati](/images/howtos/ecloud/navigate-to-data-migration-settings.jpg)
1. Clicca sul pulsante "Accedi con Google" per autenticare il tuo account Google su Murena Workspace
![Schermata che mostra come autenticarsi con Google](/images/howtos/ecloud/click-on-sign-in-with-google.jpg)
1. Google ti domanderà di dare le autorizzazioni ad accedere ai servizi come Calendar, Drive, Photos e Contacts. Puoi scegliere di fornire solo alcune autorizzazioni in modo da sbloccare solo alcune parti dell'App
![Schermata che mostra come fornire il consenso a Murena Workspace](/images/howtos/ecloud/give-consent-google.jpg)
1. Dopo esserti autenticato ed aver fornito le autorizzazioni, potrai importare i dati


### Importa i Contatti
1. Premi sul pulsante "Importa Contatti Google" per importare i tuoi contatti su Murena Workspace
![Schermata che mostra il pulsante Importa Contatti Google](/images/howtos/ecloud/import-contacts.jpg)
1. Una volta importati i contatti, apparirà una notifica
![Notifica dopo che i contatti sono stati inportati](/images/howtos/ecloud/contacts-import-successful.jpg)
1. Ora puoi accedere i contatti importati con l'App Contatti di Murena Workspace o sincronizzarli sul tuo   dispositivo /e/OS

### Importa i Calendari
1. Scegli il calendario che desideri importare dall'elenco del tuo account Google (Per esempio "my-tasks-calendar")
1. Premi il pulsante "Importa calendario" per importarlo
![Schermata che mostra il pulsante Importa calendario](/images/howtos/ecloud/import-calendar.jpg)
1. Una volta importato, apparirà una notifica
![Notifica che appare dopo che il calendario è stato importato](/images/howtos/ecloud/calendar-import-successful.jpg)
1. Ora puoi accedere il calendario dall'App Murena Workspace calendar o sincronizzarlo sul tuo dispositivo /e/OS

### Importa Foto
1. Per importare le foto da Google Photos su Murena Workspace, scegli la directory sulla quale importarle
1. Dopo averla scelta, premi il pulsante "Importa Google photos"
![Schermata che mostra il pulsante Importa Google photos](/images/howtos/ecloud/import-photos.jpg)
1. Dato che le foto verranno caricate in background, puoi uscire dalla pagina e ritornarci in seguito per controllare il procedere dell'operazione
1. Una volta terminato, riceverai una notifica  
![Notifica dopo aver terminato l'importazione delle foto](/images/howtos/ecloud/photos-import-success-notification.jpg)  
1. Ora puoi accedere alle tue foto usando l'App per le foto di Murena Workspace

### Importare files da Drive
1. Scegli il formato di importazione dei documenti
1. Imposta la directory di importazione
1. Premi il pulsante "Importa file Google Drive"
![Schermata che mostra il pulsante Importa file Google Drive](/images/howtos/ecloud/import-drive.jpg)
1. Dato che i file dal tuo Drive verranno importati in background, puoi uscire dalla pagina e ritornarci in seguito per controllare il procedere dell'operazione
1. Una volta terminato, riceverai una notifica  
![Notifica dopo aver terminato l'importazione da drive](/images/howtos/ecloud/drive-import-success-notification.jpg)  
1. Ora puoi accedere i file dall'App File di Murena Workspace