## Parlaci della tua esperienza!

Pensiamo che /e/OS diventerà il terzo sistema operativo mobile in futuro. Puoi far parte della storia! Probabilmente stiamo vivendo la parte più emozionante di questa avventura: pochi di noi insieme - sviluppatori e utenti - stanno dando forma a una rivoluzione.

Dato che sei parte di questa rivoluzione, vorremmo raccontare la tua storia con /e/OS e non solo la nostra.

Parla di te e della tua esperienza: fai un semplice post su Twitter, Mastodon e altri social media, mostra com'è, perché è importante per te. Puoi anche aprire un account su Medium per parlare e scrivere un piccolo articolo con illustrazioni. Pubblica alcuni video su [peertube](https://joinpeertube.org/en/) (o anche su youtube...)...

## E ti contatteremo!

Ci piace ascoltare le tue esperienze positive, e trasmetteremo tutte le storie più belle su /e/OS sui social media, nelle nostre newsletter... Basta che ci mandi un ping menzionando [@gael_duval](https://twitter.com/gael_duval) per i post su Twitter o [@gael@mastodon.social](https://mastodon.social/@gael) per i post su Mastodon, oppure che ci scriva per avvisarci a: <helpdesk@murena.com>

Grazie ancora per il tuo sostegno!
