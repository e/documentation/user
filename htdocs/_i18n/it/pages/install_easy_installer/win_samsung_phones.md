## Telefoni Samsung
Per quanto riguarda i telefoni Samsung, esiste un tool esterno che si chiama "wdi-simple.exe" che viene fornito con easy-installer. Se richiesto, sarà avviato durante il processo e installa i driver necessari per far funzionare heimdall.

Per i curiosi, questo tool consiste in una nostra build del [progetto libwdi](https://github.com/pbatard/libwdi). 
