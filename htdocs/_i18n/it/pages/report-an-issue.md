Cosa fare quando si incontra un problema con /e/OS?

## Connettiti al nostro server GitLab

   * Se hai un account, [accedi](https://gitlab.e.foundation/users/sign_in)
   * Altrimenti, [creane uno](https://gitlab.e.foundation/e)

## Vai nel cruscotto delle discussioni

   - le discussioni su /e/OS sono create e visualizzate [qui](https://gitlab.e.foundation/e/backlog/-/issues)

   - Prima di creare una nuova discussione, controlla se è già stata riportata. Per farlo

   - Scrivi l'errore nella casella di ricerca in cima allo schermo come mostrato qui sotto

   ![Nuova Ricerca](/images/new_issue_search.png)

   - La schermata visualizzerà anche discussioni con parole simili

   - Se la ricerca non trova alcun risultato apri una nuova discussione

## Crea una nuova discussione

   - Seleziona un modello per la discussione dal menu a discesa 'Seleziona un modello'
   - 'Applica modello'
   -  Aggiungi un titolo
   - Completa con i dettagli come richiesto dal modello
   - [Aggiungi i log](/support-topics/create-a-log) quando possibile. Questo aiuta gli sviluppatori per individuare l'errore.
   - Quando hai finito clicca sul pulsante `Segnala il problema`.



## Ricevi un messaggio di errore quando crei un nuovo ID in /e/OS Gitlab?

   - Alcuni utenti ricevono un messaggio di errore quando provano a creare un nuovo ID nel /e/OS Gitlab
   - Il messaggio di errore potrebbe chiederti di contattare Murena all'indirizzo <helpdesk@murena.com>. 
   - Nel caso in cui si verifichi un errore di questo tipo, per piacere invia una mail con i seguenti dati
      - Cognome e nome (Full Name):
      - Nome utente (Desired username):
      - Email (Email):
   - <a href="mailto:helpdesk@murena.com?subject=Open%20GitLab%20account&body=Hi%2CI%20want%20to%20create%20a%20Gitlab%20acccount%20to%20report%20issue.%0A%0AFull%20Name%3A%20%0ADesired%20username%3A%20%0AEmail%3A%20">Apri una segnalazione per la creazione di un utente GitLab</a>



   - Il problema è dovuto al fatto che qualche tempo fa abbiamo avuto un gran numero di ID fasulli registrati nei nostri server. Per correggere questo problema abbiamo permesso la registrazione solo per un limitato numero di domini E-mail. Gli utenti che vogliono registrarsi con il loro indirizzo email privato o un indirizzo Gmail, possono inviare la richiesta a <helpdesk@murena.com> ed il team creerà il loro account. 



## Sintassi e Fonte

   * [Fonte](https://docs.gitlab.com/ee/user/project/description_templates.html#setting-a-default-template-for-issues-and-merge-requests)
   * [Sintassi Markdown (GitLab)](https://docs.gitlab.com/ee/user/markdown.html)

