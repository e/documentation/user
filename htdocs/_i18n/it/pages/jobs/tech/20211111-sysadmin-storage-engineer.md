## Responsibilities 

- Mantenere ed espandere il nostro parco di server di storage dedicati, bilanciando capacità, prestazioni e costi.
- Configurare e mantenere cluster Ceph di staging e di produzione in diversi DC in tutto il mondo. 
- Monitorare e migliorare in modo proattivo le nostre routine di backup** e i controlli di integrità dei dati.
- Garantire l'alta disponibilità dei nostri sistemi di storage. Risolvere i problemi di hardware, sistema operativo, applicazioni e rete.
- Fornire assistenza di livello 2/3 al team di assistenza clienti per problemi relativi all'infrastruttura.
- Partecipare ai **programmi di servizio** per risolvere gli incidenti che influiscono sulla disponibilità delle applicazioni.

## Requirements

- Obbligatorio: Esperienza nella gestione di **almeno un cluster Ceph di produzione su scala TeraByte**; preferibilmente sia RADOS Gateway che CephFS.
- MinIO o altre soluzioni di storage a oggetti sono un plus.
- Esperienza con **NFS, RAID e LVM**.
- Conoscenza di Borg o di una soluzione simile per i backup criptati.
- Conoscenza di TCP/IP, UDP, SSL/TLS e altri protocolli Internet correlati.
- Conoscenza di shell scripting.
- Proattività, eccellente capacità di risolvere i problemi e di lavorare in gruppo.
- Comunicazione fluente in inglese.
