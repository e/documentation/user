## Informazioni sul Progetto /e/OS

> Scopri di più sul progetto /e/OS

- [Cos'è /e/OS? Leggi la descrizione del prodotto!]({% translate_link what-s-e %})
- [Installa /e/OS su uno smartphone supportato!]({% translate_link devices %})
- [Crea un account Murena Workspace gratuito]({% translate_link create-a-murena-cloud-account %})
- [HOWTO]({% translate_link support-topics %})
- [FAQ]({% translate_link support-topics %})
- [Condividi la tua /e/ esperienza!]({% translate_link testimonies %})


## Contribuisci ad /e/OS

> Vuoi aiutare il progetto?

- [Projects looking for contributors]({% translate_link projects-looking-for-contributors %})
- [Open positions at Murena](https://murena.com/jobs/)
- [ROM Maintainer]({% translate_link rom-maintainer %})
- [Translators]({% translate_link translators %})
- [Testers]({% translate_link testers %})
- [Volunteers]({% translate_link volunteers %})

## Per saperne di più

> Informazioni sul progetto /e/OS o le sue app

- [Easy-installer /e/OS]({% translate_link easy-installer %})
- [App Maps]({% translate_link maps %})
- [Differenti tipologie di build]({% translate_link build-status %})
- [App Lounge ]({% translate_link app-lounge-main %})
- [Advanced Privacy]({% translate_link support-topics/advanced_privacy_main %})
- [e.foundation Avvisi Legali & Privacy](https://e.foundation/legal-notice-privacy/)

## Scorciatoie

> Link veloci al sito web /e/OS, ai forum e ai canali telegram

- [Sito web /e/OS](https://e.foundation/)
- [Codice Sorgente /e/OS](https://gitlab.e.foundation/e)
- [Serve aiuto? Supporto?](https://e.foundation/contact/)
- [Segnalare un bug in /e/OS o nelle app](https://gitlab.e.foundation/e/backlog/-/issues/new)
- [Chiedi di aggiunere il tuo dispositivo all'elenco di quelli supportati](https://community.e.foundation/c/e-devices/request-a-device)
