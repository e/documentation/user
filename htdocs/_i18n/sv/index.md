## Om /e/OS Projektet

> Upptäck mer om projektet /e/OS

- [What's /e/OS? Read the product description!]({% translate_link what-s-e %})
- [Install /e/OS on a supported smartphone!]({% translate_link devices %})
- [Create a free Murena.io Workspace account]({% translate_link create-a-murena-cloud-account %})
- [HOWTOs]({% translate_link support-topics %})
- [FAQ aobut /e/OS]({% translate_link support-topics %})
- [Share your /e/OS experience!]({% translate_link testimonies %})

## Lär dig mer om

> Information om /e/OS-projektet och dess applikationer

- [/e/OS easy-installer]({% translate_link easy-installer %})
- [Maps application]({% translate_link maps %})
- [The different build types]({% translate_link build-status %})
- [App Lounge ]({% translate_link app-lounge-main %})
- [Advanced Privacy]({% translate_link support-topics/advanced_privacy_main %})
- [e.foundation Legal Notice & Privacy](https://e.foundation/legal-notice-privacy/)

## Bidra till /e/OS!

> Vill du hjälpa projektet?

- [Projects looking for contributors]({% translate_link projects-looking-for-contributors %})
- [Open positions at Murena](https://murena.com/jobs/)
- [ROM Maintainer]({% translate_link rom-maintainer %})
- [Translators]({% translate_link translators %})
- [Testers]({% translate_link testers %})
- [Volunteers]({% translate_link volunteers %})

## Genvägar

> Snabblänkar till /e/OS websidor, forum och telegramkanaler

- [/e/OS web site](https://e.foundation/)
- [/e/OS Source Code](https://gitlab.e.foundation/e)
- [Need Help? Support?](https://e.foundation/contact/)
- [Report a bug in /e/OS or apps](https://gitlab.e.foundation/e/backlog/-/issues/new)
- [Add your Smartphone to the supported list](https://community.e.foundation/c/e-devices/request-a-device)

