var selectedFilters = {}

// Allow sorting by number, ref https://github.com/DataTables/Plugins/blob/master/sorting/any-number.js
_anyNumberSort = function (a, b, high) {
  var reg = /[+-]?((\d+(\.\d*)?)|\.\d+)([eE][+-]?[0-9]+)?/;
  a = a.replace(',', '.').match(reg);
  a = a !== null ? parseFloat(a[0]) : high;
  b = b.replace(',', '.').match(reg);
  b = b !== null ? parseFloat(b[0]) : high;
  return ((a < b) ? -1 : ((a > b) ? 1 : 0));
}

jQuery.extend(jQuery.fn.dataTableExt.oSort, {
  "any-number-asc": function (a, b) {
    return _anyNumberSort(a, b, Number.POSITIVE_INFINITY);
  },
  "any-number-desc": function (a, b) {
    return _anyNumberSort(a, b, Number.NEGATIVE_INFINITY) * -1;
  }
});

// // To allow reseting the sort options, ref https://stackoverflow.com/questions/27008837/jquery-datatable-reset-sorting
// jQuery.fn.dataTableExt.oApi.fnSortNeutral = function ( oSettings )
// {
//     /* Remove any current sorting */
//     oSettings.aaSorting = [];

//     /* Sort display arrays so we get them in numerical order */
//     oSettings.aiDisplay.sort( function (x,y) {
//         return x-y;
//     } );
//     oSettings.aiDisplayMaster.sort( function (x,y) {
//         return x-y;
//     } );

//     /* Redraw */
//     oSettings.oApi._fnReDraw( oSettings );
// };

function updateFilters() {
  $(".smartphone-table tr").filter(function () {
    $(this).toggle(true)
  });
  for (var column in selectedFilters) {
    value = selectedFilters[column].value.toLowerCase();

    if (column === "device-type" && value != "-1") {
      $(".smartphone-table tbody tr").each((_, el) => {
        const deviceType = $(el).attr("data-device-type");
        if (value !== deviceType) {
          $(el).toggle(false);
        }
      })
      continue;
    }
    if (column === "verified-boot" && value == "0") {
      $(".toggle-vb").hide();
      continue;
    }else{
      $(".toggle-vb").show();
      if (column === "verified-boot" && value != "-1") {
        $(".smartphone-table tbody tr").each((_, el) => {
          const dataVerifiedBoot = $(el).attr("data-verified-boot");
          if (value !== dataVerifiedBoot) {
            $(el).toggle(false);
          }
        })
        continue;
      }
    } 
    if (column === "legacy") {
      $(".smartphone-table tbody tr").each((_, el) => {
        const legacyDevice = $(el).attr("data-legacy");
        if (value === 'yes') {
          $(el).show();
        } else {
          if (legacyDevice === 'yes') {
            $(el).hide();
          }
        }
      })
      continue;
    }
    $(".smartphone-table tr ." + column).filter(function () {
      var lineValue = $(this).text().toLowerCase().replaceAll(/\s/g, ' ');
      if (selectedFilters[column].type == "truncate") {
        lineValue = Math.trunc(parseFloat(lineValue))
      }

      if ((selectedFilters[column].type == "text" && lineValue.indexOf(value) < 0 ||
        lineValue != value && selectedFilters[column].type == "truncate") &&
        value != -1) {
        $(this.closest('tr')).toggle(false)
      }
    });
  }

}

function addFilterSelector(selectorId, colomnClass, type = "text") {
  $("#" + selectorId).change(function (event) {
    var value = event.target.value;
    selectedFilters[colomnClass] = {}
    selectedFilters[colomnClass].value = value
    selectedFilters[colomnClass].type = type
    updateFilters()
  })
}

function addFilterTruncate(selectorId, colomnClass) {
  $("#" + selectorId).change(function (event) {
    var value = event.target.value;
    selectedFilters[colomnClass] = {}
    selectedFilters[colomnClass].value = value
    selectedFilters[colomnClass].type = "truncate"
    updateFilters()
  })
}

function addFilterRadio(radioName, colomnClass) {
  $('input[type=radio][name=' + radioName + ']').change(function (event) {
    var value = event.target.value;
    selectedFilters[colomnClass] = {}
    selectedFilters[colomnClass].type = "text"
    selectedFilters[colomnClass].value = value
    updateFilters()
  })
}

$(document).ready(function () {
  $("#myInput").on("keyup", function () {
    var value = $(this).val().toLowerCase();
    $(".smartphone-table tbody tr").filter(function () {
      $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
    });
  });

  $('.custom-select').change(function () {
    var txtSearch = "";
    txtSearch = txtSearch + $(this).find('option:selected').text();
  });

  setupPopoverForDevicesModels();

  addFilterSelector("brand", "brand");
  addFilterSelector("release", "release");
  addFilterSelector("channel", "channel");
  addFilterSelector("install", "install");
  addFilterSelector("device-type", "device-type", "device-type");
  addFilterSelector("legacy", "legacy", "legacy");

  addFilterRadio("battery", "battery");
  addFilterRadio("verified-boot", "verified-boot");

  $('#btnReset').on('click', function () {
    clear_form_elements("device-search");
    clear_form_elements("myCollapse");
    $("#myInput").keyup();
    // $('#smartphone-table').dataTable().fnSortNeutral();
  });

  var table = $('#smartphone-table').DataTable({
    paging: false,
    stateSave: true,
    fixedHeader: true,
    bFilter: false,
    info: false,
    "columns": [
      null,
      null,
      null,
      null,
      null,
      null,
      null,
    ],
    "initComplete": function (settings, json) {
      var order_type = 'asc';
      if ($('.ui-state-default.sorting_desc').length) {
        order_type = 'desc';
      }
      if ($('.sorting_1 ul').length > 0) {
        $('.sorting_1 ul').each(function (i, obj) {
          sortList($(this)[0], order_type);
        });
      }
    }
  });

  $('#smartphone-table').on('order.dt', function () {
    var order = table.order();
    if ($('.sorting_1 ul').length > 0) {
      $('.sorting_1 ul').each(function (i, obj) {
        sortList($(this)[0], order[0][1]);
      });
    }
  });
});

function setupPopoverForDevicesModels() {
  $('[data-toggle="popover-models"]').popover({
    html: true,
    trigger: 'hover',
  })
}

function clear_form_elements(class_name) {
  $("." + class_name).find(':input').each(function () {
    switch (this.type) {
      case 'text':
      case 'select-multiple':
        $(this).val('');
        break;
      case 'radio':
        if(this.value == '-1' || (this.name == 'legacy' && this.value == 'no')){
          this.checked = true;
        }else{
          this.checked = false;
        }
        break;
    }
  });
  selectedFilters = {};
  updateFilters();
}

function sortList(ul, sortType) {
  var new_ul = ul.cloneNode(false);

  // Add all lis to an array
  var lis = [];
  for (var i = ul.childNodes.length; i--;) {
    if (ul.childNodes[i].nodeName === 'LI')
      lis.push(ul.childNodes[i]);
  }

  if (sortType == 'asc') {
    // Sort the lis in ascending order
    lis.sort(function (a, b) {
      let aval = a.childNodes[0].data.trim();
      let bval = b.childNodes[0].data.trim();
      return aval.localeCompare(bval);
    });
  } else {
    // Sort the lis in descending order
    lis.sort(function (a, b) {
      let aval = a.childNodes[0].data.trim();
      let bval = b.childNodes[0].data.trim();
      return bval.localeCompare(aval);
    });
  }
  // Add them into the ul in order
  for (var i = 0; i < lis.length; i++) {
    new_ul.appendChild(lis[i]);
  }
  ul.parentNode.replaceChild(new_ul, ul);
}

var gitlab = "https://gitlab.e.foundation/api/v4/groups/e/issues"

function getNbGitlabIssues(label) {
  fetch(
    `${gitlab}?labels=${label},type::Bug&per_page=1&state=opened`
  ).then(function (response) {
    if (response.ok) {
      return response.headers;
    } else {
      return Promise.reject(response);
    }
  }).then(function (headers) {
    let issues = document.querySelector("#issue-badge");
    issues.textContent = headers.get("x-total")
    return headers.get("x-total");
  }).catch(function (err) {
    return null;
  });
}

$(document).ready(() => {
	$('#legacy-1').trigger('click');
	$('#legacy-1').trigger('change');
  $('#verified-boot-0').trigger('change');
  $('#collapse-selector-screen').on('hidden.bs.collapse', function () {
    $('#btnReset').hide();
  });
  $('#collapse-selector-screen').on('shown.bs.collapse', function () {
    $('#btnReset').show();
  });
});
