---
layout: page
title: Steps to install a SDCard and update Teracube 2e from it
namespace: support-topics/teracube-2e-sdcard-update
permalink: /support-topics/teracube-2e-sdcard-update
---

{% tf pages/howtos/teracube-2e-sdcard-update.md %}
