---
layout: page
title: How to disable recently contacted address book?
namespace: support-topics/disable-recently-contacted-address-book
permalink: /support-topics/disable-recently-contacted-address-book
toc: true
---

{% tf pages/howtos/disable-recently-contacted-address-book.md %}
