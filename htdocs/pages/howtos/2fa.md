---
layout: page
title: How to enable Two Factor Authentication (2FA) 🔑
permalink: /support-topics/two-factor-authentication
toc: true
---

{% tf pages/howtos/2fa.md %}
