---
layout: page
title: Enable additional apps on Murena Workspace
permalink: /support-topics/additional-ecloud-apps
toc: true
---

{% tf pages/howtos/additional-ecloud-apps.md %}
