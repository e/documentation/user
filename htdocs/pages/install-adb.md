---
layout: page
permalink: /pages/install-adb
title: Installing adb and fastboot on a Linux PC
search: exclude
toc: true
---

{% tf pages/install-adb.md %}
