---
layout: page
title: Browser Custom Ad Block filters
namespace: browser-custom-filters
permalink: /browser-custom-filters
custom_licence: '"PAGE_TITLE" is a modified version of the original project bromite.github.io (<a href="https://github.com/bromite/bromite.github.io/blob/master/custom-filters.md">original source code</a>), licensed under <a href="https://github.com/bromite/bromite.github.io/blob/master/LICENSE">GPLv3</a> license.'
---

{% tf pages/browser-custom-filters.md %}
