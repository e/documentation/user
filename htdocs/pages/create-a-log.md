---
layout: page
title: Capture a debug information from /e/OS
permalink: /support-topics/create-a-log
namespace: support-topics/create-a-log
search: exclude
toc: true
---

{% tf pages/create-a-log.md %}
