---
layout: page
title: How to Backup and Restore an /e/OS phone
namespace: support-topics/backup-and-restore-ephone
permalink: /support-topics/backup-and-restore-ephone
toc: true
---

{% tf pages/support_topics/backup_and_restore_ephone.md %}
