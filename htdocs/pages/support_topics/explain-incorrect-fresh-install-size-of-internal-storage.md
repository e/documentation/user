---
layout: page
title: Explanation for internal storage size mismatch on fresh install
namespace: support-topics/explain-incorrect-fresh-install-size-of-internal-storage
permalink: /support-topics/explain-incorrect-fresh-install-size-of-internal-storage
---

{% tf pages/support_topics/explain_incorrect_fresh_install_size_of_internal_storage.md %}