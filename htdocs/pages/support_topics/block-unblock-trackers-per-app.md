---
layout: page
title: How to allow or block trackers per app using Advanced Privacy on /e/OS?
namespace: support-topics/block-unblock-trackers-per-app
permalink: /support-topics/block-unblock-trackers-per-app
---

{% tf pages/support_topics/block-unblock-trackers-per-app.md %}
