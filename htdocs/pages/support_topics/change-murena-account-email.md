---
layout: page
title: How to change Murena Workspace account recovery email?
namespace: support-topics/change-murena-account-email
permalink: /change-murena-account-email
---

{% tf pages/support_topics/change-murena-account-email.md %}
