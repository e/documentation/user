---
layout: page
title: Fix your Fairphone 3+ after v1.6 update
namespace: support-topics/fairphone-3-plus-v-1-point-6-fix
permalink: /support-topics/fairphone-3-plus-v-1-point-6-fix
---

{% tf pages/support_topics/fairphone-3-plus-v-1-point-6-fix.md %}
