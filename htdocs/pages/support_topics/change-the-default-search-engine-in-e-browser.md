---
layout: page
title: How to change the default search engine in /e/OS browser?
namespace: support-topics/change-the-default-search-engine-in-e-browser
permalink: /support-topics/change-the-default-search-engine-in-e-browser
---

{% tf pages/support_topics/change-the-default-search-engine-in-e-browser.md %}
