---
layout: page
title: Anonymous calls to Google servers
namespace: support-topics/calls_to_google_servers
permalink: /calls_to_google_servers
toc: true
---

{% tf pages/support_topics/calls_to_google_servers.md %}
