---
layout: page
title: Upgrade the Samsung s7/s7+ to /e/OS R with SD card set as internal storage
permalink: /support-topics/s7-upgrade-to-r-guide
namespace: support-topics/s7-upgrade-to-r-guide
toc: true
---

{% tf pages/support_topics/s7_upgrade_to_r_guide.md %}
