---
layout: page
title: Workaround for /e/OS users on SRF (France) in case of random reboots
namespace: support-topics/workaround-for-srf-france-for-fp3
permalink: /support-topics/workaround-for-srf-france-for-fp3
---

{% tf pages/support_topics/workaround_for_srf_france_for_fp3.md %}
